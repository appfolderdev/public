package dev.appfolder.commons.web;

import dev.appfolder.commons.constants.CommonsConstants;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.experimental.UtilityClass;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.function.Consumer;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@UtilityClass
public class Servlets {

    public static void addCors(final String originAllowed,
                               final HttpServletRequest request,
                               final HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Credentials", "true");
        response.addHeader("Access-Control-Allow-Origin", originAllowed);

        final String reqHeaders = request.getHeader("Access-Control-Request-Headers");
        if (isNotEmpty(reqHeaders)) {
            response.addHeader("Access-Control-Allow-Headers", reqHeaders);
        }

        final String reqMethod = request.getHeader("Access-Control-Request-Method");
        if (isNotEmpty(reqMethod)) {
            response.addHeader("Access-Control-Allow-Methods", reqMethod);
        }
    }

    public static String getReferer(final HttpServletRequest request) {
        return request.getHeader(HttpHeaders.REFERER);
    }

    /**
     * Extrai todos os headers para a forma canonica de chave e valores.
     */
    public static Map<String, List<String>> headersFrom(final HttpServletRequest request) {
        final Map<String, List<String>> headers = new HashMap<>();
        final Enumeration<String> headerNamesEnum = request.getHeaderNames();
        if (headerNamesEnum != null) {
            while (headerNamesEnum.hasMoreElements()) {
                final String headerName = headerNamesEnum.nextElement();
                final Enumeration<String> headerValueEnum = request.getHeaders(headerName);
                final List<String> headerValues = new ArrayList<>();
                headers.put(headerName.toLowerCase(), headerValues);
                while (headerValueEnum.hasMoreElements()) {
                    final String headerValue = headerValueEnum.nextElement();
                    headerValues.add(headerValue);
                }
            }
        }
        return headers;
    }

    public static void internalRedirect(final ServletRequest request,
                                        final ServletResponse response,
                                        final String url) throws ServletException, IOException {
        final RequestDispatcher dispatcher = request
            .getServletContext()
            .getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    public static void internalRedirect(final HttpServletRequest request,
                                        final HttpServletResponse response,
                                        final String url,
                                        final int status) throws ServletException, IOException {
        response.setStatus(status);
        response.setCharacterEncoding(CommonsConstants.DEF_CHARSET.name());
        internalRedirect(request, response, url);
    }

    public static void sendEmptyJsonOk(final HttpServletResponse res) throws IOException {
        writeBody(res, "{}".getBytes(CommonsConstants.DEF_CHARSET));
        res.setStatus(HttpServletResponse.SC_OK);
        res.setContentType("application/json");
    }

    public static void writeBody(final HttpServletResponse response, final byte[] body) throws IOException {
        try (
            final BufferedOutputStream output = new BufferedOutputStream(
                response.getOutputStream(),
                body.length
            )
        ) {
            response.setBufferSize(body.length);
            output.write(body);
            response.flushBuffer();
            response.addHeader(HttpHeaders.CONTENT_LENGTH, String.valueOf(body.length));
        }
    }

    public static void writeBody(final HttpServletResponse response, final Consumer<OutputStream> writer) throws IOException {
        final ServletOutputStream outputStream = response.getOutputStream();
        try (
            final BufferedOutputStream output = new BufferedOutputStream(outputStream)
        ) {
            writer.accept(output);
            response.flushBuffer();
        }
    }
}
