# Esteira

- desenvolve tudo novo na branch "release/v<atual>-SNAPSHOT"
- commits na release disparam testes e qualidade de codigo
- gera pull request
- integra a branch na master e deleta a branch
- esteira gera a versão automaticamente, publica e passa a tag
- esteira cria uma nova branch "release/v<atual+1>-SNAPSHOT"

