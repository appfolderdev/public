package dev.appfolder.commons.http;

import dev.appfolder.commons.helper.Casts;
import dev.appfolder.commons.helper.IOHelper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLConnection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HttpClient {

    public static HttpResponse get(final String url) {
        return get(url, Collections.emptyMap());
    }

    public static HttpResponse get(final String url, final Map<String, List<String>> headers) {
        return request(url, "GET", headers);
    }

    public static HttpResponse post(final String url) {
        return post(url, Collections.emptyMap());
    }

    public static HttpResponse post(final String url, final Map<String, List<String>> headers) {
        return request(url, "POST", headers);
    }

    private static HttpResponse request(final String url,
                                        final String method,
                                        final Map<String, List<String>> headers) {
        try {
            return requestDelegate(url, method, headers);
        } catch (final Exception e) {
            return new HttpResponse(500, null);
        }
    }

    private static HttpResponse requestDelegate(final String url,
                                                final String method,
                                                final Map<String, List<String>> headers) throws IOException, URISyntaxException {
        final HttpURLConnection conn = (HttpURLConnection) new URI(url).toURL().openConnection();
        conn.setRequestMethod(method);
        addHeaders(conn, headers);
        try (final InputStream is = conn.getInputStream()) {
            return new HttpResponse(conn.getResponseCode(), IOHelper.readBytes(is));
        }
    }

    private static void addHeaders(final URLConnection conn, final Map<String, List<String>> headers) {
        final Set<Map.Entry<String, List<String>>> set = Casts.simpleCast(headers.entrySet());
        for (final Map.Entry<String, List<String>> entry : set) {
            conn.setRequestProperty(entry.getKey(), entry.getValue().get(0));
        }
    }

}
