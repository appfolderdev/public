package dev.appfolder.commons.designpattern.strategy;

import java.io.Serializable;

public interface Strategy<T> extends Serializable {

    T getQualifier();

}
