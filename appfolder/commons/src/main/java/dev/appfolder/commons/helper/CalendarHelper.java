package dev.appfolder.commons.helper;

import dev.appfolder.commons.constants.CommonsConstants;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.TimeZone;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CalendarHelper {
    private static final DateTimeFormatter FORMATTER_DATE_TIME = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

    private static final DateTimeFormatter DTF_INPUT_DATE_FIELD = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private static final DateTimeFormatter DTF_INPUT_DATETIME_FIELD = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");

    public static LocalDateTime parseDateTimeFromInputField(final String value, final TimeZone timeZone) {
        return value == null ? null : fromZone(LocalDateTime.parse(value, DTF_INPUT_DATETIME_FIELD), timeZone);
    }

    public static String formatDateToInputField(final LocalDate value) {
        return value == null ? null : value.format(DTF_INPUT_DATE_FIELD);
    }

    public static String formatDateTimeToInputField(final LocalDateTime value, final TimeZone timeZone) {
        return value == null ? null : toZone(value, timeZone).format(DTF_INPUT_DATETIME_FIELD);
    }

    public static String formatDateTime(final LocalDateTime value, final TimeZone timeZone, final Locale locale) {
        if (value == null) {
            return null;
        }
        final DateTimeFormatter df = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM, FormatStyle.SHORT).withLocale(locale);
        return toZone(value, timeZone).format(df);
    }

    public static String formatDate(final LocalDateTime value, final TimeZone timeZone, final Locale locale) {
        if (value == null) {
            return null;
        }
        final DateTimeFormatter df = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(locale);
        return toZone(value, timeZone).format(df);
    }

    private static LocalDateTime toZone(final LocalDateTime value, final TimeZone timeZone) {
        return convertZone(value, timeZone.toZoneId(), CommonsConstants.DEF_ZONE_OFFSET).toLocalDateTime();
    }

    private static LocalDateTime fromZone(final LocalDateTime value, final TimeZone timeZone) {
        return convertZone(value, CommonsConstants.DEF_ZONE_OFFSET, timeZone.toZoneId()).toLocalDateTime();
    }

    private static ZonedDateTime convertZone(final LocalDateTime dateTime, final ZoneId toZone, final ZoneId fromZone) {
        return dateTime
            .atZone(fromZone)
            .withZoneSameInstant(toZone);
    }

    public static String formatDatetime(final LocalDateTime value, final TimeZone timeZone) {
        return value == null ? null : toZone(value, timeZone).format(FORMATTER_DATE_TIME);
    }
}
