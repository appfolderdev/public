package dev.appfolder.commons.helper;

import dev.appfolder.commons.constants.CommonsConstants;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.io.*;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class IOHelper {

    public static byte[] readBytes(final InputStream is) throws IOException {
        return is.readAllBytes();
    }

    public static String readString(final InputStream is) throws IOException {
        return new String(readBytes(is), CommonsConstants.DEF_CHARSET);
    }

    public static void writeString(final String content, final OutputStream fo) throws IOException {
        try (
            final OutputStreamWriter os = new OutputStreamWriter(fo, CommonsConstants.DEF_CHARSET);
            final Writer bw = new BufferedWriter(os)
        ) {
            bw.write(content);
        }
    }

    public static void write(final InputStream in, final OutputStream out) throws IOException {
        final int bufsize = 100;
        final byte[] buffer = new byte[bufsize];
        int read;
        while ((read = in.read(buffer, 0, bufsize)) >= 0) {
            out.write(buffer, 0, read);
        }
    }
}
