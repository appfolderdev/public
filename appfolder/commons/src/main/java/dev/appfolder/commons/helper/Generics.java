package dev.appfolder.commons.helper;

import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Auxilia nas operações que utilizam Java Generics.
 *
 * @author Jonas Pereira
 * @version 2012-12-20
 * @since 2012-05-12
 */
public class Generics {

    /**
     * @param t Objeto genérico.
     * @return Classe genérica do objeto.
     */
    @SuppressWarnings("unchecked")
    public static <T> Class<T> getClass(final T t) {
        return (Class<T>) t.getClass();
    }

    /**
     * @param type Tipo dos elementos que o array irá conter.
     * @param size Tamanho do array.
     * @return Array do tamanho e tipo especificado.
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] newArray(final Class<T> type, final int size) {
        return (T[]) Array.newInstance(type, size);
    }

    /**
     * <p>toArray.</p>
     *
     * @param clasz      a {@link Class} object.
     * @param collection a {@link Collection} object.
     * @param <T>        a T object.
     * @return an array of T objects.
     */
    public static <T> T[] toArray(final Class<T> clasz,
                                  final Collection<T> collection) {
        //isto é seguro em tempo de compilação
        @SuppressWarnings("unchecked") final T[] emptyArray = (T[]) Array.newInstance(clasz, 0);

        return collection.toArray(emptyArray);
    }

    /**
     * @param genericType Tipo genérico de uma classe, campo ou outro parametrizável.
     * @return Tipos parametrizados.
     * @throws ClassNotFoundException   ClassNotFoundException.
     * @throws IllegalArgumentException Se tipo genérico não está parametrizado ou se há multiplos tipos
     *                                  parametrizados.
     */
    public static Class<?> getParameterizedType(final Type genericType) throws ClassNotFoundException {
        final List<Class<?>> types = getParameterizedTypes(genericType);
        if (types.isEmpty()) {
            throw new IllegalArgumentException("Raw type.");
        } else if (types.size() > 1) {
            throw new IllegalArgumentException("Multiples types is parameterized.");
        } else {
            return types.get(0);
        }
    }

    /**
     * @param genericType Tipo genérico de uma classe, campo ou outro parametrizável.
     * @return Tipos parametrizados.
     * @throws ClassNotFoundException ClassNotFoundException.
     */
    public static List<Class<?>> getParameterizedTypes(final Type genericType)
            throws ClassNotFoundException {
//        final ParameterizedType pt = Assertions.instanceOf(ParameterizedType.class,                genericType);
        final ParameterizedType pt = (ParameterizedType) genericType;
        final List<Class<?>> listTypes = new ArrayList<>();
        final Type[] typeArguments = pt.getActualTypeArguments();

        for (final Type type : typeArguments) {
            final Class<?> typeClass;
            if (type instanceof ParameterizedType) {
                final ParameterizedType parameterizedType = (ParameterizedType) type;
                typeClass = (Class<?>) parameterizedType.getRawType();
            } else if (type instanceof TypeVariable) {
                throw new UnsupportedOperationException(TypeVariable.class.getName() + " types is not supported. Probably the parameterized type is defined by other generic class.");
            } else {
                typeClass = (Class<?>) type;
            }
            listTypes.add(typeClass);
        }

        return listTypes;
    }
}
