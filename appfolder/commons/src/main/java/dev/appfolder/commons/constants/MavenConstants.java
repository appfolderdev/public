package dev.appfolder.commons.constants;

import dev.appfolder.commons.helper.PropertyHelper;

import java.io.File;

public class MavenConstants {
    public static final String MVN_TARGET_TEST_RESOURCE_REL_DIR = "target/test-classes/";

    public static final String MVN_TARGET_RESOURCE_REL_DIR = "target/classes/";

    public static final String BASE_WORK_DIR = PropertyHelper.getMvnPrjDir();

    public static final String TEST_CLASSES_DIR = BASE_WORK_DIR + MVN_TARGET_TEST_RESOURCE_REL_DIR;

    public static final String CLASSES_DIR = BASE_WORK_DIR + MVN_TARGET_RESOURCE_REL_DIR + File.separator;

    private static final String TEST_DIR = BASE_WORK_DIR + "src" + File.separator + "test" + File.separator;

    public static final String RESOURCES_TEST_DIR = TEST_DIR + "resources" + File.separator;

    private static final String MAIN_DIR = BASE_WORK_DIR + "src" + File.separator + "main" + File.separator;

    public static final String SRC_DIR = MAIN_DIR + "java" + File.separator;

    public static final String RESOURCES_DIR = MAIN_DIR + "resources" + File.separator;
}
