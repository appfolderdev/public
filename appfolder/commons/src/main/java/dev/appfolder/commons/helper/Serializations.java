package dev.appfolder.commons.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class Serializations {

    private static final ObjectMapper JACKSON_OBJECT_MAPPER = new ObjectMapper()
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    public static <T> T json2Obj(final Class<T> type, final byte[] json) throws IOException {
        return json2Obj(type, json, JACKSON_OBJECT_MAPPER);
    }

    public static <T> T json2Obj(final Class<T> type, final byte[] json, final ObjectMapper objectMapper) throws IOException {
        return objectMapper.readValue(json, type);
    }

    public static <T> byte[] obj2Json(final T obj) throws JsonProcessingException {
        return obj2Json(obj, JACKSON_OBJECT_MAPPER);
    }

    public static <T> byte[] obj2Json(final T obj, final ObjectMapper objectMapper) throws JsonProcessingException {
        return objectMapper.writeValueAsBytes(obj);
    }
}
