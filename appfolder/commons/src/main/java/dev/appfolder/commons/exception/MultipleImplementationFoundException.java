package dev.appfolder.commons.exception;

import java.util.List;
import java.util.stream.Collectors;

public class MultipleImplementationFoundException extends InjectionException {

    public <T> MultipleImplementationFoundException(final Class<T> clasz, final List<T> implementations) {
        super(clasz, buildMessage(implementations, clasz));
    }

    private static <T> String buildMessage(final List<T> implementations, final Class<T> type) {
        final String implsString = implementations.stream().map(t -> t.getClass().getName())
                .collect(Collectors.joining("\n"));
        return "Múltiplas implementações encontradas para o tipo " + type.getName() + ":\n\n" + implsString + "\n";
    }
}
