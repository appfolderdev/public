package dev.appfolder.commons.designpattern.strategy;

public class StrategyNotFoundException extends RuntimeException {
    public <S extends Strategy<T>, T> StrategyNotFoundException(Class<S> type, T qualifier) {
        super("Strategy not found for type " + type.getName() + " qualifier " + qualifier);
    }
}
