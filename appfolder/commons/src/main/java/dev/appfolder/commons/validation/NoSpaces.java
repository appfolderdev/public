package dev.appfolder.commons.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = NoSpacesValidator.class)
@Target({FIELD})
@Retention(RUNTIME)
@Documented
public @interface NoSpaces {

    String DEF_MESSAGE = "{dev.appfolder.commons.validation.NoSpaces.message}";

    String message() default DEF_MESSAGE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}