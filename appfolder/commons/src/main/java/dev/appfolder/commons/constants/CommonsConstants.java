package dev.appfolder.commons.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.ZoneOffset;
import java.util.Locale;
import java.util.TimeZone;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CommonsConstants {

    public static final ZoneOffset DEF_ZONE_OFFSET = ZoneOffset.UTC;

    public static final String DEF_ENCODING = "UTF-8";

    public static final Charset DEF_CHARSET = StandardCharsets.UTF_8;

    public static final Locale DEF_LOCALE = new Locale("Pt", "BR");

    public static final String DEF_TIMEZONE_NAME = "UTC";

    public static final TimeZone DEF_TIMEZONE = TimeZone.getTimeZone(DEF_TIMEZONE_NAME);

}
