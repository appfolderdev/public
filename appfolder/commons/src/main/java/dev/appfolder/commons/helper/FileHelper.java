package dev.appfolder.commons.helper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import java.io.*;
import java.net.URL;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Optional;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FileHelper {
    private static final String WINDOWS_SPACE_CHAR = "%20";

    public static String putDirPathEndDelim(final String dirPath) {
        if (dirPath.endsWith("/") || dirPath.endsWith("\\")) {
            return dirPath;
        } else {
            return dirPath + File.separator;
        }
    }

    public static String getDelimPattern() {
        final String delim = File.separator;

        return getDelimPattern(delim);
    }

    public static boolean setReadPermission(final Path path) {
        try {
            Files.setPosixFilePermissions(path, PosixFilePermissions.fromString("rwxr-xr-x"));
            return true;
        } catch (final Exception e) {
            // aqui lança "unsupported operation no windows"
            return false;
        }
    }

    public static boolean setReadPermission(final String dir, final String fileName) {
        final Path path = Paths.get(dir, fileName);
        return setReadPermission(path);
    }

    public static String getDelimPattern(final String delim) {
        final String delimPattern;

        if (delim.equals("/")) {
            delimPattern = delim;
        } else {
            delimPattern = "\\\\";
        }

        return delimPattern;
    }

    public static File getJavaFile(final Class<?> aClass, final boolean isTest) {
        final String simpleName = aClass.getSimpleName();
        final String javaFileDir = getJavaFileDir(aClass, isTest);
        return new File(javaFileDir, simpleName + ".java");
    }

    @SneakyThrows
    public static String getModuleDir(final Class<?> aClass, final boolean isTest) {
        final int nPathsReturn = aClass.getPackage().getName().split("\\.").length + 4;
        final String pathsReturn = new String(new char[nPathsReturn]).replace("\0", "../");
        return new File(getJavaFileDir(aClass, isTest) + pathsReturn).getCanonicalPath();
    }

    public static String getJavaFileDir(final Class<?> aClass, final boolean isTest) {
        final String simpleName = aClass.getSimpleName();
        final URL resource = aClass.getResource(simpleName + ".class");
        final String file = resource.getFile();
        final String replace;
        if (isTest) {
            replace = file.replace("target/test-classes", "src/test/java");
        } else {
            replace = file.replace("target/classes", "src/main/java");
        }
        return new File(replace).getParent();
    }

    public static File getResourceFile(final String relPath) throws IOException {
        final ClassLoader loader = Thread.currentThread().getContextClassLoader();
        final URL url = loader.getResource(relPath);

        if (url == null) {
            throw new FileNotFoundException("Path: " + relPath);
        }

        File file = new File(url.getFile());
        String path = file.getCanonicalPath();

        // correção necessária no windows para caminhos com espaço
        if (path.contains(WINDOWS_SPACE_CHAR)) {
            path = path.replace(WINDOWS_SPACE_CHAR, " ");
            file = new File(path);
        }

        return file;
    }

    public static String read(final String filePath) throws IOException {
        try (final InputStream is = new FileInputStream(filePath)) {
            return IOHelper.readString(is);
        }
    }

    public static File getTempDir() {
        return new File(PropertyHelper.getTempDir());
    }

    /**
     * Normaliza todos delimitadores para barra, não usa contra barras.
     *
     * @param filePath Caminho do arquivo.
     * @return Caminho do arquivo normalizado.
     */
    public static String normalizeFilePathDelim(final String filePath) {
        final String newfilePath;

        if (filePath.contains("\\")) {
            newfilePath = filePath.replaceAll("\\\\", "/");
        } else {
            newfilePath = filePath;
        }

        return newfilePath;
    }

    public static void delete(final File file) throws IOException {
        final Path pathToBeDeleted = Path.of(file.getAbsolutePath());
        Files.walkFileTree(pathToBeDeleted, new SimpleFileVisitor<>() {
            @Override
            public FileVisitResult postVisitDirectory(final Path dir, final IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    /**
     * Cria diretórios pai caso não existam.
     *
     * @param file Caminho absoluto do arquivo.
     * @throws IOException IOException.
     */
    private static void mkdirParent(final File file) throws IOException {
        final String fullFilePath = file.getCanonicalPath();
        final File parent = new File(fullFilePath).getParentFile();
        parent.mkdirs();
    }

    public static void write(final File file, final String content) throws IOException {
        mkdirParent(file);
        try (final FileOutputStream fo = new FileOutputStream(file, false)) {
            IOHelper.writeString(content, fo);
        }
    }

    public static Optional<String> readTestFileResource(final Class<?> type, final String relPath) throws IOException {
        final File file = getTestFileResource(type, relPath);
        if (file.exists()) {
            return Optional.of(read(file.getAbsolutePath()).replaceAll("\r", ""));
        } else {
            return Optional.empty();
        }
    }

    public static File getTestFileResource(final Class<?> type, final String relPath) {
        final String typeName = type.getSimpleName() + ".class";
        final String thisClassFilePath = type.getResource(typeName).getFile();
        return new File(thisClassFilePath
            .replace(typeName, relPath)
            .replace("target/classes", "src/test/resources"));
    }
}
