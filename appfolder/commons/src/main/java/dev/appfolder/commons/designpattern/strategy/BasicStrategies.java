package dev.appfolder.commons.designpattern.strategy;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BasicStrategies<T, S extends Strategy<T>> implements Strategies<T, S> {

    private final Map<T, S> instances;

    public BasicStrategies(final S... strategies) {
        this(Stream.of(strategies));

    }

    public BasicStrategies(final List<S> strategies) {
        this(strategies.stream());
    }

    private BasicStrategies(final Stream<S> strategies) {
        this.instances = strategies.collect(Collectors.toMap(Strategy::getQualifier, s -> s));
    }

    @Override
    public Stream<S> stream() {
        return instances.values().stream();
    }

    @Override
    public S getStrategy(final T qualifier) {
        return instances.get(qualifier);
    }

    @Override
    public boolean contains(final T qualifier) {
        return instances.containsKey(qualifier);
    }

    @Override
    public Iterator<T> iterator() {
        return instances.keySet().iterator();
    }
}
