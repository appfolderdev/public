package dev.appfolder.commons.helper;

import lombok.SneakyThrows;

import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

import static dev.appfolder.commons.constants.CommonsConstants.DEF_CHARSET;
import static dev.appfolder.commons.constants.CommonsConstants.DEF_ENCODING;
import static org.apache.commons.lang3.StringUtils.isEmpty;

public class SecureHelper {

    public static final int HASH_SIZE = 128;

    public static final int MAX_KEY_LENGTH = 64;

    public static final String DEFAULT_ALGORITHM = "PBEWithMD5AndDES";

    private final Cipher ecipher;

    private final Cipher dcipher;

    public SecureHelper() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, InvalidKeySpecException {
        this(Randoms.nextString());
    }

    public SecureHelper(final String passPhrase) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, InvalidKeySpecException {
        if (isEmpty(passPhrase)) {
            throw new IllegalArgumentException("Key cannot be null or empty.");
        }

        if (passPhrase.length() > MAX_KEY_LENGTH) {
            throw new IllegalArgumentException("Key length greater then max: " + MAX_KEY_LENGTH);
        }

        final byte[] salt = randomSalt(8);

        final int iterationCount = 19;
        final KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);
        final SecretKey secretKey = SecretKeyFactory.getInstance(DEFAULT_ALGORITHM).generateSecret(keySpec);

        ecipher = Cipher.getInstance(secretKey.getAlgorithm());
        dcipher = Cipher.getInstance(secretKey.getAlgorithm());

        final AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

        ecipher.init(Cipher.ENCRYPT_MODE, secretKey, paramSpec);
        dcipher.init(Cipher.DECRYPT_MODE, secretKey, paramSpec);
    }

    public static String getRandomKey() {
        final byte[] salt = randomSalt(16);
        return encodeBytes(salt);
    }

    private static byte[] randomSalt(final int size) {
        final SecureRandom random = new SecureRandom();
        final byte[] salt = new byte[size];
        random.nextBytes(salt);
        return salt;
    }

    private static String encodeBytes(final byte[] salt) {
        return Base64.getEncoder().encodeToString(salt);
    }

    @SneakyThrows
    public static String getHash(final String key, final String text) {
        return applySha512(key, key + text);
    }

    private static String applySha512(final String key, final String text) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        final byte[] salt = decodeBytes(key);
        final byte[] txtBytes = text.getBytes(DEF_CHARSET);
        final byte[] varSalt = new byte[salt.length + txtBytes.length];
        System.arraycopy(salt, 0, varSalt, 0, salt.length);
        System.arraycopy(txtBytes, 0, varSalt, salt.length, txtBytes.length);

        final String generatedPassword;
        final MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(varSalt);
        final byte[] bytes = md.digest(text.getBytes(DEF_ENCODING));
        final StringBuilder sb = new StringBuilder();
        for (final byte aByte : bytes) {
            sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
        }
        generatedPassword = sb.toString();
        return generatedPassword;
    }

    private static byte[] decodeBytes(final String key) {
        return Base64.getDecoder().decode(key);
    }

    public String crypt(final String str) throws IllegalBlockSizeException, BadPaddingException {
        final byte[] decrypted = str.getBytes(DEF_CHARSET);
        final byte[] enc = ecipher.doFinal(decrypted);
        return encodeBytes(enc);
    }

    public String decrypt(final String str) throws IllegalBlockSizeException, BadPaddingException {
        final byte[] dec = decodeBytes(str);
        final byte[] decrypted = dcipher.doFinal(dec);
        return new String(decrypted, DEF_CHARSET);
    }
}