package dev.appfolder.commons.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class IssuesCodes {

    public static final String WILDCARDS_IN_RETURN = "java:S1452";

    public static final String ASSERTION_IN_PRODUCTION_CODE = "java:S5960";

    public static final String ACCESSIBILITY_UPDATE = "java:S3011";
}
