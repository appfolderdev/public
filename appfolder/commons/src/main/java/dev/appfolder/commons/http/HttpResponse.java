package dev.appfolder.commons.http;

public record HttpResponse(int status, byte[] content) {
    public boolean isSuccess() {
        return status == 200;
    }
}
