package dev.appfolder.commons.helper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static java.text.MessageFormat.format;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Randoms {

    private static long index = 1;

    /**
     * @param min Valor mínimo inclusive.
     * @param max Valor máximo inclusive.
     */
    public static int nextInt(final int min, final int max) {
        return getRandom().nextInt(min, max + 1);
    }

    public static int nextInt() {
        return getRandom().nextInt();
    }

    public static long nextIndex() {
        return index++;
    }

    public static BigDecimal nextDecimal() {
        return BigDecimal.valueOf(nextFloat());
    }

    public static BigDecimal nextDecimal(final int min, final int max) {
        return BigDecimal.valueOf(getRandom().nextFloat(min, max));
    }

    public static String nextEmail() {
        return format("{0}{1}@email.com", nextName(), nextIndex());
    }

    public static String nextName() {
        return nextString(5);
    }

    public static String nextString() {
        return nextString(nextInt(3, 5));
    }

    public static boolean nextBoolean() {
        return getRandom().nextBoolean();
    }

    public static String nextString(final int chars) {
        final int leftLimit = 48; // numeral '0'
        final int rightLimit = 122; // letter 'z'

        return getRandom().ints(leftLimit, rightLimit + 1)
            .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
            .limit(chars)
            .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
            .toString();
    }

    private static ThreadLocalRandom getRandom() {
        return ThreadLocalRandom.current();
    }

    public static <T extends Enum<?>> T nextEnum(final Class<T> etype) {
        final Enum[] enums = (Enum[]) etype.getEnumConstants();
        final int i = nextInt(0, enums.length - 1);
        return Casts.simpleCast(enums[i]);
    }

    public static float nextFloat() {
        return Math.abs(getRandom().nextFloat());
    }

    public static LocalDateTime nextDatetime() {
        final LocalDate date = LocalDate.now().plus(nextInt(-1000, 1000), ChronoUnit.DAYS);
        final LocalTime time = LocalTime.of(nextInt(0, 23), nextInt(0, 59), nextInt(0, 59));
        return date.atTime(time);
    }

    public static <T> T choose(final List<T> choices) {
        if (isEmpty(choices)) {
            return null;
        }
        return choices.get(nextInt(0, choices.size() - 1));
    }
}
