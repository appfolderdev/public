package dev.appfolder.commons.exception;

import lombok.Getter;

public class InjectionException extends Exception {
    @Getter
    private final Class<?> type;

    public InjectionException(final Class<?> type, final String message) {
        super(message);
        this.type = type;
    }

    public InjectionException(final Class<?> type, final String message, final Throwable cause) {
        super(message, cause);
        this.type = type;
    }
}
