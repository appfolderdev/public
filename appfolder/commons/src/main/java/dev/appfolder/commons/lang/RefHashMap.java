package dev.appfolder.commons.lang;

import java.util.HashMap;

/**
 * Implementação de um hashmap que diferencia as chaves segundo suas referências em memória, ou seja, duas
 * chaves são consideradas iguais se apontam para um mesmo endereço em memória. O hashcode é considerado como
 * único.
 *
 * @author Jonas Pereira
 * @since 2013-10-19
 */
public class RefHashMap<K, V> extends HashMap<RefEntry<K>, V> {

    public boolean containsRefKey(final K key) {
        return super.containsKey(new RefEntry<K>(key));
    }

    public V getByRef(final K key) {
        return super.get(new RefEntry<K>(key));
    }

    @Override
    public V remove(final Object key) {
        return super.remove(new RefEntry<K>(key));
    }

    public V putNew(final K key, final V value) {
        return super.put(new RefEntry<K>(key), value);
    }

}
