package dev.appfolder.commons.reflection;

import dev.appfolder.commons.functional.Condition;

import java.lang.reflect.Field;

/**
 * Condições que se aplicam a um campo.
 *
 * @author Jonas Pereira
 * @version 2013-06-02
 * @since 2013-06-02
 */
public interface FieldCondition extends Condition<Field> {

}
