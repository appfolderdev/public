package dev.appfolder.commons.functional;

import java.io.Serializable;
import java.util.function.Function;

/**
 * Use para passar métodos como parâmetros a outros métodos.
 *
 * @param <P> Tipo do objeto a ser processado pela execução de uma ação.
 * @param <R> Tipo do resultado do processamento da ação.
 * @author Jonas Pereira
 * @since 2013-06-19
 */
@FunctionalInterface
public interface Action<P, R> extends Serializable, Function<P, R> {

}
