package dev.appfolder.commons.designpattern.strategy;

import dev.appfolder.commons.exception.InjectionException;
import dev.appfolder.commons.helper.Casts;
import dev.appfolder.commons.helper.InjectorHelper;
import lombok.RequiredArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class InjectableStrategies<T, S extends Strategy<T>> implements Strategies<T, S> {

    private final Class<S> type;

    private Map<T, Class<? extends S>> MAP_INSTANCES;

    private void init() {
        if (MAP_INSTANCES != null) {
            return;
        }
        final List<? extends S> instances;
        try {
            instances = InjectorHelper.newInstances(type, false);
        } catch (final InjectionException e) {
            throw new RuntimeException(e);
        }
        MAP_INSTANCES = instances.stream().collect(Collectors.toMap(
            Strategy::getQualifier,
            object -> Casts.simpleCast(object.getClass())
        ));
    }

    @Override
    public S getStrategy(final T qualifier) {
        return findStrategy(qualifier).map(InjectorHelper::newInstanceSilent)
            .orElseThrow(() -> new StrategyNotFoundException(type, qualifier));
    }

    private Optional<Class<? extends S>> findStrategy(final T qualifier) {
        init();
        if (!MAP_INSTANCES.containsKey(qualifier)) {
            Optional.empty();
        }
        return Optional.ofNullable(MAP_INSTANCES.get(qualifier));
    }

    @Override
    public boolean contains(final T qualifier) {
        return findStrategy(qualifier).isPresent();
    }

    @Override
    public Iterator<T> iterator() {
        return getQualifiers().iterator();
    }

    public Set<T> getQualifiers() {
        init();
        return MAP_INSTANCES.keySet();
    }

    @Override
    public Stream<S> stream() {
        return getQualifiers().stream().map(this::getStrategy);
    }
}
