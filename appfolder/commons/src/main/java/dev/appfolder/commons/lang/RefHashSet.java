package dev.appfolder.commons.lang;

import java.util.*;

/**
 * Implementação de um hashset que diferencia os items segundo suas referências em memória, ou seja, dois
 * items são consideradas iguais se apontam para um mesmo endereço em memória. O hashcode é considerado como
 * único.
 *
 * @author Jonas Pereira
 * @since 2013-11-24
 */
public class RefHashSet<T> extends HashSet<RefEntry<T>> {

    public boolean containsRef(final T item) {
        return super.contains(new RefEntry<T>(item));
    }

    public boolean addNew(final T item) {
        return super.add(new RefEntry<T>(item));
    }

    public boolean removeByRef(final T item) {
        return super.remove(new RefEntry<T>(item));
    }

    public Set<T> toSet() {
        final Set<T> set = new HashSet<>();
        fillCollection(set);
        return set;
    }

    private void fillCollection(final Collection<T> collection) {
        for (final RefEntry<T> item : this) {
            collection.add(item.getValue());
        }
    }

    public List<T> toList() {
        final List<T> list = new ArrayList<>();
        fillCollection(list);
        return list;
    }
}
