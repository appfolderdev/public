package dev.appfolder.commons.helper;

import dev.appfolder.commons.exception.InjectionException;
import dev.appfolder.commons.exception.MultipleImplementationFoundException;
import dev.appfolder.commons.reflection.Reflections;
import jakarta.inject.Inject;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.*;

import static java.text.MessageFormat.format;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class InjectorHelper {

    @SneakyThrows
    public static <T> T newInstanceSilent(final Class<T> clasz) {
        return newInstance(clasz);
    }

    public static <T> T newInstance(final Class<T> clasz) throws InjectionException {
        return newInstance(clasz, true, Collections.emptyMap());
    }

    public static <T> List<T> newInstances(final Class<T> clasz, final boolean recursive) throws InjectionException {
        return newInstances(clasz, recursive, Collections.emptyMap());
    }

    public static <T> T newInstance(final Class<T> clasz, final Map<Class<?>, Object> extMap) throws InjectionException {
        return newInstance(clasz, true, extMap);
    }

    private static <T> T newInstance(final Class<T> clasz, final boolean recursive, final Map<Class<?>, Object> extMap) throws InjectionException {
        final List<T> instances = newInstances(clasz, recursive, extMap);
        if (instances.size() > 1) {
            throw new MultipleImplementationFoundException(clasz, instances);
        }
        return instances.get(0);
    }

    public static <T> List<T> newInstances(final Class<T> clasz) throws InjectionException {
        return newInstances(clasz, true, Collections.emptyMap());
    }

    @SneakyThrows
    public static <T> List<T> newInstancesSilent(final Class<T> clasz) {
        return newInstances(clasz);
    }

    private static <T> List<T> newInstances(final Class<T> clasz, final boolean recursive, final Map<Class<?>, Object> extMap) throws InjectionException {
        return newInstancesDelegate(clasz, recursive, extMap);
    }

    private static <T> List<T> newInstancesDelegate(final Class<T> clasz, final boolean recursive, final Map<Class<?>, Object> extMap) throws InjectionException {
        final List<T> instances = new ArrayList<>();
        if (clasz.isInterface()) {
            final ServiceLoader<T> loader;
            try {
                loader = ServiceLoader.load(clasz);
            } catch (final Throwable e) {
                throw new InjectionException(clasz, "Erro ao carregar implementações de " + clasz.getName(), e);
            }
            for (final T provider : loader) {
                instances.add(provider);
            }
        } else {
            try {
                final Constructor<T> constructor = clasz.getDeclaredConstructor();
                constructor.setAccessible(true);
                instances.add(constructor.newInstance());
            } catch (final Throwable e) {
                throw new InjectionException(clasz, "Erro ao invocar construtor do tipo " + clasz.getName(), e);
            }
        }

        if (instances.isEmpty()) {
            throw new InjectionException(clasz, "Nenhuma implementação encontrada para o tipo " + clasz.getName());
        }

        if (recursive) {
            injectFields(instances, extMap);
        }

        return instances;
    }

    private static <T> void injectFields(final List<T> instances, final Map<Class<?>, Object> extMap) throws InjectionException {
        for (final T instance : instances) {
            final Class<?> clasz = instance.getClass();
            final List<Field> fields = Reflections.getFields(clasz, object -> object.isAnnotationPresent(Inject.class));
            for (final Field field : fields) {
                if (extMap.containsKey(field.getType())) {
                    final Object extInstance = extMap.get(field.getType());
                    setField(instance, field, extInstance);
                } else {
                    try {
                        injectField(instance, field, extMap);
                    } catch (final Throwable e) {
                        throw new InjectionException(clasz, format("Erro ao injetar campo {0} na instância {1}", field.getName(), clasz), e);
                    }
                }
            }
        }
    }

    private static <T> void injectField(final T instance, final Field field, final Map<Class<?>, Object> extMap) throws ClassNotFoundException, InjectionException, IllegalAccessException {
        final Class<?> fieldType = field.getType();
        if (fieldType == List.class) {
            injectListField(instance, field, extMap);
        } else {
            final Object fieldInstance = newInstance(fieldType, true, extMap);
            setField(instance, field, fieldInstance);
        }
    }

    @SneakyThrows
    private static <T> void setField(final T instance, final Field field, final Object object) {
        field.setAccessible(true);
        field.set(instance, object);
    }

    private static <T> void injectListField(final T instance, final Field field, final Map<Class<?>, Object> extMap) throws ClassNotFoundException, InjectionException, IllegalAccessException {
        final Class<?> itemType = Generics.getParameterizedType(field.getGenericType());
        final List<?> fieldInstances = newInstances(itemType, true, extMap);
        setField(instance, field, fieldInstances);
    }

}
