package dev.appfolder.commons.reflection;

import dev.appfolder.commons.functional.Condition;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;

/**
 * Condições que exige que um elemento esteja anotado com uma determinada anotação.
 *
 * @author Jonas Pereira
 * @since 2013-09-17
 */
@AllArgsConstructor
public class AnnotatedElementCondition<T extends AnnotatedElement> implements Condition<T> {

    @Getter
    private final Class<? extends Annotation> annotation;

    @Override
    public Boolean apply(final T obj) {
        return obj.isAnnotationPresent(annotation);
    }

}
