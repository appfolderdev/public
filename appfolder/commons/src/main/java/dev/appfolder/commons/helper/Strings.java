package dev.appfolder.commons.helper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang3.StringUtils.isEmpty;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Strings {

    public static String toUpperCaseFirstLetter(final String text) {
        if (isEmpty(text)) {
            return text;
        }

        final String delim = " ";
        final StringTokenizer tokenizer = new StringTokenizer(text, delim, true);

        String first;
        final StringBuilder textUpperBuilder = new StringBuilder();

        String word;

        while (tokenizer.hasMoreElements()) {
            word = tokenizer.nextToken();

            if (word.equals(delim)) {
                textUpperBuilder.append(delim);
            } else {
                first = word.charAt(0) + "";
                textUpperBuilder.append(first.toUpperCase())
                        .append(word.substring(1));
            }
        }

        return textUpperBuilder.toString();
    }

    public static String toLowerCaseFirstLetter(final String text) {
        if (isEmpty(text)) {
            return text;
        }

        final String delim = " ";
        final StringTokenizer tokenizer = new StringTokenizer(text, delim, true);

        String first;
        final StringBuilder textLowerBuilder = new StringBuilder();

        String word;

        while (tokenizer.hasMoreElements()) {
            word = tokenizer.nextToken();

            if (word.equals(delim)) {
                textLowerBuilder.append(delim);
            } else {
                first = word.charAt(0) + "";
                textLowerBuilder.append(first.toLowerCase())
                        .append(word.substring(1));
            }
        }

        return textLowerBuilder.toString();
    }

    public static String toUpperCaseCamelCase(final String fullText) {
        if (isEmpty(fullText)) {
            return fullText;
        }
        final String[] parts = fullText.trim().split(" ");
        final String text;
        if (parts.length < 2) {
            text = parts[0];
        } else {
            return Stream.of(parts)
                    .filter(s -> !isEmpty(s))
                    .map(Strings::toUpperCaseCamelCase)
                    .collect(Collectors.joining("_"));
        }

        final StringBuilder builder = new StringBuilder();
        for (final char c : toLowerCaseFirstLetter(text).toCharArray()) {
            if (c < 97) {
                // maiusculo
                builder.append("_");
            }
            builder.append(c);
        }
        return builder.toString().toUpperCase();
    }

}
