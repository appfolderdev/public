package dev.appfolder.commons.helper;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;

import static dev.appfolder.commons.constants.MavenConstants.MVN_TARGET_TEST_RESOURCE_REL_DIR;
import static java.text.MessageFormat.format;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class PropertyHelper {
    private final Properties properties;

    @SneakyThrows
    public static PropertyHelper fromFile(final File file) {
        final FileInputStream inputStream = new FileInputStream(file);
        return newInstance(inputStream);
    }

    public static PropertyHelper fromResource(final String fileName) {
        final String filePath = format("/{0}.properties", fileName);
        final InputStream is = PropertyHelper.class.getResourceAsStream(filePath);
        return newInstance(is);
    }

    /**
     * @return Diretório do projeto baseado no padrão do maven. Este diretório pode ser diferente do diretório
     * de trabalho quando os arquivos de projeto de uma IDE não coincidir com o diretório das classes em si.
     */
    public static String getMvnPrjDir() {
        return getPrjDir(MVN_TARGET_TEST_RESOURCE_REL_DIR);
    }

    /**
     * @param relResourceDir Caminho relativo do diretório de recursos do projeto.
     * @return Diretório do projeto. Este diretório pode ser diferente do diretório de trabalho quando os
     * arquivos de projeto de uma IDE não coincidir com o diretório do projeto em si.
     */
    public static String getPrjDir(final String relResourceDir) {
        final String path = PropertyHelper.class.getClassLoader().getResource(".").getPath();
        final int offset;
        if (File.separator.equals("/")) {
            offset = 0;
        } else {
            offset = 1;
        }
        return path.substring(offset, path.length() - relResourceDir.length());
    }

    private static PropertyHelper newInstance(final InputStream is) {
        final Properties properties = new Properties();
        try {
            properties.load(is);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
        return new PropertyHelper(properties);
    }

    public static String getTempDir() {
        return getFromSystem("java.io.tmpdir");
    }

    public static String getFromSystem(final String key) {
        return System.getProperty(key);
    }

    public boolean getAsBoolean(final String key) {
        return Boolean.parseBoolean(get(key));
    }

    public long getAsNumber(final String key) {
        return Long.parseLong(get(key));
    }

    public String get(final String key) {
        return properties.getProperty(key);
    }

    public Optional<String> getOptional(final String key) {
        final String value = get(key);
        return isEmpty(value) ? Optional.empty() : Optional.of(value);
    }
}
