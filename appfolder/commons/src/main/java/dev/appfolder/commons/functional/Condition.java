package dev.appfolder.commons.functional;

/**
 * Representa uma condição a ser analisada, deve gerar uma resposta booleana se a condição pra um determinado
 * contexto foi satisfeita.
 *
 * @param <T> Contexto.
 * @author Jonas Pereira
 * @since 2013-05-31
 */
public interface Condition<T> extends Action<T, Boolean> {

}
