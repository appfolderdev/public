package dev.appfolder.commons.designpattern.strategy;

import java.util.stream.Stream;

public interface Strategies<T, S extends Strategy<T>> extends Iterable<T> {
    Stream<S> stream();

    S getStrategy(final T qualifier);

    boolean contains(T qualifier);
}
