package dev.appfolder.commons.behavior;

public interface Randomizable {
    void randomize();
}
