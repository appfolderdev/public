package dev.appfolder.commons.lang;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Comparador de objetos. São considerados objetos iguais se tiverem a mesma refência em memória. Não é
 * relevante se um é maior que outro.
 *
 * @author Jonas Pereira
 * @since 2013-10-19
 */
public class ReferenceComparator<T> implements Comparator<T>, Serializable {

    public static int compareRefs(final Object o1, final Object o2) {
        return o1 == o2 ? 0 : 1;
    }

    /**
     * @param o1 Primeiro objeto.
     * @param o2 Segundo objeto.
     * @return Se referências dos objetos são iguais retorna 0, senão retorna -1 ou 1.
     */
    @Override
    public int compare(final T o1, final T o2) {
        return compareRefs(o1, o2);
    }
}
