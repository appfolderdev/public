package dev.appfolder.commons.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EntityConstants {
    public static final String LIST_SUFFIX = "List";

    public static final String ID_FIELD_SUFFIX = "Id";

    public static final String ID_NAME = "id";

    public static final char JOIN_CHAR = '_';

    public static final String ID_SUFFIX = JOIN_CHAR + ID_NAME;

    public static final String ROLE_PREFFIX = "ROLE_";

    public static final String REST_ENTITY_NAME = "entity";

    public static final String REST_ENTITY_PATH = "/" + REST_ENTITY_NAME;
}
