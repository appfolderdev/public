package dev.appfolder.commons.validation;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public abstract class AbstractValidatorTest {

    private List<String> validate(final Object obj) {
        final Set<ConstraintViolation<Object>> violations;
        try (final ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {
            final Validator validator = factory.getValidator();
            violations = validator.validate(obj);
        }
        return violations.stream().map(ConstraintViolation::getMessage).toList();
    }

    @Test
    void isValid() {
        final List<String> violations = validate(buildValid());
        assertTrue(violations.isEmpty());
    }

    protected abstract Object buildValid();

    @Test
    void isInvalid() {
        final List<String> violations = validate(buildInvalid());
        assertFalse(violations.isEmpty());
        validateViolations(violations);
    }

    protected abstract void validateViolations(List<String> violations);

    protected abstract Object buildInvalid();

}
