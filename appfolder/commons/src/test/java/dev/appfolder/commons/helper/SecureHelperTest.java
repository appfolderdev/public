package dev.appfolder.commons.helper;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SecureHelperTest {

    /**
     * @see SecureHelper#getHash(String, String)
     */
    @Test
    @DisplayName("Hash de um texto é calculado de forma previsível.")
    void getHash() {
        final String key = SecureHelper.getRandomKey();
        final String text = "text1çã@0)_-=§";
        final String crypted = SecureHelper.getHash(key, text);
        final String crypted2 = SecureHelper.getHash(key, text);
        assertEquals(crypted, crypted2);
        assertEquals(SecureHelper.HASH_SIZE, crypted.length());
    }

    /**
     * @see SecureHelper#crypt(String)
     * @see SecureHelper#decrypt(String)
     */
    @Test
    @DisplayName("Texto é encriptado e decriptado.")
    void crypt() throws Exception {
        final SecureHelper helper = new SecureHelper();
        final String text = "text1çã@0)_-=§";
        final String crypted = helper.crypt(text);
        final String decrypted = helper.decrypt(crypted);
        assertEquals(text, decrypted);
    }
}