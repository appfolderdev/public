package dev.appfolder.commons.validation;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NoSpacesValidatorTest extends AbstractValidatorTest {

    @Override
    protected Object buildValid() {
        return new Bean("nospaces");
    }

    @Override
    protected void validateViolations(final List<String> violations) {
        violations.forEach(violation -> assertEquals(NoSpaces.DEF_MESSAGE, violation));
    }

    @Override
    protected Object buildInvalid() {
        return new Bean("with spaces");
    }

    record Bean(@NoSpaces String value) {
    }
}