package dev.appfolder.commons.helper;

import lombok.Data;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class GenericsTest {

    @Test
    void testGetClass() {
        final Class<ParameterizedClass<String, Generics, Object>> clasz = Generics.getClass(new ParameterizedClass<>());
        assertNotNull(clasz);
    }

    @Test
    void testGetParameterizedType() throws Exception {
        //TODO
    }

    @Test
    void testGetParameterizedTypes() throws Exception {
        final Field field = ParameterizedClass.class.getDeclaredField("complexMap");
        final List<Class<?>> parameterizedTypes = Generics.getParameterizedTypes(field.getGenericType());
        assertEquals(2, parameterizedTypes.size());
        assertTrue(parameterizedTypes.contains(String.class));
        assertTrue(parameterizedTypes.contains(Map.class));

        final Field intsField = ParameterizedClass.class.getDeclaredField("queue");
        final List<Class<?>> queueTypes = Generics.getParameterizedTypes(intsField.getGenericType());
        assertEquals(1, queueTypes.size());
        assertTrue(queueTypes.contains(Integer.class));
    }

    @Test
    void testGetParameterizedTypes_error() throws Exception {
        final ParameterizedClass<String, Boolean, Generics> obj = new ParameterizedClass<>();
        final Field field = obj.getClass().getDeclaredField("typeVariableField");
        assertThrows(UnsupportedOperationException.class, () -> Generics.getParameterizedTypes(field.getGenericType()));
    }

    @Test
    void testNewArray() {
        final int length = 5;
        final String[] value = Generics.newArray(String.class, length);
        assertNotNull(value);
        assertEquals(length, value.length);
    }

    @Test
    void testToArray() {
        final Character[] array = {'a', 'b', 'd'};
        final List<Character> list = new LinkedList<>();
        list.addAll(Arrays.asList(array));

        final Character[] newArray = new Generics().toArray(Character.class,
                list);

        for (int i = 0; i < newArray.length; i++) {
            assertEquals(array[i], newArray[i]);
        }
    }

    @Data
    class ParameterizedClass<A, B, C> {

        Map<String, Map<Long, Set<Boolean>>> complexMap;

        Queue<Integer> queue = new ArrayDeque<>();

        Boolean[] array;

        List<A> typeVariableField;

    }
}
