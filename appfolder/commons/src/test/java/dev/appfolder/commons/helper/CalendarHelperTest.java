package dev.appfolder.commons.helper;

import dev.appfolder.commons.constants.CommonsConstants;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @see CalendarHelper
 */
class CalendarHelperTest {
    public static final LocalDate FIXED_TODAY = LocalDate.parse("2019-07-16");

    public static final LocalTime FIXED_TIME = LocalTime.of(20, 57, 29);

    public static final LocalDateTime FIXED_NOW = FIXED_TODAY.atTime(FIXED_TIME)
        .atZone(CommonsConstants.DEF_ZONE_OFFSET)
        .toLocalDateTime();

    TimeZone timeZone = TimeZone.getTimeZone("America/Sao_Paulo");

    /**
     * @see CalendarHelper#formatDateTimeToInputField(LocalDateTime, TimeZone)
     */
    @Test
    void formatDateTimeToInputField() {
        final String given = CalendarHelper.formatDateTimeToInputField(FIXED_NOW, timeZone);
        assertEquals("2019-07-16T17:57", given);
    }

    /**
     * @see CalendarHelper#parseDateTimeFromInputField(String, TimeZone)
     */
    @Test
    void parseDateTimeFromInputField() {
        final LocalDateTime value = CalendarHelper.parseDateTimeFromInputField("2019-07-16T17:57", timeZone);
        assertEquals("2019-07-16T20:57", value.toString());
    }

    /**
     * @see CalendarHelper#formatDateToInputField(LocalDate)
     */
    @Test
    void formatDateToInputField() {
        final String given = CalendarHelper.formatDateToInputField(FIXED_TODAY);
        final String expected = "2019-07-16";
        assertEquals(expected, given);
    }

}
