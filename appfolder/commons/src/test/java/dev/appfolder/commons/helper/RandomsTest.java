package dev.appfolder.commons.helper;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RandomsTest {

    /**
     * @see Randoms#nextInt(int, int)
     */
    @Test
    @DisplayName("Inteiro aleatório gerado dentro de um intervalo.")
    void nextInt() {
        assertEquals(2, Randoms.nextInt(2, 2));
        final int value = Randoms.nextInt(6, 10);
        assertTrue(value >= 6 && value <= 10);
    }
}