package dev.appfolder.commons.helper;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;

class NumbersTest {
    @Test
    void testCompare() {
        assertEquals(0, Numbers.compare(1, 1));
        assertEquals(0, Numbers.compare(1L, 1L));
        assertEquals(0, Numbers.compare(1F, 1F));
        assertEquals(0, Numbers.compare(1D, 1D));

        assertEquals(0, Numbers.compare(0, 0));
        assertEquals(0, Numbers.compare(0L, 0L));
        assertEquals(0, Numbers.compare(0F, 0F));
        assertEquals(0, Numbers.compare(0D, 0D));

        assertEquals(0, Numbers.compare(new Integer(2), new Integer(2)));
        assertEquals(0, Numbers.compare(new Long(-1), new Long(-1)));
        assertEquals(0, Numbers.compare(new Float(0), new Float(0)));
        assertEquals(0,
                Numbers.compare(new Double(1.123456789),
                        new Double(1.123456789)));
        assertEquals(1,
                Numbers.compare(new Double(1.123456789),
                        new Double(1.123456788)));
        assertEquals(-1,
                Numbers.compare(new Double(1.123456788),
                        new Double(1.123456789)));

        assertEquals(1, Numbers.compare(2d, 1d));
        assertEquals(-1, Numbers.compare(1d, 2d));

        assertEquals(-1, Numbers.compare(-2d, 1d));
        // exceção, o long é ciclado
        assertEquals(1, Numbers.compare(2, -1));

        assertEquals(-1, Numbers.compare(-2, -1));
        assertEquals(1, Numbers.compare(-1, -2));
        assertEquals(0, Numbers.compare(-1, -1));
        assertEquals(-1, Numbers.compare(-2e7, -1e6));
        assertEquals(1, Numbers.compare(2e7, 1e6));
    }

    @Test
    void testFormatCurrency() {
        assertEquals("0.00", Numbers.formatCurrency(BigDecimal.ZERO));
        assertEquals("1000000.00", Numbers.formatCurrency(BigDecimal.valueOf(1000000)));

        assertEquals("1000000.23", Numbers.formatCurrency(BigDecimal.valueOf(1000000.23)));

        assertEquals("1000000.24", Numbers.formatCurrency(BigDecimal.valueOf(1000000.236)));
        assertEquals("1000000.23", Numbers.formatCurrency(BigDecimal.valueOf(1000000.234)));

        assertEquals("1000000.24", Numbers.formatCurrency(BigDecimal.valueOf(1000000.235)));
        assertEquals("1000000.24", Numbers.formatCurrency(BigDecimal.valueOf(1000000.245)));
    }

    @Test
    void testGetDecimalFormat() throws ParseException {
        DecimalFormat decimalFormat;
        BigDecimal decimal;

        decimalFormat = Numbers.getDecimalFormat("0.0", '.');
        decimal = (BigDecimal) decimalFormat.parse("45.213");
        assertEquals("45.213", decimal.toString());

        decimalFormat = Numbers.getDecimalFormat("0.0", ',');
        decimal = (BigDecimal) decimalFormat.parse("45.21");
        assertEquals("45", decimal.toString());
    }

    @Test
    void testIsEven() {
        // testIsOdd
    }

    /**
     * @see Numbers#addLeadingZeros(long, int)
     */
    @Test
    void addLeadingZeros() {
        assertEquals("00003", Numbers.addLeadingZeros(3, 5));
        assertEquals("0033", Numbers.addLeadingZeros(33, 4));
        assertEquals("3333", Numbers.addLeadingZeros(3333, 4));
        assertEquals("3333", Numbers.addLeadingZeros(3333, 3));

        assertEquals("-01", Numbers.addLeadingZeros(-1, 3));
    }

    @Test
    void testIsNumber() {
        assertFalse(Numbers.isNumber("3"));
        assertFalse(Numbers.isNumber(null));
        assertTrue(Numbers.isNumber(3));
        assertTrue(Numbers.isNumber(3L));
        assertTrue(Numbers.isNumber(3.5f));
        assertTrue(Numbers.isNumber(new Double(3)));
        assertTrue(Numbers.isNumber(new Short((short) 1)));
    }

    @Test
    void testIsOdd() {
        for (int i = -1000, j = -999; i < 1001; i += 2, j += 2) {
            assertTrue(Numbers.isEven(j));
            assertTrue(new Numbers().isOdd(i));
            assertFalse(Numbers.isEven(i));
            assertFalse(Numbers.isOdd(j));
        }
    }
}
