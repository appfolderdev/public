package dev.appfolder.commons.helper;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class FileHelperTest {

    /**
     * @see FileHelper#getResourceFile(String)
     * @see FileHelper#read(String)
     * @see IOHelper#readBytes(InputStream)
     * @see IOHelper#readString(InputStream)
     */
    @Test
    @DisplayName("Arquivo é lido a partir do diretório de recursos.")
    void readResourceFile() throws IOException {
        final File file = FileHelper.getResourceFile("dev/appfolder/commons/helper/file with spaces.txt");
        final String given = FileHelper.read(file.getAbsolutePath());
        assertEquals("hey!", given);

        assertThrows(FileNotFoundException.class, () -> FileHelper.getResourceFile("nops"));
    }

    /**
     * @see FileHelper#getJavaFile(Class, boolean)
     * @see FileHelper#getJavaFileDir(Class, boolean)
     */
    @Test
    @DisplayName("Caminho completo de uma classe é resolvido corretamente.")
    void getJavaFile() {
        File file = FileHelper.getJavaFile(FileHelper.class, false);
        assertTrue(file.exists());
        file = FileHelper.getJavaFile(FileHelperTest.class, true);
        assertTrue(file.exists());
    }

    /**
     * @see FileHelper#getJavaFile(Class, boolean)
     * @see FileHelper#getModuleDir(Class, boolean)
     */
    @Test
    @DisplayName("Caminho completo do módulo onde uma classe está é resolvido corretamente.")
    void getModuleDir() {
        final String dir = FileHelper.getModuleDir(this.getClass(), true);
        assertTrue(dir.endsWith("commons"));
    }

    @Test
    @DisplayName("Separadores de caminhos de arquivo são alterados para barra.")
    void normalizeFilePathDelim() {
        assertEquals("/a/b/c", FileHelper.normalizeFilePathDelim("/a/b/c"));
        assertEquals("/a/b/c", FileHelper.normalizeFilePathDelim("\\a\\b\\c"));
        assertEquals("/a/b/c", FileHelper.normalizeFilePathDelim("/a/b\\c"));
    }

    /**
     * @see FileHelper#write(File, String)
     * @see FileHelper#delete(File)
     * @see FileHelper#getTempDir()
     * @see IOHelper#writeString(String, OutputStream)
     */
    @Test
    @DisplayName("Arquivos temporários são criados e deletados.")
    void writeAndDelete() throws IOException {
        final File tempDir = FileHelper.getTempDir();
        final File baseDir = new File(tempDir, UUID.randomUUID().toString());
        final File otherDir = new File
            (baseDir, UUID.randomUUID().toString());
        baseDir.mkdirs();

        FileHelper.write(new File(baseDir, "file1.txt"), "1");
        final File file2 = new File(otherDir, "file2.txt");
        FileHelper.write(file2, "2");

        final File file3 = new File(baseDir, "file3.txt");
        FileHelper.write(file3, "3");
        assertTrue(file3.exists());

        FileHelper.delete(file3);
        assertFalse(file3.exists());

        FileHelper.delete(baseDir);
        assertFalse(file2.exists());
    }
}