package dev.appfolder.commons.helper;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CastsTest {

    private static String varArgsMethod(final Object... strings) {
        return strings[0].toString();
    }

    @Test
    void testGetClass() {
        final SuperClass obj = new SubClass();
        final Class<SuperClass> aClass = Casts.getClass(obj);
        assertNotNull(aClass);
        assertNull(Casts.getClass(null));
    }

    @Test
    void testObjCast_3args() {
        //testObjCast_Class_Object
    }

    @Test
    void testObjCast_Class_Object() {
        final Object superObj = new SuperClass();
        assertNotNull(Casts.objCast(SuperClass.class, superObj));
        assertNotNull(Casts.objCast(SubClass.class, superObj));
        assertNotNull(Casts.objCast(SubSubClass.class, superObj));
        assertNull(Casts.objCast(SubClass.class, null));
    }

    @Test
    void testObjCast_error() {
        final Object superObj = new OtherClass();
        final Casts casts = new Casts();
        assertThrows(ClassCastException.class, () -> casts.objCast(SuperClass.class, superObj));
    }

    @Test
    void testObjClassCast() {
        final SuperClass obj = new SubClass();
        final Class<SuperClass> objClassCast = Casts.objClassCast(obj, obj.getClass());
        assertNotNull(objClassCast);
        assertNull(Casts.objClassCast(obj, null));
    }

    @Test
    final void testRawClassCast() {
        final Class<?> superClass = SuperClass.class;
        assertNotNull(Casts.rawClassCast(SubClass.class, superClass));
        assertNull(Casts.rawClassCast(SubClass.class, null));

        //isto funciona !! CUIDADO
        final Class subClass = OtherClass.class;
        assertNotNull(Casts.rawClassCast(SuperClass.class, subClass));
    }

    @Test
    void testSimpleCast() {
        final SuperClass obj = new SubClass();
        final SuperClass objClassCast = Casts.simpleCast(obj);
        assertNotNull(objClassCast);
    }

    @Test
    void testSubClassCast() {
        assertNotNull(Casts.subClassCast(SubClass.class, SuperClass.class));
        assertNotNull(Casts.subClassCast(SubSubClass.class, SuperClass.class));
        assertNotNull(Casts.subClassCast(SubSubClass.class, SubClass.class));
        assertNull(Casts.subClassCast(SubSubClass.class, null));
    }

    @Test
    final void testSubObjCast() {
        assertNotNull(Casts.subObjCast(SubClass.class, new SuperClass()));
        assertNotNull(Casts.subObjCast(SubSubClass.class, new SuperClass()));
        assertNotNull(Casts.subObjCast(SubSubClass.class, new SubClass()));
        assertNull(Casts.subObjCast(SubClass.class, null));
    }

    @Test
    void testValueOfEnum() {
        final EnumClass enumExp = EnumClass.VALUE1;
        final Object objValue = Casts.valueOfEnum(EnumClass.class,
                enumExp.name());
        final EnumClass enumGet = Casts.simpleCast(objValue);
        assertEquals(enumExp, enumGet);
    }

    @Test
    void testValueOfEnum_invalidType() {
        assertThrows(IllegalArgumentException.class, () -> Casts.valueOfEnum(OtherClass.class, "name"));
    }

    @Test
    void testVarArgs() {
        final String[] strings = {"v1", "v2"};
        final String result = varArgsMethod(Casts.varArgs(strings));
        assertEquals("v1", result);
    }

    private enum EnumClass {
        VALUE1,
        VALUE2
    }

    ///////////////
    private class SuperClass {

    }

    private class OtherClass {

    }

    private class SubClass extends SuperClass {

    }

    private class SubSubClass extends SubClass {

    }
}
