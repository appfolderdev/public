package dev.appfolder.commons.helper;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class StringsTest {

    /**
     * @see Strings#toUpperCaseCamelCase(String)
     */
    @Test
    @DisplayName("Coloca no formato de nome de constantes.")
    void testToUpperCaseCamelCase() {
        assertNull(Strings.toUpperCaseCamelCase(null));
        assertEquals("TESTE_OK", Strings.toUpperCaseCamelCase("testeOk"));
        assertEquals("TESTE_OK", Strings.toUpperCaseCamelCase("Teste Ok"));
        assertEquals("TESTE_OK", Strings.toUpperCaseCamelCase("teste ok"));
        assertEquals("TESTE_OK", Strings.toUpperCaseCamelCase(" teste  ok  "));
    }

    /**
     * @see Strings#toUpperCaseFirstLetter(String)
     */
    @Test
    @DisplayName("Coloca primeira maiúscula em todas palavras.")
    void testToUpperCaseFirstLetter() {
        assertNull(Strings.toUpperCaseFirstLetter(null));
        assertEquals("", Strings.toUpperCaseFirstLetter(""));
        assertEquals("Abc", Strings.toUpperCaseFirstLetter("abc"));
        assertEquals("Abc Bcd", Strings.toUpperCaseFirstLetter("abc bcd"));
    }

    /**
     * @see Strings#toLowerCaseFirstLetter(String)
     */
    @Test
    @DisplayName("Coloca primeira minúscula em todas palavras.")
    void toLowerCaseFirstLetter() {
        assertNull(Strings.toUpperCaseFirstLetter(null));
        assertEquals("", Strings.toLowerCaseFirstLetter(""));
        assertEquals("abc", Strings.toLowerCaseFirstLetter("Abc"));
        assertEquals("aBC", Strings.toLowerCaseFirstLetter("ABC"));
        assertEquals("abc bcd", Strings.toLowerCaseFirstLetter("Abc Bcd"));
    }

}