package br.com.simpou.pub.spring.commons.event;

import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

/**
 * Created by jonas on 04/02/17.
 */
@Component
public class EventPublisherImpl implements ApplicationEventPublisherAware, EventPublisher {

    private ApplicationEventPublisher publisher;

    public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    @Override
    public void publish(final ApplicationEvent event) {
        new AsyncPublisher(event).start();
    }

    @AllArgsConstructor
    class AsyncPublisher extends Thread {

        final ApplicationEvent event;

        @Override
        public void run() {
            publisher.publishEvent(event);
        }
    }
}
