package br.com.simpou.pub.spring.commons.event;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * Created by jonas on 05/02/17.
 */
@EqualsAndHashCode(of = "typedSource", callSuper = false)
public class BasicApplicationEvent<T> extends ApplicationEvent {

    @Getter
    private final T typedSource;

    public BasicApplicationEvent(final T source) {
        super(source);
        this.typedSource = source;
    }
}
