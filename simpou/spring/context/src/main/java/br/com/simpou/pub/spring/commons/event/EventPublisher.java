package br.com.simpou.pub.spring.commons.event;

import org.springframework.context.ApplicationEvent;

/**
 * Created by jonas on 12/02/17.
 */
public interface EventPublisher {

    void publish(ApplicationEvent event);
}
