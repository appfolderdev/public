package br.com.simpou.pub.spring.persistence.jpa.dsl;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

public abstract class AbstractDslRepository<ID extends Serializable, E>
        extends QuerydslRepositorySupport {

    private static final int MAX_PAGE_SIZE = 100;

    @Autowired
    private EntityManager entityManager;

    protected AbstractDslRepository(final Class<E> domainClass) {
        super(domainClass);
    }

    protected final <T> Page<T> executePaginated(final JPQLQuery<T> query,
                                                 final OrderSpecifier<?> orderSpecifier,
                                                 final Pageable pageable) {
        final List<T> result = pagingAndSorting(query, orderSpecifier, pageable).fetch();
        final long total = query.fetchCount();
        return getPage(orderSpecifier, pageable, total, result);
    }

    protected final <T> PageImpl<T> getPage(final OrderSpecifier<?> orderSpecifier, final Pageable pageable, final long total, final List<T> result) {
        final PageRequest page = buildPageable(orderSpecifier, pageable);
        return new PageImpl<>(result, page, total);
    }

    protected final <T> JPQLQuery<T> pagingAndSorting(final JPQLQuery<T> query, final OrderSpecifier<?> orderSpecifier, final Pageable pageable) {
        validatePagination(pageable);
        return query.orderBy(orderSpecifier)
                .limit(pageable.getPageSize())
                .offset(pageable.getPageNumber() * (long) pageable.getPageSize());
    }

    protected final <U, T> Page<U> executePaginated(final JPQLQuery<T> query,
                                                    final OrderSpecifier<?> orderSpecifier,
                                                    final Expression<U> selection,
                                                    final Pageable pageable) {
        validatePagination(pageable);
        final long total = query.fetchCount();
        final List<U> result = pagingAndSorting(query, orderSpecifier, pageable)
                .select(selection)
                .fetch();
        final PageRequest page = buildPageable(orderSpecifier, pageable);
        return new PageImpl<>(result, page, total);
    }

    public void refresh(final E entity) {
        entityManager.refresh(entity);
    }

    private PageRequest buildPageable(final OrderSpecifier<?> orderSpecifier, final Pageable pageable) {
        final Order order = orderSpecifier.getOrder();
        final Expression<?> target = orderSpecifier.getTarget();
        return PageRequest.of(
                pageable.getPageNumber(),
                pageable.getPageSize(),
                Sort.by(Sort.Direction.valueOf(order.name()), target.toString())
        );
    }

    private void validatePagination(final Pageable pageable) {
        if (pageable.getPageNumber() < 0 || pageable.getPageSize() > MAX_PAGE_SIZE) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
