package br.com.simpou.pub.spring.persistence.service;

import br.com.simpou.pub.commons.utils.violation.BusinessViolationException;
import br.com.simpou.pub.spring.persistence.jpa.dsl.BaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.io.Serializable;
import java.util.Optional;
import java.util.Set;

@Transactional(readOnly = true)
public abstract class BaseService<I extends Serializable, E> {

    @Autowired
    private Validator validator;

    private static <I extends Serializable, E> Optional<E> findById(final BaseRepository<I, E> repository, final I id) {
        return repository.findById(id);
    }

    public Optional<E> findById(final I id) {
        return findById(getRepository(), id);
    }

    public E getById(final I id) {
        return getById(getRepository(), id);
    }

    protected <I extends Serializable, E> E getById(final BaseRepository<I, E> repository, final I id) {
        return findById(repository, id).orElseThrow(ResourceNotFoundException::new);
    }

    protected abstract BaseRepository<I, E> getRepository();

    protected final <T> void runBeansValidation(final T form) {
        final Set<ConstraintViolation<T>> result = validator.validate(form);
        if (!result.isEmpty()) {
            throw new ConstraintViolationException(result);
        }
    }
}
