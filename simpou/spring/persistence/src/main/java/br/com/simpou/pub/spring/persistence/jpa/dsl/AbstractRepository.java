package br.com.simpou.pub.spring.persistence.jpa.dsl;

import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import javax.persistence.EntityManager;
import java.io.Serializable;

public abstract class AbstractRepository<ID extends Serializable, E>
        extends SimpleJpaRepository<E, ID>
        implements BaseRepository<ID, E> {

    protected AbstractRepository(final Class<E> domainClass, final EntityManager em) {
        super(domainClass, em);
    }
}
