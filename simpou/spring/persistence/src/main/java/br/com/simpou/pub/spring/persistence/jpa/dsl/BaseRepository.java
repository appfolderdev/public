package br.com.simpou.pub.spring.persistence.jpa.dsl;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface BaseRepository<ID extends Serializable, E> extends JpaRepository<E, ID> {

}
