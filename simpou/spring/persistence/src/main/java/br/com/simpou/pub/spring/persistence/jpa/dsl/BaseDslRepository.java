package br.com.simpou.pub.spring.persistence.jpa.dsl;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface BaseDslRepository<ID extends Serializable, E>
        extends BaseRepository<ID, E>, QuerydslPredicateExecutor<E> {

}
