package br.com.simpou.pub.scaffold.context;

import java.lang.annotation.*;

@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SimpouModel {

    boolean generateBusiness() default true;

    boolean generateRoles() default true;

    boolean generateTests() default true;

    boolean generateViews() default true;

}
