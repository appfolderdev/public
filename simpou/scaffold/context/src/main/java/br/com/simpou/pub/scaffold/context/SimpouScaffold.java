package br.com.simpou.pub.scaffold.context;

import java.lang.annotation.*;

@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SimpouScaffold {

    String name() default "";

}
