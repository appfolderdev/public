package br.com.simpou.pub.scaffold.context;

import java.lang.annotation.*;

@Inherited
@Target(ElementType.ANNOTATION_TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SimpouRoles {
    String DEFAULT = "DEFAULT";
    String PUBLIC = "PUBLIC";
    String AUTHENTICATED = "AUTHENTICATED";

    String create() default DEFAULT;

    String delete() default DEFAULT;

    String detail() default DEFAULT;

    String list() default DEFAULT;

    String update() default DEFAULT;
}
