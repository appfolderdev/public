package br.com.simpou.pub.scaffold.context;

public enum MixinViews {
    CREATE,
    DETAIL,
    FILTER,
    LIST,
    UPDATE,
    ALL
}
