package br.com.simpou.pub.scaffold.context;

import java.lang.annotation.*;

@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SimpouEntity {

    //    SimpouController controller() default @SimpouController;

    boolean controller() default true;
    boolean generateTests() default true;

    SimpouEntityId id() default @SimpouEntityId(path = SimpouEntityId.DEF_PATH, type = SimpouEntityId.DEF_ID_TYPE);

    boolean repository() default true;
    boolean service() default true;

    String tx() default "";
}
