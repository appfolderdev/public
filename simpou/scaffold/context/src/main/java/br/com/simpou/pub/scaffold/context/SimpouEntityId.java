package br.com.simpou.pub.scaffold.context;

import br.com.simpou.pub.commons.model.entity.rest.LongRestId;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({})
@Retention(RetentionPolicy.SOURCE)
public @interface SimpouEntityId {

    boolean DEF_IS_PRIMITIVE = false;
    String DEF_ID_TYPE = "java.lang.Long";
    String DEF_PATH = LongRestId.ID_PATH;

    boolean isPrimitive() default DEF_IS_PRIMITIVE;

    String path();

    String type();

}
