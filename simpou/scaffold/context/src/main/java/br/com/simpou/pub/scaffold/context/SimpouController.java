package br.com.simpou.pub.scaffold.context;

import java.lang.annotation.*;

@Inherited
@Target(ElementType.ANNOTATION_TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SimpouController {

    boolean generate() default true;

    SimpouRoles roles() default @SimpouRoles;
}
