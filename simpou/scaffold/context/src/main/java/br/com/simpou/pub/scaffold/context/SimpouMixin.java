package br.com.simpou.pub.scaffold.context;

import java.lang.annotation.*;

@Inherited
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SimpouMixin {

    String itemName() default "";

    SimpouMixinRef[] refs() default {};

    MixinViews[] value() default {};
}
