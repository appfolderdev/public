package br.com.simpou.pub.scaffold.context;

public interface CacheDefinition {

    String[] cacheNames();

}
