package br.com.simpou.pub.scaffold.mojo;

import br.com.simpou.pub.commons.utils.file.FileHelper;
import br.com.simpou.pub.scaffold.context.SimpouScaffold;
import br.com.simpou.pub.scaffold.core.GeneratedData;
import br.com.simpou.pub.scaffold.core.TemplateBuilder;
import br.com.simpou.pub.scaffold.core.lang.ModelTemplateDecorator;
import br.com.simpou.pub.scaffold.core.spring.SpringTemplateDecorator;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.stream.Collectors;

import static java.text.MessageFormat.format;

@Mojo(
        name = "scaffold",
        threadSafe = true,
        defaultPhase = LifecyclePhase.COMPILE
)
public class MainMojo extends AbstractMojo {

    private final String mainSrcRelDir = "/src/main/java/";

    private final String testSrcRelDir = "/src/test/java/";

    private final String testResourcesRelDir = "/src/test/resources/";

    private final String annotProcOutDir = "/target/generated-sources/annotations/";

    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    private MavenProject project;

    //    @Parameter(property = "pkgScans.item", required = false, readonly = true)
    //    private String[] pkgScans;
    //
    //    @Parameter(property = "groupIds.item", required = false, readonly = true)
    //    private String[] groupIds;

    @Override
    public void execute() {
        try {
            _execute();
        } catch (final Exception e) {
            getLog().error(e);
        }
    }

    private void _execute() throws Exception {
        //        final String scaffoldOutFile = this.project.getBuild().getOutputDirectory() + "/simpou-scaffold.out";
        Set<Class<?>> annotateds = new HashSet<>();
        final Class<SimpouScaffold> annotationType = SimpouScaffold.class;
        final File targetFile = this.project.getFile();
        final String outputDirectory = targetFile.getParent();
        //        if (FileHelper.exists(scaffoldOutFile)) {
        //            final String[] classnames = FileHelper.read(scaffoldOutFile).split("\n");
        //            annotateds = new HashSet<>();
        //            final URL[] urlsForClassLoader = urlsForClassLoader();
        //            final URLClassLoader urlClassLoader = new URLClassLoader(urlsForClassLoader,
        //                    this.getClass().getClassLoader());
        //            for (final String classname : classnames) {
        //                annotateds.add(urlClassLoader.loadClass(classname));
        //            }
        //        } else {

        final Log log = getLog();

        //isso aqui não funciona se a entidade extender outra que não está no mesmo módulo
        // para resolver tem que adicionar a dependencia dentro da declaração do plugin
        try {
            final Reflections reflections = new Reflections(buildConfiguration());
            annotateds = reflections.getTypesAnnotatedWith(annotationType, false);
        } catch (final Exception e) {
            log.error(e.getMessage());
            //return;
        }

        //        }
        final Map<String, Class<?>> mapElements = new HashMap<>();
        for (final Class<?> annotated : annotateds) {
            final SimpouScaffold annot = annotated.getAnnotation(SimpouScaffold.class);
            if (annot != null) {
                mapElements.put(annot.name(), annotated);
            }
        }
        log.info(format("Serão processadas {1} classes no projeto {0}", this.project, mapElements.size()));
        for (final Map.Entry<String, Class<?>> entry : mapElements.entrySet()) {
            final Class<?> annotated = entry.getValue();
            final String name = entry.getKey();
            final MojoDataSource dataSource = new MojoDataSource(log, annotated, mapElements);
            final Collection<GeneratedData> generateds = new TemplateBuilder(dataSource, name)
                    .decorate(SpringTemplateDecorator.newMojoInstance())
                    .decorate(ModelTemplateDecorator.newMojoInstance())
                    //.decorate(new MixinTemplateDecorator())
                    .generate();
            for (final GeneratedData generated : generateds) {
                final String pkgDir = generated.getPackageOf().replace(".", "/");
                final File srcDir = new File(FileHelper.normalizeFilePathDelim(outputDirectory + this.mainSrcRelDir + pkgDir));
                final File testDir = new File(FileHelper.normalizeFilePathDelim(outputDirectory + this.testSrcRelDir + pkgDir));
                final String className = generated.getSimpleName() + ".java";
                final File file = new File(generated.isTestClass() ? testDir : srcDir, className);
                if (!file.exists()) {
                    final String generatedPath = FileHelper.normalizeFilePathDelim(outputDirectory + this.annotProcOutDir + pkgDir + "/" + className);
                    log.info("Removing " + generatedPath);
                    try {
                        FileHelper.delete(generatedPath);
                        log.info("Writing " + file.getAbsolutePath());
                        FileHelper.write(file.getAbsolutePath(), generated.getContent(), false, false);
                    } catch (final Exception e) {
                        log.error(e.getMessage());
                    }
                }
            }
        }
    }

    private ConfigurationBuilder buildConfiguration() throws MalformedURLException {
        final URL[] urls = urlsForClassLoader();
        final URLClassLoader urlClassLoader = new URLClassLoader(urls,
                this.getClass().getClassLoader());
        final ConfigurationBuilder config = new ConfigurationBuilder();
        config.addClassLoader(urlClassLoader);
        config.addUrls(urls);
        config.addUrls(ClasspathHelper.forClassLoader(urlClassLoader));
        return config;
    }

    private URL[] urlsForClassLoader() throws MalformedURLException {
        final List<String> groupIdsList = new ArrayList<>(
                //                Stream.of(groupIds).collect(Collectors.toSet())
        );
        groupIdsList.add("br.com.simpou.pub");

        final String outputDirectory = this.project.getBuild().getOutputDirectory();
        final List<Artifact> dependencies = this.project.getArtifacts().stream().filter(artifact ->
                groupIdsList.contains(artifact.getGroupId())
        ).collect(Collectors.toList());

        final String[] buildPaths = new String[1 + dependencies.size()];
        buildPaths[0] = outputDirectory;
        int iter = 1;
        for (final Artifact dependency : dependencies) {
            buildPaths[iter++] = dependency.getFile().getAbsolutePath();
        }

        final URL[] urlsForClassLoader = new URL[buildPaths.length];
        for (int i = 0; i < buildPaths.length; i++) {
            final String buildPath = buildPaths[i];
            urlsForClassLoader[i] = new File(buildPath).toURI().toURL();
        }
        return urlsForClassLoader;
    }

}
