package br.com.simpou.pub.scaffold.mojo;

import br.com.simpou.pub.commons.utils.string.Strings;
import br.com.simpou.pub.scaffold.core.FieldDef;
import br.com.simpou.pub.scaffold.core.MethodDef;
import br.com.simpou.pub.scaffold.core.TemplateDataSource;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.apache.maven.plugin.logging.Log;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class MojoDataSource implements TemplateDataSource {
    private final Log log;

    private final Class<?> annotated;

    private final Map<String, Class<?>> mapElements;

    @Override
    public Date currentDate() {
        return new Date();
    }

    @Override
    public <T extends Annotation> T getAnnotation(final Class<T> annotationType) {
        return this.annotated.getAnnotation(annotationType);
    }

    @Override
    public <T extends Annotation> Collection<FieldDef<T>> getFields(final Class<T> annotType) {
        return Collections.emptyList();
    }

    @Override
    public <T extends Annotation> Collection<MethodDef<T>> getMethods(final Class<T> annotType) {
        return Collections.emptyList();
    }

    @Override
    public String getPackageOf() {
        return this.annotated.getPackage().getName();
    }

    @Override
    public String getRef(final String scaffoldName) {
        return this.mapElements.get(scaffoldName).getPackage().getName() + "." + Strings.toUpperCaseFirstLetter(scaffoldName);
    }

    @Override
    public String getSimpleName() {
        return this.annotated.getSimpleName();
    }

    @Override
    public void log(final String msg) {
        this.log.info(msg);
    }
}
