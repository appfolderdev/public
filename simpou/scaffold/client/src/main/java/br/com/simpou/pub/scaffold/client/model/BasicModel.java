package br.com.simpou.pub.scaffold.client.model;

import br.com.simpou.pub.scaffold.context.SimpouModel;
import br.com.simpou.pub.scaffold.context.SimpouScaffold;

@SimpouScaffold(name = BasicModel.SCAFFOLD_NAME)
@SimpouModel
public class BasicModel {
    public static final String SCAFFOLD_NAME = "modelo";
}
