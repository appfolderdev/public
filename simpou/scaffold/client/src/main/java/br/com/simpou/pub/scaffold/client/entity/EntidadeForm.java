package br.com.simpou.pub.scaffold.client.entity;

import lombok.*;
import lombok.experimental.FieldNameConstants;
import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@FieldNameConstants
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EntidadeForm implements Serializable {

    private java.lang.Long idEntidade;

}
