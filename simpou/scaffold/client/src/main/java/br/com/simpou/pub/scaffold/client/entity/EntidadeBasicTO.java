package br.com.simpou.pub.scaffold.client.entity;

import lombok.*;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Getter
@SuperBuilder
@FieldNameConstants(level = AccessLevel.PACKAGE)
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EntidadeBasicTO implements Serializable {

    @ToString.Include
    @EqualsAndHashCode.Include
    private java.lang.Long id;

}
