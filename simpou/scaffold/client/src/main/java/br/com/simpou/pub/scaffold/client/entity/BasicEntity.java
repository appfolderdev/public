package br.com.simpou.pub.scaffold.client.entity;

import br.com.simpou.pub.commons.model.jpa.entity.BaseEntity;
import br.com.simpou.pub.scaffold.context.SimpouEntity;
import br.com.simpou.pub.scaffold.context.SimpouModel;
import br.com.simpou.pub.scaffold.context.SimpouScaffold;
import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.Id;

@SimpouScaffold(name = BasicEntity.SCAFFOLD_NAME)
@SimpouModel
@SimpouEntity
@Entity
public class BasicEntity extends BaseEntity<Long> {
    public static final String SCAFFOLD_NAME = "entidade";

    @Id
    @Getter
    private Long id;
}
