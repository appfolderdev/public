package br.com.simpou.pub.scaffold.client.entity;

import br.com.simpou.pub.spring.persistence.jpa.dsl.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository("br.com.simpou.pub.scaffold.client.entity.EntidadeRepository")
@RepositoryRestResource(exported = false)
interface EntidadeRepository extends BaseRepository<java.lang.Long, BasicEntity>, EntidadeRepositoryCustom {

}
