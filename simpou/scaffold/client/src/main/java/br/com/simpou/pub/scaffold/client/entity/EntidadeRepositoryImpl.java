package br.com.simpou.pub.scaffold.client.entity;

import br.com.simpou.pub.spring.persistence.jpa.dsl.AbstractDslRepository;

class EntidadeRepositoryImpl
        extends AbstractDslRepository<java.lang.Long, BasicEntity>
        implements EntidadeRepositoryCustom {

    EntidadeRepositoryImpl() {
        super(BasicEntity.class);
    }

}
