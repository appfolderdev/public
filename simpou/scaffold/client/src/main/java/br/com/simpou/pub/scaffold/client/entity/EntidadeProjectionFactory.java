package br.com.simpou.pub.scaffold.client.entity;

import br.com.simpou.pub.commons.model.entity.EntityConstants;
import br.com.simpou.pub.commons.utils.lang.IssuesCodes;
import com.google.common.collect.ImmutableMap;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Map;

import static br.com.simpou.pub.scaffold.client.entity.QBasicEntity.basicEntity;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EntidadeProjectionFactory {

    static final String QUALIFIER = EntidadeView_.NAME;

    private static final Map<String, Expression<?>> DETAILED_VIEW = ImmutableMap.<String, Expression<?>>builder()
            .putAll(basicView(null))
            .build();

    private static QBean<EntidadeBasicTO> basicProjection(final Map<String, Expression<?>> bindings) {
        return Projections.fields(EntidadeBasicTO.class, bindings);
    }

    public static QBean<EntidadeBasicTO> basicProjection(final String qualifier) {
        return basicProjection(basicView(qualifier));
    }

    @SuppressWarnings(IssuesCodes.WILDCARDS_IN_RETURN)
    public static Map<String, Expression<?>> basicView(final String qualifier) {
        final QBasicEntity selector = qualifier == null ? basicEntity : newSelector(qualifier);
        return ImmutableMap.<String, Expression<?>>builder()
                .build();
    }

    static QBean<EntidadeDetailTO> detailedProjection() {
        return Projections.fields(EntidadeDetailTO.class, DETAILED_VIEW);
    }

    public static QBasicEntity newSelector(final String qualifier) {
        return new QBasicEntity(QUALIFIER + EntityConstants.JOIN_CHAR + qualifier);
    }
}
