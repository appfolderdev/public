package br.com.simpou.pub.scaffold.client.entity;

import br.com.simpou.pub.spring.persistence.service.BaseService;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Component;

@Component
@CommonsLog
@RequiredArgsConstructor
class EntidadeServiceImpl extends BaseService<java.lang.Long, BasicEntity> implements EntidadeService {

    @Getter(AccessLevel.PROTECTED)
    private final EntidadeRepository repository;

}
