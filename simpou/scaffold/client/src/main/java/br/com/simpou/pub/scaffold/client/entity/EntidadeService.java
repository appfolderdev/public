package br.com.simpou.pub.scaffold.client.entity;

import java.util.Optional;

public interface EntidadeService {

    Optional<BasicEntity> findById(java.lang.Long id);

    BasicEntity getById(java.lang.Long id);

}
