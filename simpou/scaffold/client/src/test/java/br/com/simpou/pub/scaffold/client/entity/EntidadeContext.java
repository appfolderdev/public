package br.com.simpou.pub.scaffold.client.entity;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import lombok.extern.apachecommons.CommonsLog;

@Component
@CommonsLog
@RequiredArgsConstructor
public class EntidadeContext {

    private final EntidadeServiceImpl service;

    public EntidadeService getService(){
        return service;
    }

    public BasicEntity random() {
        return newRandom();
    }

    public static BasicEntity newRandom() {
        return new BasicEntity();
    }
}
