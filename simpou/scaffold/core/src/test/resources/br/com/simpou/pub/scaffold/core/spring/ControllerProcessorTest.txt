package br.com.simpou.pub.scaffold.core;

import br.com.simpou.pub.spring.commons.service.AbstractService;
import com.fasterxml.jackson.annotation.JsonView;
import br.com.simpou.pub.commons.model.entity.EntityConstants;
import br.com.simpou.pub.commons.web.http.HttpMimeType;
import br.com.simpou.pub.spring.commons.http.AbstractEntityRest;
import br.com.simpou.pub.spring.commons.http.RestParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.annotation.Generated;
import javax.validation.Valid;

@Controller
@RequestMapping(
        value = TestProcessorController_.PATH,
        produces = {HttpMimeType.JSON_UTF8_VALUE, HttpMimeType.XML_UTF8_VALUE}
)
@Secured(TestProcessorRoles_.MAIN)
@Generated(value = "br.com.simpou.pub.scaffold.core.TemplateBuilderTest", date = "Sat Apr 04 08:57:57 BRT 2020")
class TestProcessorController_ extends AbstractEntityRest<java.lang.Long, br.com.simpou.pub.scaffold.core.TemplateBuilderTest> {

    static final String PATH = EntityConstants.REST_ENTITY_PATH + "/" + TestProcessorView_.NAME + "/";

    @Autowired
    private AbstractService<java.lang.Long, br.com.simpou.pub.scaffold.core.TemplateBuilderTest> service;

    @JsonView(TestProcessorView_.VDetail.class)
    @Secured(TestProcessorRoles_.DETAIL)
    @RequestMapping(value = "/{id}/", method = RequestMethod.GET)
    @ResponseBody
    public Object retrieveOne(@PathVariable final  java.lang.Long id,
                              final HttpServletRequest req) throws Exception {
        return super.getObjectResponse(
                service.retrieve(id, new RestParameters(req)),
                req
        );
    }

    @JsonView(TestProcessorView_.VList.class)
    @Secured(TestProcessorRoles_.LIST)
    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public Object retrieveAll(@JsonView(TestProcessorView_.VFilter.class) final br.com.simpou.pub.scaffold.core.TemplateBuilderTest filter,
                              final Pageable page,
                              final HttpServletRequest req) throws Exception {
        return super.getListResponse(
                page,
                service.retrieve(
                        page,
                        super.applyView(filter, TestProcessorView_.VFilter.class),
                        new RestParameters(req)
                ),
                req,
                TestProcessorView_.NAME
        );
    }

    @Secured(TestProcessorRoles_.CREATE)
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Object create(@Valid @RequestBody @JsonView(TestProcessorView_.VCreate.class) final br.com.simpou.pub.scaffold.core.TemplateBuilderTest data,
                         final HttpServletRequest req) {
        final br.com.simpou.pub.scaffold.core.TemplateBuilderTest entity = service.create(data, new RestParameters(req));
        return super.created(
                entity.identity(),
                TestProcessorView_.NAME
        );
    }

    @Secured(TestProcessorRoles_.UPDATE)
    @RequestMapping(value = "/{id}/", method = RequestMethod.PUT)
    public Object update(@PathVariable final  java.lang.Long id,
                         @RequestBody @JsonView(TestProcessorView_.VUpdate.class) final br.com.simpou.pub.scaffold.core.TemplateBuilderTest data,
                         final HttpServletRequest req) {
        service.update(id, data, new RestParameters(req));
        return super.ok(id, TestProcessorView_.NAME);
    }

    @Secured(TestProcessorRoles_.REMOVE)
    @RequestMapping(value = "/{id}/", method = RequestMethod.DELETE)
    public Object delete(@PathVariable final  java.lang.Long id,
                         final HttpServletRequest req) {
        service.delete(id, new RestParameters(req));
        return super.ok(id, TestProcessorView_.NAME);
    }

}
