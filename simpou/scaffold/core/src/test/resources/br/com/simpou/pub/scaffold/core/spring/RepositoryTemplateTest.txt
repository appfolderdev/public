package br.com.simpou.pub.scaffold.core;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.annotation.Generated;

@Repository("br.com.simpou.pub.scaffold.core.TestProcessorRepository_")
@Generated(value = "br.com.simpou.pub.scaffold.core.TemplateBuilderTest", date = "Sat Apr 04 08:57:57 BRT 2020")
interface TestProcessorRepository_ extends JpaRepository<br.com.simpou.pub.scaffold.core.TemplateBuilderTest, java.lang.Long>, TestProcessorDao {

}
