package br.com.simpou.pub.scaffold.core;

import br.com.simpou.pub.commons.utils.file.FileHelper;
import br.com.simpou.pub.scaffold.context.SimpouEntity;
import br.com.simpou.pub.scaffold.context.SimpouEntityId;
import br.com.simpou.pub.scaffold.context.SimpouModel;
import br.com.simpou.pub.scaffold.core.lang.ModelTemplateDecorator;
import br.com.simpou.pub.scaffold.core.mixin.MixinTemplateDecorator;
import br.com.simpou.pub.scaffold.core.spring.SpringTemplateDecorator;
import org.junit.Test;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TemplateBuilderTest {

    @Test
    void newInstance() throws Exception {
        final TemplateDataSource dataSource = mock(TemplateDataSource.class);

        when(dataSource.getPackageOf()).thenReturn(this.getClass().getPackage().getName());
        when(dataSource.getSimpleName()).thenReturn(this.getClass().getSimpleName());
        when(dataSource.currentDate()).thenReturn(new Date(1586001477212L));

        final SimpouModel simpouModel = mock(SimpouModel.class);
        when(dataSource.getAnnotation(SimpouModel.class)).thenReturn(simpouModel);

        final SimpouEntity simpouEntity = mock(SimpouEntity.class);
        when(dataSource.getAnnotation(SimpouEntity.class)).thenReturn(simpouEntity);
        when(simpouEntity.tx()).thenReturn("");

        final SimpouEntityId simpouEntityId = mock(SimpouEntityId.class);
        when(dataSource.getAnnotation(SimpouEntityId.class)).thenReturn(simpouEntityId);
        when(simpouEntityId.path()).thenReturn("{id}");
        //        when(restIdType.type()).thenReturn(Long.class);

        final TemplateBuilder templateBuilder = new TemplateBuilder(dataSource, "testProcessor")
                .decorate(ModelTemplateDecorator.newProcessorInstance())
                .decorate(SpringTemplateDecorator.newProcessorInstance())
                .decorate(new MixinTemplateDecorator());
        final Collection<GeneratedData> generateds = templateBuilder.generate();

        final Map<String, String> mapContent = new HashMap<>();
        for (final GeneratedData generated : generateds) {
            mapContent.put(generated.getSimpleName(), generated.getContent());
        }

        assertContent("spring/ServiceProcessorTest", mapContent.get("TestProcessorService_"));
        assertContent("lang/ViewProcessorTest", mapContent.get("TestProcessorView_"));
        assertContent("spring/ControllerProcessorTest", mapContent.get("TestProcessorController_"));
        assertContent("spring/RepositoryTemplateTest", mapContent.get("TestProcessorRepository_"));
        assertContent("lang/RolesProcessorTest", mapContent.get("TestProcessorRoles_"));
        assertContent("spring/DaoTemplateTest", mapContent.get("TestProcessorDao"));
    }

    private void assertContent(final String expectedFile, final String givenContent) throws IOException {
        final String given = givenContent.replace("\r\n", "").replace("\n", "");
        final String expectedFileName = this.getClass().getPackage().getName().replace(".", "/") + "/" + expectedFile + ".txt";
        final String expectedFilePath = FileHelper.getResourceFile(expectedFileName).getAbsolutePath();
        final String expected = FileHelper.read(expectedFilePath).replace("\r\n", "").replace("\n", "");
        assertEquals(expected, given);
    }
}
