package br.com.simpou.pub.scaffold.core.mixin;

import br.com.simpou.pub.scaffold.context.MixinViews;
import br.com.simpou.pub.scaffold.context.SimpouMixin;
import br.com.simpou.pub.scaffold.context.SimpouMixinRef;
import br.com.simpou.pub.scaffold.context.SimpouModel;
import br.com.simpou.pub.scaffold.core.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static br.com.simpou.pub.scaffold.core.ScaffoldConstants.VIEW_REF;

public class MixinTemplateDecorator implements TemplateDecorator {
    @Override
    public void decorate(final TemplateBuilder templateBuilder) {
        final TemplateDataSource dataSource = templateBuilder.getDataSource();
        final String ref = templateBuilder.getRef(VIEW_REF);

        boolean generateMethods = false;
        final Collection<MethodDef<SimpouMixin>> methods = dataSource.getMethods(SimpouMixin.class);
        for (final MethodDef<SimpouMixin> method : methods) {
            final SimpouMixin simpouMixinAnnot = method.getAnnotation();
            final StringBuilder viewsString = new StringBuilder();
            addViews(viewsString, simpouMixinAnnot, dataSource, ref);
            if (viewsString.length() > 0) {
                method.setCustom(viewsString.substring(0, viewsString.length() - 2));
                generateMethods = true;
            }
        }

        if (generateMethods) {
            templateBuilder.setParameter("methods", methods);
        }

        boolean generateFields = false;
        final Collection<FieldDef<SimpouMixin>> fields = dataSource.getFields(SimpouMixin.class);
        for (final FieldDef<SimpouMixin> field : fields) {
            final SimpouMixin simpouMixinAnnot = field.getAnnotation();
            final StringBuilder viewsString = new StringBuilder();
            addViews(viewsString, simpouMixinAnnot, dataSource, ref);
            field.setItemName(simpouMixinAnnot.itemName());
            if (viewsString.length() > 0) {
                field.setCustom(viewsString.substring(0, viewsString.length() - 2));
                generateFields = true;
            }
        }

        if (generateFields) {
            templateBuilder.setParameter("fields", fields);
        }

        if (generateFields || generateMethods) {
            final String pkgName = this.getClass().getPackage().getName();
            templateBuilder.add(new TemplateData(pkgName, "MixinProcessor", ScaffoldConstants.MIXIN_SUFFIX, "mixinName", true, false));
        }
    }

    @Override
    public boolean isQualified(final TemplateDataSource dataSource) {
        return dataSource.getAnnotation(SimpouModel.class) != null;
    }

    private void addViews(final List<MixinViews> views, final StringBuilder viewsString, final String ref) {
        if (views.contains(MixinViews.ALL)) {
            viewsString.append("br.com.simpou.pub.commons.model.view.GlobalView.class, ");
            return;
        }
        if (views.contains(MixinViews.LIST)) {
            viewsString.append(ref).append(".V").append("List.class, ");
        }
        if (views.contains(MixinViews.DETAIL)) {
            viewsString.append(ref).append(".V").append("Detail.class, ");
        }
        if (views.contains(MixinViews.UPDATE)) {
            viewsString.append(ref).append(".V").append("Update.class, ");
        }
        if (views.contains(MixinViews.FILTER)) {
            viewsString.append(ref).append(".V").append("Filter.class, ");
        }
        if (views.contains(MixinViews.CREATE)) {
            viewsString.append(ref).append(".V").append("Create.class, ");
        }
    }

    private void addViews(final StringBuilder viewsString,
                          final SimpouMixin simpouMixinAnnot,
                          final TemplateDataSource dataSource,
                          final String ref) {
        final MixinViews[] configViews = simpouMixinAnnot.value();
        final SimpouMixinRef[] configRefs = simpouMixinAnnot.refs();
        final List<MixinViews> views = Arrays.asList(configViews);
        addViews(views, viewsString, ref);
        if (configRefs.length > 0) {
            for (final SimpouMixinRef configRef : configRefs) {
                final List<MixinViews> viewsRef = Arrays.asList(configRef.value());
                addViews(viewsRef, viewsString, dataSource.getRef(configRef.name()) + ScaffoldConstants.VIEW_SUFFIX);
            }
        }
    }

}
