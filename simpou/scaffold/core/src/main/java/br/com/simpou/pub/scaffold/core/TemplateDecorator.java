package br.com.simpou.pub.scaffold.core;

public interface TemplateDecorator {
    void decorate(TemplateBuilder templateBuilder) throws Exception;

    boolean isQualified(final TemplateDataSource dataSource);

}
