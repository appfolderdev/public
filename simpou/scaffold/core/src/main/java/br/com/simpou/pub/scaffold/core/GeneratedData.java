package br.com.simpou.pub.scaffold.core;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class GeneratedData {
    private final String packageOf;

    private final String simpleName;

    private final String content;

    private final boolean testClass;

    public String getFullName() {
        return this.packageOf + "." + this.simpleName;
    }

    @Override
    public String toString() {
        return this.content;
    }
}
