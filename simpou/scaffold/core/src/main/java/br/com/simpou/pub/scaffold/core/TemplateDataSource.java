package br.com.simpou.pub.scaffold.core;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Date;

public interface TemplateDataSource {
    Date currentDate();

    <T extends Annotation> T getAnnotation(Class<T> annotationType);

    <T extends Annotation> Collection<FieldDef<T>> getFields(final Class<T> annotType);

    <T extends Annotation> Collection<MethodDef<T>> getMethods(final Class<T> annotType);

    String getPackageOf();

    String getRef(String scaffoldName);

    String getSimpleName();

    void log(String msg);
}
