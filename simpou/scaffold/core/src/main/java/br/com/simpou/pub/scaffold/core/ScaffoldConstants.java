package br.com.simpou.pub.scaffold.core;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ScaffoldConstants {
    public static final String CONTROLLER_SUFFIX = "Controller";

    public static final String FORM_SUFFIX = "Form";

    public static final String MIXIN_SUFFIX = "Mixin_";

    public static final String ROLES_SUFFIX = "Roles_";

    public static final String VIEW_SUFFIX = "View_";

    public static final String BUSINESS_SUFFIX = "Business";

    public static final String TEST_SUFFIX = "Test";

    public static final String VALIDATOR_SUFFIX = "Validator";

    public static final String ENRICHING_SUFFIX = "Enriching";

    public static final String VIEW_REF = "viewName";

    public static final String SERVICE_SUFFIX = "Service_";

    public static final String PUBLIC_SERVICE_SUFFIX = "Service";

    public static final String PUBLIC_SERVICE_IMPL_SUFFIX = "ServiceImpl";

    public static final String ENTITY_BASIC_TO_SUFFIX = "BasicTO";

    public static final String ENTITY_DETAIL_TO_SUFFIX = "DetailTO";

    public static final String PROJECTION_FACTORY_SUFFIX = "ProjectionFactory";

    public static final String CONTEXT_SUFFIX = "Context";

    public static final String DAO_CUSTOM_SUFFIX = "RepositoryCustom";

    public static final String DAO_SUFFIX = "Repository";

    public static final String DAO_IMPL_SUFFIX = "RepositoryImpl";
}
