package br.com.simpou.pub.scaffold.core;

import br.com.simpou.pub.commons.utils.file.VelocityHelper;
import br.com.simpou.pub.commons.utils.string.Strings;
import lombok.Getter;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class TemplateBuilder {
    @Getter
    private final TemplateDataSource dataSource;

    private final Map<String, Object> params = new HashMap<>();

    private final Map<String, Template> templates = new HashMap<>();

    private final String name;

    public TemplateBuilder(final TemplateDataSource dataSource, final String generatedName) {
        this.dataSource = dataSource;

        final String simpleName = this.dataSource.getSimpleName() + "";
        if (!StringUtils.isEmpty(generatedName)) {
            this.name = generatedName;
        } else {
            this.name = simpleName;
        }

        final String nameFirstLowerCase = Strings.toLowerCaseFirstLetter(this.name);
        final String nameFirstUpperCase = Strings.toUpperCaseFirstLetter(this.name);
        final String nameUpperCamelCase = Strings.toUpperCaseCamelCase(this.name);
        final String packageOf = this.dataSource.getPackageOf() + "";

        this.params.put("srcClassName", simpleName);
        this.params.put("srcClassNameFirstLowerCase", Strings.toLowerCaseFirstLetter(simpleName));
        this.params.put("srcClass", packageOf + "." + simpleName);
        this.params.put("pkgName", packageOf);
        this.params.put("nameFirstLowerCase", nameFirstLowerCase);
        this.params.put("nameFirstUpperCase", nameFirstUpperCase);
        this.params.put("nameUpperCamelCase", nameUpperCamelCase);
        this.params.put("nameLowerCamelCase", nameUpperCamelCase.toLowerCase());
        this.params.put("dateGenerated", this.dataSource.currentDate().toString());
    }

    public void add(final TemplateData data) {
        final String classNameFix = Strings.toUpperCaseFirstLetter(this.name + data.getClassName());
        if (data.isGenerate()) {
            this.templates.put(data.getTemplatePkg() + "." + data.getTemplateName(), new Template(classNameFix, data.isTestClass()));
        }
        this.params.put(data.getVarRef(), classNameFix);
    }

    public TemplateBuilder decorate(final TemplateDecorator decorator) {
        if (decorator.isQualified(this.dataSource)) {
            //            this.dataSource.log("Aplicando template " + decorator.getClass().getSimpleName() + "...");
            try {
                decorator.decorate(this);
            } catch (final Exception e) {
                this.dataSource.log("Erro: " + e.getClass().getName() + " - " + e.getMessage());
            }
        }
        return this;
    }

    public Collection<GeneratedData> generate() throws Exception {
        final Collection<GeneratedData> generateds = new ArrayList<>();
        final String packageOf = this.dataSource.getPackageOf() + "";
        for (final Map.Entry<String, Template> templateEntry : this.templates.entrySet()) {
            final String templatePath = templateEntry.getKey().replace(".", "/") + ".vsl";
            final VelocityHelper velocityHelper = new VelocityHelper(templatePath);
            for (final Map.Entry<String, Object> paramEntry : this.params.entrySet()) {
                velocityHelper.setParameter(paramEntry.getKey(), paramEntry.getValue());
            }
            final Template value = templateEntry.getValue();
            generateds.add(new GeneratedData(packageOf, value.name, velocityHelper.merge(), value.test));
        }
        return generateds;
    }

    public String getRef(final String varRef) {
        return this.params.get(varRef) + "";
    }

    public String getScaffoldName(final String name) {
        return Strings.toUpperCaseFirstLetter(name);
    }

    public void setParameter(final String key, final Object value) {
        this.params.put(key, value);
    }

    @Value
    private class Template {
        String name;

        boolean test;
    }
}
