package br.com.simpou.pub.scaffold.core;

import lombok.Value;

@Value
public class TemplateData {
    String templatePkg;

    String templateName;

    String className;

    String varRef;

    boolean generate;

    boolean testClass;
}
