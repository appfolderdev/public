package br.com.simpou.pub.scaffold.core;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.lang.annotation.Annotation;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Getter
@RequiredArgsConstructor
public class FieldDef<T extends Annotation> {

    private final String canonicalName;

    private final String fieldName;

    private final T annotation;

    @Setter
    private String itemName;

    @Setter
    private String custom;

    public boolean isList() {
        return isNotEmpty(itemName) && List.class.getName().equals(canonicalName);
    }

}
