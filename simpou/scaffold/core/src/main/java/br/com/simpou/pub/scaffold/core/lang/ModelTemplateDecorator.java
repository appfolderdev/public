package br.com.simpou.pub.scaffold.core.lang;

import br.com.simpou.pub.scaffold.context.SimpouModel;
import br.com.simpou.pub.scaffold.core.*;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import static br.com.simpou.pub.scaffold.core.ScaffoldConstants.*;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ModelTemplateDecorator implements TemplateDecorator {

    private final boolean outputTargetDir;

    public static ModelTemplateDecorator newMojoInstance() {
        return new ModelTemplateDecorator(false);
    }

    public static ModelTemplateDecorator newProcessorInstance() {
        return new ModelTemplateDecorator(true);
    }

    @Override
    public void decorate(final TemplateBuilder templateBuilder) {
        final SimpouModel annot = templateBuilder.getDataSource().getAnnotation(SimpouModel.class);

        final boolean generateRoles = annot.generateRoles();
        final boolean generateViews = annot.generateViews();

        final String pkgName = ModelTemplateDecorator.class.getPackage().getName();
        if (generateRoles) {
            templateBuilder.add(new TemplateData(pkgName, "RolesProcessor", ScaffoldConstants.ROLES_SUFFIX, "rolesName", outputTargetDir, false));
        }
        if (generateViews) {
            templateBuilder.add(new TemplateData(pkgName, "ViewProcessor", ScaffoldConstants.VIEW_SUFFIX, VIEW_REF, outputTargetDir, false));
        }
        if(annot.generateBusiness()) {
            templateBuilder.add(new TemplateData(pkgName, "BusinessTemplate", BUSINESS_SUFFIX, "businessName", !outputTargetDir, false));
            if(annot.generateTests()) {
                templateBuilder.add(new TemplateData(pkgName, "BusinessTest", BUSINESS_SUFFIX + TEST_SUFFIX, "businessTestName", !outputTargetDir, true));
            }
        }
    }

    @Override
    public boolean isQualified(final TemplateDataSource dataSource) {
        return dataSource.getAnnotation(SimpouModel.class) != null;
    }
}
