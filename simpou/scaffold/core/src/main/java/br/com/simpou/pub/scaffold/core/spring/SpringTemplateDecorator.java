package br.com.simpou.pub.scaffold.core.spring;

import br.com.simpou.pub.scaffold.context.SimpouEntity;
import br.com.simpou.pub.scaffold.context.SimpouEntityId;
import br.com.simpou.pub.scaffold.context.SimpouRoles;
import br.com.simpou.pub.scaffold.core.TemplateBuilder;
import br.com.simpou.pub.scaffold.core.TemplateData;
import br.com.simpou.pub.scaffold.core.TemplateDataSource;
import br.com.simpou.pub.scaffold.core.TemplateDecorator;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import static br.com.simpou.pub.commons.utils.string.Strings.isEmpty;
import static br.com.simpou.pub.scaffold.context.SimpouRoles.AUTHENTICATED;
import static br.com.simpou.pub.scaffold.context.SimpouRoles.PUBLIC;
import static br.com.simpou.pub.scaffold.core.ScaffoldConstants.*;
import static org.apache.commons.lang3.BooleanUtils.or;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class SpringTemplateDecorator implements TemplateDecorator {

    private final boolean outputTargetDir;

    public static SpringTemplateDecorator newMojoInstance() {
        return new SpringTemplateDecorator(false);
    }

    public static SpringTemplateDecorator newProcessorInstance() {
        return new SpringTemplateDecorator(true);
    }

    @Override
    public void decorate(final TemplateBuilder templateBuilder) {
        final SimpouEntity annot = templateBuilder.getDataSource().getAnnotation(SimpouEntity.class);

        //        final SimpouController controllerAnnot = annot.controller();
        //        final boolean generateController = controllerAnnot.generate();

        final String pkgName = SpringTemplateDecorator.class.getPackage().getName();
        final boolean outputSrcDir = !outputTargetDir;

        if (annot.repository()) {
            templateBuilder.add(new TemplateData(pkgName, "RepositoryTemplate", DAO_SUFFIX, "repositoryName", outputSrcDir, false));
            templateBuilder.add(new TemplateData(pkgName, "DaoTemplate", DAO_CUSTOM_SUFFIX, "daoName", outputSrcDir, false));
            templateBuilder.add(new TemplateData(pkgName, "EntityBasicTO", ENTITY_BASIC_TO_SUFFIX, "toName", outputSrcDir, false));
            templateBuilder.add(new TemplateData(pkgName, "EntityDetailTO", ENTITY_DETAIL_TO_SUFFIX, "detailToName", outputSrcDir, false));
            templateBuilder.add(new TemplateData(pkgName, "FactoryTO", PROJECTION_FACTORY_SUFFIX, "projectionFactoryName", outputSrcDir, false));
            templateBuilder.add(new TemplateData(pkgName, "RepositoryImplTemplate", DAO_IMPL_SUFFIX, "repositoryImplName", outputSrcDir, false));
            if (annot.service()) {
                templateBuilder.add(new TemplateData(pkgName, "PublicService", PUBLIC_SERVICE_SUFFIX, "serviceName", outputSrcDir, false));
                templateBuilder.add(new TemplateData(pkgName, "PublicServiceImpl", PUBLIC_SERVICE_IMPL_SUFFIX, "serviceNameImpl", outputSrcDir, false));
                templateBuilder.add(new TemplateData(pkgName, "TestContext", CONTEXT_SUFFIX, "testContextName", outputSrcDir, true));
                templateBuilder.add(new TemplateData(pkgName, "EntityForm", FORM_SUFFIX, "formName", outputSrcDir, false));
                if (annot.generateTests()) {
                    templateBuilder.add(new TemplateData(pkgName, "ServiceTest", PUBLIC_SERVICE_SUFFIX + TEST_SUFFIX, "serviceTestName", !outputTargetDir, true));
                }
                if (annot.controller()) {
                    templateBuilder.add(new TemplateData(pkgName, "ControllerProcessor", CONTROLLER_SUFFIX, "controllerName", outputSrcDir, false));
                    if (annot.generateTests()) {
                        templateBuilder.add(new TemplateData(pkgName, "ControllerTest", CONTROLLER_SUFFIX + TEST_SUFFIX, "controllerTestName", !outputTargetDir, true));
                    }
                }
            }
        }
        //        templateBuilder.add(new TemplateData(pkgName, "BusinessTemplate", BUSINESS_SUFFIX, "businessName", outputSrcDir, false));

        //        templateBuilder.add(new TemplateData(pkgName, "ServiceProcessor", SERVICE_SUFFIX, "serviceName", this.isProcessor));
        //        templateBuilder.add(new TemplateData(pkgName, "PublicServiceImpl", PUBLIC_SERVICE_IMPL_SUFFIX, "publicServiceImplName", true));
        //        templateBuilder.add(new TemplateData(pkgName, "ServiceEnrichingTemplate", ENRICHING_SUFFIX, "serviceEnrichingName", true));
        //        templateBuilder.add(new TemplateData(pkgName, "ServiceSanitizerTemplate", VALIDATOR_SUFFIX, "serviceSanitizerName", true));

        final SimpouEntityId simpouEntityId = annot.id();
        templateBuilder.setParameter("idPath", simpouEntityId.path());
        templateBuilder.setParameter("idType", simpouEntityId.type());
        templateBuilder.setParameter("txName", isEmpty(annot.tx()) ? null : annot.tx());
        templateBuilder.setParameter("basicId", SimpouEntityId.DEF_PATH.equals(simpouEntityId.path()));
        //        templateBuilder.setParameter("customRoles", new CustomRoles(controllerAnnot.roles()));
    }

    @Override
    public boolean isQualified(final TemplateDataSource dataSource) {
        return dataSource.getAnnotation(SimpouEntity.class) != null;
    }

    @Getter
    public class CustomRoles {
        private final String create;

        private final String delete;

        private final String detail;

        private final String list;

        private final String update;

        private final boolean hasPreAuthorize;

        CustomRoles(final SimpouRoles config) {
            this.create = config.create();
            this.delete = config.delete();
            this.detail = config.detail();
            this.list = config.list();
            this.update = config.update();
            this.hasPreAuthorize = or(new boolean[]{
                    PUBLIC.equalsIgnoreCase(this.create),
                    PUBLIC.equalsIgnoreCase(this.delete),
                    PUBLIC.equalsIgnoreCase(this.detail),
                    PUBLIC.equalsIgnoreCase(this.list),
                    PUBLIC.equalsIgnoreCase(this.update),
                    AUTHENTICATED.equalsIgnoreCase(this.create),
                    AUTHENTICATED.equalsIgnoreCase(this.delete),
                    AUTHENTICATED.equalsIgnoreCase(this.detail),
                    AUTHENTICATED.equalsIgnoreCase(this.list),
                    AUTHENTICATED.equalsIgnoreCase(this.update)
            });
        }
    }
}
