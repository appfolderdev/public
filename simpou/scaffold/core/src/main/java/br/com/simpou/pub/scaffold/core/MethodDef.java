package br.com.simpou.pub.scaffold.core;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.lang.annotation.Annotation;

@Getter
@RequiredArgsConstructor
public class MethodDef<T extends Annotation> {

    private final String signature;

    private final T annotation;

    @Setter
    private String custom;

}
