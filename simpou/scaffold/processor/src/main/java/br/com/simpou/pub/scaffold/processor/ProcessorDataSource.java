package br.com.simpou.pub.scaffold.processor;

import br.com.simpou.pub.commons.utils.string.Strings;
import br.com.simpou.pub.scaffold.core.FieldDef;
import br.com.simpou.pub.scaffold.core.MethodDef;
import br.com.simpou.pub.scaffold.core.TemplateDataSource;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import javax.annotation.processing.Messager;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import java.lang.annotation.Annotation;
import java.util.*;

import static javax.tools.Diagnostic.Kind.NOTE;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class ProcessorDataSource implements TemplateDataSource {

    private final Element annotatedElement;

    private final Map<String, Element> mapElements;

    private final Elements elementUtils;

    private final Types typeUtils;

    private final Messager messager;

    @Override
    public Date currentDate() {
        return new Date();
    }

    @Override
    public final <T extends Annotation> T getAnnotation(final Class<T> annotationType) {
        return this.annotatedElement.getAnnotation(annotationType);
    }

    @Override
    public final <T extends Annotation> Collection<FieldDef<T>> getFields(final Class<T> annotType) {
        final List<VariableElement> variableElements = ElementFilter.fieldsIn(this.annotatedElement.getEnclosedElements());
        final Collection<FieldDef<T>> fields = new ArrayList<>();
        for (final VariableElement variableElement : variableElements) {
            final T annotation = variableElement.getAnnotation(annotType);
            if (annotation != null) {
                final Element element = this.typeUtils.asElement(variableElement.asType());
                fields.add(new FieldDef<>(
                        this.elementUtils.getPackageOf(element) + "." + element.getSimpleName(),
                        variableElement.getSimpleName() + "",
                        annotation
                ));
            }
        }
        return fields;
    }

    @Override
    public <T extends Annotation> Collection<MethodDef<T>> getMethods(final Class<T> annotType) {
        final List<ExecutableElement> executableElements = ElementFilter.methodsIn(this.annotatedElement.getEnclosedElements());
        final Collection<MethodDef<T>> methods = new ArrayList<>();
        for (final ExecutableElement executableElement : executableElements) {
            final T annotation = executableElement.getAnnotation(annotType);
            if (annotation != null) {
                final Element returnType = this.typeUtils.asElement(executableElement.getReturnType());
                methods.add(new MethodDef<>(
                        this.elementUtils.getPackageOf(returnType) + "." + returnType.getSimpleName() + " " + executableElement.toString(),
                        annotation
                ));
            }
        }
        return methods;
    }

    @Override
    public String getPackageOf() {
        return this.elementUtils.getPackageOf(this.annotatedElement) + "";
    }

    @Override
    public String getRef(final String scaffoldName) {
        final Element element = this.mapElements.get(scaffoldName);
        return this.elementUtils.getPackageOf(element) + "." + Strings.toUpperCaseFirstLetter(scaffoldName);
    }

    @Override
    public String getSimpleName() {
        return this.annotatedElement.getSimpleName() + "";
    }

    @Override
    public void log(final String msg) {
        this.messager.printMessage(NOTE, msg);
    }
}
