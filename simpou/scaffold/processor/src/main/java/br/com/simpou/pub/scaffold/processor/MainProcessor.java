package br.com.simpou.pub.scaffold.processor;

import br.com.simpou.pub.commons.utils.file.FileHelper;
import br.com.simpou.pub.scaffold.context.SimpouScaffold;
import br.com.simpou.pub.scaffold.core.GeneratedData;
import br.com.simpou.pub.scaffold.core.TemplateBuilder;
import br.com.simpou.pub.scaffold.core.lang.ModelTemplateDecorator;
import br.com.simpou.pub.scaffold.core.mixin.MixinTemplateDecorator;
import br.com.simpou.pub.scaffold.core.spring.SpringTemplateDecorator;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import java.io.File;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static java.text.MessageFormat.format;
import static javax.tools.Diagnostic.Kind.ERROR;
import static javax.tools.Diagnostic.Kind.NOTE;

@SupportedAnnotationTypes({
        "br.com.simpou.pub.scaffold.context.SimpouScaffold"
})
@SupportedSourceVersion(SourceVersion.RELEASE_7)
public class MainProcessor extends AbstractProcessor {
    private Messager messager;

    private String filePath;

    @Override
    public boolean process(final Set<? extends TypeElement> annotations, final RoundEnvironment roundEnv) {
        final Elements elementUtils = super.processingEnv.getElementUtils();
        this.messager = this.processingEnv.getMessager();
        final Types typeUtils = super.processingEnv.getTypeUtils();
        final Filer filer = super.processingEnv.getFiler();

        final StringBuilder classesFound = new StringBuilder();

        boolean result = true;
        for (final TypeElement annotation : annotations) {
            final Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
            final Map<String, Element> mapElements = new HashMap<>();
            for (final Element annotatedElement : annotatedElements) {
                final SimpouScaffold annot = annotatedElement.getAnnotation(SimpouScaffold.class);
                mapElements.put(annot.name(), annotatedElement);
            }
            for (final Element annotatedElement : annotatedElements) {
                this.messager.printMessage(NOTE, "Processando " + annotatedElement.getSimpleName() + "...");
                final SimpouScaffold annot = annotatedElement.getAnnotation(SimpouScaffold.class);
                final ProcessorDataSource dataSource = new ProcessorDataSource(annotatedElement, mapElements, elementUtils, typeUtils, this.messager);
                final TemplateBuilder templateBuilder = new TemplateBuilder(dataSource, annot.name())
                        .decorate(ModelTemplateDecorator.newProcessorInstance())
                        //.decorate(SpringTemplateDecorator.newProcessorInstance())
                        .decorate(new MixinTemplateDecorator());
                try {
                    final Collection<GeneratedData> generateds = templateBuilder.generate();
                    writeJavaFile(generateds, filer, annotatedElement);
                    classesFound.append(dataSource.getPackageOf()).append(".").append(dataSource.getSimpleName()).append("\n");
                } catch (final Exception e) {
                    this.messager.printMessage(ERROR, format(
                            "Erro: {1}. Mensagem: {0}",
                            e.getMessage(),
                            e.getClass().getName()
                    ));
                    result = false;
                }
            }
        }

        try {
            final FileObject builderFile = filer.createResource(StandardLocation.CLASS_OUTPUT, "", "simpou-scaffold.out");
            this.filePath = builderFile.toUri().getPath().substring(1);
            try (final PrintWriter out = new PrintWriter(builderFile.openWriter())) {
                out.print(classesFound.toString());
            }
        } catch (final Exception e) {
            //TODO cai aqui quando cria o arquivo duas vezes, achar uma forma de burlar isso
            this.messager.printMessage(NOTE, format("Erro: {1}. Mensagem: {0}", e.getMessage(), e.getClass().getName()));
        }

        return result;
    }

    private void writeJavaFile(final Collection<GeneratedData> generateds, final Filer filer, final Element annotatedElement) throws Exception {
        for (final GeneratedData generated : generateds) {
            final FileObject mainJavaFile = filer.getResource(StandardLocation.CLASS_OUTPUT, generated.getPackageOf(), generated.getSimpleName() + ".java");
            final String classPath = FileHelper.normalizeFilePathDelim(mainJavaFile.toUri().getPath());
            final String srcPath = classPath.replace("target/classes", "src/main/java");
            final File srcFile = new File(srcPath);
            if (srcFile.exists()) {
                this.messager.printMessage(NOTE, "Not generated. Files exists: " + srcFile.getAbsolutePath());
            } else {
                final JavaFileObject builderFile = filer.createSourceFile(generated.getFullName(), annotatedElement);
                try (final PrintWriter out = new PrintWriter(builderFile.openWriter())) {
                    out.print(generated.getContent());
                    this.messager.printMessage(NOTE, "Generated: " + builderFile.getName());
                }
            }
        }
    }

}
