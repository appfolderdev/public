package br.com.simpou.pub.commons.persistence.jpa.dao.impl;

import br.com.simpou.pub.commons.model.entity.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;

import javax.persistence.EntityManager;

/**
 * Created by jonas.pereira on 14/07/15.
 */
public class DefaultTypedJpaDAOImpl<T extends BaseEntity> extends AbstractTypedJpaDAOImpl<T> {

    @Getter(AccessLevel.PROTECTED)
    private final EntityManager entityManager;

    public DefaultTypedJpaDAOImpl(final Class<T> entityClass,
                                  final EntityManager entityManager) {
        super(entityClass);
        this.entityManager = entityManager;
    }
}
