package br.com.simpou.pub.commons.persistence.dao.impl;

import br.com.simpou.pub.commons.model.entity.IdentifiableEntity;
import br.com.simpou.pub.commons.persistence.common.Cache;

import java.io.Serializable;


/**
 * Não realiza controle de cache.
 *
 * @author Jonas Pereira
 * @since 2013-12-15
 */
public class DummyCache implements Cache {
    @Override
    public <T extends IdentifiableEntity<?>> void evict(final Class<T> entityClass, final Serializable identity) {

    }

    @Override
    public void evictAll() {

    }
}
