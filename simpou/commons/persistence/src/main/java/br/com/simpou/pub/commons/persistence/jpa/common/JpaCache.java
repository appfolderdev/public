package br.com.simpou.pub.commons.persistence.jpa.common;

import br.com.simpou.pub.commons.model.entity.IdentifiableEntity;
import br.com.simpou.pub.commons.persistence.common.Cache;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * Gerenciador de cache da persistência jpa.
 *
 * @author Jonas Pereira
 * @since 2013-12-14
 */
public class JpaCache implements Cache {

    private final javax.persistence.Cache cache;

    public JpaCache(EntityManager entityManager) {
        this.cache = entityManager.getEntityManagerFactory().getCache();
    }

    @Override
    public <T extends IdentifiableEntity<?>> void evict(final Class<T> entityClass, final Serializable identity) {
        if(cache.contains(entityClass, identity)){
            cache.evict(entityClass, identity);
        }
    }

    @Override
    public void evictAll() {
        cache.evictAll();
    }
}
