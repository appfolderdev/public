package br.com.simpou.pub.commons.persistence.dao.impl;

import br.com.simpou.pub.commons.persistence.common.Transaction;

/**
 * Não realiza controle de transações.
 *
 * @author Jonas Pereira
 * @since 2013-06-01
 * @version 2013-06-01
 */
public class DummyTransaction implements Transaction {
    @Override
    public void begin() throws IllegalStateException {
    }

    @Override
    public void commit() throws Exception {
    }

    @Override
    public void rollback() {
    }

    @Override
    public boolean isActive() {
        return false;
    }
}
