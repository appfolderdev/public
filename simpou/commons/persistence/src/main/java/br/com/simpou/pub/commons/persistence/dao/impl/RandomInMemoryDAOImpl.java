package br.com.simpou.pub.commons.persistence.dao.impl;

import br.com.simpou.pub.commons.model.entity.BaseEntity;
import br.com.simpou.pub.commons.utils.rand.Randomizable;
import br.com.simpou.pub.commons.utils.rand.Randomizer;
import br.com.simpou.pub.commons.utils.rand.Randoms;

/**
 * Implementação básica de um DAO que "persiste" os dados em memória com dados aleatórios. Os dados existem
 * somente em modo execução.
 *
 * @author Jonas Pereira
 * @since 2012-07-10
 */
public class RandomInMemoryDAOImpl extends InMemoryDAOImpl {

    /**
     * Popula de forma randômica.
     */
    public <T extends BaseEntity & Randomizable> void populate(final Class<T> clasz) throws Exception {
        populate(clasz, Randoms.getList(clasz));
    }

    /**
     * Popula de forma randômica.
     *
     * @param randomizer Regras de geração randômica personalizadas.
     */
    public <T extends BaseEntity> void populate(final Class<T> clasz, Randomizer<T> randomizer) throws Exception {
        populate(clasz, Randoms.getList(clasz, randomizer));
    }
}
