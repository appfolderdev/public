package br.com.simpou.pub.commons.persistence.jpa.common;

import br.com.simpou.pub.commons.persistence.common.Transaction;

import lombok.RequiredArgsConstructor;

import javax.persistence.EntityManager;


/**
 * Transação sobre uma operação JPA DAO.
 *
 * @author Jonas Pereira
 * @since 2013-06-01
 * @version 2013-06-01
 */
@RequiredArgsConstructor
public class JpaTransaction implements Transaction {
    private final EntityManager entityManager;

    @Override
    public void begin() throws IllegalStateException {
        entityManager.getTransaction().begin();
    }

    @Override
    public void commit() throws Exception {
        entityManager.getTransaction().commit();
    }

    @Override
    public void rollback() {
        if (isActive()) {
            entityManager.getTransaction().rollback();
        }
    }

    @Override
    public boolean isActive() {
        return entityManager.getTransaction().isActive();
    }
}
