package br.com.simpou.pub.commons.persistence.jpa.model;

import br.com.simpou.pub.commons.model.entity.BaseEntity;
import br.com.simpou.pub.commons.utils.lang.Assertions;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.metamodel.SingularAttribute;


/**
 * Representa um item de cláusula "where" em instruções SQL que usam a JPA API
 * Criteria. Ex.: SELECT * FROM Table WHERE key=value.
 *
 * @author Jonas Pereira
 * @since 2012-05-24
 * @version 2012-07-16
 */
public class SingularAttributeEntry<X extends BaseEntity, T> {
    /**
     * Chave ou atributo.
     */
    @Getter
    private final SingularAttribute<X, T> key;

    /**
     * Valor do atributo esperado.
     */
    @Getter
    @Setter
    private Object value;

    /**
     * <p>Constructor for SingularAttributeEntry.</p>
     *
     * @param key a {@link javax.persistence.metamodel.SingularAttribute} object.
     */
    public SingularAttributeEntry(final SingularAttribute<X, T> key) {
        this.key = Assertions.notNull(key);
    }

    /**
     * <p>Constructor for SingularAttributeEntry.</p>
     *
     * @param key a {@link javax.persistence.metamodel.SingularAttribute} object.
     * @param value a {@link java.lang.Object} object.
     */
    public SingularAttributeEntry(final SingularAttribute<X, T> key,
        final Object value) {
        this(key);
        this.value = value;
    }
}
