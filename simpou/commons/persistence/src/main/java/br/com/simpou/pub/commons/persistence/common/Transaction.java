package br.com.simpou.pub.commons.persistence.common;


/**
 * Transação sobre uma operação DAO.
 *
 * @author Jonas Pereira
 * @since 2013-06-01
 * @version 2013-06-01
 */
public interface Transaction {
    /**
     * Inicia uma transação.
     *
     * @throws IllegalStateException Se transação está aberta ou ativa.
     */
    void begin() throws IllegalStateException;

    /**
     * Aplica todas operações parciais realizadas dentro da transação iniciada.
     *
     * @throws Exception Se transação não foi aberta ou se ocorreu um erro
     * durante aplicação das ações.
     */
    void commit() throws Exception;

    /**
     * Desfaz e descarta as ações parciais realizadas. Encerra a transação
     * aberta.
     */
    void rollback();

    /**
     * @return true se já existe uma transação iniciada.
     */
    boolean isActive();
}
