package br.com.simpou.pub.commons.persistence.dao;

import br.com.simpou.pub.commons.model.entity.BaseEntity;
import br.com.simpou.pub.commons.utils.pagination.PageLimits;

import java.util.List;


/**
 * DAO com operações baseados em queries nomeadas.
 *
 * @author Jonas Pereira
 * @since 2013-06-01
 * @version 2013-06-01
 */
public interface NamedQueryDAO extends BaseDao, BaseNamedQueryDAO {
    /**
     * Consulta de entidades feita de forma paginada. Evita a manipulação de
     * grandes quantidades de dados desnecessariamente.
     *
     * @param clasz
     *            Classe da entidade de retorno
     * @param namedQuery
     *            NamedQuery definida na entidade. Os parâmetros devem ser
     *            sinalizados com "?" seguido de seu índice começando em 1.
     * @param params
     *            Parâmetros da query.
     * @param limits
     *            Limitação da busca. Null para obter todos.
     * @return Entidade.
     * @throws java.lang.Exception
     *             Se houver alguma.
     */
    public <T extends BaseEntity> List<T> getNamedList(final Class<T> clasz,
        final String namedQuery, final PageLimits limits, final Object... params)
        throws Exception;

    /**
     * Obtém uma entidade persistida que seja unicamente identificável.
     *
     * @param clasz
     *            Classe da entidade de retorno
     * @param namedQuery
     *            NamedQuery definida na entidade. Os parâmetros devem ser
     *            sinalizados com "?" seguido de seu índice começando em 1.
     * @param params
     *            Parâmetros da query.
     * @return Entidade.
     * @throws java.lang.Exception
     *             Se houver alguma.
     */
    public <T extends BaseEntity> T getNamedSingle(final Class<T> clasz,
        final String namedQuery, final Object... params)
        throws Exception;
}
