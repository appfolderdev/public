package br.com.simpou.pub.commons.persistence.jpa.helper;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;


/**
 * Operações úteis sobre o tratamento exceções lançadas durante  persistência
 * de entidades.
 *
 * @author Jonas Pereira
 * @since 2012-05-24
 * @version 2012-07-16
 */
public class JpaExceptionHelper {
    /**
     * <p>getConstraintViolationMessage.</p>
     *
     * @param ex a {@link javax.validation.ConstraintViolationException} object.
     * @return Mensagem formatada dos erros de validação ocorridos durante tentativa de persistência de uma entidade.
     */
    public static String toString(final ConstraintViolationException ex) {
        Set<ConstraintViolation<?>> v = ex.getConstraintViolations();
        StringBuilder msg = new StringBuilder();

        for (ConstraintViolation<?> cv : v) {
            msg.append("Constraint error: ").append(cv.toString());
            msg.append("Constraint invalid value: ").append(cv.getInvalidValue());
        }

        return msg.toString();
    }
}
