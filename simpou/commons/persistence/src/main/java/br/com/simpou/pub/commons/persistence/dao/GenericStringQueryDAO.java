package br.com.simpou.pub.commons.persistence.dao;

import br.com.simpou.pub.commons.model.entity.BaseEntity;
import br.com.simpou.pub.commons.utils.pagination.PageLimits;
import br.com.simpou.pub.commons.utils.pagination.Paginator;

import java.util.List;


/**
 * DAO com operações baseados em string queries tipadas.
 *
 * @author Jonas Pereira
 * @since 2013-06-02
 * @version 2013-06-02
 */
public interface GenericStringQueryDAO<T extends BaseEntity>
    extends StringQueryDAO {
    /**
     * Consulta de objetos feita de forma paginada. Evita a manipulação de
     * grandes quantidades de dados desnecessariamente.
     *
     * @see Paginator Veja para auxílio na operação de paginação.
     * @param stringQuery Query. Os parâmetros devem ser sinalizados com "?"
     * seguido de seu índice começando em 1.
     * @param limits Limitação da busca. Null para obter todos.
     * @param params Parâmetros da query.
     * @return Lista limitada dos elementos retornados pela execução da query.
     * @throws java.lang.Exception Se houver alguma.
     */
    public List<T> getList(final String stringQuery, final PageLimits limits,
        final Object... params) throws Exception;

    /**
     * Obtém um objeto persistido que seja unicamente identificável.
     *
     * @param stringQuery Query. Os parâmetros devem ser sinalizados com "?"
     * seguido de seu índice começando em 1.
     * @param params Parâmetros da query.
     * @return Objeto identificado de forma única.
     * @throws java.lang.Exception Se houver alguma.
     */
    public T getSingle(final String stringQuery, final Object... params)
        throws Exception;
}
