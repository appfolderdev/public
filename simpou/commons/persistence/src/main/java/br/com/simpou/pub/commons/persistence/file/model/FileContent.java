package br.com.simpou.pub.commons.persistence.file.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Conteúdo e localização relativa de um arquivo.
 * 
 * @author Jonas Pereira
 * @since 2013-08-22
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "location")
public class FileContent {

    /**
     * Localização do arquivo.
     */
    private FileLocation location = new FileLocation();
    
    /**
     * Conteúdo do arquivo.
     */
    private byte[] content;

}
