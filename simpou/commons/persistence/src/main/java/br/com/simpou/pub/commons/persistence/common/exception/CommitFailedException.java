package br.com.simpou.pub.commons.persistence.common.exception;

/**
 * @author Jonas Pereira
 * @since 2013-08-22
 */
public class CommitFailedException extends Exception {

    public CommitFailedException(Throwable cause, String msg) {
        super(msg, cause);
    }
    
    public CommitFailedException(Throwable cause) {
        super(cause);
    }
}
