package br.com.simpou.pub.commons.persistence.dao;


/**
 * Define o padrão Data Access Object para persistência entidades.
 *
 * @author Jonas Pereira
 * @since 2013-06-01
 * @version 2013-06-01
 */
public interface ObjectDAO extends BasicDAO, StringQueryDAO, NamedQueryDAO {
}
