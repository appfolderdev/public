package br.com.simpou.pub.commons.persistence.jpa.dao.impl;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.persistence.EntityManager;

/**
 * @author jonas1
 * @version 19/09/13
 * @since 19/09/13
 */
@RequiredArgsConstructor
public class DefaultJpaDAOImpl extends AbstractJpaDAOImpl  {

    @Getter(AccessLevel.PROTECTED)
    private final EntityManager entityManager;

}
