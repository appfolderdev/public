package br.com.simpou.pub.commons.persistence.common.exception;

/**
 * @author Jonas Pereira
 * @since 2013-08-22
 */
public class RollbackFailedException extends RuntimeException {

    public RollbackFailedException(final StackTraceElement[] stackTraces) {
        super("Rollback fail.");
        this.setStackTrace(stackTraces);
    }
}
