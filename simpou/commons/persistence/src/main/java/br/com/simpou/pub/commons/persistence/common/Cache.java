package br.com.simpou.pub.commons.persistence.common;


import br.com.simpou.pub.commons.model.entity.IdentifiableEntity;

import java.io.Serializable;

/**
 * Gerenciador de cache da persistência.
 *
 * @author Jonas Pereira
 * @since 2013-12-14
 */
public interface Cache {

    /**
     * Remove uma entidade do cache de persistência se estiver presente, senão não realiza nada.
     *
     * @param entityClass Tipo da entidade a ser invalidada do cache.
     * @param id          Identidade da entidade.
     */
    public <T extends IdentifiableEntity<?>> void evict(Class<T> entityClass, Serializable id);

    /**
     * Invalida todas entidades do cache de persistência.
     */
    public void evictAll();
}
