package br.com.simpou.pub.commons.persistence.file.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Localização relativa de um arquivo.
 * 
 * @author Jonas Pereira
 * @since 2013-08-22
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"dir", "name"})
@ToString(of = {"dir", "name"})
public class FileLocation {

    /**
     * Diretório relativo de um arquivo.
     */
    private String dir;

    /**
     * Nome do arquivo.
     */
    private String name;

    /**
     * @return Caminho relativo do arquivo.
     */
    public String getPath() {
        return dir + "/" + name;
    }
}
