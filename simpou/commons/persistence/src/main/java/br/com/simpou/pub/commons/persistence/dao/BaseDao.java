package br.com.simpou.pub.commons.persistence.dao;

import br.com.simpou.pub.commons.persistence.common.Cache;
import br.com.simpou.pub.commons.persistence.common.Transaction;

/**
 * Dao base para os demais.
 *
 * @author Jonas Pereira
 * @since 2013-06-01
 * @version 2013-06-01
 */
public interface BaseDao {
    /**
     * Aplica as alterações parciais efetuadas sem de fato persisti-las.
     *
     * @throws java.lang.Exception Se houver alguma.
     */
    void flush() throws Exception;

    /**
     * @return Transação.
     */
    Transaction getTransaction();

    /**
     * @return Cache.
     */
    Cache getCache();
}
