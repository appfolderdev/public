package br.com.simpou.pub.commons.persistence.dao;

import java.util.List;

import br.com.simpou.pub.commons.utils.pagination.PageLimits;
import br.com.simpou.pub.commons.utils.pagination.Paginator;


/**
 * DAO com operações baseados em string queries.
 * 
 * @author Jonas Pereira
 * @since 2012-09-29
 * @version 2013-06-01
 */
public interface StringQueryDAO extends BaseDao {
	/**
	 * Realiza a contagem dos objetos de um determinado tipo.
	 * 
	 * @param stringQuery
	 *            Query. Os parâmetros devem ser sinalizados com "?" seguido de
	 *            seu índice começando em 1.
	 * @param params
	 *            Parâmetros da query.
	 * @return Quantidade total de objetos persistidos.
	 * @throws java.lang.Exception
	 *             Se houver alguma.
	 */
	Long count(final String stringQuery, final Object... params)
			throws Exception;

	/**
	 * Consulta de objetos feita de forma paginada. Evita a manipulação de
	 * grandes quantidades de dados desnecessariamente.
	 * 
	 * @see Paginator Veja para auxílio na operação de paginação.
	 * @param clasz
	 *            Classe dos objetos da lista de retorno
	 * @param stringQuery
	 *            Query. Os parâmetros devem ser sinalizados com "?" seguido de
	 *            seu índice começando em 1.
	 * @param limits
	 *            Limitação da busca. Null para obter todos.
	 * @param params
	 *            Parâmetros da query.
	 * @return Lista limitada dos elementos retornados pela execução da query.
	 * @throws java.lang.Exception
	 *             Se houver alguma.
	 */
	public <T> List<T> getList(final Class<T> clasz,
			final String stringQuery, final PageLimits limits,
			final Object... params) throws Exception;

	/**
	 * Obtém um objeto persistido que seja unicamente identificável.
	 * 
	 * @param clasz
	 *            Classe do objeto de retorno
	 * @param stringQuery
	 *            Query. Os parâmetros devem ser sinalizados com "?" seguido de
	 *            seu índice começando em 1.
	 * @param params
	 *            Parâmetros da query.
	 * @return Objeto identificado de forma única.
	 * @throws java.lang.Exception
	 *             Se houver alguma.
	 */
	public <T> T getSingle(final Class<T> clasz,
			final String stringQuery, final Object... params)
					throws Exception;

	/**
	 * Executa alterações do tipo UPDATE ou DELETE.
	 * 
	 * @param stringQuery
	 *            Query. Os parâmetros devem ser sinalizados com "?" seguido de
	 *            seu índice começando em 1.
	 * @param params
	 *            Parâmetros da query.
	 * @return Número de tuplas afetadas pelo comando.
	 * @throws java.lang.Exception
	 *             Se houver alguma.
	 */
	int execute(final String stringQuery, final Object... params)
			throws Exception;
}
