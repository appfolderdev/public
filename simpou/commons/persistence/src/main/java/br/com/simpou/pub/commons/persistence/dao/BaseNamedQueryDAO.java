package br.com.simpou.pub.commons.persistence.dao;


/**
 * Base para DAOs baseados em queries nomeadas.
 *
 * @author Jonas Pereira
 * @since 2013-06-01
 * @version 2013-06-01
 */
public interface BaseNamedQueryDAO extends BaseDao {
    /**
     * @param namedQuery NamedQuery definida na entidade. Os parâmetros devem
     * @param params Parâmetros da query.
     * @return Quantidade de items afetados pela query.
     * @throws Exception Se houver.
     */
    int namedExecute(final String namedQuery, final Object... params)
        throws Exception;
}
