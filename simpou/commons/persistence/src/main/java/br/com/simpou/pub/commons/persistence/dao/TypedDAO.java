package br.com.simpou.pub.commons.persistence.dao;

import br.com.simpou.pub.commons.model.entity.BaseEntity;


/**
 * Define o padrão Data Access Object para persistência entidades tipadas.
 *
 * @author Jonas Pereira
 * @since 2013-06-01
 * @version 2013-06-02
 */
public interface TypedDAO<T extends BaseEntity> extends GenericDAO<T>,
    GenericStringQueryDAO<T>, TypedNamedQueryDAO<T> {
}
