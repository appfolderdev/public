package br.com.simpou.pub.commons.persistence.jpa.entity;

import br.com.simpou.pub.commons.model.jpa.entity.AbstractJpaIndexedEntity;
import br.com.simpou.pub.commons.utils.rand.Randomizable;
import br.com.simpou.pub.commons.utils.rand.Randoms;


import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@MappedSuperclass
public abstract class AbstractTestEntity extends AbstractJpaIndexedEntity
        implements Randomizable {

    @NotNull
    @Size(max = 50)
    @Column(length = 50, nullable = false)
    private String name;

    @Id
    @Column(name = "sid")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void fillRandom() {
        setName(Randoms.getString(1, 40, true));
    }
}
