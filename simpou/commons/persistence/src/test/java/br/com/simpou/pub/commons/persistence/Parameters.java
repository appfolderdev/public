package br.com.simpou.pub.commons.persistence;

public interface Parameters {
    String PU_NAME = "persistenceTestsPU";

    /**
     * Caminho relativo do arquivo persistence.xml do projeto de testes.
     */
    String PERSIST_XML_TEST_REL_PATH = "src/test/resources/META-INF/persistence.xml";
    String RESOURCES_TEST_DIR = "src/test/resources/";
}
