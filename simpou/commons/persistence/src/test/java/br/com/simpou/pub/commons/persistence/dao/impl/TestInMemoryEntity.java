package br.com.simpou.pub.commons.persistence.dao.impl;

import br.com.simpou.pub.commons.utils.lang.annot.RandomFillableConfig;
import br.com.simpou.pub.commons.model.jpa.entity.AbstractJpaIndexedEntity;
import br.com.simpou.pub.commons.utils.rand.Randomizable;
import br.com.simpou.pub.commons.utils.rand.Randoms;

import lombok.Getter;
import lombok.Setter;


@RandomFillableConfig(maxLength = 3, minLength = 2)
public class TestInMemoryEntity extends AbstractJpaIndexedEntity implements Randomizable {
    @Getter
    @Setter
    private String name;

    @Override
    public void fillRandom() {
        name = Randoms.getString(1, 5, true);
    }
}
