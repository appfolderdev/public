package br.com.simpou.pub.commons.persistence.jpa.entity;

import javax.persistence.*;


@NamedQueries({@NamedQuery(name = "findAll",query = "SELECT c FROM TestDaoEntity c")
    , @NamedQuery(name = "findByName",query = "SELECT c FROM TestDaoEntity c WHERE c.name=?1")
    , @NamedQuery(name = "updateName",query = "UPDATE TestDaoEntity c SET c.name=?1 WHERE c.name=?2")
    , })
@Entity
public class TestDaoEntity extends AbstractTestEntity {
   
   
}
