package br.com.simpou.pub.commons.persistence.jpa.dao.impl;

import br.com.simpou.pub.commons.model.jpa.entity.AbstractJpaIndexedEntity;
import br.com.simpou.pub.commons.persistence.Parameters;
import br.com.simpou.pub.commons.persistence.common.Transaction;
import br.com.simpou.pub.commons.persistence.jpa.entity.TestDaoEntity;
import br.com.simpou.pub.commons.persistence.jpa.entity.TestDaoEntity_;
import br.com.simpou.pub.commons.persistence.jpa.model.SingularAttributeEntry;
import br.com.simpou.pub.commons.utils.lang.Casts;
import br.com.simpou.pub.commons.utils.pagination.PageLimits;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.Repeat;
import br.com.simpou.pub.commons.utils.tests.annot.TestOrder;
import lombok.Getter;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.metamodel.SingularAttribute;
import java.util.ArrayList;
import java.util.List;

import static br.com.simpou.pub.commons.utils.rand.Randoms.*;
import static org.junit.Assert.*;

@ClassTest(AbstractTypedJpaDAOImpl.class)
@Ignore
public class AbstractTypedJpaDAOImplITCase {
    private static final TestJpaDAOImpl dao = new TestJpaDAOImpl();

    private static final List<TestDaoEntity> entities = new ArrayList<>();

    private static void checkName(final List<TestDaoEntity> list, final String name) {
        for (final TestDaoEntity entity : list) {
            assertEquals(name, entity.getName());
        }
    }

    private static TestDaoEntity getRandomEntityUpdated(final TestDaoEntity entity) {
        final Long id = entity.getId();
        entity.fillRandom();
        entity.setId(id);

        return entity;
    }

    @Test
    @TestOrder(1)
    @Repeat(10)
    public static void testCreate() throws Exception {
        final TestDaoEntity entity = new TestDaoEntity();
        entity.fillRandom();
        dao.create(entity);
        entities.add(entity);
    }

    @Test(expected = EntityNotFoundException.class)
    @TestOrder(6)
    public static void testDelete_Serializable_error() throws Exception {
        dao.delete(TestDaoEntity.class, 1L);
    }

    @Before
    public void setUp() {
        dao.getTransaction().begin();
    }

    @After
    public void tearDown() throws Exception {
        final Transaction transaction = dao.getTransaction();

        if (transaction.isActive()) {
            transaction.commit();
        }
    }

    @Test
    @TestOrder(2)
    public void testCount_0args() throws Exception {
        final Long count = dao.count();
        assertEquals(entities.size(), count.intValue());
    }

    @Test
    public void testCount_List_List() throws Exception {
        //testGetList_String_ObjectArr
    }

    @Test
    public void testCount_String_ObjectArr() throws Exception {
        //testGetList_String_ObjectArr
    }

    @Test
    @TestOrder(5)
    public void testDeleteAll() throws Exception {
        dao.deleteAll();

        final Long count = dao.count();
        assertEquals(0, count.intValue());
    }

    @Test
    @TestOrder(4)
    public void testDelete_GenericType() throws Exception {
        TestDaoEntity entityRet;
        final TestDaoEntity entity = getRandomEntity();
        final Long id = entity.getId();
        entityRet = dao.getSingle(TestDaoEntity.class, id);
        assertNotNull(entityRet);
        dao.delete(entity);
        entityRet = dao.getSingle(TestDaoEntity.class, id);
        assertNull(entityRet);
        entities.remove(entity);
    }

    @Test
    @TestOrder(4)
    public void testDelete_Serializable() throws Exception {
        TestDaoEntity entityRet;
        final TestDaoEntity entity = getRandomEntity();
        final Long id = entity.getId();
        entityRet = dao.getSingle(TestDaoEntity.class, id);
        assertNotNull(entityRet);
        dao.delete(TestDaoEntity.class, id);
        entityRet = dao.getSingle(TestDaoEntity.class, id);
        assertNull(entityRet);
        entities.remove(entity);
    }

    @Test
    @TestOrder(3)
    public void testExecute() throws Exception {
        final TestDaoEntity entity = getRandomEntity();
        final String oldName = entity.getName();
        final String newName = oldName + "new";
        final String className = TestDaoEntity.class.getSimpleName();
        final Long id = entity.getId();
        final String queryUpdate = "UPDATE " + className +
                " e SET e.name=?1 WHERE e.id=?2";
        final String queryRetrieve = "SELECT e FROM " + className +
                " e WHERE e.id=?1";
        final int result = dao.execute(queryUpdate, newName, id);
        assertEquals(1, result);

        final TestDaoEntity entityRet = dao.getSingle(queryRetrieve, id);
        assertEquals(newName, entityRet.getName());
    }

    @Test
    public void testFlush() {
        //testUpdate
    }

    @Test
    public void testGetCache() {
        //TODO
    }

    @Test
    @TestOrder(2)
    public void testGetList_0args() throws Exception {
        final List<TestDaoEntity> list = dao.getList();

        for (final TestDaoEntity entity : entities) {
            assertTrue(list.contains(entity));
        }
    }

    @Test
    public void testGetList_3args_1() throws Exception {
        //testGetList_String_ObjectArr
    }

    @Test
    public void testGetList_3args_2() throws Exception {
        //testGetList_String_ObjectArr
    }

    @Test
    @TestOrder(3)
    public void testGetList_PageLimits() throws Exception {
        final List<TestDaoEntity> listAll = dao.getList();
        final PageLimits limits = getLimits(listAll.size());
        final List<TestDaoEntity> listLimits = dao.getList(limits);
        assertEquals(limits.getSize(), listLimits.size());

        for (int i = limits.getFirst(), j = 0; i < (limits.getLast() + 1);
             i++, j++) {
            assertEquals(listAll.get(i), listLimits.get(j));
        }
    }

    @Test
    @TestOrder(3)
    public void testGetList_String_ObjectArr() throws Exception {
        Long count;
        List<TestDaoEntity> list;
        final int countExp = 3;
        final String className = TestDaoEntity.class.getSimpleName();
        final TestDaoEntity entity1 = entities.get(0);
        final TestDaoEntity entity2 = entities.get(1);
        final TestDaoEntity entity3 = entities.get(2);
        final String newNameSuffix = "**NewName**";
        final String newName = getString(10, 20, true) + newNameSuffix;
        entity1.setName(newName);
        entity2.setName(newName);
        entity3.setName(newName);
        dao.update(entity1);
        dao.update(entity2);
        dao.update(entity3);

        final List<SingularAttributeEntry<TestDaoEntity, ?>> listAttbEqual = new ArrayList<>();
        listAttbEqual.add(new SingularAttributeEntry<>(
                TestDaoEntity_.name, newName));

        final List<SingularAttributeEntry<TestDaoEntity, String>> listAttbLike = new ArrayList<>();
        listAttbLike.add(new SingularAttributeEntry<>(
                TestDaoEntity_.name, newNameSuffix));

        final String selectQuery = "SELECT e FROM " + className +
                " e WHERE e.name=?1";
        final PageLimits limits = getLimits(countExp);

        // list all
        list = dao.getList(selectQuery, null, newName);
        assertEquals(countExp, list.size());
        checkName(list, newName);

        // limit list
        list = dao.getList(selectQuery, limits, newName);
        assertEquals(limits.getSize(), list.size());
        checkName(list, newName);

        // list attbs equal
        list = dao.getList(listAttbEqual, null);
        assertEquals(countExp, list.size());
        checkName(list, newName);

        // list attbs like
        list = dao.getList(null, listAttbLike);
        assertEquals(countExp, list.size());
        checkName(list, newName);

        // count attbs
        count = dao.count(listAttbEqual, null);
        assertEquals(countExp, count.intValue());

        // limit list attbs
        list = dao.getList(limits, listAttbEqual, null);
        assertEquals(limits.getSize(), list.size());
        checkName(list, newName);

        // count
        count = dao.count("SELECT count(e) FROM " + className +
                " e WHERE e.name=?1", newName);
        assertEquals(countExp, count.intValue());
    }

    @Test
    @TestOrder(2)
    public void testGetNamedList() throws Exception {
        final List<TestDaoEntity> list = dao.getNamedList("findAll", null);

        for (final TestDaoEntity entity : entities) {
            assertTrue(list.contains(entity));
        }
    }

    @Test
    @TestOrder(3)
    public void testGetNamedSingle() throws Exception {
        //testNamedExecute
    }

    @Test
    @TestOrder(3)
    public void testGetSingle_List_List() throws Exception {
        final TestDaoEntity entity = getRandomEntity();
        final Long id = entity.getId();
        final List<SingularAttributeEntry<TestDaoEntity, ?>> listAttbEqual = new ArrayList<>();

        // sem RAW não funciona, dado que "id" pertence à super classe.
        final SingularAttribute<AbstractJpaIndexedEntity, Long> singularAttribute = TestDaoEntity_.id;
        final SingularAttribute<TestDaoEntity, Long> castSingularAttribute =
                Casts.simpleCast(singularAttribute);
        final SingularAttributeEntry<TestDaoEntity, Long> singAttbEntry =
                new SingularAttributeEntry<>(castSingularAttribute);
        singAttbEntry.setValue(id);
        listAttbEqual.add(singAttbEntry);

        final TestDaoEntity entityRet = dao.getSingle(listAttbEqual, null);
        assertNotNull(entityRet);
        assertEquals(id, entityRet.getId());
    }

    @Test
    public void testGetSingle_Serializable() throws Exception {
        //testUpdate
    }

    @Test
    @TestOrder(6)
    public void testGetSingle_Serializable_noResult() throws Exception {
        final TestDaoEntity entity = dao.getSingle("SELECT e FROM " +
                TestDaoEntity.class.getSimpleName() + " e WHERE e.id=?1", 1L);
        assertNull(entity);
    }

    @Test
    public void testGetSingle_String_ObjectArr() throws Exception {
        //testExecute
    }

    @Test
    public void testGetTransaction() throws Exception {
        //setUp
        final TestDaoEntity entity = new TestDaoEntity();
        entity.fillRandom();
        assertNull(entity.identity());
        dao.create(entity);
        dao.flush();
        assertNotNull(entity.identity());
        dao.getTransaction().rollback();
        assertNull(dao.getSingle(TestDaoEntity.class, entity.identity()));
        dao.getTransaction().rollback(); //transação não ativa não gera erros
    }

    @Test
    @TestOrder(3)
    public void testNamedExecute() throws Exception {
        final TestDaoEntity entity = getRandomEntity();
        final String oldName = entity.getName();
        final String newName = oldName + "new";
        final Long id = entity.getId();
        final int result = dao.namedExecute("updateName", newName, oldName);
        assertEquals(1, result);

        final TestDaoEntity entityRet = dao.getNamedSingle("findByName", newName);
        assertEquals(entity, entityRet);
    }

    @Test
    @TestOrder(3)
    public void testUpdate() throws Exception {
        final TestDaoEntity randomEntity = getRandomEntity();
        final String oldName = randomEntity.getName();
        final String newName = oldName + "new";
        randomEntity.setName(newName);
        dao.update(randomEntity);
        dao.flush();

        final TestDaoEntity entityRet = dao.getSingle(TestDaoEntity.class,
                randomEntity.getId());
        assertEquals(newName, entityRet.getName());
    }

    private TestDaoEntity getRandomEntity() {
        return entities.get(getInteger(0, entities.size() - 1));
    }

    private TestDaoEntity getRandomEntityUpdated() {
        final TestDaoEntity entity = getRandomEntity();

        return getRandomEntityUpdated(entity);
    }

    @Getter
    static class TestJpaDAOImpl extends AbstractTypedJpaDAOImpl<TestDaoEntity> {
        private final EntityManager entityManager;

        public TestJpaDAOImpl() {
            super(TestDaoEntity.class);
            this.entityManager = Persistence.createEntityManagerFactory(Parameters.PU_NAME)
                    .createEntityManager();
        }

        @Override
        public EntityManager getEntityManager() {
            return this.entityManager;
        }
    }
}
