package br.com.simpou.pub.commons.persistence.file;

import br.com.simpou.pub.commons.persistence.common.Transaction;
import br.com.simpou.pub.commons.persistence.file.model.FileContent;
import br.com.simpou.pub.commons.persistence.file.model.FileLocation;
import br.com.simpou.pub.commons.utils.file.FileHelper;
import br.com.simpou.pub.commons.utils.file.PropertyHelper;
import br.com.simpou.pub.commons.utils.rand.Randomizer;
import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.ExtraTest;
import br.com.simpou.pub.commons.utils.tests.annot.TestOrder;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

@ClassTest(TransactedFileManager.class)
@Ignore
public class TransactedFileManagerTest {

    private static final Randomizer<FileContent> xmlDataRandmomizer = new Randomizer<FileContent>() {

        @Override
        public void fillRandom(final FileContent data) {
            data.setContent(Randoms.getString().getBytes());
            data.getLocation().setDir(Randoms.getString());
            data.getLocation().setName(Randoms.getString());
        }

    };

    private static TransactedFileManager dao;

    private static File baseFile;

    private static FileContent xmlData;

    private static FileContent xmlData2;

    private static Transaction tx;

    @BeforeClass
    public static void setUpClass() {
        baseFile = new File(PropertyHelper.getTempDir(), Randoms.getSHA256());
        dao = new TransactedFileManager(baseFile);
        tx = dao.getTransaction();
        {
            xmlData = Randoms.getSingle(FileContent.class, xmlDataRandmomizer);
            xmlData2 = Randoms.getSingle(FileContent.class, xmlDataRandmomizer);
            xmlData2.getLocation().setDir(xmlData.getLocation().getDir());
        }
    }

    @Test
    @TestOrder(1)
    public static void testWrite() throws Exception {
        tx.begin();
        dao.write(xmlData);
        dao.write(xmlData2);
        tx.commit();
    }

    @Test
    @TestOrder(3)
    public void testDelete() throws Exception {
        tx = dao.newTransaction();
        try {
            tx.begin();
            dao.delete(xmlData.getLocation());
            tx.commit();
            final FileContent read = dao.read(xmlData.getLocation());
            assertNull(read);
        } finally {
            FileHelper.delete(baseFile.getAbsolutePath());
        }
        assertFalse(baseFile.exists());
    }

    @Test
    public void testGetTransaction() {
        //setUpClass
    }

    @Test
    @TestOrder(2)
    public void testList() throws Exception {
        final FileLocation query = new FileLocation(xmlData.getLocation().getDir(), null);
        final List<FileLocation> list = dao.list(query);
        assertTrue(list.contains(xmlData.getLocation()));
        assertTrue(list.contains(xmlData2.getLocation()));
    }

    @Test
    public void testNewTransaction() {
        //testRollback
    }

    @Test
    @TestOrder(2)
    public void testRead() throws Exception {
        final FileContent read = dao.read(xmlData.getLocation());
        assertEquals(new String(xmlData.getContent()), new String(read.getContent()));
    }

    @Test
    @ExtraTest
    @TestOrder(2)
    public void testRollback() throws Exception {
        final FileContent xmlData1 = Randoms.getSingle(FileContent.class, xmlDataRandmomizer);
        final FileContent xmlData4 = Randoms.getSingle(FileContent.class, xmlDataRandmomizer);
        final FileContent xmlData3 = Randoms.getSingle(FileContent.class, xmlDataRandmomizer);
        final FileContent xmlData5 = Randoms.getSingle(FileContent.class, xmlDataRandmomizer);
        System.out.println(xmlData1.getLocation());
        System.out.println(xmlData4.getLocation());
        System.out.println(xmlData3.getLocation());
        System.out.println(xmlData2.getLocation());
        final Transaction transaction = dao.newTransaction();
        transaction.begin();
        dao.write(xmlData1);//anulada
        dao.write(xmlData4);//ok
        dao.delete(xmlData1.getLocation());//anulada
        dao.delete(xmlData2.getLocation());//ok
        dao.delete(xmlData3.getLocation());//ops: arquivo não existe, rollback
        dao.write(xmlData5);// não chega a executar
        try {
            transaction.commit();
            fail();
        } catch (final Exception exception) {
        }
        assertNull(dao.read(xmlData1.getLocation()));
        assertNull(dao.read(xmlData4.getLocation()));
        assertNull(dao.read(xmlData3.getLocation()));
        assertNotNull(dao.read(xmlData2.getLocation()));
    }
}
