package br.com.simpou.pub.commons.persistence.jpa.helper;

import br.com.simpou.pub.commons.persistence.jpa.entity.AbstractTestEntity;
import br.com.simpou.pub.commons.utils.rand.Randoms;

import javax.persistence.*;


@Entity
public class TestEmHelperEntity extends AbstractTestEntity {
    @Column(nullable = false)
    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public void fillRandom() {
        super.fillRandom();
        desc = Randoms.getString(1, 5, true);
    }

    public void fillRandomInvalid() {
        setName(null);
        setDesc(null);
    }
}
