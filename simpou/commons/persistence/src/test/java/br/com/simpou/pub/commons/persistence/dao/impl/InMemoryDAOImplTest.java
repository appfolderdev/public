package br.com.simpou.pub.commons.persistence.dao.impl;

import br.com.simpou.pub.commons.utils.pagination.PageLimits;
import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.Repeat;
import br.com.simpou.pub.commons.utils.tests.annot.TestOrder;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

@Ignore
@ClassTest(InMemoryDAOImpl.class)
public class InMemoryDAOImplTest {
    private static final Class<TestInMemoryEntity> clasz = TestInMemoryEntity.class;

    private static InMemoryDAOImpl dao;

    private static int count = 0;

    @BeforeClass
    public static void setUpClass() throws Exception {
        dao = new InMemoryDAOImpl();
    }

    @Test(expected = UnsupportedOperationException.class)
    public static void testCount_String_ObjectArr() throws Exception {
        dao.count("");
    }

    @Test(expected = UnsupportedOperationException.class)
    public static void testExecute() throws Exception {
        dao.execute(null);
    }

    @Test(expected = UnsupportedOperationException.class)
    public static void testGetList_4args() throws Exception {
        dao.getList(clasz, "", null);
    }

    @Test(expected = UnsupportedOperationException.class)
    public static void testGetNamedList() throws Exception {
        dao.getNamedList(null, null, null);
    }

    @Test(expected = UnsupportedOperationException.class)
    public static void testGetNamedSingle() throws Exception {
        dao.getNamedSingle(null, null);
    }

    @Test
    public static void testGetSingle_3args() throws Exception {
        dao.getSingle(clasz, -1L);
    }

    @Test(expected = UnsupportedOperationException.class)
    public static void testNamedExecute() throws Exception {
        dao.namedExecute(null);
    }

    @Test
    @TestOrder(2)
    public void testCount_Class() throws Exception {
        assertEquals(10, dao.count(clasz).intValue());
    }

    @Test
    @TestOrder(1)
    @Repeat(10)
    public void testCreate() throws Exception {
        final TestInMemoryEntity entity = new TestInMemoryEntity();
        entity.fillRandom();
        entity.setId(new Long(count));

        dao.getTransaction().begin(); //dummy

        final TestInMemoryEntity created = dao.create(entity);
        dao.flush(); //dummy
        assertNotNull(created);
        assertEquals(++count, dao.count(clasz).intValue());
        dao.getTransaction().commit(); //dummy
        dao.getTransaction().rollback(); //dummy
    }

    @Test
    @TestOrder(4)
    public void testDeleteAll() throws Exception {
        dao.deleteAll(clasz);
        assertEquals(0, dao.count(clasz).intValue());
    }

    @Test
    public void testDelete_BaseEntity() throws Exception {
        //testDelete_Class_Serializable
    }

    @Test
    @TestOrder(3)
    public void testDelete_Class_Serializable() throws Exception {
        final TestInMemoryEntity entity = new TestInMemoryEntity();
        entity.setId(1L);
        dao.delete(TestInMemoryEntity.class, entity.identity());
        assertEquals(9, dao.count(clasz).intValue());
        assertFalse(dao.getList(clasz).contains(entity));
    }

    @Test
    public void testFlush() throws Exception {
    }

    @Test
    public void testGetCache() {
    }

    @Test
    @TestOrder(2)
    public void testGetList_Class() throws Exception {
        final List<TestInMemoryEntity> list = dao.getList(clasz);
        assertEquals(10, list.size());
    }

    @Test
    @TestOrder(2)
    public void testGetList_Class_PageLimits() throws Exception {
        final PageLimits limits = new PageLimits(3, 2);
        final List<TestInMemoryEntity> list = dao.getList(clasz, limits);
        assertEquals(2, list.size());
        assertEquals(3, list.get(0).getId().intValue());
    }

    @Test
    @TestOrder(2)
    public void testGetSingle_Class_Serializable() throws Exception {
        final Long id = 1L;
        final TestInMemoryEntity entity = dao.getSingle(clasz, id);
        assertEquals(id, entity.identity());
    }

    @Test
    public void testGetTransaction() {
    }

    /**
     * @see InMemoryDAOImpl#populate(Class, List)
     */
    @Test
    @TestOrder(5)
    public void testPopulate() throws Exception {
        assertEquals(0, dao.count(clasz).intValue());
        dao.populate(TestInMemoryEntity.class, Randoms.getList(TestInMemoryEntity.class, 2, 2));
        assertEquals(2, dao.count(clasz).intValue());
        dao.deleteAll(TestInMemoryEntity.class);
    }

    @Test
    public void testRefresh() {
    }

    @Test
    @TestOrder(2)
    public void testUpdate() throws Exception {
        final TestInMemoryEntity entity = new TestInMemoryEntity();
        entity.fillRandom();
        entity.setId(1L);
        dao.update(entity);
        testCount_Class();
    }
}
