package br.com.simpou.pub.commons.persistence.jpa.entity;

import br.com.simpou.pub.commons.model.jpa.entity.AbstractJpaIndexedEntity_;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;


@StaticMetamodel(AbstractTestEntity.class)
public abstract class AbstractTestEntity_ extends AbstractJpaIndexedEntity_ {
    public static volatile SingularAttribute<TestDaoEntity, String> name;
}
