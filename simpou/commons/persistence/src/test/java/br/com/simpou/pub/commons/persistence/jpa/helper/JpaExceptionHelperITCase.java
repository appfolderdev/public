package br.com.simpou.pub.commons.persistence.jpa.helper;

import br.com.simpou.pub.commons.persistence.Parameters;
import br.com.simpou.pub.commons.persistence.dao.TypedDAO;
import br.com.simpou.pub.commons.persistence.jpa.dao.impl.AbstractTypedJpaDAOImpl;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Ignore;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.validation.ConstraintViolationException;

import static org.junit.Assert.assertNotNull;

@ClassTest(JpaExceptionHelper.class)
@Ignore
public class JpaExceptionHelperITCase {
    @Test
    public void testToString() throws Exception {
        final TypedDAO<TestEmHelperEntity> dao = new AbstractTypedJpaDAOImpl<TestEmHelperEntity>(TestEmHelperEntity.class) {
            final EntityManager em = Persistence.createEntityManagerFactory(Parameters.PU_NAME)
                    .createEntityManager();

            @Override
            protected EntityManager getEntityManager() {
                return this.em;
            }
        };

        final TestEmHelperEntity entity = new TestEmHelperEntity();
        entity.fillRandomInvalid();
        dao.getTransaction().begin();

        try {
            dao.create(entity);
        } catch (final ConstraintViolationException ex) {
            final String message = JpaExceptionHelper.toString(ex);
            assertNotNull(message);
        }

        dao.getTransaction().rollback();
    }
}
