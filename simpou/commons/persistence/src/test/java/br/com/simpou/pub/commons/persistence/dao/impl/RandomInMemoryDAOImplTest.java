package br.com.simpou.pub.commons.persistence.dao.impl;

import br.com.simpou.pub.commons.utils.lang.annot.RandomFillableConfig;
import br.com.simpou.pub.commons.utils.rand.Randomizer;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.ExtraTest;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

@ClassTest(RandomInMemoryDAOImpl.class)
public class RandomInMemoryDAOImplTest {

    @Test
    @ExtraTest
    public void testNewInstance() throws Exception {
        final Class<TestInMemoryEntity> clasz = TestInMemoryEntity.class;
        final RandomInMemoryDAOImpl dao = new RandomInMemoryDAOImpl();
        dao.populate(clasz);
        final int count = dao.count(clasz).intValue();
        assertTrue((count > 1) && (count < 4));

        final Class<TestInMemoryEntity2> clasz2 = TestInMemoryEntity2.class;
        final RandomInMemoryDAOImpl dao2 = new RandomInMemoryDAOImpl();
        dao2.populate(clasz2);
        final int count2 = dao2.count(clasz2).intValue();
        assertTrue((count2 > (RandomFillableConfig.MIN_LENGTH - 1)) &&
                (count2 < (RandomFillableConfig.MAX_LENGTH + 1)));
    }

    /**
     * @see RandomInMemoryDAOImpl#populate(Class)
     */
    @Test
    public void testPopulate_class() throws Exception {
        //testNewInstance andre
    }

    /**
     * @see RandomInMemoryDAOImpl#populate(Class, Randomizer)
     */
    @Test
    public void testPopulate_class_randomizer() throws Exception {
    }

}
