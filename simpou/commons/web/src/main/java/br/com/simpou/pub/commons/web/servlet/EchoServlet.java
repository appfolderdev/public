package br.com.simpou.pub.commons.web.servlet;

import br.com.simpou.pub.commons.utils.lang.IOHelper;
import lombok.extern.apachecommons.CommonsLog;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@CommonsLog
public class EchoServlet extends HttpServlet {

    private String redirectUrl;

    @Override
    protected void doDelete(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPut(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    public void init(final ServletConfig config) throws ServletException {
        super.init(config);
        this.redirectUrl = getServletConfig().getInitParameter("redirectUrl");
    }

    private void process(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        log.info("Method: " + req.getMethod());
        log.info("IP: " + req.getRemoteAddr());
        log.info("QueryString: " + req.getQueryString());
        final Map<String, String[]> parameterMap = req.getParameterMap();
        for (final Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            log.info("param: " + entry.getKey() + "=" + entry.getValue()[0]);
        }
        final Map<String, List<String>> headersMap = Servlets.headersFrom(req);
        for (final Map.Entry<String, List<String>> entry : headersMap.entrySet()) {
            log.info("header: " + entry.getKey() + "=" + entry.getValue().get(0));
        }
        try {
            final String body = IOHelper.toString(req.getInputStream());
            log.info("Body: " + body);
        } catch (final Exception e) {
            log.info("Erro ao ler body: " + e.getMessage());
        }
        resp.sendRedirect(this.redirectUrl);
    }
}
