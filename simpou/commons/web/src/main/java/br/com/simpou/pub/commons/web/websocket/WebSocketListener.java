package br.com.simpou.pub.commons.web.websocket;

/**
 * Manipulador para tratamento de mensagens recebidas pelo servidor. Usado para registrar assinantes aptos
 * a receberem mensagens via websocket.
 *
 * @author Jonas Pereira
 * @since 20/07/2017
 */
public interface WebSocketListener {

    /**
     * Ponto de entrada para mensagens. Recebimento é assíncrono, emissor não fica bloqueado aguardando o
     * tratamento da mensagem pelo receptor. Considerar possibilidade de sincronização de threads. São
     * recebidas as mensagens de todos os clientes, internamente deve ser realizada a filtragem segundo o
     * id do emissor.
     */
    void onMessage(WebSocketMessage message);

}
