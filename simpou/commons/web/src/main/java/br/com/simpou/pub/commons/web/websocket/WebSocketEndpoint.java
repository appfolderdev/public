package br.com.simpou.pub.commons.web.websocket;


/**
 * Endpoint padrão para operações com websocket. Deve ser o ponto único de acesso à essa API.
 *
 * @author Jonas Pereira
 * @since 19/07/2017
 */
public interface WebSocketEndpoint {

    /**
     * Envia mensagens. Método assíncrono.
     *
     * @param message Conteúdo.
     * @param target  Destinatário.
     */
    void sendMessage(WebSocketMessage message, ClientLocation target);

    /**
     * Ponto de entrada para recebimento de mensagens de clientes conectados ao servidor websocket.
     *
     * @param listener Manipulador das mensagens de entrada. Sempre que alguma mensagem chega, o
     *                 manipulador é executado de maneira assíncrona.
     */
    void addMessageListener(WebSocketListener listener);
}
