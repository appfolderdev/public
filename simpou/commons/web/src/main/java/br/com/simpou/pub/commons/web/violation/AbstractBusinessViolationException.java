package br.com.simpou.pub.commons.web.violation;

import br.com.simpou.pub.commons.utils.violation.BusinessViolation;

/**
 * Base para geração de erro de negócio com status HTTP associado.
 *
 * @author Jonas Pereira
 * @since 2014-03-31
 */
public abstract class AbstractBusinessViolationException extends ViolationsException {

    /**
     * Define status e gera violação de negócio.
     *
     * @param status Código de status w3c http.
     * @param code   Código da mensagem internacionalizada no arquivo "RestMessages.properties".
     * @param params Parametrização da mensagem internacionalizada.
     */
    protected AbstractBusinessViolationException(final int status, final String code, final Object... params) {
        super(status, new BusinessViolation(code, params));
    }
}
