package br.com.simpou.pub.commons.web.exception;

import br.com.simpou.pub.commons.model.entity.rest.RestEntity;
import br.com.simpou.pub.commons.web.http.StatusHttp;
import br.com.simpou.pub.commons.web.violation.AbstractBusinessViolationException;

public class EntityRequiredException extends AbstractBusinessViolationException {

    public EntityRequiredException(final RestEntity<?> entity) {
        super(StatusHttp.BAD_REQUEST.value(), "commons.business.entityRequired", entity.typeName());
    }
}
