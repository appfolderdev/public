package br.com.simpou.pub.commons.web.model;

import br.com.simpou.pub.commons.model.cache.Cacheable;
import br.com.simpou.pub.commons.model.mixin.BaseEntityMixin;
import br.com.simpou.pub.commons.model.serialize.ResponseObject;
import br.com.simpou.pub.commons.model.view.GlobalView;
import br.com.simpou.pub.commons.utils.lang.Nulls;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by jonas on 26/01/17.
 */
//@JsonView(GlobalView.class)
@JsonPropertyOrder({"data", "meta"})
@JsonRootName(value = ResponseObject.ROOT_NAME)
@XmlRootElement(name = ResponseObject.ROOT_NAME)
public class ObjectResponse<T> implements Serializable {//TODO pq esse cara não implementa ResponseObject?

    @JsonView(GlobalView.class)
    @JacksonXmlElementWrapper(useWrapping = false)
//    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME/*, property = XML_ITEM_TYPE_NAME*/)
//    @JsonTypeIdResolver(DefaultTypeIdResolver.class)
    private final T data;

    @JsonView(GlobalView.class)
    private final ResponseMetadata meta;

    public ObjectResponse(final T data) {
        this(data, null);
    }

    @JsonCreator
    public ObjectResponse(@JsonProperty("data") final T data,
                          @JsonProperty(BaseEntityMixin.TYPE_NAME) final String type) {
        final Long lastModified;
        if (data instanceof Cacheable) {
            lastModified = Nulls.getTime(((Cacheable) data).lastModified());
        } else {
            lastModified = null;
        }
        this.meta = new ResponseMetadata(null, lastModified, type, true, false);
        this.data = data;
    }

    public ObjectResponse(final T data, final String type, final Date lastModified) {
        this(data, type, lastModified, null);
    }

    public ObjectResponse(final T data,
                          final String type,
                          final Date lastModified,
                          final ResponseMetadata.Pagination pagination) {
        this(data, type, lastModified, true, pagination);
    }

    ObjectResponse(final T data,
                   final String type,
                   final Date lastModified,
                   final boolean success,
                   final ResponseMetadata.Pagination pagination) {
        this.meta = new ResponseMetadata(pagination, Nulls.getTime(lastModified), type, success, pagination != null);
        this.data = data;
    }

    ObjectResponse(final T data, final String type, final Date lastModified, final boolean success) {
        this(data, type, lastModified, success, null);
    }
}
