package br.com.simpou.pub.commons.web.http;

/**
 * Created by jonas.pereira on 02/07/18.
 */
public enum HttpMimeKind {

    TEXT, BINARY, IMAGE, VIDEO, AUDIO;
}
