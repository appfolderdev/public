package br.com.simpou.pub.commons.web.violation;

import br.com.simpou.pub.commons.model.entity.AbstractEntity;
import br.com.simpou.pub.commons.model.serialize.ResponseObject;
import br.com.simpou.pub.commons.model.violation.Violations;
import br.com.simpou.pub.commons.utils.violation.Violation;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.ConstraintViolation;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author Jonas Pereira
 * @since 17/11/13
 */
@Getter
@NoArgsConstructor
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ViolationsWrapper
        extends br.com.simpou.pub.commons.utils.violation.ViolationsWrapper
        implements ResponseObject {

    /**
     * @param constraintViolations conjunto de violações.
     */
    public ViolationsWrapper(final Set<ConstraintViolation<? extends AbstractEntity>> constraintViolations) {
        super(Violations.extractViolations(constraintViolations));
    }

    /**
     * @param violations Lista de Violações.
     */
    public ViolationsWrapper(final List<? extends Violation> violations) {
        super(violations);
    }

    /**
     * @param violations Violações.
     */
    public ViolationsWrapper(final Violation... violations) {
        super(violations);
    }

    @Override
    public Date lastModified() {
        return null;
    }

    @Override
    public String objectTag() {
        return null;
    }
}
