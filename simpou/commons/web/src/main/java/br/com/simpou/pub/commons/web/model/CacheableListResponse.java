package br.com.simpou.pub.commons.web.model;

import br.com.simpou.pub.commons.model.serialize.ResponseObject;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by jonas on 05/02/17.
 */
public class CacheableListResponse<T extends Serializable>
        implements ResponseObject {

    @Getter
    private final List<T> data;

    @Setter
    private Date lastModified;

    private final String objectTag;

    public CacheableListResponse(final List<T> data,
                                 final Date lastModified,
                                 final String objectTag) {
        this.lastModified = lastModified;
        this.objectTag = objectTag;
        this.data = data;
    }

    @Override
    public Date lastModified() {
        return lastModified;
    }

    @Override
    public String objectTag() {
        return objectTag;
    }
}
