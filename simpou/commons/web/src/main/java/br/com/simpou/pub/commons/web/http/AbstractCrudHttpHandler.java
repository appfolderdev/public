package br.com.simpou.pub.commons.web.http;

import br.com.simpou.pub.commons.model.serialize.ResponseObject;
import br.com.simpou.pub.commons.model.serialize.Serializations;
import br.com.simpou.pub.commons.utils.behavior.Identifiable;
import br.com.simpou.pub.commons.utils.lang.IOHelper;
import br.com.simpou.pub.commons.web.helper.CacheHelper;
import br.com.simpou.pub.commons.web.helper.FormParamsToPojoConversor;
import br.com.simpou.pub.commons.web.violation.ResourceNotFoundException;
import br.com.simpou.pub.commons.web.model.IterableResponse;
import com.sun.net.httpserver.HttpExchange;

import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by jonas.pereira on 28/03/16.
 */
public abstract class AbstractCrudHttpHandler<T extends ResponseObject & Identifiable>
        extends AbstractHttpHandler {

    private final FormParamsToPojoConversor<T> conversor;

    public AbstractCrudHttpHandler(final Class<T> entityClass, final String path) {
        super(path);
        conversor = new FormParamsToPojoConversor<T>(entityClass);
    }

    @Override
    public HttpHandlerResponse apply(final HttpExchange req) throws Exception {
        final String[] urlParts = HttpExchangeHelper.urlParts(req);
        if (urlParts.length > 4) {
            throw new ResourceNotFoundException();
        }

        final CacheHelper.ResponseFactory<byte[]> action;
        final HttpDataTransferBuilder<byte[], HttpDataTransfer<byte[]>> builder =
                HttpDataTransfer.newBuilder().mime(HttpMimeType.JSON_UTF8);

        final byte[] body;
        switch (req.getRequestMethod().toUpperCase()) {
            case "GET": {
                final Long id = extractIdFromUrl(urlParts);
                if (id == null) {
                    final IterableResponse<T> respObj = new IterableResponse<>(findAll());
                    action = new CacheHelper.ResponseFactory<byte[]>() {
                        @Override
                        public byte[] getResponseBody() {
                            return Serializations.objToJsonJackson(respObj);
                        }
                    };
                    builder.cache(respObj);
                } else {
                    final T obj = findOne(Long.valueOf(id));
                    if (obj == null) {
                        throw new ResourceNotFoundException();
                    }
                    action = new CacheHelper.ResponseFactory<byte[]>() {
                        @Override
                        public byte[] getResponseBody() {
                            return Serializations.objToJsonJackson(obj);
                        }
                    };
                    builder.cache(obj);
                }
                body = null;
                break;
            }
            case "POST": {
                final T entity = extractObjFromBody(req.getRequestBody());
                create(entity);
                body = buildSaveBodyResponse(entity.identity());
                builder.status(StatusHttp.CREATED)
                        .header(HttpHeaders.LOCATION, req.getRequestURI().toString() + "/" + entity.identity());
                action = null;
                break;
            }
            case "PUT": {
                final Long id = extractIdFromUrl(urlParts);
                update(id, extractObjFromBody(req.getRequestBody()));
                body = buildSaveBodyResponse(id);
                action = null;
                break;
            }
            case "DELETE": {
                final Long id = extractIdFromUrl(urlParts);
                delete(id);
                body = buildSaveBodyResponse(id);
                action = null;
                break;
            }
            default:
                throw new ResourceNotFoundException();
        }
        builder.body(body);

        return new HttpHandlerResponse(builder, action);
    }

    protected Iterable<T> findAll() {
        throw new ResourceNotFoundException();
    }

    protected T findOne(Long id) {
        throw new ResourceNotFoundException();
    }

    protected void create(T entity) {
        throw new ResourceNotFoundException();
    }

    protected void update(Long id, T entity) {
        throw new ResourceNotFoundException();
    }

    protected void delete(Long id) throws Exception {
        throw new ResourceNotFoundException();
    }

    private T extractObjFromBody(final InputStream is) {
        final String stringBody = IOHelper.toString(is);
        final Map<String, List<String>> multMap = FormParamsToPojoConversor.stringParamsToMap(stringBody);
        return conversor.apply(multMap);
    }

    private byte[] buildSaveBodyResponse(final Serializable id) {
        return ("{\"id\":" + id + "}").getBytes();
    }

}
