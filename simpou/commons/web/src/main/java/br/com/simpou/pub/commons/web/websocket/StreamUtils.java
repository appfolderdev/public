package br.com.simpou.pub.commons.web.websocket;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

import static br.com.simpou.pub.commons.utils.Constants.DEFAULT_CHARSET;

public class StreamUtils {

    public static void writeBody(final HttpServletResponse response, final byte[] body) throws IOException {
        final BufferedOutputStream output = new BufferedOutputStream(
                response.getOutputStream(),
                body.length
        );
        response.setBufferSize(body.length);
        output.write(body);
        response.flushBuffer();
        response.addHeader("Content-Length", String.valueOf(body.length));
    }

    public static String readLines(final InputStream in, final boolean forceNotTrim) throws IOException {

        final String requestString;

        {
            String data = "";
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(in, DEFAULT_CHARSET));
            String line = bufferRead.readLine();
            while (line != null && (forceNotTrim || line.trim().length() > 0)) {
                data += line + "\n";
                line = bufferRead.readLine();
            }
            requestString = data;
        }
        return requestString;
    }

}
