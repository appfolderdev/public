package br.com.simpou.pub.commons.web.model;

import br.com.simpou.pub.commons.model.cache.DummyCacheable;
import br.com.simpou.pub.commons.model.serialize.ResponseObject;
import br.com.simpou.pub.commons.utils.behavior.Identifiable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

/**
 * Created by jonas.pereira on 21/04/16.
 */
@Getter
@RequiredArgsConstructor
public class IdentifiableResponse<T extends Serializable>
        extends DummyCacheable
        implements ResponseObject, Identifiable<T> {

    private final T id;

    @Override
    public T identity() {
        return id;
    }

    @Override
    public void checkedId(final T id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void uncheckedId(final Serializable id) {
        throw new UnsupportedOperationException();
    }
}
