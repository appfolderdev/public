package br.com.simpou.pub.commons.web.http;

import br.com.simpou.pub.commons.model.cache.BasicCacheable;
import br.com.simpou.pub.commons.model.serialize.Serializations;
import br.com.simpou.pub.commons.utils.lang.Nulls;
import br.com.simpou.pub.commons.utils.violation.BundleViolations;
import br.com.simpou.pub.commons.utils.violation.SystemViolation;
import br.com.simpou.pub.commons.web.helper.CacheHelper;
import br.com.simpou.pub.commons.web.violation.ResourceNotFoundException;
import br.com.simpou.pub.commons.web.violation.ViolationsException;
import br.com.simpou.pub.commons.web.violation.ViolationsWrapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by jonas.pereira on 31/03/16.
 */
@RequiredArgsConstructor
public abstract class AbstractHttpHandler implements HttpHandler {

    @Getter
    private final String path;

    @Getter
    @RequiredArgsConstructor
    protected class HttpHandlerResponse {

        private final HttpDataTransferBuilder<byte[], HttpDataTransfer<byte[]>> builder;

        private final CacheHelper.ResponseFactory<byte[]> action;
    }

    protected abstract HttpHandlerResponse apply(final HttpExchange object) throws Exception;

    @Override
    public void handle(final HttpExchange req) throws IOException {
        HttpHandlerResponse response;
        HttpDataTransferBuilder<byte[], HttpDataTransfer<byte[]>> builder;
        try {
            response = apply(req);
            builder = response.getBuilder();

            final BasicCacheable cacheable = builder.getCacheable();
            CacheHelper.doCaching(
                    builder,
                    cacheable.lastModified(),
                    cacheable.objectTag(),
                    req.getRequestHeaders(),
                    response.getAction()
            );

//            final CacheHelper.HttpCacheInfo cacheInfo = CacheHelper.getCacheInfo(
//                    cacheable.lastModified(),
//                    cacheable.objectTag(),
//                    req.getRequestHeaders()
//            );
//            if (!cacheInfo.isDisabled()) {
//                if (cacheInfo.isClientCacheAvailable()) {
//                    if (cacheInfo.isClientUpdated()) {
//                        // objeto em cache no cliente, nada a ser feito
//                        //builder = HttpDataTransfer.newBuilder();
//                        builder.status(StatusHttp.NOT_MODIFIED);
//                        req.sendResponseHeaders(builder.getStatus(), 0);
//                    } else {
//                        // objeto em cache do cliente desatualizado, informa novos valores e atualiza infos de cache
//                        sendObjectNotInCache(cacheInfo, builder, req, response.getAction());
//                    }
//                } else {
//                    // objeto suporta cache mas cliente não conhece, inicializa dados de cache no cliente,
//                    // próxima requisição para o mesmo recurso já será cacheada
//                    sendObjectNotInCache(cacheInfo, builder, req, response.getAction());
//                }
//            }

        } catch (ViolationsException e) {
            final int status = e.getModelEntity().isInternalError()
                    ? StatusHttp.INTERNAL_SERVER_ERROR.value()
                    : e.getStatus();

            String localeString = Nulls.get(req.getRequestHeaders().get(HttpHeaders.ACCEPT_LANG), 0);
            final Locale locale;
            if (localeString == null) {
                locale = Locale.getDefault();
            } else {
                if (localeString.contains(",")) {
                    localeString = localeString.substring(0, localeString.indexOf(","));
                } else if (localeString.contains(";")) {
                    localeString = localeString.substring(0, localeString.indexOf(";"));
                }
                locale = Locale.forLanguageTag(localeString);
            }

            BundleViolations.translate(e.getModelEntity().joinAll(), locale);
            builder = HttpDataTransfer.newBuilder()
                    .body(
                            Serializations.objToJsonJackson(e.getModelEntity().prepareSerialization())
                    )
                    .status(status)
                    .mime(HttpMimeType.JSON_UTF8);
        } catch (Exception e) {
            final ViolationsWrapper wrapper = new ViolationsWrapper(new SystemViolation(e));
            builder = HttpDataTransfer.newBuilder()
                    .body(
                            Serializations.objToJsonJackson(wrapper.prepareSerialization())
                    )
                    .status(StatusHttp.INTERNAL_SERVER_ERROR)
                    .mime(HttpMimeType.JSON_UTF8);
        }

        if (builder.getType() != null) {
            req.getResponseHeaders().put(HttpHeaders.CONTENT_TYPE, Collections.singletonList(builder.getType()));
        }
        for (final Map.Entry<String, List<String>> entry : builder.getHeaders().entrySet()) {
            req.getResponseHeaders().put(entry.getKey(), entry.getValue());
        }

        final byte[] body = builder.getBody();
        if (body == null || builder.getStatus() == StatusHttp.NOT_MODIFIED.value()) {
            req.sendResponseHeaders(builder.getStatus(), -1);
        } else {
            req.sendResponseHeaders(builder.getStatus(), body.length);
            try (final OutputStream os = req.getResponseBody()) {
                os.write(body);
            }
        }
    }

    protected Long extractIdFromUrl(final String[] urlParts) {
        if (urlParts.length < 4) {
            return null;
        } else {
            try {
                return Long.valueOf(urlParts[urlParts.length - 1]);
            } catch (Exception e) {
                throw new ResourceNotFoundException();
            }
        }
    }
}
