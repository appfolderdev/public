package br.com.simpou.pub.commons.web.http;

import com.sun.net.httpserver.HttpServer;
import lombok.extern.apachecommons.CommonsLog;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by jonas.pereira on 31/03/16.
 */
@CommonsLog
public class StandaloneWebServer {

    private final HttpServer server;

    private static final int DEFAULT_EXECUTOR_POOL_SIZE = 10;

    private static final int DEFAULT_EXECUTOR_MAX_POOL_SIZE = 100;

    private static final long DEFAULT_EXECUTOR_KEEP_ALIVE_TIME = 10000;

    public StandaloneWebServer(final Collection<? extends AbstractHttpHandler> beans,
                               final int port,
                               final String restPath,
                               final String resourcesPath) throws IOException {
        this(
                beans,
                port,
                restPath,
                resourcesPath,
                new ThreadPoolExecutor(
                        DEFAULT_EXECUTOR_POOL_SIZE,
                        DEFAULT_EXECUTOR_MAX_POOL_SIZE,
                        DEFAULT_EXECUTOR_KEEP_ALIVE_TIME,
                        TimeUnit.MILLISECONDS,
                        new LinkedBlockingQueue<Runnable>()
                )
        );
    }

    public StandaloneWebServer(final Collection<? extends AbstractHttpHandler> beans,
                               final int port,
                               final String restPath,
                               final String resourcesPath,
                               final ExecutorService threadPoolExecutor) throws IOException {
        this.server = HttpServer.create(new InetSocketAddress(port), 0);
        for (final AbstractHttpHandler bean : beans) {
            final String servicePath = "/" + restPath + "/" + bean.getPath();
            server.createContext(servicePath, bean);
            log.debug("Service context " + servicePath + " added.");
        }
        server.createContext("/", new StaticResourcesHandler(resourcesPath));
        server.setExecutor(threadPoolExecutor);
        log.debug("Standalone server created at http://localhost:" + port + "/" + restPath);
        log.debug("Looking for resources at " + resourcesPath);
    }

    public StandaloneWebServer(final int port,
                               final String restPath,
                               final String resourcesPath,
                               final AbstractHttpHandler... beans)
            throws IOException {
        this(Arrays.asList(beans), port, restPath, resourcesPath);
    }

    public void start() {
        server.start();
        log.debug("Standalone server running...");
    }

    public void stop() {
        server.stop(1);
        log.debug("Standalone server stopped.");
    }

}
