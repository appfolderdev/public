package br.com.simpou.pub.commons.web.violation;

import br.com.simpou.pub.commons.utils.lang.Assertions;
import br.com.simpou.pub.commons.utils.violation.Violation;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Exceção indicativa de violação de regras do aplicativo, sejam elas de negócio, de sistema ou outro tipo
 * qualquer.
 *
 * @author Jonas Pereira
 * @since 2012-12-28
 */
public class ViolationsException extends br.com.simpou.pub.commons.utils.violation.ViolationsException {

    /**
     * Status W3C HTTP da resposta à requisição que gerou as violações.
     */
    @Getter
    private final int status;

    /**
     * Cabeçalho da resposta HTTP.
     */
    @Getter
    private final Map<String, Object> headers;

    /**
     * Ver {@link ViolationsException#ViolationsException(Integer,
     * java.util.Map, java.util.List)}. Status null e sem headers.
     *
     * @param violations Ver {@link #entity}.
     */
    public ViolationsException(final Violation... violations) {
        this(null, violations);
    }

    /**
     * Ver {@link ViolationsException#ViolationsException(Integer,
     * java.util.Map, java.util.List)}. Sem headers.
     *
     * @param status     Ver {@link #status}.
     * @param violations Ver {@link #entity}.
     */
    public ViolationsException(final Integer status, final Violation... violations) {
        this(status, null, Arrays.asList(violations));
    }

    /**
     * @param status     Ver {@link #status}.
     * @param headers    Ver {@link #headers}.
     * @param violations Ver {@link #entity}.
     * @see ViolationsException#ViolationsException(Integer, java.util.Map,
     * java.util.List)
     */
    public ViolationsException(final Integer status, final Map<String, Object> headers, final Violation... violations) {
        this(status, headers, Arrays.asList(violations));
    }

    /**
     * @param status     Ver {@link #status}.
     * @param headers    Ver {@link #headers}. Array será convertido segundo regras de {@link
     *                   #extractHeadersFromArray(String[][])}.
     * @param violations Ver {@link #entity}.
     * @see ViolationsException#ViolationsException(Integer, java.util.Map,
     * java.util.List)
     */
    public ViolationsException(final Integer status, final String[][] headers, final Violation... violations) {
        this(status, extractHeadersFromArray(headers), Arrays.asList(violations));
    }

    /**
     * Ver {@link ViolationsException#ViolationsException(Integer,
     * java.util.Map, java.util.List)}. Status null e sem headers.
     *
     * @param violations Ver {@link #entity}.
     */
    public ViolationsException(final List<? extends Violation> violations) {
        this(null, null, violations);
    }

    /**
     * @param status     Ver {@link #status}. Se status null e houver pelo menos uma exceção de sistema,
     *                   status da resposta será erro interno no servidor (500). Se status null e não houver
     *                   nenhuma exceção de sistema, status da resposta será requisição ruim (400).
     * @param headers    Ver {@link #headers}.
     * @param violations Ver {@link #entity}. Violações irão compor o corpo da resposta no formato de um
     *                   objeto do tipo {@link br.com.simpou.pub.commons.utils.violation.ViolationsWrapper}.
     *                   Violações são ordenadas segundo seu mecanismo natural e conjuntos vazios são
     *                   removidos.
     */
    public ViolationsException(final Integer status,
                               final Map<String, Object> headers,
                               final List<? extends Violation> violations) {
        super(new ViolationsWrapper(violations));
        if (getEntity().isInternalError()) {
            this.status = 500;
        } else if (status != null) {
            this.status = status.intValue();
        } else {
            this.status = 400;
        }
        this.headers = headers;
    }

    /**
     * @param headers Primeira dimensão representa o par chave/valor, sendo a chave o primeiro item da segunda
     *                dimensão e o valor o segundo item.
     * @return Array copiado para o mapa de cabeçalhos.
     * @throws IllegalArgumentException Se array não tiver exatamente dois items em cada posição.
     */
    public static Map<String, Object> extractHeadersFromArray(final String[][] headers) {
        final Map<String, Object> map = new HashMap<String, Object>();
        for (final String[] h : headers) {
            Assertions.lengthRequired(h, 2);
            map.put(h[0], h[1]);
        }
        return map;
    }

    public ViolationsWrapper getModelEntity() {
        return (ViolationsWrapper) super.getEntity();
    }
}
