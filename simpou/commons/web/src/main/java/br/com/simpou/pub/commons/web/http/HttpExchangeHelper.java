package br.com.simpou.pub.commons.web.http;

import com.sun.net.httpserver.HttpExchange;

/**
 * Created by jonas on 6/12/16.
 */
public class HttpExchangeHelper {

    public static String[] urlParts(final HttpExchange exchange){
        return exchange.getRequestURI().toString().split("/");
    }

    public static HttpMethod method(final HttpExchange exchange){
        return HttpMethod.valueOf(exchange.getRequestMethod().toUpperCase());
    }

}
