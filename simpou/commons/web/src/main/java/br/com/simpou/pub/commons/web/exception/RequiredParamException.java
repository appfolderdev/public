package br.com.simpou.pub.commons.web.exception;

import br.com.simpou.pub.commons.web.http.StatusHttp;
import br.com.simpou.pub.commons.web.violation.AbstractBusinessViolationException;

public class RequiredParamException extends AbstractBusinessViolationException {

    public RequiredParamException(final String param) {
        super(StatusHttp.BAD_REQUEST.value(), "global.requiredParams", param);
    }
}
