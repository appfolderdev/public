package br.com.simpou.pub.commons.web.model;

import br.com.simpou.pub.commons.model.cache.BasicCacheable;
import br.com.simpou.pub.commons.model.mixin.BaseEntityMixin;
import br.com.simpou.pub.commons.model.serialize.ResponseObject;
import br.com.simpou.pub.commons.model.view.GlobalView;
import br.com.simpou.pub.commons.utils.lang.Nulls;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * Created by jonas on 26/01/17.
 */
//@JsonView(GlobalView.class)
@JsonPropertyOrder({"data", "meta"})
@JsonRootName(value = ResponseObject.ROOT_NAME)
@XmlRootElement(name = ResponseObject.ROOT_NAME)
public class ListResponse<T extends Serializable> extends BasicCacheable implements ResponseObject {

    @JacksonXmlProperty(localName = ResponseObject.XML_ITEM_NAME)
    @JacksonXmlElementWrapper(localName = ResponseObject.XML_LIST_NAME)
    //    @JsonTypeIdResolver(DefaultTypeIdResolver.class)
    //    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME/*, property = XML_ITEM_TYPE_NAME*/)
    @JsonView(GlobalView.class)
    private final Collection<T> data;

    @JsonView(GlobalView.class)
    private final ResponseMetadata meta;

    public ListResponse(final Date lastModified,
                        final String objectTag,
                        final Collection<T> data) {
        this(lastModified, objectTag, data, null);
    }

    protected ListResponse(final Collection<T> data,
                           final Date lastModified,
                           final String objectTag,
                           final ResponseMetadata.Pagination pagination,
                           final String type,
                           final boolean success) {
        super(lastModified, objectTag);
        this.data = data;
        this.meta = new ResponseMetadata(
                pagination,
                Nulls.getTime(lastModified),
                type,
                success,
                true
        );
    }

    @JsonCreator
    public ListResponse(@JsonProperty("data") final Collection<T> data,
                        @JsonProperty("lastModified") final Date lastModified,
                        @JsonProperty("objectTag") final String objectTag,
                        @JsonProperty("pagination") final ResponseMetadata.Pagination pagination,
                        @JsonProperty(BaseEntityMixin.TYPE_NAME) final String type) {
        this(data, lastModified, objectTag, pagination, type, true);
    }

    public ListResponse(final Date lastModified,
                        final String objectTag,
                        final Collection<T> data,
                        final String type) {
        this(
                data,
                lastModified,
                objectTag,
                new ResponseMetadata.Pagination(data.size()),
                type
        );
    }

    public ListResponse(final Collection<T> data) {
        this(null, null, data);
    }
}
