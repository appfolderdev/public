package br.com.simpou.pub.commons.web.http;

import br.com.simpou.pub.commons.utils.file.FileHelper;
import br.com.simpou.pub.commons.utils.violation.SystemViolation;
import br.com.simpou.pub.commons.web.helper.CacheHelper;
import br.com.simpou.pub.commons.web.violation.ViolationsException;
import com.sun.net.httpserver.HttpExchange;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jonas.pereira on 09/03/16.
 */
public class StaticResourcesHandler extends AbstractHttpHandler {

    public StaticResourcesHandler(final String path) {
        super(path);
    }

    private static final Map<String, ResourceFile> FILES = new HashMap<>();

    static final Date LAST_MODIFIED = new Date();

    @Getter
    @RequiredArgsConstructor
    private static class ResourceFile {

        private final byte[] content;

        private final Date lastModified;
    }

    @Override
    public HttpHandlerResponse apply(final HttpExchange req) {
        final BasicHttpDataTransferBuilder builder = HttpDataTransfer.newBuilder();

        final String filename;
        {
            String filenameAux = req.getRequestURI().toString().substring(1).split("\\?")[0];
            if (filenameAux.isEmpty()) {
                filenameAux = "index.html";
            }
            filename = getPath() + "/" + filenameAux;
        }

        builder.cache(LAST_MODIFIED, "static:" + filename);
        final String[] parts = filename.split("\\.");
        switch (parts[parts.length - 1]) {
            case "html":
                builder.mime(HttpMimeType.TEXT_HTML);
                break;
            case "css":
                builder.mime(HttpMimeType.TEXT_CSS);
                break;
            case "js":
                builder.mime(HttpMimeType.TEXT_JS);
                break;
        }
        return new HttpHandlerResponse(
                builder,
                new CacheHelper.ResponseFactory<byte[]>() {
                    @Override
                    public byte[] getResponseBody() {
                        final byte[] content;
                        if (!FILES.containsKey(filename)
                                || LAST_MODIFIED.after(FILES.get(filename).getLastModified())) {
                            // arquivo ainda não lido ou desatualizado
                            try {
                                content = FileHelper.readResourceFile(filename);
                            } catch (IOException e) {
                                throw new ViolationsException(new SystemViolation(e));
                            }
                            FILES.put(filename, new ResourceFile(content, LAST_MODIFIED));
                        } else {
                            // arquivo já lido anteriormente
                            content = FILES.get(filename).getContent();
                        }

                        return content;
                    }
                }
        );
    }
}
