package br.com.simpou.pub.commons.web.websocket;

import lombok.extern.apachecommons.CommonsLog;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Gerenciador de conexões websockets.
 *
 * @author Jonas Pereira
 * @since 22/06/2017
 */
@CommonsLog
final class WebsocketManager {

    private final ConnectionManager connectionManager;

    private final WebSocketListener messageListener;
    public static final int PORT = 9911;

    private static List connections = new ArrayList();

    WebsocketManager(final WebSocketListener messageListener) {
        final ServerSocket socket;
        try {
            socket = new ServerSocket(PORT);
        } catch (IOException e) {
            //Erro irrecuperável: impossibilidade de abertura da porta de escuta no SO.
            throw new RuntimeException(e);
        }
        this.messageListener = messageListener;
        connectionManager = new ConnectionManager(socket);
        connectionManager.start();
    }

    void shutdown() {
        connectionManager.shutdown();
    }

    synchronized void sendMessage(final WebSocketMessage message, final ClientLocation target) {
        final Collection toRemove = new ArrayList();
        {
            final Iterator connIterator = connections.iterator();
            while (connIterator.hasNext()) {
                final WebSocketConnection connection = (WebSocketConnection) connIterator.next();
                if (connection.getLocation() == null) {
                    toRemove.add(connection);
                } else if (target==null || connection.getLocation().equals(target.getAddress())) {
                    if (connection.isConnectionEstablished()) {
                        final boolean success = connection.sendMessage(message);
                        if (success) {
                            log.debug("Notificação websocket enviada para " +
                                    connection.getClientIp() + " do local " + target);
                        } else {
                            toRemove.add(connection);
                        }
                    } else {
                        toRemove.add(connection);
                    }
                }
            }
            // limpa conexões finalizadas ou inválidas
            if (toRemove.size() > 0) {
                final Iterator iterator = toRemove.iterator();
                while (iterator.hasNext()) {
                    try {
                        ((WebSocketConnection) iterator.next()).disconnect();
                    } catch (Exception e) {
                        // imprevisível, apenas para garantir desconexão
                    }
                }
                log.debug("Foram removidas " + toRemove.size() + " conexões de websocket.");
                connections.removeAll(toRemove);
            }
        }
    }

    /**
     * Aguarda clientes se conectarem.
     */
    private class ConnectionManager extends Thread {

        /**
         * Se true, servidor finalizado, não serão aceitas mais conexões. Estado irrecuperável.
         */
        private boolean offline;

        private final ServerSocket socket;

        private ConnectionManager(final ServerSocket socket) {
            this.socket = socket;
            this.offline = false;
        }

        private boolean isOffline() {
            return socket.isClosed() || !socket.isBound() || this.offline;
        }

        public void run() {
            while (!isOffline()) {// fechamento do socket causa abandono do loop
                try {
                    // Trava a execução aguardando por conexões.
                    final Socket connection = this.socket.accept();
                    final String clientIp = connection.getInetAddress().getHostAddress();

                    log.info("Solicitação de conexão ao websocket recebida de " + clientIp);

                    final WebSocketConnection handler = new WebSocketConnection(
                            connection,
                            messageListener,
                            clientIp
                    );
                    connections.add(handler);
                    handler.start();
                } catch (IOException e) {
                    // erros ocasionados possivelmente pelo cliente:
                    // desconsiderar e tratar próximas conexões
                    e.printStackTrace();
                }
            }
        }

//        private void rejectConnection(final Socket connection, final String clientIp) throws IOException {
//            connections.remove(clientIp);
//            final OutputStream out = connection.getOutputStream();
//            WebSocketUtils.writeBadRequest(out);
//            out.close();
//        }

        private void shutdown() {
            this.offline = true;
            final Iterator iterator = connections.iterator();
            while (iterator.hasNext()) {
                ((WebSocketConnection) iterator.next()).disconnect();
            }
            connections.clear();
            try {
                socket.close();
            } catch (IOException e) {
                //tratamento irrelevante
            }
        }
    }

}
