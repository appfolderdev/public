package br.com.simpou.pub.commons.web.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;
import java.util.Collection;

import static br.com.simpou.pub.commons.model.serialize.ResponseObject.XML_ITEM_NAME;
import static br.com.simpou.pub.commons.model.serialize.ResponseObject.XML_LIST_NAME;

@Getter
@RequiredArgsConstructor
public class ListResponseWrapper implements Serializable {

    @JacksonXmlProperty(localName = XML_ITEM_NAME)
    @JacksonXmlElementWrapper(localName = XML_LIST_NAME, useWrapping = false)
    private final Collection<?> data;
}
