package br.com.simpou.pub.commons.web.http;

/**
 * @author Jonas Pereira
 * @since 10/09/2014.
 */
public final class HttpHeaders {

    /**
     * Url de quem faz a requisição.
     */
    public static final String REFERER = "Referer";

    /**
     * Para indicar como o navegador irá tratar o retorno de um arquivo. Ex.: fazer download, ou agragar na
     * página.
     */
    public static final String CONTENT_DISPOSITION = "Content-Disposition";

    /**
     * Indica para o navegador quando o cache deve ser expirado.
     */
    public static final String EXPIRES = "Expires";

    /**
     * Classe utilitária.
     *
     * @throws UnsupportedOperationException Sempre.
     */
    private HttpHeaders() {
        throw new UnsupportedOperationException();
    }

    /**
     * constante CONTENT_TYPE.
     */
    public static final String CONTENT_TYPE = "Content-Type";

    /**
     * Tamanho do conteúdo no corpo da resposta.
     */
    public static final String CONTENT_LENGTH = "Content-Length";

    /**
     * Constante ETAG.
     */
    public static final String ETAG = "ETag";

    /**
     * Constante IF_NONE_MATCH.
     */
    public static final String IF_NONE_MATCH = "If-None-Match";

    /**
     * Contante IF_MODIFIED_SINCE.
     */
    public static final String IF_MODIFIED_SINCE = "If-Modified-Since";

    /**
     * Constante LAST_MODIFIED.
     */
    public static final String LAST_MODIFIED = "Last-Modified";

    /**
     * Indica a localização de um recurso criado;.
     */
    public static final String LOCATION = "Location";

    /**
     * Indica o tipo de cacheamento a ser realizado no cliente e em seus intermediários.
     */
    public static final String CACHE_CONTROL = "Cache-Control";

    /**
     * Define na requisição o mime type esperado na resposta.
     */
    public static final String ACCEPT = "Accept";

    /**
     * Define o locale.
     */
    public static final String ACCEPT_LANG = "Accept-language";

}
