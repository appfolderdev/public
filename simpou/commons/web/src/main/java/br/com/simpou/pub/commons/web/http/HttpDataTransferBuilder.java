package br.com.simpou.pub.commons.web.http;

import br.com.simpou.pub.commons.model.cache.BasicCacheable;
import br.com.simpou.pub.commons.model.cache.Cacheable;
import br.com.simpou.pub.commons.model.cache.DummyCacheable;

import java.net.URI;
import java.util.*;

/**
 * Created by jonas on 22/04/2016.
 *
 * @param <T> Tipo do objeto no corpo.
 * @param <E> Tipo do objeto a ser gerado durante build.
 */
public abstract class HttpDataTransferBuilder<T, E> {

    private T body;

    private int status = StatusHttp.OK.value();

    private String type;

    private Map<String, List<String>> headers = new HashMap<>();

    private String method = "GET";

    private URI uri;

    private BasicCacheable cacheable;

//    public final HttpDataTransferBuilder body(final InputStream is) {
//        try {
//            body(IOHelper.toBytes(is));
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//        return this;
//    }

    public final HttpDataTransferBuilder<T, E> body(final T body) {
        this.body = body;
        return this;
    }

    public final HttpDataTransferBuilder<T, E> cache(final Date lastModified, final String objectTag) {
        this.cacheable = new BasicCacheable(lastModified, objectTag);
        return this;
    }

    public final HttpDataTransferBuilder<T, E> cache(final Cacheable cacheable) {
        cache(cacheable.lastModified(), cacheable.objectTag());
        return this;
    }

    public final HttpDataTransferBuilder<T, E> cache(final long lastModified, final String objectTag) {
        cache(new Date(lastModified), objectTag);
        return this;
    }

    public final HttpDataTransferBuilder<T, E> status(final int status) {
        this.status = status;
        return this;
    }

    public final HttpDataTransferBuilder<T, E> status(final StatusHttp status) {
        status(status.value());
        return this;
    }

    public final HttpDataTransferBuilder<T, E> mime(final String mimeType) {
        this.type = mimeType;
        return this;
    }

    public final HttpDataTransferBuilder<T, E> mime(final HttpMimeType mimeType) {
        mime(mimeType.getValue());
        return this;
    }

    public final HttpDataTransferBuilder<T, E> method(final String method) {
        this.method = method;
        return this;
    }

    public final HttpDataTransferBuilder<T, E> method(final HttpMethod method) {
        method(method.name());
        return this;
    }

    public final HttpDataTransferBuilder<T, E> uri(final String urlPath) {
        uri(URI.create(urlPath));
        return this;
    }

    public final HttpDataTransferBuilder<T, E> uri(final URI uri) {
        this.uri = uri;
        return this;
    }

    public final HttpDataTransferBuilder<T, E> headers(final Map<String, List<String>> headers) {
        this.headers.putAll(headers);
        return this;
    }

    public final HttpDataTransferBuilder<T, E> header(final String name, final String... values) {
        this.headers.put(name, Arrays.asList(values));
        return this;
    }

    public abstract E build();

    public final T getBody() {
        return body;
    }

    public final int getStatus() {
        return status;
    }

    public final String getType() {
        return type;
    }

    public final Map<String, List<String>> getHeaders() {
        return Collections.unmodifiableMap(headers);
    }

    public final String getMethod() {
        return method;
    }

    public final URI getUri() {
        return uri;
    }

    public final BasicCacheable getCacheable() {
        return cacheable == null ? new DummyCacheable() : cacheable;
    }
}
