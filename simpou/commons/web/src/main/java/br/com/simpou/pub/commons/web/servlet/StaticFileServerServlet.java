package br.com.simpou.pub.commons.web.servlet;

import br.com.simpou.pub.commons.utils.Constants;
import br.com.simpou.pub.commons.utils.file.FileHelper;
import br.com.simpou.pub.commons.utils.file.PropertyHelper;
import br.com.simpou.pub.commons.utils.lang.Checks;
import br.com.simpou.pub.commons.web.helper.CacheHelper;
import br.com.simpou.pub.commons.web.http.HttpHeaders;
import br.com.simpou.pub.commons.web.http.ServletDataTransferBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

/**
 * @author Jonas Pereira
 * @since 2017-01-22
 */
@CommonsLog
public class StaticFileServerServlet extends HttpServlet {

    /**
     * Mapeia a extensão do arquivo com seu mime-type adequado.
     */
    static final Map<String, String> DEF_MIMES_MAP;

    static final Set<String> DISPO_TYPES;

    static {
        {
            DISPO_TYPES = new HashSet<>(Arrays.asList(
                    "jpg", "png", "gif", "ico",
                    "pdf"
            ));
        }
        {
            final Map<String, String> mapMimeTypes = new HashMap<>();

            // webapp
            mapMimeTypes.put("js", "application/javascript");
            mapMimeTypes.put("css", "text/css");
            mapMimeTypes.put("html", "text/html");
            mapMimeTypes.put("htm", "text/html");
            mapMimeTypes.put("json", "application/json");
            mapMimeTypes.put("xml", "application/xml");

            // default
            mapMimeTypes.put(null, "application/xml");

            // fontes
            mapMimeTypes.put("svg", "image/svg+xml");
            mapMimeTypes.put("ttf", "font/ttf");
            mapMimeTypes.put("woff", "font/woff");
            mapMimeTypes.put("woff2", "font/woff2");
            mapMimeTypes.put("eot", "font/eot");//?

            // arquivos
            mapMimeTypes.put("pdf", "application/pdf");
            mapMimeTypes.put("txt", "text/plain");

            // pacotes
            mapMimeTypes.put("gz", "application/gzip");
            mapMimeTypes.put("gzip", "application/gzip");

            // imagens
            mapMimeTypes.put("jpg", "image/jpeg");
            mapMimeTypes.put("png", "image/png");
            mapMimeTypes.put("gif", "image/gif");
            mapMimeTypes.put("ico", "image/x-icon");

            DEF_MIMES_MAP = Collections.unmodifiableMap(mapMimeTypes);
        }
    }

    /**
     * Caminho completo do diretório onde serão buscadas os arquivos.
     */
    private String basePath;

    private String charSet;

    @Override
    public void init() throws ServletException {
        final String sysProp = getServletConfig().getInitParameter("baseLocation");
        this.basePath = PropertyHelper.getSystemProperty(sysProp) + File.separator +
                getServletConfig().getInitParameter("lookupDir");
        FileHelper.mkdir(this.basePath);
        final String charset = getServletConfig().getInitParameter("charset");
        if (Checks.isEmpty(charset)) {
            this.charSet = Constants.DEFAULT_CHARSET;

        } else {
            this.charSet = charset;
        }

        //TODO adicionar limite configurável pro cache
        //final String cacheLimit = getServletConfig().getInitParameter("cacheLimit");

        //TODO adicionar suporte personalização de mais tipos
        //final String mimeTypes = getServletConfig().getInitParameter("mimeTypes");

    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        // resolve a requisição
        final File file;
        final String reqFileName;
        final String pathInfo;
        {
            pathInfo = request.getPathInfo();
            String reqFileNameAux = pathInfo;

            if (Checks.isEmpty(reqFileNameAux) || reqFileNameAux.endsWith("/")) {
                reqFileNameAux = reqFileNameAux + "index.html";
            }
            reqFileName = reqFileNameAux;
            file = newFile(reqFileName);
            if (!file.exists()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
        }

        final String fileExt;
        final String contentType;
        {
            final int lastDot = reqFileName.lastIndexOf('.');
            if (lastDot < 0) {
                fileExt = null;
            } else {
                fileExt = reqFileName.substring(lastDot + 1, reqFileName.length());
            }
            contentType = DEF_MIMES_MAP.get(fileExt);
        }

        final ServletDataTransferBuilder builder = new ServletDataTransferBuilder(response);
        {
            builder.mime(contentType);
            if (DISPO_TYPES.contains(fileExt)) {
                builder.header(
                        HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + file.getName() + "\""
                );
            }
        }
        CacheHelper.doCaching(
                builder,
                new Date(file.lastModified()),
                pathInfo,
                Servlets.headersFrom(request),
                new FileResponse(file)
                             );
        builder.build();
    }

    private File newFile(final String imgName) throws UnsupportedEncodingException {
        return new File(
                basePath,
                URLDecoder.decode(imgName, charSet));
    }

    @RequiredArgsConstructor
    private class FileResponse implements CacheHelper.ResponseFactory<byte[]> {

        private final File file;

        @Override
        public byte[] getResponseBody() {
            try {
                return FileHelper.read(file);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
