package br.com.simpou.pub.commons.web.websocket;

import br.com.simpou.pub.commons.model.serialize.Serializations;
import br.com.simpou.pub.commons.utils.Constants;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

class WebSocketUtils {

    static void writeBadRequest(final OutputStream out) throws IOException {
        writeResponse(out, HttpServletResponse.SC_BAD_REQUEST + " Bad Request");
    }

    static void writeResponse(final OutputStream out, final String text) throws IOException {
        final byte[] response = (
                "HTTP/1.1 " + text + "\r\n\r\n"
        ).getBytes(Constants.DEFAULT_CHARSET);
        out.write(response, 0, response.length);
    }

    static void sendMessageToClient(final WebSocketMessage mess, final OutputStream out) throws IOException {
        final Serializable payload = mess.getPayload();
        final byte[] rawData = Serializations.objToJsonJackson(payload);

        final byte[] frame = new byte[10];

        frame[0] = (byte) (129 + mess.getOpcode() - 1);

        final int frameCount;
        if (rawData.length <= 125) {
            frame[1] = (byte) rawData.length;
            frameCount = 2;
        } else if (rawData.length >= 126 && rawData.length <= 65535) {
            frame[1] = (byte) 126;
            int len = rawData.length;
            frame[2] = (byte) ((len >> 8) & (byte) 255);
            frame[3] = (byte) (len & (byte) 255);
            frameCount = 4;
        } else {
            frame[1] = (byte) 127;
            int len = rawData.length;
            frame[2] = (byte) ((len >> 56) & (byte) 255);
            frame[3] = (byte) ((len >> 48) & (byte) 255);
            frame[4] = (byte) ((len >> 40) & (byte) 255);
            frame[5] = (byte) ((len >> 32) & (byte) 255);
            frame[6] = (byte) ((len >> 24) & (byte) 255);
            frame[7] = (byte) ((len >> 16) & (byte) 255);
            frame[8] = (byte) ((len >> 8) & (byte) 255);
            frame[9] = (byte) (len & (byte) 255);
            frameCount = 10;
        }

        final int bLength = frameCount + rawData.length;
        final byte[] reply = new byte[bLength];

        int bLim = 0;
        for (int i = 0; i < frameCount; i++) {
            reply[bLim] = frame[i];
            bLim++;
        }
        for (int i = 0; i < rawData.length; i++) {
            reply[bLim] = rawData[i];
            bLim++;
        }

        out.write(reply);
        out.flush();
    }

    static WebSocketMessage decodeClientMessage(byte[] message) {

        //considera qua a mensagem é sempre textual, não faz chacagens de tipo
        final byte rLength = (byte) (message[1] & 127);

        final int rMaskIndex;
        if (rLength == (byte) 126) {
            rMaskIndex = 4;
        } else if (rLength == (byte) 127) {
            rMaskIndex = 10;
        } else {
            rMaskIndex = 2;
        }

        final byte[] masks = new byte[4];
        for (int i = rMaskIndex, j = 0; i < (rMaskIndex + 4); j++, i++) {
            masks[j] = message[i];
        }

        final int rDataStart = rMaskIndex + 4;
        final int messLen = message.length - rDataStart;
        final byte[] payload = new byte[messLen];
        for (int i = rDataStart, j = 0; i < message.length; i++, j++) {
            payload[j] = (byte) (message[i] ^ masks[j % 4]);
        }

        final byte opcode = (byte) (message[0] & 127);

        return new WebSocketMessage(opcode, new String(payload), null);
    }

}
