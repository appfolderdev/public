package br.com.simpou.pub.commons.web.websocket;

public interface ResolvedRequest {

    String getResourcePath();

    String getMethod();

    boolean containsParam(String param);

//    Integer getUserId();
//
//    Integer getPoloId();

    String getHeader(String key);

    boolean containsHeader(String key);

//    ResolvedSession getSession();

    QueryParameters.Value getParameter(String key);

    //Map getBodyValues();

    String getServerName();

    String getPathElement(int i);

    long getLongId() throws Exception;

    String getId();

//    SessionInfo getSessionInfo();

    Object payload(final Class type) throws Exception;
}
