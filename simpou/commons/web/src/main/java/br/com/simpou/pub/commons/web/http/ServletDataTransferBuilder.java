package br.com.simpou.pub.commons.web.http;

import br.com.simpou.pub.commons.web.servlet.Servlets;
import lombok.RequiredArgsConstructor;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by jonas on 22/01/2017.
 */
@RequiredArgsConstructor
public class ServletDataTransferBuilder extends HttpDataTransferBuilder<byte[], HttpServletResponse> {

    private final HttpServletResponse response;

    @Override
    public HttpServletResponse build() {
        response.reset();
        if (super.getStatus() == StatusHttp.NOT_MODIFIED.value()) {
            response.setStatus(StatusHttp.NOT_MODIFIED.value());
        } else {
            if (super.getType() != null) {
                response.setContentType(super.getType());
            }

            final byte[] body = super.getBody();
            Servlets.addHeaders(response, super.getHeaders());
            if (body != null) {
                Servlets.writeBody(response, body);
            }
            response.setStatus(super.getStatus());
        }
        return response;
    }

}
