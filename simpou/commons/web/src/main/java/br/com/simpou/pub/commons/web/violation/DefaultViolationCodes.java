package br.com.simpou.pub.commons.web.violation;

/**
 * Códigos i18n para as violações de regras.
 *
 * @author Jonas Pereira
 * @since 2013-01-18
 */
public final class DefaultViolationCodes {

    public static final String FORBIDDEN = "access_denied";

    /**
     * Classe utilitária.
     *
     * @throws UnsupportedOperationException Sempre.
     */
    private DefaultViolationCodes() {
        throw new UnsupportedOperationException();
    }

    /**
     * Recurso não encontrado.
     */
    public static final String NOT_FOUND = "resource_not_found";
    public static final String INVALID_DATE_FORMAT = "invalid_date_format";

    /**
     * Impossibilidade de conversão de parâmetros de requisição em um objeto.
     */
    public static final String INVALID_POST_PUT_PARAMS = "invalidPostPutParameters";

    /**
     * Parâmetros de uma requisição PUT passados via payload não puderem ser lidos corretamente.
     */
    public static final String INVALID_PAYLOAD_PARAMS = "invalidPayloadParams";

    /**
     * Erros de integridade violada, normalmente causado por unicidade de um campo violado.
     */
    public static final String INTEGRITY_VIOLATION = "integrity_violation";

    /**
     * Outros erros que não se encaixariam nos demais.
     */
    public static final String INTERNAL_ERROR = "internal_error";

    /**
     * Requisição inválida. Normalmente por parâmetros incorretos.
     */
    public static final String INVALID_REQUEST = "invalidRequest";

    /**
     * Use para indicar que um determinado método HTTP não está disponível para uma determinada URL.
     */
    public static final String METHOD_NOT_ALLOWED = "methodNotAllowed";

}
