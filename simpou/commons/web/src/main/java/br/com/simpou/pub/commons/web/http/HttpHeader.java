package br.com.simpou.pub.commons.web.http;

import lombok.Getter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by jonas.pereira on 14/03/16.
 */
@Getter
public class HttpHeader {

    private final String key;

    private final List<String> values;

    public HttpHeader(final String key, final List<String> values) {
        this.key = key;
        this.values = Collections.unmodifiableList(values);
    }

    public HttpHeader(final String key, final String... values) {
        this(key, Arrays.asList(values));
    }
}
