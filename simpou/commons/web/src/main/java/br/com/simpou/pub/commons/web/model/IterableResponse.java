package br.com.simpou.pub.commons.web.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonRootName;
import br.com.simpou.pub.commons.model.cache.Cacheable;
import br.com.simpou.pub.commons.model.cache.CacheableListsUtil;
import br.com.simpou.pub.commons.model.serialize.ResponseObject;
import lombok.Getter;
import lombok.experimental.Delegate;

/**
 * Created by jonas.pereira on 31/03/16.
 */
@JsonRootName("response")
public class IterableResponse<T extends Cacheable> implements ResponseObject {

    @Delegate
    @JsonIgnore
    private CacheableListsUtil cacheableUtil;

    @Getter
    private final ResponseMetadata meta;

    @Getter
    private final Iterable<T> data;

    public IterableResponse(final Iterable<T> data, final String type) {
        this.cacheableUtil = new CacheableListsUtil(data);
        this.data = data;
        this.meta = new ResponseMetadata(
                new ResponseMetadata.Pagination(this.cacheableUtil.size()),
                this.cacheableUtil.lastModified() == null ? null : this.cacheableUtil.lastModified().getTime(),
                type == null ? this.cacheableUtil.itemType().getSimpleName() : type,
                true, true
        );
    }

    public IterableResponse(final Iterable<T> data) {
        this(data, null);
    }

}
