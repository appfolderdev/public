package br.com.simpou.pub.commons.web.websocket;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Responsável por distribuir as mensagens de chegada dos clientes para os assinantes registrados.
 *
 * @author Jonas Pereira
 * @since 2017-05-26
 */
public class WebSocketEndpointImpl implements WebSocketEndpoint {

    private static final WebsocketManager MANAGER_INSTANCE = new WebsocketManager(new WebSocketListener() {
        public void onMessage(final WebSocketMessage message) {
            fireNotifications(message);
        }
    });

    private static Collection listeners = new ArrayList();

    public void sendMessage(final WebSocketMessage message, final ClientLocation target) {
        MANAGER_INSTANCE.sendMessage(message, target);
    }

    public synchronized void addMessageListener(final WebSocketListener listener) {
        listeners.add(listener);
    }

    public void shutdown() {
        MANAGER_INSTANCE.shutdown();
    }

    /**
     * Notifica todos ouvintes da chegada de uma nova mensagem de forma assíncrona, de forma que um
     * listener não interfira no outro.
     */
    private static synchronized void fireNotifications(final WebSocketMessage message) {
        final Iterator iterator = listeners.iterator();
        while (iterator.hasNext()) {
            new WebSocketListenerWrapper((WebSocketListener) iterator.next(), message).start();
        }
    }

    /**
     * Executa o processamento da mensagem no ouvinte.
     */
    private static class WebSocketListenerWrapper extends Thread {

        private final WebSocketListener listener;

        private final WebSocketMessage message;

        private WebSocketListenerWrapper(final WebSocketListener listener,
                                         final WebSocketMessage message) {
            this.listener = listener;
            this.message = message;
        }

        public void run() {
            listener.onMessage(message);
        }
    }

}
