package br.com.simpou.pub.commons.web.websocket;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ClientLocation {

    private String address;

}
