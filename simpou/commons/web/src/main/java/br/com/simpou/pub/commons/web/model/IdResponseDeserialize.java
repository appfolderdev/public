package br.com.simpou.pub.commons.web.model;

import lombok.Getter;

import java.io.Serializable;

@Getter
public class IdResponseDeserialize<T> implements Serializable {

    private IdData<T> data;

    public T identity() {
        return this.data.getId();
    }
}

@Getter
class IdData<T> implements Serializable {

    private T id;
}
