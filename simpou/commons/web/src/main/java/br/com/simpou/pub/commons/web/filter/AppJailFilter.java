package br.com.simpou.pub.commons.web.filter;

import br.com.simpou.pub.commons.utils.lang.Checks;
import br.com.simpou.pub.commons.web.helper.Urls;
import br.com.simpou.pub.commons.web.servlet.RequestUrl;
import br.com.simpou.pub.commons.web.servlet.Servlets;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Aprisiona uma aplicação para consumir somente recursos de seu diretório.
 * Torna o diretório da app como seu context root.
 * * <p>
 * Parâmetros:
 * - basePath: contexto base/root da requisição.
 * -appNames: nomes das aplicações separados por vírgula.
 */
public class AppJailFilter implements Filter {

    private String basePath;

    private String[] basePathArr;

    /**
     * Posição do nome do app na url. Contagem inicia-se em 1.
     */
    private int level;

    private String apiBase;

    private Set<String> apps;

    @Override
    public void destroy() {
    }

    void setInitParams(final String basePath,
                       final String apiBase,
                       final String appsParam) {
        setBasePath(basePath);
        setApiBase(apiBase);
        setApps(appsParam);
    }

    private void setBasePath(final String basePath) {
        this.basePath = Urls.relativize(basePath);
        if (this.basePath.isEmpty()) {
            this.level = 1;
            this.basePathArr = new String[0];
        } else {
            this.basePathArr = this.basePath.split("/");
            level = this.basePathArr.length + 1;
        }
        this.basePath = this.basePath.isEmpty() ? "" : this.basePath + "/";
    }

    private void setApiBase(final String apiBase) {
        this.apiBase = Urls.relativize(apiBase);
    }

    private void setApps(final String appsParam) {
        final String[] appsArr = appsParam.split(",");
        final Set<String> apps = new HashSet<>();
        for (final String appName : appsArr) {
            apps.add(appName.toLowerCase());
        }
        this.apps = Collections.unmodifiableSet(apps);
    }

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        setInitParams(
                filterConfig.getInitParameter("basePath"),
                filterConfig.getInitParameter("apiBase"),
                filterConfig.getInitParameter("appNames")
        );
    }

    JailInfo getJailInfo(final String referer, final String target) {
        if (referer == null) {
            return new JailInfo(true);
        }
        final String[] src = Urls.relativize(referer).split("/");

        // o numero de partes da origem não é suficiente para comparar, logo não é um app
        if (src.length < level) {
            return new JailInfo(src, true);
        }

        // se o caminho base n bate, logo não é um app
        for (int i = 0; i < basePathArr.length; i++) {
            if (!src[i].equalsIgnoreCase(basePathArr[i])) {
                return new JailInfo(src, true);
            }
        }

        // o app solicitante não está listado pra ser enjaulado
        final String srcAppName = src[level - 1];
        if (!apps.contains(srcAppName.toLowerCase())) {
            return new JailInfo(src, true);
        }

        /* o app solicitando deve ser enjaulado */

        final String[] tgt = Urls.relativize(target).split("/");

        // alvo não tem condições de estar no contexto
        if (tgt.length < level) {
            return new JailInfo(src, tgt, false);
        }

        // trata se for uma requisição para a apiBase
        String tgtAppName = tgt[level - 1];
        if (tgtAppName.equalsIgnoreCase(apiBase)) {
            if (tgt.length <= level) {
                new JailInfo(src, tgt, false);
            }
            tgtAppName = tgt[level];
        }
        // bate o app de origem e destino
        final boolean jailed = tgtAppName.equalsIgnoreCase(srcAppName);
        return new JailInfo(src, tgt, jailed);
    }

    String buildRedirect(final String referer, final String target, final String query) {
        final String appName = Urls.relativize(referer).split("/")[level - 1];
        String url = basePath + appName + "/" + Urls.relativize(target);
        if (!Checks.isEmpty(query)) {
            url += "?" + query;
        }
        return url;
    }

    public void redirect(final HttpServletRequest request,
                         final HttpServletResponse response,
                         final String toUrl) throws IOException, ServletException {

//        Servlets.internalRedirect(
//                request,
//                response,
//                toUrl
//        );
        Servlets.redirectPermanently(
                response,
                toUrl
                                    );
//        response.sendRedirect(toUrl);
    }

    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            final HttpServletRequest httpRequest = (HttpServletRequest) request;
            final HttpServletResponse httpResponse = (HttpServletResponse) response;

            final RequestUrl requestUrl = Servlets.extractUrl(httpRequest);
            final String referer = requestUrl.getReferer();
            final String requestUrlPath = requestUrl.getPath();
            final JailInfo jailInfo = getJailInfo(referer, requestUrlPath);
            if (requestUrl.containsQuery("forceGo") || jailInfo.isJailed()) {
                if (isToApi(jailInfo)) {
                    final String toUrl = requestUrl.getContext() +
                            buildApiRedirect(jailInfo, requestUrlPath, requestUrl.getStringQuery());
                    redirect(httpRequest, httpResponse, toUrl);
                } else {
                    chain.doFilter(request, response);
                }
            } else {
                final String toUrl = requestUrl.getContext() +
                        buildRedirect(referer, requestUrlPath, requestUrl.getStringQuery());
                redirect(httpRequest, httpResponse, toUrl);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    private String buildApiRedirect(final JailInfo info, final String path, final String query) {
        final String appName = info.getSrc()[level - 1];
        String url = apiBase + "/" + appName +
                Urls.relativize(path).substring((basePath + appName + apiBase).length() + 1) + "/";
        if (!Checks.isEmpty(query)) {
            url += "?" + query;
        }
        return url;
    }

    private boolean isToApi(final JailInfo info) {
        final String[] tgt = info.getTgt();
        return tgt != null && tgt.length > level && tgt[level].equalsIgnoreCase(apiBase);
    }

    @Getter
    @RequiredArgsConstructor
    class JailInfo {

        private final String[] src;

        private final String[] tgt;

        private final boolean jailed;

        JailInfo(final boolean jailed) {
            this(null, null, jailed);
        }

        JailInfo(final String[] src, final boolean jailed) {
            this(src, null, jailed);
        }
    }

}
