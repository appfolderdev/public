package br.com.simpou.pub.commons.web.model;

import br.com.simpou.pub.commons.utils.behavior.TypeNamed;

public class DefaultTypeIdResolver extends AbstractTypeIdResolver {

    @Override
    public String idFromValueAndType(final Object obj, final Class<?> clazz) {
        if (TypeNamed.class.isAssignableFrom(clazz)) {
            return ((TypeNamed) obj).typeName();
        } else {
            return clazz.getSimpleName();
        }
    }
}
