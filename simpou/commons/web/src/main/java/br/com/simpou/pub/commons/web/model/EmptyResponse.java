package br.com.simpou.pub.commons.web.model;

import br.com.simpou.pub.commons.model.cache.DummyCacheable;
import br.com.simpou.pub.commons.model.serialize.ResponseObject;
import br.com.simpou.pub.commons.utils.behavior.Identifiable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

/**
 * Created by jonas.pereira on 21/04/16.
 */
@Getter
@RequiredArgsConstructor
public class EmptyResponse
        extends DummyCacheable
        implements ResponseObject, Identifiable<String> {

    @Override
    public String identity() {
        return this.getClass().getName();
    }

    @Override
    public void checkedId(final String id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void uncheckedId(final Serializable id) {
        throw new UnsupportedOperationException();
    }
}
