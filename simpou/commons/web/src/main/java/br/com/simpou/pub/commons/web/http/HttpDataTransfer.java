package br.com.simpou.pub.commons.web.http;

import br.com.simpou.pub.commons.model.cache.Cacheable;
import br.com.simpou.pub.commons.utils.lang.Nulls;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;

import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * Created by jonas.pereira on 25/11/15.
 */
@Getter
@CommonsLog
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class HttpDataTransfer<T> {

    private final T body;

    private final int status;

    private final String type;

    private final Map<String, List<String>> headers;

    private final String method;

    private final URI uri;

    private final Cacheable cacheInfo;

    public String getHeader(final String name) {
        return Nulls.get(headers.get(name), 0);
    }

    public static BasicHttpDataTransferBuilder newBuilder() {
        return new BasicHttpDataTransferBuilder();
    }

//    private static final PojoToFormParamsConversor POJO_TO_PARAMS_CONVERSOR = new PojoToFormParamsConversor(
//            AbstractPojoAndFormParamsConversor.ConversorContext.getBuilder().build()
//    );
//
//    public static <K extends Serializable, E extends Identifiable<K>>
//    HttpDataTransfer<Map<String, List<String>>> newSaveRequest(final String baseUrl,
//                                                               final E obj,
//                                                               final Class<E> type) {
//        final boolean isPost = obj.identity() == null;
//        final Class<Map<String, List<String>>> entityClass = Casts.simpleCast(Map.class);
//        return newBuilder(entityClass)
//                .uri(isPost ? baseUrl : baseUrl + "/" + obj.identity())
//                .method(isPost ? HttpMethod.POST : HttpMethod.PUT)
//                .body(POJO_TO_PARAMS_CONVERSOR.apply(obj))
//                .mime(HttpMimeType.BIN_FORM)
//                .build();
//    }
//
//    public static HttpDataTransfer<Void> newRetrieveRequest(final String url,
//                                                            final String accept) {
//        return newBuilder(Void.class)
//                .uri(url)
//                .method(HttpMethod.GET)
//                .mime(accept)
//                .build();
//    }
//
//    public static <K extends Serializable> HttpDataTransfer<Void> newRemoveRequest(final String baseUrl,
//                                                                                   final K id) {
//        return newBuilder(Void.class)
//                .uri(baseUrl + "/" + id)
//                .method(HttpMethod.DELETE)
//                .build();
//    }
//
//    public static HttpDataTransfer<String> newSaveResponse(
//            final int status,
//            final Object id) {
//        String value = id.toString();
//        if (!Numbers.isNumber(id)) {
//            value = "\"" + value + "\"";
//        }
//        return newBuilder(String.class)
//                .body("{\"id\" : " + value + "}")
//                .status(status)
//                .mime(HttpMimeType.JSON_UTF8)
//                .build();
//    }
//
//    public static <T> HttpDataTransfer<T> newFindResponse(final String url,
//                                                          final String accept) {
//        return null;
//    }
//
//    public static HttpDataTransfer<Void> newRemoveResponse(final String url) {
//        return null;
//    }

//    public HttpDataTransfer(final Object body,
//                            final int status,
//                            final String type,
//                            final Map<String, List<String>> headers) {
//        this.headers = Collections.unmodifiableMap(headers);
//        if (body instanceof ResponseObject) {
//            this.type = type;
//            this.status = status;
//            this.body = Serializations.objToJsonJackson(body);
//        } else {
//            this.type = HttpMimeType.TEXT_PLAIN.getValue();
//            this.status = StatusHttp.INTERNAL_SERVER_ERROR.value();
//            this.body = ("Server error. See logs.").getBytes();
//            log.error(
//                    "Unsupported response body type: "+body.getClass().getName()
//                            +", "+ResponseObject.class.getName()+" is required.");
//        }
//    }

}
