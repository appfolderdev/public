package br.com.simpou.pub.commons.web.websocket;



import br.com.simpou.pub.commons.utils.lang.Assertions;

import java.io.Serializable;

/**
 * Mensagem padrão trafegada via mecanismo de websocket. Use para enviar e receber mensagens.
 *
 * @author Jonas Pereira
 * @since 20/07/2017
 */
public class WebSocketMessage {

    private final byte opcode;

    private final Serializable payload;

    private final int fin;

    private final ClientLocation sender;

    /**
     * Comando para indicar que a mensagem é do tipo textual.
     */
    public static final byte OP_TEXT = 0x1;

    /**
     * Comando para indicar que a mensagem é do tipo binário.
     * <p>
     * NÂO USADO ATUALMENTE!
     */
    public static final byte OP_BINARY = 0x2;

    /**
     * Comando de solicitação, por parte de um cliente, para que uma conexão websocket seja finalizada pelo
     * servidor.
     */
    public static final byte OP_DISCONNECT = 0x8;

    //public static final byte OP_PING = 0x9;

    //public static final byte OP_PONG = 0xA;

    /**
     * Flag indicativo de que a mensagem é completa e única, não é necessário esperar por outras partes.
     */
    public static final int FIN_FULL = 1;

    /**
     * Flag indicativo de que a mensagem foi particionada. Destinatário deve esperar por outras partes.
     * <p>
     * NÃO USADO ATUALMENTE!
     */
    public static final int FIN_SPLIT = 0;

    /**
     * Nova mensagem do tipo textual e completa.
     *
     * @see #WebSocketMessage(byte, Serializable, ClientLocation)
     */
    public WebSocketMessage(final Serializable payload, final ClientLocation sender) {
        this(OP_TEXT, payload, sender);
    }

    /**
     * Nova mensagem completa.
     *
     * @param opcode  Ver {@link #getOpcode()}.
     * @param payload Ver {@link #getPayload()}.
     * @param sender  Ver {@link #getSender()}.
     * @throws IllegalArgumentException Se emissor null.
     */
    public WebSocketMessage(final byte opcode, final Serializable payload, final ClientLocation sender) {
        this.opcode = opcode;
        this.payload = payload;
        this.sender = Assertions.notNull(sender);
        this.fin = FIN_FULL;
    }

    /**
     * @return Define se a mensagem é quebrada em várias partes ou completa. Por padrão, são consideradas
     * apenas mensagens completas.
     * @see #FIN_FULL
     */
    public int getFin() {
        return fin;
    }

    /**
     * @return Tipo do conteúdo da mensagem. Tipos suportados: {@link #OP_TEXT} e {@link #OP_DISCONNECT}.
     */
    public byte getOpcode() {
        return opcode;
    }

    /**
     * @return Conteúdo da mensagem. Ao enviar e receber mensagens, é necessário definir um mecanismo de
     * conversão deste objeto de/para bytes. Logo, este conteúdo não é necessariamente aquele que irá trafegar
     * na mensagem em si, mas apenas uma representação. Ex.: tem-se um objeto DTO mas deseja-se trafegar
     * apenas um JSON correspondente a ele, cabendo ao receptor a conversão inversa.
     */
    public Serializable getPayload() {
        return payload;
    }

    /**
     * @return Emissor da mensagem.
     */
    public ClientLocation getSender() {
        return sender;
    }
}
