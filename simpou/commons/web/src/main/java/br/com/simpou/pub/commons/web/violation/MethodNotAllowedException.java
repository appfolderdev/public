package br.com.simpou.pub.commons.web.violation;

import br.com.simpou.pub.commons.web.http.StatusHttp;

public class MethodNotAllowedException extends AbstractBusinessViolationException {

    public MethodNotAllowedException() {
        super(StatusHttp.METHOD_NOT_ALLOWED.value(), DefaultViolationCodes.METHOD_NOT_ALLOWED);
    }
}
