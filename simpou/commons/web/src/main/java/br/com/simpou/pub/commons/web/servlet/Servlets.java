package br.com.simpou.pub.commons.web.servlet;

import br.com.simpou.pub.commons.utils.Constants;
import br.com.simpou.pub.commons.web.helper.CacheHelper;
import br.com.simpou.pub.commons.web.http.HttpHeaders;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.*;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * Created by jonas.pereira on 18/04/16.
 */
@UtilityClass
public class Servlets {

    public static void addCors(final String originAllowed, final HttpServletRequest request, final HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Credentials", "true");
        response.addHeader("Access-Control-Allow-Origin", originAllowed);

        final String reqHeaders = request.getHeader("Access-Control-Request-Headers");
        if(isNotEmpty(reqHeaders)) {
            response.addHeader("Access-Control-Allow-Headers", reqHeaders);
        }

        final String reqMethod = request.getHeader("Access-Control-Request-Method");
        if(isNotEmpty(reqMethod)) {
            response.addHeader("Access-Control-Allow-Methods", reqMethod);
        }
    }

    public static void addHeaders(final HttpServletResponse response,
                                  final Map<String, List<String>> headers) {
        for (final Map.Entry<String, List<String>> entry : headers.entrySet()) {
            response.setHeader(entry.getKey().toLowerCase(), entry.getValue().get(0));
        }
    }

    public static void addLocation(final HttpServletRequest request) {

    }

    public static RequestUrl extractUrl(final HttpServletRequest request) {
        return new RequestUrl(request);
    }

    public static String getReferer(final HttpServletRequest request) {
        return request.getHeader(HttpHeaders.REFERER);
    }

    /**
     * Extrai todos os headers para a forma canonica de chave e valores.
     */
    public static Map<String, List<String>> headersFrom(final HttpServletRequest request) {
        final Map<String, List<String>> headers = new HashMap<>();
        final Enumeration<String> headerNamesEnum = request.getHeaderNames();
        if (headerNamesEnum != null) {
            while (headerNamesEnum.hasMoreElements()) {
                final String headerName = headerNamesEnum.nextElement();
                final Enumeration<String> headerValueEnum = request.getHeaders(headerName);
                final List<String> headerValues = new ArrayList<>();
                headers.put(headerName.toLowerCase(), headerValues);
                while (headerValueEnum.hasMoreElements()) {
                    final String headerValue = headerValueEnum.nextElement();
                    headerValues.add(headerValue);
                }
            }
        }
        return headers;
    }

    @SneakyThrows
    public static void internalRedirect(final ServletRequest request,
                                        final ServletResponse response,
                                        final String url) {
        final RequestDispatcher dispatcher = request
                .getServletContext()
                .getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    public static void internalRedirect(final HttpServletRequest request,
                                        final HttpServletResponse response,
                                        final String url,
                                        final int status) {
        response.setStatus(status);
        response.setCharacterEncoding(Constants.DEFAULT_CHARSET);
        internalRedirect(request, response, url);
    }

    public static void redirectPermanently(final HttpServletResponse httpResponse,
                                           final String url) throws IOException {
        //httpResponse.reset();
        httpResponse.setHeader(HttpHeaders.LOCATION, url);
        httpResponse.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
        httpResponse.setHeader(HttpHeaders.EXPIRES, CacheHelper.generateMaxExpiresDate());
    }

    public static void writeBody(final HttpServletResponse response, final byte[] body) {
        try (
                final BufferedOutputStream output = new BufferedOutputStream(
                        response.getOutputStream(),
                        body.length
                )
        ) {
            response.setBufferSize(body.length);
            output.write(body);
            response.flushBuffer();
            response.addHeader(HttpHeaders.CONTENT_LENGTH, String.valueOf(body.length));
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }
}
