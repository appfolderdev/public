package br.com.simpou.pub.commons.web.filter;

import br.com.simpou.pub.commons.web.helper.Urls;
import br.com.simpou.pub.commons.web.http.HttpHeaders;
import br.com.simpou.pub.commons.web.http.HttpMimeType;
import br.com.simpou.pub.commons.web.servlet.RequestUrl;
import br.com.simpou.pub.commons.web.servlet.Servlets;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Possibilita acessar via url aplicações que monitaram o hash da página e trasformam em urls relativas.
 * Ex.: uma single page app rodando em /app tem uma sub-página /app/home que, se acessada diretamente via
 * url do navegador irá gerar 404 not found, já que está página não existe, ela é virtal do aplicação que
 * monitora este tipo de url.
 * <p>
 * Para estes casos, este filtro monitora os acessos e encaminha para o /app tratar a requisição e
 * processar a página virtual /app/home. São usados redirecionamento virtuais, logo o url de requisição não
 * é alterada.
 * <p>
 * O filtro trata apenas requisições cujo Accept header inicia-se por text/html.
 * <p>
 * As urlPatterns devem definir os caminhos das aplicações a serem consideradas. No caso do exemplo seria
 * portanto "/app/*".
 * <p>
 * Parâmetros:
 * - basePath: contexto base/root da requisição.
 */
public class PushstateHandlerFilter implements Filter {

    private String basePath;

    private int level;

    @Override
    public void destroy() {
    }

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        this.basePath = Urls.relativize(filterConfig.getInitParameter("basePath"));
        if (this.basePath.isEmpty()) {
            this.level = 1;
        } else {
            level = this.basePath.split("/").length + 1;
        }
    }

    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            final HttpServletRequest httpRequest = (HttpServletRequest) request;

            // trata apenas requisições de páginas para recursos
            final String acceptHeader = httpRequest.getHeader(HttpHeaders.ACCEPT);
            if (acceptHeader == null || !acceptHeader.startsWith(HttpMimeType.TEXT_HTML.getValue())) {
                chain.doFilter(request, response);
                return;
            }

            final RequestUrl reqUrl = Servlets.extractUrl(httpRequest);
            final String[] parts = Urls.relativize(reqUrl.getPath()).split("/");
            if (parts.length > level && !parts[level].equalsIgnoreCase("index.html")) {
                final String basePath = this.basePath.isEmpty() ? "" : this.basePath + "/";
                Servlets.internalRedirect(
                        request,
                        response,
                        reqUrl.getContext() + basePath + parts[level - 1] + "/"
                );
            } else {
                chain.doFilter(request, response);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

}
