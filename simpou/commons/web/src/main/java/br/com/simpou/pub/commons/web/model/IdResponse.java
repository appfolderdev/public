package br.com.simpou.pub.commons.web.model;

import br.com.simpou.pub.commons.model.cache.DummyCacheable;
import br.com.simpou.pub.commons.model.entity.EntityConstants;
import br.com.simpou.pub.commons.model.mixin.BaseEntityMixin;
import br.com.simpou.pub.commons.model.serialize.ResponseObject;
import br.com.simpou.pub.commons.model.view.GlobalView;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Created by jonas on 26/01/17.
 */
@Getter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonRootName(value = ResponseObject.ROOT_NAME)
@XmlRootElement(name = ResponseObject.ROOT_NAME)
public class IdResponse extends DummyCacheable implements ResponseObject {

    @JsonView(GlobalView.class)
    private final ResponseMetadata meta;

    @JsonView(GlobalView.class)
    private final Data data;

    @JsonCreator
    public IdResponse(
            @JsonProperty(BaseEntityMixin.TYPE_NAME) final String type,
            @JsonProperty(EntityConstants.ID_NAME) final Serializable id
    ) {
        this.meta = new ResponseMetadata(null, type, true);
        this.data = new Data(type, id);
    }

    @JsonIgnore
    public Serializable getId(){
        return this.data.getId();
    }
}

@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
class Data implements Serializable {

    @JsonProperty(ResponseObject.JSON_ITEM_TYPE_NAME)
    @JacksonXmlProperty(localName = ResponseObject.XML_ITEM_TYPE_NAME, isAttribute = true)
    private String type;

    @JsonDeserialize(using = StringDeserializer.class)
    private Serializable id;
}

