package br.com.simpou.pub.commons.web.websocket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QueryParameters {

    private static final String pattern = "((\\w+)([\\[](\\w+)[\\]])?)=(\\w+)";

    private final Value params;

    public QueryParameters(final String query) {
        this.params = new Value(null);
        if (query != null) {
            final Pattern pattern = Pattern.compile(QueryParameters.pattern);
            final Matcher matcher = pattern.matcher(query);

            while (matcher.find()) {
                final String name = matcher.group(2);
                final String childName = matcher.group(4);
                final String value = matcher.group(5);

                addChild(params, childName, name, value);
            }
        }
    }

    private static void addChild(final Value params,
                                 final String childName,
                                 final String name,
                                 final String value) {
        final Value child;
        if (params.childs.containsKey(name)) {
            child = (Value) params.childs.get(name);
        } else {
            child = new Value(childName);
            params.childs.put(name, child);
        }
        if (childName == null) {
            child.values.add(value);
        } else {
            addChild(child, null, childName, value);
        }
    }

    public Value get(final String key) {
        if (params.childs.containsKey(key)) {
            return (Value) params.childs.get(key);
        } else {
            return Value.simple(null);
        }
    }

    public static class Value {

        private final String name;

        private List values = new ArrayList();

        private final Map childs = new HashMap();

        private Value(final String name) {
            this.name = name;
        }

        public static Value simple(String value) {
            final Value objValue = new Value(null);
            objValue.values.add(value);
            return objValue;
        }

        public String first() {
            return values.size() < 1 ? null : (String) values.get(0);
        }

        public String[] values() {
            return (String[]) values.toArray(new String[values.size()]);
        }
    }
}
