package br.com.simpou.pub.commons.web.model;

import br.com.simpou.pub.commons.model.mixin.BaseEntityMixin;
import br.com.simpou.pub.commons.model.view.GlobalView;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;

/**
 * Created by jonas on 10/03/17.
 */

@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseMetadata {

    @JsonView(GlobalView.class)
    private final Pagination pagination;

//    private final Long lastModified;
//
//    private final String type;

    @JsonView(GlobalView.class)
    private final boolean success;

    @JsonView(GlobalView.class)
    private final boolean array;

    @JsonCreator
    public ResponseMetadata(
            @JsonProperty("lastModified") final Long lastModified,
            @JsonProperty(BaseEntityMixin.TYPE_NAME) final String type,
            @JsonProperty("success") final boolean success
    ) {
        this(null, lastModified, type, success, false);
    }

    public ResponseMetadata(final Pagination pagination,
                            final Long lastModified,
                            final String type,
                            final boolean success,
                            final boolean isList) {
        this.pagination = pagination;
//        this.lastModified = lastModified;
//        this.type = type;
        this.success = success;
        this.array = isList;
    }

    @Getter
    public static class Pagination {
        @JsonView(GlobalView.class)
        private final Integer totalPages;
        @JsonView(GlobalView.class)
        private final Long totalElements;
        @JsonView(GlobalView.class)
        private final Integer elements;

        @JsonView(GlobalView.class)
        private final Boolean last;
        @JsonView(GlobalView.class)
        private final Boolean first;

        @JsonView(GlobalView.class)
        private final Integer page;

        @JsonCreator
        public Pagination(@JsonProperty("totalPages") final Integer totalPages,
                          @JsonProperty("totalElements") final Long totalElements,
                          @JsonProperty("elements") final Integer elements,
                          @JsonProperty("last") final Boolean last,
                          @JsonProperty("first") final Boolean first,
                          @JsonProperty("page") final Integer page) {
            this.totalPages = totalPages;
            this.totalElements = totalElements;
            this.elements = elements;
            this.last = last;
            this.first = first;
            this.page = page;
        }

        public Pagination(final Integer elements) {
            this.totalPages = 1;
            this.totalElements = elements.longValue();
            this.elements = elements;
            this.last = true;
            this.first = true;
            this.page = 0;
        }
    }
}
