package br.com.simpou.pub.commons.web.http;

/**
 * Created by jonas on 22/04/2016.
 */
public class BasicHttpDataTransferBuilder extends HttpDataTransferBuilder<byte[], HttpDataTransfer<byte[]>> {

    public HttpDataTransfer<byte[]> build() {
        return new HttpDataTransfer<>(
                getBody(),
                getStatus(),
                getType(),
                getHeaders(),
                getMethod(),
                getUri(),
                getCacheable()
        );
    }

}
