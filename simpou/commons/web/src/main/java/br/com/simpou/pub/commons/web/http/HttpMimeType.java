package br.com.simpou.pub.commons.web.http;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * Created by jonas.pereira on 31/03/16.
 */
@RequiredArgsConstructor
@Getter
@ToString(of = "value")
public enum HttpMimeType {

    JSON_UTF8(HttpMimeType.JSON_UTF8_VALUE, HttpMimeKind.TEXT),
    XML_UTF8(HttpMimeType.XML_UTF8_VALUE, HttpMimeKind.TEXT),

    BIN_FORM("application/x-www-form-urlencoded", HttpMimeKind.BINARY),

    TEXT_JSON("application/json", HttpMimeKind.TEXT),
    TEXT_XML("application/xml", HttpMimeKind.TEXT),
    TEXT_PLAIN("plain/text", HttpMimeKind.TEXT),
    TEXT_HTML("text/html", HttpMimeKind.TEXT),
    TEXT_CSS("text/css", HttpMimeKind.TEXT),
    TEXT_JS("application/javascript", HttpMimeKind.TEXT),

    IMAGE_GIF("image/gif", HttpMimeKind.IMAGE),
    IMAGE_JPEG("image/jpeg", HttpMimeKind.IMAGE),
    IMAGE_PNG("image/png", HttpMimeKind.IMAGE),
    IMAGE_BMP("image/bmp", HttpMimeKind.IMAGE),

    VIDEO_FLV("video/x-flv", HttpMimeKind.VIDEO),
    VIDEO_MP4("video/mp4", HttpMimeKind.VIDEO),
    VIDEO_M3U8("application/x-mpegURL", HttpMimeKind.VIDEO),
    VIDEO_TS("video/MP2T", HttpMimeKind.VIDEO),
    VIDEO_3GP("video/3gpp", HttpMimeKind.VIDEO),
    VIDEO_MOV("video/quicktime", HttpMimeKind.VIDEO),
    VIDEO_AVI("video/x-msvideo", HttpMimeKind.VIDEO),
    VIDEO_WMV("video/x-ms-wmv", HttpMimeKind.VIDEO),
    VIDEO_WEBM("video/webm", HttpMimeKind.VIDEO),
    VIDEO_OGG("video/ogg", HttpMimeKind.VIDEO),

    AUDIO_MIDI("audio/midi", HttpMimeKind.AUDIO),
    AUDIO_MPEG("audio/mpeg", HttpMimeKind.AUDIO),
    AUDIO_WEBM("audio/webm", HttpMimeKind.AUDIO),
    AUDIO_OGG("audio/ogg", HttpMimeKind.AUDIO),
    AUDIO_WAV("audio/wav", HttpMimeKind.AUDIO);

    public static final String JSON_UTF8_VALUE = "application/json;charset=utf-8";

    public static final String XML_UTF8_VALUE = "application/xml;charset=utf-8";

    private final String value;

    private final HttpMimeKind kind;
}
