package br.com.simpou.pub.commons.web.helper;

import br.com.simpou.pub.commons.model.cache.BasicCacheable;
import br.com.simpou.pub.commons.model.cache.Cacheable;
import br.com.simpou.pub.commons.model.cache.CacheableHelper;
import br.com.simpou.pub.commons.utils.cipher.Hashs;
import br.com.simpou.pub.commons.utils.lang.Assertions;
import br.com.simpou.pub.commons.utils.lang.Checks;
import br.com.simpou.pub.commons.utils.lang.Dates;
import br.com.simpou.pub.commons.utils.lang.Nulls;
import br.com.simpou.pub.commons.utils.violation.BusinessViolation;
import br.com.simpou.pub.commons.utils.violation.ViolationsException;
import br.com.simpou.pub.commons.web.http.HttpDataTransferBuilder;
import br.com.simpou.pub.commons.web.http.HttpHeaders;
import br.com.simpou.pub.commons.web.http.StatusHttp;
import br.com.simpou.pub.commons.web.violation.DefaultViolationCodes;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Jonas Pereira
 * @since 27/02/14.
 */
public final class CacheHelper {

    /**
     * Pattern para formatador de datas exigido por cache http headers.
     */
    private static final SimpleDateFormat CACHE_DF = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");

    /**
     * Sempre deve consultar servidor para novos dados, nunca aguardar tempo nenhum com dados em cache e sem
     * consultar servidor.
     */
    private static final String CACHE_CONTROL_VALUE = "private, must-revalidate, max-age=0";

    /**
     * Classe utilitária.
     */
    private CacheHelper() {
    }

    /**
     * @param lastModified    Data da última alteração do objeto de resposta.
     * @param objectTag       Identificador ETag do objeto de resposta.
     * @param headers         Headers da requisição HTTP.
     * @param responseFactory Função que irá gerar o corpo da resposta caso cliente não esteja atualizado.
     * @throws Exception Não previsto.
     */
    public static <T> void doCaching(final HttpDataTransferBuilder<T, ?> respBuilder,
                                     final Date lastModified,
                                     final String objectTag,
                                     final Map<String, List<String>> headers,
                                     final ResponseFactory<T> responseFactory) {
        Assertions.notNull(responseFactory);

        final CacheHelper.HttpCacheInfo cacheInfo = CacheHelper.getCacheInfo(
                lastModified,
                objectTag,
                headers
        );

        if (cacheInfo.isDisabled()) {
            // objeto não cacheável
            respBuilder.body(responseFactory.getResponseBody());
        } else {
            if (cacheInfo.isClientCacheAvailable()) {
                if (cacheInfo.isClientUpdated()) {
                    // objeto em cache no cliente, nada a ser feito
                    respBuilder.status(StatusHttp.NOT_MODIFIED.value());
                } else {
                    // objeto em cache do cliente desatualizado, informa novos valores e atualiza infos de cache
                    getCachedResponse(
                            respBuilder,
                            cacheInfo.getEtag(),
                            cacheInfo.getObjLastModified(),
                            responseFactory
                    );
                }
            } else {
                // objeto suporta cache mas cliente não conhece, inicializa dados de cache no cliente,
                // próxima requisição para o mesmo recurso já será cacheada
                getCachedResponse(
                        respBuilder,
                        cacheInfo.getEtag(),
                        cacheInfo.getObjLastModified(),
                        responseFactory
                );
            }
        }
    }

    public static Cacheable extractCacheable(final HttpServletRequest request) {
        Date lastModified;
        try {
            lastModified = CacheHelper.parseDate(request.getHeader(HttpHeaders.IF_MODIFIED_SINCE));
        } catch (final ParseException e) {
            lastModified = null;
        }
        return new BasicCacheable(
                lastModified,
                request.getHeader(HttpHeaders.IF_NONE_MATCH)
        );
    }

    /**
     * @param date Data.
     * @return String de data no formato suportado pelo cache http.
     */
    public static synchronized String formatDate(final Date date) {
        return CACHE_DF.format(date);
    }

    public static String generateMaxExpiresDate() {
        return CacheHelper.formatDate(Dates.addDate(new Date(), 1, 0, 0));
    }

    public static HttpCacheInfo getCacheInfo(final Cacheable cacheable,
                                             final Map<String, List<String>> headers) {
        return getCacheInfo(cacheable.lastModified(), cacheable.objectTag(), headers);
    }

    /**
     * Analisa e disponibiliza todas as informações de cacheamento da requisição.
     */
    public static HttpCacheInfo getCacheInfo(final Date lastModified,
                                             final String objectTag,
                                             final Map<String, List<String>> headers) {

        // headers não são case sensitive, isso causa problemas na hora de comparar as chaves
        final Map<String, List<String>> lowerCaseHeaders = new HashMap<>();
        for (final Map.Entry<String, List<String>> entry : headers.entrySet()) {
            lowerCaseHeaders.put(entry.getKey().toLowerCase(), entry.getValue());
        }

        final HttpCacheInfo result;
        if (CacheableHelper.isCacheDisabled(lastModified, objectTag)) {
            // objeto não cacheável
            result = new HttpCacheInfo();
        } else {
            final String etag = Hashs.getHashNoErrors(objectTag);
            final String reqEtag = Nulls.get(lowerCaseHeaders.get(HttpHeaders.IF_NONE_MATCH.toLowerCase()), 0);
            final String reqLastModifiedHeader = Nulls.get(lowerCaseHeaders.get(HttpHeaders.IF_MODIFIED_SINCE.toLowerCase()), 0);
            if (reqLastModifiedHeader != null) {
                final Date reqLastModified;
                try {
                    reqLastModified = parseDate(reqLastModifiedHeader);
                } catch (final ParseException e) {
                    throw new ViolationsException(
                            new BusinessViolation(
                                    DefaultViolationCodes.INVALID_DATE_FORMAT,
                                    reqLastModifiedHeader
                            )
                    );
                }
                result = new HttpCacheInfo(etag, reqEtag, lastModified, reqLastModified);
            } else {
                result = new HttpCacheInfo(etag, reqEtag, lastModified, null);
            }
        }
        return result;
    }

    private static <T> void getCachedResponse(
            final HttpDataTransferBuilder<T, ?> respBuilder,
            final String etag,
            final Date lastModified,
            final ResponseFactory<T> responseFactory
    ) {
        respBuilder.headers(getCachedResponseHeaders(
                etag,
                lastModified
        ));
        if (responseFactory != null) {
            respBuilder.body(responseFactory.getResponseBody());
        }
    }

    public static Map<String, List<String>> getCachedResponseHeaders(final Cacheable cacheable) {
        return getCachedResponseHeaders(
                cacheable.objectTag(),
                cacheable.lastModified()
        );
    }

    public static Map<String, List<String>> getCachedResponseHeaders(final String etag,
                                                                     final Date lastModified) {
        final Map<String, List<String>> headers = new HashMap<>();
        headers.put(HttpHeaders.ETAG, Collections.singletonList(etag));

        headers.put(
                HttpHeaders.CACHE_CONTROL,
                Collections.singletonList(CACHE_CONTROL_VALUE)
        );

        headers.put(
                HttpHeaders.EXPIRES,
//                Collections.singletonList(formatDate(Dates.addTime(new Date(), 1, 0, 0)))
                Collections.singletonList("0")
        );

        headers.put(HttpHeaders.LAST_MODIFIED, Collections.singletonList(formatDate(lastModified)));
        return headers;
    }

    /**
     * Para cobertura de instanciação de classe utilitária.
     */
    static void newInstance() {
        new CacheHelper();
    }

    /**
     * @param date String de data no formato suportado pelo cache http.
     * @return Objeto data correspondente.
     * @throws ParseException Erro na conversão.
     */
    public static synchronized Date parseDate(final String date) throws ParseException {
        if (Checks.isEmpty(date)) {
            return null;
        }
        return CACHE_DF.parse(date);
    }

    public interface ResponseFactory<T> {

        T getResponseBody();

    }

    @RequiredArgsConstructor
    public static class EchoResponseFactory<T> implements ResponseFactory<T> {

        final T response;

        @Override
        public T getResponseBody() {
            return this.response;
        }

    }

    /**
     * Classe para lidar com informação da cache.
     */
    public static class HttpCacheInfo {

        /**
         * Identificador do objeto no servidor.
         */
        @Getter
        private final String etag;

        /**
         * Identificador do objeto no cliente.
         */
        //@Getter
        private final String reqEtag;

        /**
         * @see #getObjLastModified()
         */
        private final Date objLastModified;

        /**
         * @see #getReqLastModified()
         */
        private final Date reqLastModified;

        /**
         * Inicializa todos campos com null.
         */
        public HttpCacheInfo() {
            this.etag = null;
            this.reqEtag = null;
            this.objLastModified = null;
            this.reqLastModified = null;
        }

        /**
         * @param etag            {@link #etag}.
         * @param reqEtag         {@link #reqEtag}.
         * @param objLastModified {@link #getObjLastModified()}.
         * @param reqLastModified {@link #getReqLastModified()}.
         */
        public HttpCacheInfo(final String etag,
                             final String reqEtag,
                             final Date objLastModified,
                             final Date reqLastModified) {
            this.etag = etag;
            this.reqEtag = reqEtag;
            this.objLastModified = objLastModified == null ? null : (Date) objLastModified.clone();
            this.reqLastModified = reqLastModified == null ? null : (Date) reqLastModified.clone();
        }

        public Cacheable getCacheable() {
            return new BasicCacheable(this.objLastModified, this.etag);
        }

        /**
         * @return Data de última modificação do objeto no servidor.
         */
        public Date getObjLastModified() {
            return this.objLastModified == null ? null : (Date) this.objLastModified.clone();
        }

        /**
         * @return Data de última modificação do objeto no cliente.
         */
        public Date getReqLastModified() {
            return this.reqLastModified == null ? null : (Date) this.reqLastModified.clone();
        }

        /**
         * Verifica se a cache do cliente está disponível.
         *
         * @return boolean
         */
        public boolean isClientCacheAvailable() {
            return this.reqEtag != null && this.reqLastModified != null;
        }

        /**
         * Verifica se o cliente está atualizado.
         *
         * @return boolean.
         */
        public boolean isClientUpdated() {
            // http caches são consideradas a nível de segundos, se considerar milissegundos falha a comparação
            final long milliToSec = 1000;
            return Nulls.equals(this.etag, this.reqEtag)
                    && this.reqLastModified.getTime() / milliToSec >= this.objLastModified.getTime() / milliToSec;
        }

        /**
         * Verifica se está desabilitado.
         *
         * @return boolean
         */
        public boolean isDisabled() {
            return this.etag == null || this.objLastModified == null;
        }
    }

}
