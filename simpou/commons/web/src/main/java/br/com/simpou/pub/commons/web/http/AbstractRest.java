package br.com.simpou.pub.commons.web.http;

import br.com.simpou.pub.commons.model.cache.Cacheable;
import br.com.simpou.pub.commons.utils.Constants;
import br.com.simpou.pub.commons.utils.lang.ArraysHelper;
import br.com.simpou.pub.commons.utils.string.Strings;
import br.com.simpou.pub.commons.utils.violation.BusinessViolation;
import br.com.simpou.pub.commons.utils.violation.BusinessViolationException;
import br.com.simpou.pub.commons.utils.violation.ViolationsException;
import br.com.simpou.pub.commons.web.helper.CacheHelper;
import br.com.simpou.pub.commons.web.helper.FormParamsToPojoConversor;
import br.com.simpou.pub.commons.web.servlet.Servlets;
import br.com.simpou.pub.commons.web.violation.DefaultViolationCodes;
import br.com.simpou.pub.commons.web.violation.ResourceNotFoundException;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Classe base para serviços REST.
 *
 * @author Jonas Pereira
 * @since 13/08/2014.
 */
public class AbstractRest {

    protected <T> Object getResponse(final Object obj,
                                     final Date lastModified,
                                     final String objectTag,
                                     final HttpServletRequest httpRequest,
                                     final HttpDataTransferBuilder<T, ?> builder,
                                     final CacheHelper.ResponseFactory<T> responseFactory) throws Exception {
        if (obj == null) {
            throw new ResourceNotFoundException();
        }
        return this.getResponse(lastModified, objectTag, httpRequest, builder, responseFactory);
    }

    protected <T> Object getResponse(final Date lastModified,
                                     final String objectTag,
                                     final HttpServletRequest httpRequest,
                                     final HttpDataTransferBuilder<T, ?> builder,
                                     final CacheHelper.ResponseFactory<T> responseFactory) {
        CacheHelper.doCaching(
                builder,
                lastModified,
                objectTag,
                Servlets.headersFrom(httpRequest),
                responseFactory
        );
        return builder.build();
    }

    protected <T> Object getResponse(
            final T obj,
            final Date lastModified,
            final String objectTag,
            final HttpServletRequest httpRequest,
            final HttpDataTransferBuilder<T, ?> builder)
            throws Exception {
        return getResponse(obj,
                lastModified,
                objectTag,
                httpRequest,
                builder,
                new CacheHelper.EchoResponseFactory<>(obj)
        );
    }

    protected <T> Object getResponse(final Cacheable obj,
                                     final HttpServletRequest httpRequest,
                                     final HttpDataTransferBuilder<T, ?> builder,
                                     final CacheHelper.ResponseFactory<T> responseFactory) throws Exception {
        return this.getResponse(obj, obj.lastModified(), obj.objectTag(), httpRequest, builder, responseFactory);
    }

    protected <T extends Cacheable> Object getResponse(
            final T obj,
            final HttpServletRequest httpRequest,
            final HttpDataTransferBuilder<T, ?> builder) throws Exception {
        return getResponse(
                obj,
                obj.lastModified(),
                obj.objectTag(),
                httpRequest,
                builder
        );
    }

    /**
     * Dados os parâmetros de uma requisição POST ou PUT, converte isto para um objeto.
     *
     * @param params   Parâmetros da requisição. Correspondentes aos campos do formulário que está sendo
     *                 postado.
     * @param objClass Classe do objeto esperado.
     * @param <T>      Tipo do objeto esperado.
     * @return Objeto do tipo especificado preenchido com os valores do formulário submetido.
     * @throws br.com.simpou.pub.commons.utils.violation.ViolationsException Erros diversos na conversão.
     */
    private <T> T extractObjectFromParamMap(final Class<T> objClass,
                                            final Map<String, String[]> params) {
        final FormParamsToPojoConversor<T> conversor = new FormParamsToPojoConversor<>(objClass);
        final Map<String, List<String>> multMap = new HashMap<>();
        for (final Map.Entry<String, String[]> entry : params.entrySet()) {
            multMap.put(entry.getKey(), Arrays.asList(entry.getValue()));
        }
        final T obj;
        try {
            obj = conversor.apply(multMap);
        } catch (Exception e) {
            throw new BusinessViolationException(
                    DefaultViolationCodes.INVALID_POST_PUT_PARAMS,
                    e.getMessage()
            );
        }
        return obj;
    }

    /**
     * Use para POSTs e PUTs para obter o objeto de uma requisição a partir dos parâmetros passados.
     * <p/>
     * Não use caso requisição tenha parâmetros explícitos na URL que não irão para o objeto de retorno.
     * <p/>
     * O spring não converte corretamente JSONs complexos alvos de requisições no formato form url params e
     * nem trata corretamente parâmetros PUTs, que vão no payload incorretamente.
     * <p/>
     * Para casos simples, use o próprio conversor do spring que já resolve.
     *
     * @param httpRequest Requisição HTTP.
     * @param objClass    Classe do objeto esperado.
     * @param <T>         Tipo do objeto esperado.
     * @return Objeto do tipo especificado preenchido com os valores do formulário submetido.
     * @throws br.com.simpou.pub.commons.utils.violation.ViolationsException Erro ao obter stream da requisição.
     */
    protected <T> T extractObjectFromParamMap(final HttpServletRequest httpRequest,
                                              final Class<T> objClass) {

        final Map<String, String[]> parameterMap = httpRequest.getParameterMap();
        if (parameterMap.isEmpty()) {
            final ServletInputStream inputStream;
            try {
                inputStream = httpRequest.getInputStream();
            } catch (Exception e) {
                throw new ViolationsException(new BusinessViolation(
                        DefaultViolationCodes.INVALID_PAYLOAD_PARAMS,
                        e.getMessage()
                ));
            }
            return extractObjectFromPayload(inputStream, objClass);
        } else {
            return extractObjectFromParamMap(objClass, parameterMap);
        }
    }

    /**
     * Use para métodos PUT que recebem parâmetros tanto pelo URL como pelo payload da requisição.
     *
     * @param sis      Payload da requisição. Deve conter string no formato de parâmetros de URL.
     * @param objClass Classe do objeto esperado.
     * @param <T>      Tipo do objeto esperado.
     * @return Objeto do tipo especificado preenchido com os valores extraídos do corpo da requisição.
     */
    private <T> T extractObjectFromPayload(final ServletInputStream sis,
                                           final Class<T> objClass) {
        final Map<String, String[]> reqParameterMap;
        try {
            reqParameterMap = extractParamsFromPayload(sis);
        } catch (Exception e) {
            throw new ViolationsException(new BusinessViolation(
                    DefaultViolationCodes.INVALID_PAYLOAD_PARAMS,
                    e.getMessage()
            ));
        }
        return extractObjectFromParamMap(objClass, reqParameterMap);
    }

    /**
     * @param is Payload da requisição.
     * @return Parâmetros de URL colocados erroneamente no payload pelo spring.
     * @throws IOException Problemas ao ler payload.
     */
    private Map<String, String[]> extractParamsFromPayload(final InputStream is) throws IOException {
        final String payload = Strings.extract(is, Constants.DEFAULT_CHARSET);
        final String[] parts = payload.split("&");
        final Map<String, String[]> params = new HashMap<>();
        for (final String part : parts) {
            final String[] keyValue;
            {
                final String[] tmpKeyValue = part.split("=");
                if (tmpKeyValue.length < 2) {
                    keyValue = new String[]{tmpKeyValue[0], ""};
                } else {
                    keyValue = tmpKeyValue;
                }
            }
            final String key = java.net.URLDecoder.decode(keyValue[0], Constants.DEFAULT_CHARSET);
            final String value = java.net.URLDecoder.decode(keyValue[1], Constants.DEFAULT_CHARSET);
            if (params.containsKey(key)) {
                params.put(key, ArraysHelper.append(params.get(key), value));
            } else {
                params.put(key, new String[]{value});
            }
        }
        return params;
    }

}
