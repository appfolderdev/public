package br.com.simpou.pub.commons.web.websocket;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class SocketRequest implements ResolvedRequest {

    private final Log logger = LogFactory.getLog(SocketRequest.class);

    private final String method;

    private final String path;

    private final Map headers;

    SocketRequest(final InputStream in) throws IOException {
        final String requestString = StreamUtils.readLines(in, false);
        logger.debug("Requisição websocket recebida. Conteúdo:\n" + requestString);

        final String[] requestStringLines = requestString.split("\n");
        {
            final String[] split = requestStringLines[0].split(" ");
            method = split[0];
            path = split[1];
        }
        {
            final Map headers = new HashMap();
            for (int i = 1; i < requestStringLines.length; i++) {
                final String line = requestStringLines[i];
                final int indexOf = line.indexOf(":");
                if (indexOf < 0) {
                    continue;
                }
                final String key = line.substring(0, indexOf);
                final String value = line.substring(indexOf + 2, line.length());
                headers.put(key, value);
            }
            this.headers = Collections.unmodifiableMap(headers);
        }
    }

//    private static String getHeaderValue(final String name, final String request, final OutputStream out) throws
//            IOException {
//        Matcher match = Pattern.compile(name + ": (.*)").matcher(request);
//        if (match.find()) {
//            return match.group(1);
//        } else {
//            return null;
//        }
//    }

    public String getResourcePath() {
        return path;
    }

    public String getMethod() {
        return method;
    }

    public boolean containsParam(final String param) {
        throw new UnsupportedOperationException();
    }

    public Integer getUserId() {
        throw new UnsupportedOperationException();
    }

    public Integer getPoloId() {
        throw new UnsupportedOperationException();
    }

    public String getHeader(final String key) {
        return containsHeader(key) ? headers.get(key).toString() : null;
    }

    public boolean containsHeader(final String key) {
        return headers.containsKey(key);
    }

//    public ResolvedSession getSession() {
//        return new SocketSession();
//    }

    public QueryParameters.Value getParameter(final String key) {
        final Matcher match = Pattern.compile(key + "=(.*)").matcher(path);
        if (match.find()) {
            return QueryParameters.Value.simple(match.group(1).split("&")[0]);
        } else {
            return null;
        }
    }

    public Map getBodyValues() {
        throw new UnsupportedOperationException();
    }

    public String getServerName() {
        throw new UnsupportedOperationException();
    }

    public String getPathElement(final int i) {
        throw new UnsupportedOperationException();
    }

    public long getLongId() {
        throw new UnsupportedOperationException();
    }

    public String getId() {
        throw new UnsupportedOperationException();
    }

//    public SessionInfo getSessionInfo() {
//        throw new UnsupportedOperationException();
//    }

    public Object payload(final Class type) {
        throw new UnsupportedOperationException();
    }

    public Serializable getBody(final Class type) {
        throw new UnsupportedOperationException();
    }
}
