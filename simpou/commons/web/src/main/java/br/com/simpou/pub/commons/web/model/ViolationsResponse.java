package br.com.simpou.pub.commons.web.model;

import br.com.simpou.pub.commons.utils.violation.Violation;
import com.fasterxml.jackson.annotation.JsonRootName;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

@JsonRootName(value = "response")
@XmlRootElement(name = "response")
public class ViolationsResponse extends ListResponse<Violation> {

    public ViolationsResponse(final Collection<Violation> data) {
        super(data, null, null, new ResponseMetadata.Pagination(data.size()), "violation", false);
    }
}
