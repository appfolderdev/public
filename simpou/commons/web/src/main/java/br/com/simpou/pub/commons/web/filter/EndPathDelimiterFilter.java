package br.com.simpou.pub.commons.web.filter;

import br.com.simpou.pub.commons.web.servlet.Servlets;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Redireciona permanente para a mesma URL de requisição, mas com o delimitador no final.
 * <p/>
 * Created by jonas on 11/01/2017.
 */
public class EndPathDelimiterFilter implements Filter {

    @Override
    public void destroy() {
    }

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            final HttpServletRequest httpRequest = (HttpServletRequest) request;
            final String url = httpRequest.getRequestURL().toString();
            if (url.endsWith("/")) {
                chain.doFilter(request, response);
            } else {
                final HttpServletResponse httpResponse = (HttpServletResponse) response;
                Servlets.redirectPermanently(httpResponse,url + "/");
            }
        } else {
            chain.doFilter(request, response);
        }
    }

}
