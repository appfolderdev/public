package br.com.simpou.pub.commons.web.model;

import lombok.Getter;

import java.io.Serializable;

@Getter
public class ObjectResponseDeserialize<T> implements Serializable {

    private T data;

}
