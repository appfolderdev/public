package br.com.simpou.pub.commons.web.http;

import br.com.simpou.pub.commons.model.entity.rest.AbstractEntity;
import br.com.simpou.pub.commons.model.entity.rest.AbstractRestEntity;
import br.com.simpou.pub.commons.model.entity.rest.RestEntity;
import br.com.simpou.pub.commons.web.exception.EntityRequiredException;
import br.com.simpou.pub.commons.web.exception.RequiredParamException;
import br.com.simpou.pub.commons.web.violation.ResourceNotFoundException;
import lombok.experimental.UtilityClass;

import java.util.Collection;

@UtilityClass
public class RestChecks {

    public <T extends RestEntity<?>> T checkFound(final T entity) {
        if (entity == null) {
            throw new ResourceNotFoundException();
        }
        return entity;
    }

    public <T extends RestEntity<?>> T checkRequired(final T entity) {
        if (!entity.exists()) {
            throw new EntityRequiredException(entity);
        }
        return entity;
    }

    public void checkExistentEntityParam(final AbstractEntity entity, final String field) {
        if (AbstractRestEntity.exists(entity)) {
            throw new RequiredParamException(field);
        }
    }

    public void checkRequiredParam(final String name, final Object value) {
        if (value == null) {
            throw new RequiredParamException(name);
        } else if (value instanceof Collection) {
            if (((Collection) value).isEmpty()) {
                throw new RequiredParamException(name);
            }
        }
    }

}
