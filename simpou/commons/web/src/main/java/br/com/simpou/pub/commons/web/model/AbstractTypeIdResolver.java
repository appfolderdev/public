package br.com.simpou.pub.commons.web.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DatabindContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;

import java.io.IOException;

public abstract class AbstractTypeIdResolver implements TypeIdResolver {

    private JavaType javaType;

    @Override
    public void init(final JavaType javaType) {
        this.javaType = javaType;
    }

    @Override
    public String idFromValue(final Object obj) {
        return idFromValueAndType(obj, obj.getClass());
    }

    @Override
    public String idFromBaseType() {
        throw new UnsupportedOperationException();
    }

    @Override
    public JavaType typeFromId(final DatabindContext databindContext, final String type) throws IOException {
//        Class<?> clazz;
//        String clazzName = "." + type;
//        try {
//            clazz = ClassUtil.findClass(clazzName);
//        } catch (ClassNotFoundException e) {
//            throw new IllegalStateException("cannot find class '" + clazzName + "'");
//        }
//        return TypeFactory.defaultInstance().constructSpecializedType(javaType, clazz);
        throw new UnsupportedOperationException();
    }

    @Override
    public String getDescForKnownTypeIds() {
        throw new UnsupportedOperationException();
    }

    @Override
    public JsonTypeInfo.Id getMechanism() {
        return JsonTypeInfo.Id.CUSTOM;
    }
}
