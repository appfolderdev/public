package br.com.simpou.pub.commons.web.violation;

import br.com.simpou.pub.commons.web.http.StatusHttp;

/**
 * HTTP erro status 403: acesso negado.
 *
 * @author Jonas Pereira
 * @since 2013-12-15
 */
public class ForbiddenViolationException extends AbstractBusinessViolationException {

    public ForbiddenViolationException() {
        super(StatusHttp.FORBIDDEN.value(), DefaultViolationCodes.FORBIDDEN);
    }
}
