package br.com.simpou.pub.commons.web.servlet;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * Created by jonas on 20/01/2017.
 */
@RequiredArgsConstructor
public class RequestUrl {

    /**
     * Formado pelo protocolo, IP e porta.
     */
    @Getter
    private final String server;

    /**
     * Caminho do servidor mais o contexto da aplicação.
     */
    @Getter
    private final String appUrl;

    /**
     * Contexto da aplicação definido pelo servidor. Útil nos casos em que estão em execução vários
     * aplicativos no mesmo, embora se aplique a uma aplicação apenas também.
     */
    @Getter
    private final String context;

    private final Map<String, String> query;

    /**
     * Caminho relativo ao servidor e ao contexto.
     */
    @Getter
    private final String path;

    /**
     * De onde se originou a requisição. Caminho relativo ao servidor.
     * Se não tiver originado do mesmo servidor, retorna o caminho completo.
     * Null caso não exista referência.
     */
    @Getter
    private final String referer;

    /**
     * Caminho completo sem query.
     */
    @Getter
    private final String url;

    /**
     * Caminho completo com query.
     */
    @Getter
    private final String fullUrl;

    @Getter
    private final String stringQuery;

    public RequestUrl(final HttpServletRequest request) {
        {
            String server = request.getScheme() + "://" + request.getServerName();
            final int port = request.getServerPort();
            if (port != 80) {
                server += ":" + port;
            }
            this.server = server;
        }
        this.context = isEmpty(request.getContextPath()) ? "" : request.getContextPath();
        this.appUrl = this.server + this.context;
        this.path = request.getRequestURI().substring(this.context.length());
        this.url = this.appUrl + this.path;
        final String stringQuery;
        {
            if (request.getQueryString() == null) {
                stringQuery = "";
                this.query = Collections.emptyMap();
            } else {
                stringQuery = request.getQueryString();
                final String[] parts = stringQuery.split("&");
                final Map<String, String> query = new HashMap<>();
                for (final String part : parts) {
                    final String[] keyValue = part.split("=");
                    final String value;
                    if (keyValue.length < 2) {
                        value = "";
                    } else {
                        value = keyValue[1];
                    }
                    query.put(keyValue[0], value);
                }
                this.query = Collections.unmodifiableMap(query);
            }
        }
        this.stringQuery = stringQuery;
        {
            String fullUrl = getUrl();
            if (!stringQuery.isEmpty()) {
                fullUrl += "?" + stringQuery;
            }
            this.fullUrl = fullUrl;
        }
        {
            final String referer = Servlets.getReferer(request);
            if (referer == null) {
                this.referer = null;
            } else if (referer.startsWith(this.appUrl)) {
                this.referer = referer.substring(this.appUrl.length());
            } else {
                this.referer = referer;
            }
        }
    }

    public boolean containsQuery() {
        return !this.query.isEmpty();
    }

    public boolean containsQuery(final String key) {
        return this.query.containsKey(key);
    }

    /**
     * @return Valor associado à chave parametrizada. Vazio caso exista e não tenha valor. Null caso chave não
     * exista na query.
     */
    public String query(final String key) {
        return this.query.get(key);
    }

    @Override
    public String toString() {
        return this.fullUrl;
    }
}
