package br.com.simpou.pub.commons.web.websocket;

import br.com.simpou.pub.commons.utils.cipher.Encryptor;
import br.com.simpou.pub.commons.web.http.HttpMethod;
import lombok.extern.apachecommons.CommonsLog;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.UUID;

/**
 * Trata uma conexão websocket com um cliente.
 * Implementação de parte da especificação rfc6455, versão 13 do websocket, para atender conexão/desconexão
 * e envio/recebimento de mensagens curtas.
 *
 * @author Jonas Pereira
 * @see <a href="https://tools.ietf.org/html/rfc6455">Especificação websocket</a>
 * @since 22/06/2017
 */
@CommonsLog
class WebSocketConnection extends Thread {

    private static final String INVALID_MESSAGE = "Mensagem inválida.";

    private static final String MAGIC_KEY = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

    private static final String HEADER_VERSION = "Sec-WebSocket-Version";

    private static final String HEADER_CLIENT_KEY = "Sec-WebSocket-Key";

    private static final String HEADER_CONNECTION = "Connection";

    private static final String HEADER_UPGRADE_VALUE = "websocket";

    private static final String HEADER_CONNECTION_VALUE = "Upgrade";

    private static final String HEADER_UPGRADE = "Upgrade";

    private static final int HANDSHAKE_STATUS = HttpServletResponse.SC_SWITCHING_PROTOCOLS;

    private static final String HEADER_SERVER_KEY = "Sec-WebSocket-Accept";

    private static final String WEBSOCKET_VERSION = "13";

    private static final String HEADER_PROTOCOL = "Sec-WebSocket-Protocol";

    private final Socket connection;

    private final WebSocketListener messageListener;

    private boolean connEstablished;

    private OutputStream out;

    private InputStream in;

    private final String clientIp;

    private String location;

    // equals/hashcode target
    private final long connId;

    // gerador de ids
    private static long countConnIds = 0;

    WebSocketConnection(final Socket connection,
                        final WebSocketListener messageListener,
                        final String clientIp) {
        this.connection = connection;
        this.messageListener = messageListener;
        this.clientIp = clientIp;
        this.connId = countConnIds++;
    }

    boolean isConnectionEstablished() {
        return connEstablished;
    }

    public String getLocation() {
        return location;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void run() {
        log.debug("Processando conexão websocket...");
        try {
            out = connection.getOutputStream();
            in = connection.getInputStream();
            final ResolvedRequest resolvedRequest = new SocketRequest(in);
            final boolean requestValid = isRequestValid(resolvedRequest);
            if (requestValid) {
                log.debug("Conexão websocket válida. Fazendo handshake...");
                doHandshake(resolvedRequest, in, out);
            } else {
                log.error("Tentativa de conexão websocket inválida vindo de " + this.clientIp);
                WebSocketUtils.writeBadRequest(out);
            }
        } catch (IOException e) {
            log.error("Erro ao processar conexão websocket.", e);
        }
    }

    void disconnect() {
        try {
            in.close();
        } catch (Exception e) {

        }
        try {
            out.close();
        } catch (Exception e) {

        }
        this.connEstablished = false;
    }

    /**
     * Checa os requisitos mínimos necessários para uma conexão websocket.
     */
    private boolean isRequestValid(final ResolvedRequest request) {
        return //SecurityUtils.isTokenValid(request) &&
                HttpMethod.GET.name().equalsIgnoreCase(request.getMethod()) &&
                //(Parameters.ORIGIN_ALLOWED == null || Parameters.ORIGIN_ALLOWED.equals(request.getHeader("Origin"))) &&
                request.containsHeader(HEADER_CLIENT_KEY) &&
                WEBSOCKET_VERSION.equals(request.getHeader(HEADER_VERSION));
    }

    /**
     * Promove a conexão HTTP simples para um websocket e estabelece comunicação persistente.
     *
     * @throws IOException Erros na conexão. Devem ser considerados como fatal e quaisquer estados devem ser
     *                     encerrados.
     */
    private void doHandshake(final ResolvedRequest request,
                             final InputStream in,
                             final OutputStream out) throws IOException {

        final String response;
        {
            final String clientKey = request.getHeader(HEADER_CLIENT_KEY);
            final String serverKey = Encryptor.cryptBase64(Encryptor.cryptSha1(clientKey + MAGIC_KEY));
            response = HANDSHAKE_STATUS + " Switching Protocols\r\n"
                    + HEADER_CONNECTION + ": " + HEADER_CONNECTION_VALUE + "\r\n"
                    + HEADER_UPGRADE + ": " + HEADER_UPGRADE_VALUE + "\r\n"
                    //+ HEADER_PROTOCOL + ": " + Parameters.WEBSOCKET_AUTH + "\r\n"
                    //+ "Access-Control-Allow-Origin" + ": " + request.getHeader("Origin") + "\r\n"
                    + HEADER_SERVER_KEY + ": " + serverKey;
        }

        log.info("Websocket handshake: \n" + response + "\n");
        WebSocketUtils.writeResponse(out, response);

        // conexão estabelecida, dispara escuta por mensagens
        this.connEstablished = true;
        new MessageListener(in).start();

        log.info("Cliente " + clientIp + " conectado ao websocket.");

        this.location = UUID.randomUUID().toString();
    }

    /**
     * Envio assíncrono de mensagens para os clientes.
     *
     * @return True se houve sucesso no envio.
     */
    boolean sendMessage(final WebSocketMessage message) {
        final boolean success;
        if (connEstablished) {
            // Como o envio é assíncrono, só é possível detectar uma desconexão no próximo envio.
            new MessageSender(message).start();
            success = true;
        } else {
            success = false;
        }
        return success;
    }

    /**
     * Emissor de mensagens.
     */
    private class MessageSender extends Thread {

        private final WebSocketMessage message;

        private MessageSender(final WebSocketMessage message) {
            this.message = message;
        }

        public void run() {
            try {
                WebSocketUtils.sendMessageToClient(message, out);
            } catch (Exception e) {
                disconnect();
            }
        }
    }

    /**
     * Aguarda o recebimento de mensagens. A qualquer momento o cliente pode enviar algo.
     */
    private class MessageListener extends Thread {

        /**
         * O tamanho da mensagem será limitado a esse valor. O excesso deve ser desconsiderado.
         */
        private static final int MAX_MESSAGE_SIZE = 1024;

        /**
         * Ponto de chegada de mensagens do cliente.
         */
        private final InputStream is;

        MessageListener(final InputStream is) {
            this.is = is;
        }

        public void run() {
            try {
                _run();
            } catch (Exception e) {
                // Neste ponto exceções são irrecuperáveis, conexão deve ser finalizada.
                try {
                    this.is.close();
                } catch (IOException ioe) {
                    //ioe.printStackTrace();
                } //finally {
                //e.printStackTrace();
                //}
            }
        }

        private void _run() throws Exception {
            while (true) { // loop é encerrado com o fechamento da conexão, pois exceção é lançada
                final byte[] data = new byte[MAX_MESSAGE_SIZE];
                int nRead = is.read(data, 0, MAX_MESSAGE_SIZE);

                final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                buffer.write(data, 0, nRead);
                buffer.flush();
                final WebSocketMessage message = WebSocketUtils.decodeClientMessage(
                        buffer.toByteArray()
                );
                if (WebSocketMessage.FIN_FULL == message.getFin()) {
                    switch (message.getOpcode()) {
                        case WebSocketMessage.OP_DISCONNECT:
                            WebSocketUtils.sendMessageToClient(message, out);
                            disconnect();
                            break;
                        case WebSocketMessage.OP_TEXT:
                            messageListener.onMessage(message);
                            break;
                        default:
                            WebSocketUtils.sendMessageToClient(
                                    new WebSocketMessage(INVALID_MESSAGE, null),
                                    out
                            );
                    }
                } else {
                    WebSocketUtils.sendMessageToClient(
                            new WebSocketMessage(INVALID_MESSAGE, null),
                            out
                    );
                }
            }
        }
    }

    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final WebSocketConnection that = (WebSocketConnection) o;

        return connId == that.connId;
    }

    public int hashCode() {
        return (int) (connId ^ (connId >>> 32));
    }
}
