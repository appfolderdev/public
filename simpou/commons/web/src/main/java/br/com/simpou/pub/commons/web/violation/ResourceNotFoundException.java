package br.com.simpou.pub.commons.web.violation;

import br.com.simpou.pub.commons.utils.violation.BusinessViolationException;

/**
 * HTTP erro status 404: not found.
 *
 * @author Jonas Pereira
 * @since 2017-02-07
 */
public class ResourceNotFoundException extends BusinessViolationException {

    public ResourceNotFoundException() {
        super(DefaultViolationCodes.NOT_FOUND);
    }
}
