package br.com.simpou.pub.commons.web.helper;

import br.com.simpou.pub.commons.utils.lang.Assertions;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * Utilitário para manipulação de URLs.
 *
 * @author Jonas Pereira
 * @since 13/01/2017
 */
public class Urls {

    /**
     * Separador de elementos componentes de uma URL.
     */
    public static final String PATH_EL_SEP = "/";

    public static String build(final boolean slashBegin, final boolean slashEnd, final Object... parts) {
        Assertions.notEmpty(parts, "At least one element required.");
        String result = Stream.of(parts)
                .filter(Objects::nonNull)
                .map(Object::toString)
                .filter(StringUtils::isNotEmpty)
                .map(s -> {
                    final String[] subparts = s.split("/");
                    if (subparts.length < 1) {
                        return null;
                    }
                    return subparts.length > 1 ? from(subparts) : subparts[0];
                })
                .filter(Objects::nonNull)
                .collect(Collectors.joining("/"));
        final String first = parts[0].toString();
        if (slashBegin || (isNotEmpty(first) && first.charAt(0) == '/')) {
            result = "/" + result;
        }
        final String last = parts[parts.length - 1].toString();
        if (slashEnd || (isNotEmpty(last) && last.charAt(last.length() - 1) == '/')) {
            result = result + "/";
        }
        return result;
    }

    /**
     * Considera que as partes já estão prontas pra montagem da url.
     */
    private static StringBuilder build(final StringBuilder builder, final String... parts) {
        for (final String part : parts) {
            builder.append(part).append(PATH_EL_SEP);
        }
        return builder;
    }

    /**
     * @return Url formada pelas partes na ordem em que são passadas. Barras duplicadas são eliminadas.
     * Se o primeiro elemento contiver barra no início, ela é mantida no início.
     * Se o último elemento contiver barra no fim, ela é mantida no fim.
     * Nulls e vazios são desconsiderados.
     */
    public static String from(final Object... parts) {
        return build(false, false, parts);
    }

    /**
     * Quebra os elementos de uma URL. Separadores não são incluídos nas partes retornadas. Se url null,
     * retorna array vazio.
     */
    public static String[] parts(final Object url) {
        if (url == null) {
            return new String[0];
        }
        final StringTokenizer st = new StringTokenizer(url.toString(), PATH_EL_SEP, false);
        final String[] parts = new String[st.countTokens()];
        int count = 0;
        while (st.hasMoreTokens()) {
            parts[count++] = st.nextToken();
        }
        return parts;
    }

    /**
     * Remove as barra iniciais e finais de uma url.
     */
    public static String relativize(final String url) {
        if (url == null) {
            return null;
        }

        String newUrl = url;
        if (url.startsWith(PATH_EL_SEP)) {
            newUrl = url.substring(1);
        }
        if (newUrl.endsWith(PATH_EL_SEP)) {
            newUrl = newUrl.substring(0, newUrl.length() - 1);
        }

        return newUrl;
    }
}
