package br.com.simpou.pub.commons.web.http;

/**
 * Created by jonas on 4/17/16.
 */
public enum HttpMethod {

    GET, POST, PUT, DELETE, OPTIONS;

}
