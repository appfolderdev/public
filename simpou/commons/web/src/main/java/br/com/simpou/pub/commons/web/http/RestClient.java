package br.com.simpou.pub.commons.web.http;

import br.com.simpou.pub.commons.utils.lang.Casts;
import br.com.simpou.pub.commons.utils.lang.IOHelper;
import lombok.extern.apachecommons.CommonsLog;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by jonas on 4/17/16.
 */
@CommonsLog
public class RestClient {

    private static void addHeaders(final URLConnection conn, final Map<String, List<String>> headers) {
        final Set<Map.Entry<String, List<String>>> set = Casts.simpleCast(headers.entrySet());
        for ( final Map.Entry<String, List<String>> entry : set) {
            conn.setRequestProperty(entry.getKey(), entry.getValue().get(0));
        }
    }

    public static HttpDataTransfer<byte[]> doRequest(final HttpDataTransfer<byte[]> request) throws IOException {
        final HttpURLConnection conn = (HttpURLConnection) request.getUri().toURL().openConnection();
        addHeaders(conn, request.getHeaders());
        conn.setRequestMethod(request.getMethod());
        final byte[] body = request.getBody();
        if(body !=null){
            conn.setDoOutput(true);
            conn.setFixedLengthStreamingMode(body.length);
            //conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            try (final OutputStream os = conn.getOutputStream()) {
                os.write(body);
                return run(conn);
            }
        }
        return run(conn);
    }

    public static HttpDataTransfer<byte[]> doRequestSilent(final HttpDataTransfer<byte[]> request) {
        try {
            return doRequest(request);
        } catch (final IOException e) {
            log.error(e);
            return  HttpDataTransfer.newBuilder().status(StatusHttp.INTERNAL_SERVER_ERROR).build();
        }
    }

    private static HttpDataTransfer<byte[]> run(final URLConnection conn) throws IOException {
        try (final InputStream is = conn.getInputStream()) {
            return HttpDataTransfer.newBuilder()
                    .body(IOHelper.toBytes(is))
                    .status( ((HttpURLConnection) conn).getResponseCode())//TODO checar tipo
                    .mime(conn.getContentType())
                    .headers(conn.getHeaderFields())
                    .build();
        }
    }
}
