package br.com.simpou.pub.commons.web.filter;

import br.com.simpou.pub.commons.utils.rand.Randoms;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by jonas on 31/01/17.
 */
public class AppJailFilterTest {

    private static AppJailFilter filter;

    @BeforeClass
    public static void setUpClass() {
        filter = new AppJailFilter();
    }

    @Test
    public void testBuildRedirect() {
        filter.setInitParams("", "api", "app");
        Assert.assertEquals("app/yy/zz", filter.buildRedirect("app/xx", "yy/zz", null));
        filter.setInitParams("base1/base2", "api", "app");
        Assert.assertEquals("base1/base2/app/yy/zz", filter.buildRedirect("base1/base2/app/xx", "yy/zz", null));
        filter.setInitParams("base", "api", "app");
        Assert.assertEquals("base/app/yy/zz", filter.buildRedirect("base/app/xx", "yy/zz", null));
    }

    @Test
    public void testIsJailed() {
        //TODO cobertura está aleatória
        _testIsJailed("");
        _testIsJailed(Randoms.getUrl(1, 3));
    }

    private void _testIsJailed(final String basePath) {
        final String appName = "app1";
        final String noAppName = "xapp1";
        filter.setInitParams(basePath, "api", appName);

        // se referer é null, testa apenas se o app deve ser enjaulado
        Assert.assertTrue(filter.getJailInfo(
                null,
                Randoms.getUrl(0, 3)
        ).isJailed());
        Assert.assertTrue(filter.getJailInfo(
                null,
                basePath + Randoms.getUrl(0, 3)
        ).isJailed());

        // se a origem da requisição não for um app alvo, então não filtra, qqer q seja o alvo
        Assert.assertTrue(filter.getJailInfo(
                noAppName + Randoms.getUrl(0, 3),
                Randoms.getUrl(0, 3)
        ).isJailed());
        Assert.assertTrue(filter.getJailInfo(
                basePath + noAppName + Randoms.getUrl(0, 3),
                Randoms.getUrl(0, 3)
        ).isJailed());

        // se o alvo for um app, filtra se a origem não iniciar com o caminho do app
        Assert.assertFalse(filter.getJailInfo(
                basePath + appName + Randoms.getUrl(0, 3),
                noAppName + Randoms.getUrl(0, 3)
        ).isJailed());
        Assert.assertFalse(filter.getJailInfo(
                basePath + appName + Randoms.getUrl(0, 3),
                basePath + noAppName + Randoms.getUrl(0, 3)
        ).isJailed());

        // origem e alvo ok, não filtra
        Assert.assertTrue(filter.getJailInfo(
                basePath + appName + Randoms.getUrl(0, 3),
                basePath + appName + Randoms.getUrl(0, 3)
        ).isJailed());
    }

}
