package br.com.simpou.pub.commons.web.http;

import br.com.simpou.pub.commons.utils.rand.Randomizable;
import br.com.simpou.pub.commons.utils.rand.Randoms;
import lombok.Getter;
import lombok.Setter;

/**
 * @author jonas1
 * @version 23/09/13
 * @since 23/09/13
 */
@Getter
@Setter
public class TestModel2 implements Randomizable {
    private boolean bool;

    @Override
    public void fillRandom() {
        bool = Randoms.getBoolean();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + (this.bool ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final TestModel2 other = (TestModel2) obj;
        return this.bool == other.bool;
    }


}
