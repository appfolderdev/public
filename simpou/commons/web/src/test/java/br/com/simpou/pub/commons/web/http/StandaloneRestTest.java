package br.com.simpou.pub.commons.web.http;

import br.com.simpou.pub.commons.utils.rand.Randoms;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by jonas on 4/19/16.
 */
public class StandaloneRestTest {

    private static StandaloneWebServer server;

    private static final String REST_PATH = "r";

    private static final String ENTITY_PATH = "e";

    private static final int PORT = 7777;

    private static final String BASE_URL = "http://localhost:" + PORT + "/" + REST_PATH + "/" + ENTITY_PATH;

    @BeforeClass
    public static void setUpClass() throws Exception {
        server = new StandaloneWebServer(
                PORT,
                REST_PATH,
                "resttests",
                new ServerHandler()
        );
        server.start();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        server.stop();
    }

    @Test
    public void testRequest() throws Exception {
//        final HttpDataTransfer resp = RestClient.request(new BasicHttpRequest(
//                HttpMethod.GET,
//                URI.create(BASE_URL)
//        ));
//        int o = 9;
    }

    static class ServerHandler extends AbstractCrudHttpHandler<TestEntity> {

        ServerHandler() {
            super(TestEntity.class, ENTITY_PATH);
        }

        @Override
        protected Iterable<TestEntity> findAll() {
            return Randoms.getList(TestEntity.class, 2, 2);
        }
    }
}
