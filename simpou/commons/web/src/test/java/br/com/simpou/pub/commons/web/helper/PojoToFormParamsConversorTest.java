package br.com.simpou.pub.commons.web.helper;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.web.http.TestModel;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

@ClassTest(PojoToFormParamsConversor.class)
public class PojoToFormParamsConversorTest {

    private static PojoToFormParamsConversor conversor;

    @BeforeClass
    public static void setUpClass() {
        final Map<Class<?>, Map<String, String>> wrappers;
        {
            wrappers = new HashMap<>();
            final Map<String, String> wrappers2 = new HashMap<>();
            wrappers2.put("complexListWrapped", "item");
            wrappers.put(TestModel.class, wrappers2);
        }
        final AbstractPojoAndFormParamsConversor.ConversorContext context = AbstractPojoAndFormParamsConversor.ConversorContext.getBuilder().useWrappers(wrappers).build();
        conversor = new PojoToFormParamsConversor(context);
    }

    @Test
    public void testApply_Object() throws Exception {
        //TODO stackoverflow
        //        TestModel obj = Randoms.getSingle(TestModel.class);
        //        Map<String, List<String>> multMap = conversor.apply(obj);
        //        assertEquals(24, multMap.size());
        //
        //        assertEquals(DateConversor.MARSHALLED, multMap.get("date").get(0));
        //        assertEquals(obj.getString(), multMap.get("string").get(0));
        //        assertEquals(obj.getString2(), multMap.get("@string2").get(0));
        //        assertEquals(obj.getValue() + "", multMap.get("@valueCustom").get(0));
        //        assertEquals(obj.getModel4().getIntt() + "", multMap.get("model4[intt]").get(0));
        //        assertEquals(obj.getModel4().getRecur3().getRecur4().getIntt() + "", multMap.get("model4[recur3][recur4][intt]").get(0));
        //        assertEquals(obj.getModel4s().get(0).getIntt()+ "", multMap.get("model4s[0][intt]").get(0));
        //        assertEquals(obj.getModel4s().get(1).getIntt() + "", multMap.get("model4s[1][intt]").get(0));
        //        assertEquals(obj.getModel4s().get(0).getRecur3().getRecur4().getIntt()+ "", multMap.get("model4s[0][recur3][recur4][intt]").get(0));
        //        assertEquals(obj.getModel4s().get(1).getRecur3().getRecur4().getIntt()+ "", multMap.get("model4s[1][recur3][recur4][intt]").get(0));
        //        assertEquals(obj.getModel2().isBool() + "", multMap.get("model2[bool]").get(0));
        //        assertEquals(obj.getComplexList().get(0).isBool() + "", multMap.get("complexList[0][bool]").get(0));
        //        assertEquals(obj.getComplexList().get(1).isBool() + "", multMap.get("complexList[1][bool]").get(0));
        //        assertEquals(obj.getComplexListAnnot().get(0).isBool() + "", multMap.get("complexListCustom[testModel2Custom][0][bool]").get(0));
        //        assertEquals(obj.getComplexListAnnot().get(1).isBool() + "", multMap.get("complexListCustom[testModel2Custom][1][bool]").get(0));
        //        assertEquals(obj.getComplexArray()[0].isBool() + "", multMap.get("complexArray[0][bool]").get(0));
        //        assertEquals(obj.getComplexArray()[1].isBool() + "", multMap.get("complexArray[1][bool]").get(0));
        //        assertEquals(obj.getBasicList(), multMap.get("basicList[]"));
        //        assertEquals(obj.getBasicListAnnot(), multMap.get("basicListAnnotCustom[item][]"));
        //        assertEquals(Arrays.asList(obj.getBasicArray()), multMap.get("basicArray[]"));
        //        assertEquals(obj.getModel4Array()[0].getIntt() + "", multMap.get("model4Array[0][intt]").get(0));
        //        assertEquals(obj.getComplexListWrapped().get(0).isBool() + "", multMap.get("complexListWrapped[item][0][bool]").get(0));
        //
        //        assertEquals(obj.getEnumType()+"", multMap.get("enumType").get(0));
        //        assertEquals(obj.getEnumList().get(0)+"", multMap.get("enumList[]").get(0));
    }

    @Test
    public void testExecute_3args() throws Exception {
    }
}
