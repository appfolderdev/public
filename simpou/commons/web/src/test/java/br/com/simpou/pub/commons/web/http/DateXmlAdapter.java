package br.com.simpou.pub.commons.web.http;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Date;

public class DateXmlAdapter
        extends XmlAdapter<String, Date> {

    public static final String MARSHALLED = "date";

    public static final Date UNMARSHALLED = new Date();

    @Override
    public Date unmarshal(String v) throws Exception {
        return UNMARSHALLED;
    }

    @Override
    public String marshal(Date v) throws Exception {
        return MARSHALLED;
    }
}
