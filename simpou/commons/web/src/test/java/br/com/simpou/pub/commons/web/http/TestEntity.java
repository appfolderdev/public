package br.com.simpou.pub.commons.web.http;

import com.fasterxml.jackson.annotation.JsonIgnore;
import br.com.simpou.pub.commons.model.entity.AbstractIndexedEntity;
import br.com.simpou.pub.commons.model.entity.RestEntity;
import br.com.simpou.pub.commons.utils.lang.annot.RandomFillableConfig;
import br.com.simpou.pub.commons.utils.rand.Randomizable;
import br.com.simpou.pub.commons.utils.rand.Randoms;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * Created by jonas on 4/19/16.
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@RandomFillableConfig(minLength = 1, maxLength = 5)
public class TestEntity extends AbstractIndexedEntity implements RestEntity, Randomizable {

    private String name;

    @JsonIgnore
    private final Date lastModified = Randoms.getDate(false);

    private static long idCounter = 0;

    @Override
    public void fillRandom() {
        setId(++idCounter);
        name = Randoms.getString();
    }

    @Override
    public Date lastModified() {
        return lastModified;
    }

    @Override
    public String objectTag() {
        return TestEntity.class.getName()+"#"+getId();
    }
}
