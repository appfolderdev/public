package br.com.simpou.pub.commons.web.helper;

import br.com.simpou.pub.commons.model.cache.Cacheable;
import br.com.simpou.pub.commons.utils.cipher.Hashs;
import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.TestOrder;
import br.com.simpou.pub.commons.utils.violation.ViolationsException;
import br.com.simpou.pub.commons.web.http.HttpDataTransferBuilder;
import br.com.simpou.pub.commons.web.http.HttpHeaders;
import br.com.simpou.pub.commons.web.http.StatusHttp;
import junit.framework.TestCase;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;

/**
 * @author Ludmily Silva
 * @since 12/09/2014
 */

@ClassTest(CacheHelper.class)
public class CacheHelperTest {

    private static Cacheable cacheable;

    final HttpDataTransferBuilder<String, ResponseEntity> builder =
            new HttpDataTransferBuilder<String, ResponseEntity>() {

                @Override
                public ResponseEntity build() {
                    return new ResponseEntity(
                            getHeaders(),
                            StatusHttp.valueOf(getStatus()),
                            getBody()
                    );
                }
            };

    @BeforeClass
    public static void setup() {
        cacheable = mock(Cacheable.class);
    }

    @Test
    public void testDoCaching() throws Exception {
        final String obj = "olá";

        final CacheHelper.ResponseFactory<String> responseFactory = new CacheHelper.ResponseFactory<String>() {
            @Override
            public String getResponseBody() {
                return obj;
            }
        };

        final Date date = Randoms.getDate(false);
        final String tag = "tag";

        final Map<String, List<String>> headers = new HashMap<>();

        // cache desabilitado no objeto
        {
            CacheHelper.doCaching(this.builder, null, tag, headers, responseFactory);
            TestCase.assertEquals(obj, this.builder.getBody());
        }

        // dado não está em cache no cliente
        {
            CacheHelper.doCaching(this.builder, date, tag, headers, responseFactory);
            TestCase.assertEquals(ResponseEntity.class, this.builder.build().getClass());
            final ResponseEntity responseEntity = (ResponseEntity) this.builder.build();
            assertEquals(
                    CacheHelper.formatDate(date),
                    responseEntity.getHeaders().get(HttpHeaders.LAST_MODIFIED).get(0)
            );
            assertNotNull(responseEntity.getHeaders().get(HttpHeaders.ETAG).get(0));
            assertEquals(obj, responseEntity.getBody());
        }

        headers.put(HttpHeaders.IF_MODIFIED_SINCE, Collections.singletonList(CacheHelper.formatDate(date)));
        headers.put(HttpHeaders.IF_NONE_MATCH, Collections.singletonList(Hashs.getHash(tag)));

        // dado em cache no cliente
        {
            CacheHelper.doCaching(this.builder, date, tag, headers, responseFactory);
            TestCase.assertEquals(ResponseEntity.class, this.builder.build().getClass());
            final ResponseEntity responseEntity = (ResponseEntity) this.builder.build();
            TestCase.assertEquals(StatusHttp.NOT_MODIFIED, responseEntity.getStatusCode());
        }

        // dado desatualizado no cliente
        {
            final Date newDate = Randoms.getDate(true);
            CacheHelper.doCaching(this.builder, newDate, tag, headers, responseFactory);
            TestCase.assertEquals(ResponseEntity.class, this.builder.build().getClass());
            final ResponseEntity responseEntity = (ResponseEntity) this.builder.build();
            assertEquals(
                    CacheHelper.formatDate(newDate),
                    responseEntity.getHeaders().get(HttpHeaders.LAST_MODIFIED).get(0)
            );
            assertEquals(obj, responseEntity.getBody());
            assertNotNull(responseEntity.getHeaders().get(HttpHeaders.ETAG).get(0));
        }
    }

    /**
     * @see CacheHelper#extractCacheable(HttpServletRequest)
     */
    @Test
    public void testExtractCacheable_HttpServletRequest() {
    }

    @Test
    public void testFormatDate() {
        //testExtractCacheable
    }

    /**
     * @see CacheHelper#generateMaxExpiresDate()
     */
    @Test
    public void testGenerateMaxExpiresDate() {

    }

    /**
     * @see CacheHelper#getCacheInfo(Cacheable, Map)
     */
    @Test
    public void testGetCacheInfo_Cacheable_Map() {
    }

    /**
     * @see CacheHelper#getCacheInfo(Date, String, Map)
     */
    @Test(expected = ViolationsException.class)
    @TestOrder(5)
    public void testGetCacheInfo_Date_String_Map() {
        final Map<String, List<String>> headers = new HashMap<>();

        CacheHelper.HttpCacheInfo httpCacheInfo = CacheHelper.getCacheInfo(null, "Tag", headers);
        assertSame(httpCacheInfo.getEtag(), new CacheHelper.HttpCacheInfo().getEtag());
        final Date lastmodified = new Date();

        headers.put(HttpHeaders.IF_MODIFIED_SINCE, Collections.singletonList("Qui, 12 Set 2014 20:42:23 GMT"));
        httpCacheInfo = CacheHelper.getCacheInfo(lastmodified, "Tag", headers);
        assertEquals(httpCacheInfo.getEtag(), new CacheHelper.HttpCacheInfo
                ("1503916a2ab2b0fd6768d3455fd8f2d9aa3b31333a8507dadcad983704a975d7", null, lastmodified,
                        lastmodified).getEtag());

        headers.put(HttpHeaders.IF_MODIFIED_SINCE, Collections.singletonList("aaa, aa aaa 2014 20:42:23 GMT"));
        httpCacheInfo = CacheHelper.getCacheInfo(lastmodified, "Tag", headers);

        headers.put(HttpHeaders.IF_MODIFIED_SINCE, null);
        httpCacheInfo = CacheHelper.getCacheInfo(lastmodified, "Tag", headers);
        assertSame(
                httpCacheInfo.getEtag(),
                new CacheHelper.HttpCacheInfo(
                        "1503916a2ab2b0fd6768d3455fd8f2d9aa3b31333a8507dadcad983704a975d7",
                        null,
                        lastmodified,
                        lastmodified
                ).getEtag()
        );
    }

    /**
     * @see CacheHelper#getCachedResponseHeaders(Cacheable)
     */
    @Test
    public void testGetCachedResponseHeaders_cacheable() {
    }

    /**
     * @see CacheHelper#getCachedResponseHeaders(String, Date)
     */
    @Test
    public void testGetCachedResponseHeaders_string_date() {
    }

    @Test
    public void testNewInstance() throws Exception {
        CacheHelper.newInstance();
    }

    @Test
    public void testParseDate() {
        //testGetCacheInfo
    }

    @Getter
    @RequiredArgsConstructor
    class ResponseEntity {

        private final Map<String, List<String>> headers;

        private final StatusHttp statusCode;

        private final Object body;

    }

}
