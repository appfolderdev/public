package br.com.simpou.pub.commons.web.http;

import br.com.simpou.pub.commons.utils.rand.Randomizable;
import br.com.simpou.pub.commons.utils.rand.Randoms;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author jonas1
 * @version 23/09/13
 * @since 23/09/13
 */
@Getter
@Setter
@EqualsAndHashCode(of = "intt")
public class TestModel4 implements Randomizable {

    private int intt;
    private TestModel3 recur3;

    @Override
    public void fillRandom() {
        recur3 = Randoms.getSingle(TestModel3.class, new TestModel3.Randz(this));
        intt = Randoms.getInteger();
    }

}
