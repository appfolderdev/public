package br.com.simpou.pub.commons.web.helper;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by jonas on 13/01/2017.
 */
public class UrlsTest {

    /**
     * @see Urls#from(Object...)
     */
    @Test
    public void testFrom() throws Exception {
        Assert.assertEquals("a/b/c", Urls.from("a", "b", "c"));
        Assert.assertEquals("/a/b/c", Urls.from("/a", "/b", "c"));
        Assert.assertEquals("/a/b", Urls.from("//a", "b"));
        Assert.assertEquals("a/b/c/d", Urls.from("a", "/b/c/", "/d"));
        Assert.assertEquals("/a/b/c/d", Urls.from("///a", null, "/b//c", "d"));
        Assert.assertEquals("/", Urls.from("////", null, "", ""));
        Assert.assertEquals("", Urls.from("", null, "", ""));
    }

    /**
     * @see Urls#parts(Object)
     */
    @Test
    public void testParts() throws Exception {
        //testBuild
    }

    /**
     * @see Urls#relativize(String)
     */
    @Test
    public void testRelativize() throws Exception {
        Assert.assertEquals("a/b/c", Urls.relativize("/a/b/c/"));
        Assert.assertEquals("a/b/c", Urls.relativize("/a/b/c"));
        Assert.assertEquals("a/b/c", Urls.relativize("a/b/c"));
        Assert.assertNull(Urls.relativize(null));
    }
}
