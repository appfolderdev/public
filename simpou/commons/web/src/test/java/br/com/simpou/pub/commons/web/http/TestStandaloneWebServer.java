package br.com.simpou.pub.commons.web.http;

import br.com.simpou.pub.commons.persistence.dao.ObjectDAO;
import br.com.simpou.pub.commons.persistence.dao.impl.RandomInMemoryDAOImpl;

import java.io.IOException;

/**
 * Created by jonas on 21/04/2016.
 */
public class TestStandaloneWebServer {

    public static void main(String[] args) throws IOException {
        final StandaloneWebServer server = new StandaloneWebServer(
                8888,
                "rest",
                "webapp",
                new StaticResourcesHandler("/"),
                new CrudHttpHandler()
        );
        server.start();
        System.out.println("Server running...");
        System.in.read();
        server.stop();
    }

    static class CrudHttpHandler extends AbstractCrudHttpHandler<TestEntity> {

        private ObjectDAO dao = new RandomInMemoryDAOImpl();

        public CrudHttpHandler() {
            super(TestEntity.class, "entity");
        }

        @Override
        protected Iterable<TestEntity> findAll() {
            try {
                return dao.getList(TestEntity.class);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        protected TestEntity findOne(final Long id) {
            try {
                return dao.getSingle(TestEntity.class, id);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
