package br.com.simpou.pub.commons.web.websocket;

import java.io.*;
import java.net.Socket;

public class WebsocketClientMain {

    public static void main(String[] args) throws Exception {
        final int port = WebsocketManager.PORT;
        final String host = "127.0.0.1";

        // conecta ao websocket
        final Socket websocket = new Socket(host, port);
        final InputStream inputStream = websocket.getInputStream();
        {
            final OutputStream outputStream = websocket.getOutputStream();
            PrintWriter pw = new PrintWriter(outputStream);
            pw.println("GET / HTTP/1.1");
            pw.println("Upgrade: websocket");
            pw.println("Connection: Upgrade");
            pw.println("Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==");
            pw.println("Sec-WebSocket-Version: 13");
            pw.println("");
            pw.flush();

            final String respConn = StreamUtils.readLines(inputStream, false);
            System.out.println("Websocket conectado!");
            System.out.println(respConn);
        }

        // recebe as notificações
        System.out.println("Aguardando notificações...");
        listenNotifications(inputStream);

        inputStream.close();
    }

    private static void listenNotifications(final InputStream inputStream) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[16384];
        byte[] resp = null;
        while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);

            //TODO gambiarra, tratar direito essa parte
            int start = data[2] == 0 ? 4 : 2;
            resp = new byte[nRead];
            for (int i = start; i < nRead; i++) {
                resp[i - start] = data[i];
            }
            break;//recebe apenas uma mensagem
        }
        buffer.flush();

        final String content = new String(resp, "UTF-8");
        System.out.println("Notificação recebida: " + content);
        listenNotifications(inputStream);
    }

}
