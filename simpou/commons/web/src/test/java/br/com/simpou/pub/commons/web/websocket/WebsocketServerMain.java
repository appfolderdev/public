package br.com.simpou.pub.commons.web.websocket;

import java.util.Date;

public class WebsocketServerMain {

    public static void main(String[] args) throws Exception {
        final WebSocketEndpointImpl instance = new WebSocketEndpointImpl();
        instance.addMessageListener(new WebSocketListener() {
            public void onMessage(final WebSocketMessage message) {
                System.out.println("\nChegou mensagem: " + message.getPayload());
           }
        });
        while (true) {
            System.out.println("1 para enviar mensagem");
            System.out.println("2 para encerrar");
            System.out.print("=");
            final int read = System.in.read();
            if (read == 49) {
                instance.sendMessage(new WebSocketMessage(new Date() + " hey!", ClientLocation.builder().address("teste").build()), null);
            } else if (read == 50) {
                break;
            }
        }
        instance.shutdown();
    }

}
