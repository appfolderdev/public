package br.com.simpou.pub.commons.web.filter;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.ExtraTest;
import org.junit.Test;

import static org.mockito.Mockito.*;

@ClassTest(AbstractCORSFilter.class)

public class AbstractCORSFilterTest {
    @Test
    @ExtraTest
    public void testFillHeader() {
        final AbstractCORSFilter.Header header = mock(AbstractCORSFilter.Header.class);
        final AbstractCORSFilter filter = new AbstractCORSFilter() {
        };

        filter.fillHeader(header, "other");
        verify(header).getRequestHeader("Origin");
        verify(header, times(0))
                .getRequestHeader("Access-Control-Request-Method");
        filter.fillHeader(header, "OPTIONS");
        verify(header, times(2)).getRequestHeader("Origin");
        verify(header, times(1))
                .getRequestHeader("Access-Control-Request-Method");
    }

    private class HeaderImpl implements AbstractCORSFilter.Header {
        @Override
        public void addResponseHeader(final String key, final String value) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public String getRequestHeader(final String key) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}
