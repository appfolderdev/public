package br.com.simpou.pub.commons.web.http;

import br.com.simpou.pub.commons.utils.rand.Randomizable;
import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.rand.StringRandGen;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author jonas1
 * @version 23/09/13
 * @since 23/09/13
 */
@Getter
@Setter
public class TestModel implements Randomizable {
    private String string;

    @XmlAttribute
    private String string2;

    private int value;

    @XmlJavaTypeAdapter(type = Date.class, value = DateXmlAdapter.class)
    private Date date;

    @XmlAttribute(name = "valueCustom")
    public int getValue() {
        return value;
    }

    private TestModel2 model2;

    //recursivo
    private TestModel4 model4;
    
    //recursivo
    private List<TestModel4> model4s;

    private TestModel4[] model4Array;

    private List<TestModel2> complexList;

    @XmlElement(name = "testModel2Custom")
    @XmlElementWrapper(name = "complexListCustom")
    private List<TestModel2> complexListAnnot;

    private List<TestModel2> complexListWrapped;

    private List<String> basicList;

    private TestType enumType;

    private List<TestType> enumList;

    @XmlElement(name = "item")
    @XmlElementWrapper(name = "basicListAnnotCustom")
    private List<String> basicListAnnot;

    private TestModel2[] complexArray;

    private String[] basicArray;

    @Override
    public void fillRandom() {
        string = Randoms.getString();
        string2 = Randoms.getString();
        value = Randoms.getInteger();
        model2 = Randoms.getSingle(TestModel2.class);
        model4 = Randoms.getSingle(TestModel4.class);
        model4s = Randoms.getList(TestModel4.class, 2, 2);
        model4Array = new TestModel4[]{model4};
        complexList = Randoms.getList(TestModel2.class, 2, 2);
        complexListAnnot = complexList;
        complexListWrapped = Randoms.getList(TestModel2.class, 1, 1);
        basicList = Randoms.getList(String.class, new StringRandGen(), 2, 2);
        basicListAnnot = basicList;
        complexArray = Randoms.getArray(TestModel2.class, 2, 2);
        basicArray = Randoms.getArray(String.class, new StringRandGen(), 2, 2);
        date = Randoms.getDate();
        enumType = Randoms.getEnum(TestType.class);
        enumList = Arrays.asList(enumType);
    }
}
