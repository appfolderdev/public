package br.com.simpou.pub.commons.web.helper;

import br.com.simpou.pub.commons.utils.lang.Casts;
import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.rand.StringRandGen;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.web.http.DateXmlAdapter;
import br.com.simpou.pub.commons.web.http.TestModel;
import br.com.simpou.pub.commons.web.http.TestModel2;
import br.com.simpou.pub.commons.web.http.TestModel4;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import static br.com.simpou.pub.commons.utils.tests.AssertExtension.assertDeepEquals;
import static org.junit.Assert.*;

@ClassTest(FormParamsToPojoConversor.class)
public class FormParamsToPojoConversorTest {

    private static FormParamsToPojoConversor<TestModel> conversor;

    @BeforeClass
    public static void setUpClass() {
        final Map<Class<?>, Map<String, String>> wrappers;
        {
            wrappers = new HashMap<>();
            final Map<String, String> wrappers2 = new HashMap<>();
            wrappers2.put("complexListWrapped", "item");
            wrappers.put(TestModel.class, wrappers2);
        }
        final AbstractPojoAndFormParamsConversor.ConversorContext context = AbstractPojoAndFormParamsConversor.ConversorContext.getBuilder().useWrappers(wrappers).build();
        conversor = new FormParamsToPojoConversor<>(TestModel.class, context);
    }

    @Test
    public void testApply() throws Exception {
        final TestModel obj = Randoms.getSingle(TestModel.class);
        final Map<String, List<String>> multiMap;
        {
            multiMap = new HashMap<>();
            multiMap.put("date", Collections.singletonList(obj.getDate().toString()));
            multiMap.put("string", Collections.singletonList(obj.getString()));
            multiMap.put("@string2", Collections.singletonList(obj.getString2()));
            multiMap.put("@valueCustom", Collections.singletonList(obj.getValue() + ""));
            multiMap.put("model2[bool]", Collections.singletonList(obj.getModel2().isBool() + ""));
            multiMap.put("complexList[0][bool]", Collections.singletonList(obj.getComplexList().get(0).isBool() + ""));
            multiMap.put("complexList[1][bool]", Collections.singletonList(obj.getComplexList().get(1).isBool() + ""));
            multiMap.put("complexListCustom[testModel2Custom][0][bool]", Collections.singletonList(obj.getComplexListAnnot().get(0).isBool() + ""));
            multiMap.put("complexListCustom[testModel2Custom][1][bool]", Collections.singletonList(obj.getComplexListAnnot().get(1).isBool() + ""));
            multiMap.put("complexArray[0][bool]", Collections.singletonList(obj.getComplexArray()[0].isBool() + ""));
            multiMap.put("complexArray[1][bool]", Collections.singletonList(obj.getComplexArray()[1].isBool() + ""));
            multiMap.put("basicList[]", obj.getBasicList());
            multiMap.put("basicListAnnotCustom[item][]", obj.getBasicListAnnot());
            multiMap.put("basicArray[]", Arrays.asList(obj.getBasicArray()));
            multiMap.put("model4[intt]", Collections.singletonList(obj.getModel4().getIntt() + ""));
            multiMap.put("model4[recur3][recur4][intt]", Collections.singletonList(obj.getModel4()
                    .getRecur3().getRecur4().getIntt() + ""));
            multiMap.put("model4s[0][intt]", Collections.singletonList(obj.getModel4s().get(0).getIntt() +
                    ""));
            multiMap.put("model4s[1][intt]", Collections.singletonList(obj.getModel4s().get(1).getIntt() + ""));
            multiMap.put("model4s[0][recur3][recur4][intt]", Collections.singletonList(obj.getModel4s().get(0).getRecur3().getRecur4().getIntt() + ""));
            multiMap.put("model4s[1][recur3][recur4][intt]", Collections.singletonList(obj.getModel4s().get(1).getRecur3().getRecur4().getIntt() + ""));
            multiMap.put("model4Array[0][intt]", Collections.singletonList(obj.getModel4Array()[0].getIntt() + ""));
            multiMap.put("enumType", Collections.singletonList(obj.getEnumType() + ""));
            multiMap.put("enumList[]", Arrays.asList(obj.getEnumList().get(0) + ""));
        }

        final TestModel ret = conversor.apply(multiMap);
        assertEquals(obj.getString(), ret.getString());
        assertEquals(obj.getModel2(), ret.getModel2());
        assertDeepEquals(obj.getComplexArray(), ret.getComplexArray());
        assertEquals(obj.getComplexList(), ret.getComplexList());
        assertDeepEquals(obj.getBasicArray(), ret.getBasicArray());
        assertEquals(obj.getBasicList(), ret.getBasicList());
        assertEquals(obj.getString2(), ret.getString2());
        assertEquals(obj.getValue(), ret.getValue());
        assertEquals(obj.getBasicListAnnot(), ret.getBasicListAnnot());
        assertEquals(obj.getComplexListAnnot(), ret.getComplexListAnnot());
        Assert.assertEquals(DateXmlAdapter.UNMARSHALLED, ret.getDate());
        assertEquals(obj.getModel4().getIntt(), ret.getModel4().getIntt());
        assertEquals(obj.getModel4s().get(0).getIntt(), ret.getModel4s().get(0).getIntt());
        assertEquals(obj.getModel4s().get(1).getIntt(), ret.getModel4s().get(1).getIntt());
        assertEquals(obj.getEnumType(), ret.getEnumType());
        assertEquals(obj.getEnumList().get(0), ret.getEnumList().get(0));

        //testa recursões
        assertSame(ret.getModel4(), ret.getModel4().getRecur3().getRecur4());
        assertSame(ret.getModel4s().get(0), ret.getModel4s().get(0).getRecur3().getRecur4());
        assertSame(ret.getModel4s().get(1), ret.getModel4s().get(1).getRecur3().getRecur4());
        assertSame(ret.getModel4Array()[0], ret.getModel4());
    }

    @Test
    public void testExecute_convertedValues() throws Exception {
        final Map<String, Object> convertedValues;
        {
            convertedValues = new HashMap<>();
            convertedValues.put("date", Randoms.getDate());
            convertedValues.put("string", Randoms.getString());
            convertedValues.put("@string2", Randoms.getString());
            convertedValues.put("@valueCustom", Randoms.getInteger().intValue());
            convertedValues.put("model2", Randoms.getSingle(TestModel2.class));
            convertedValues.put("complexList[0]", Randoms.getSingle(TestModel2.class));
            convertedValues.put("complexList[1]", Randoms.getSingle(TestModel2.class));
            convertedValues.put("complexListCustom[testModel2Custom][0]", Randoms.getSingle(TestModel2.class));
            convertedValues.put("complexListCustom[testModel2Custom][1]", Randoms.getSingle(TestModel2.class));
            convertedValues.put("complexArray[0]", Randoms.getSingle(TestModel2.class));
            convertedValues.put("complexArray[1]", Randoms.getSingle(TestModel2.class));
            convertedValues.put("basicList[]", Randoms.getList(String.class, new StringRandGen(), 1, 3));
            convertedValues.put("basicListAnnotCustom[item][]", Randoms.getList(String.class, new StringRandGen(), 1, 3));
            convertedValues.put("basicArray[]", Randoms.getArray(String.class, new StringRandGen(), 1, 3));
            convertedValues.put("model4", Randoms.getSingle(TestModel4.class));
            convertedValues.put("model4s[0]", Randoms.getSingle(TestModel4.class));
            convertedValues.put("model4s[1]", Randoms.getSingle(TestModel4.class));
            convertedValues.put("model4Array[0]", Randoms.getSingle(TestModel4.class));
            convertedValues.put("complexListWrapped[item][0]", Randoms.getSingle(TestModel2.class));
        }

        final Map<String, List<String>> multiMap;
        {
            multiMap = new HashMap<>();
            multiMap.put("date", null);
            multiMap.put("string", null);
            multiMap.put("@string2", null);
            multiMap.put("@valueCustom", null);
            multiMap.put("model2", null);
            multiMap.put("complexList[0]", null);
            multiMap.put("complexList[1]", null);
            multiMap.put("complexListCustom[testModel2Custom][0]", null);
            multiMap.put("complexListCustom[testModel2Custom][1]", null);
            multiMap.put("complexArray[0]", null);
            multiMap.put("complexArray[1]", null);
            multiMap.put("basicList[]", null);
            multiMap.put("basicListAnnotCustom[item][]", null);
            multiMap.put("basicArray[]", null);
            multiMap.put("model4", null);
            multiMap.put("model4s[0]", null);
            multiMap.put("model4s[1]", null);
            multiMap.put("model4Array[0]", null);
            multiMap.put("complexListWrapped[item][0]", null);
        }

        final FormParamsToPojoConversor.MultiMapTree tree = new FormParamsToPojoConversor.MultiMapTree(multiMap);
        final Set<String> visited = new HashSet<>();
        for (final Iterator<FormParamsToPojoConversor.MultiMapTreeLeaf> treeIterator = new FormParamsToPojoConversor.MultiMapTreeLeafIterator(tree); treeIterator.hasNext(); ) {
            final FormParamsToPojoConversor.MultiMapTreeLeaf leaf = treeIterator.next();
            final Object leafValue = convertedValues.get(leaf.getPath());
            final List<Object> leafValues;
            final Class<?> leafValueClass = leafValue.getClass();
            if (List.class.isAssignableFrom(leafValueClass)) {
                leafValues = Casts.simpleCast(leafValue);
            } else if (leafValueClass.isArray()) {
                final Object[] array = (Object[]) leafValue;
                leafValues = Arrays.asList(array);
            } else {
                leafValues = Collections.singletonList(leafValue);
            }
            leaf.setConvertedValue(leafValues);
            visited.add(leaf.getPath());
        }

        assertEquals(convertedValues.size(), visited.size());
        for (final String convValue : convertedValues.keySet()) {
            assertTrue(visited.contains(convValue));
        }

        final TestModel ret = conversor.execute(tree, true, null);
        assertEquals(convertedValues.get("date"), ret.getDate());
        assertEquals(convertedValues.get("string"), ret.getString());
        assertEquals(convertedValues.get("@string2"), ret.getString2());
        assertEquals(convertedValues.get("@valueCustom"), ret.getValue());
        assertEquals(convertedValues.get("model2"), ret.getModel2());
        assertEquals(convertedValues.get("complexList[0]"), ret.getComplexList().get(0));
        assertEquals(convertedValues.get("complexList[1]"), ret.getComplexList().get(1));
        assertEquals(convertedValues.get("complexListCustom[testModel2Custom][0]"), ret.getComplexListAnnot().get(0));
        assertEquals(convertedValues.get("complexListCustom[testModel2Custom][1]"), ret.getComplexListAnnot().get(1));
        assertEquals(convertedValues.get("complexArray[0]"), ret.getComplexArray()[0]);
        assertEquals(convertedValues.get("complexArray[1]"), ret.getComplexArray()[1]);
        assertEquals(convertedValues.get("basicList[]"), ret.getBasicList());
        assertEquals(convertedValues.get("basicListAnnotCustom[item][]"), ret.getBasicListAnnot());
        assertDeepEquals((String[]) convertedValues.get("basicArray[]"), ret.getBasicArray());
        assertEquals(convertedValues.get("model4"), ret.getModel4());
        assertEquals(convertedValues.get("model4s[0]"), ret.getModel4s().get(0));
        assertEquals(convertedValues.get("model4s[1]"), ret.getModel4s().get(1));
        assertEquals(convertedValues.get("model4Array[0]"), ret.getModel4Array()[0]);
        assertEquals(convertedValues.get("complexListWrapped[item][0]"), ret.getComplexListWrapped().get(0));
    }

    @Test
    public void testExecute_empty() throws Exception {
        final Map<String, List<String>> multiMap = new HashMap<>();
        final TestModel ret = conversor.apply(multiMap);

        assertNull(ret.getString());
        assertNull(ret.getModel2());
        assertNull(ret.getComplexArray());
        assertNull(ret.getComplexList());
        assertNull(ret.getBasicArray());
        assertNull(ret.getBasicList());
        assertNull(ret.getString2());
        assertEquals(0, ret.getValue());
        assertNull(ret.getBasicListAnnot());
        assertNull(ret.getComplexListAnnot());
        assertNull(ret.getDate());
        assertNull(ret.getModel4());
        assertNull(ret.getModel4s());
    }

    @Test
    public void testStringParamsToMap() throws Exception {
        final Map<String, List<String>> map = FormParamsToPojoConversor.stringParamsToMap("key=value&key2=[value1,v2]");
        //TODO
    }

}
