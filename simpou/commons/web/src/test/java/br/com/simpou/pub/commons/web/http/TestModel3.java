package br.com.simpou.pub.commons.web.http;

import br.com.simpou.pub.commons.utils.rand.Randomizable;
import br.com.simpou.pub.commons.utils.rand.Randomizer;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * @author jonas1
 * @version 23/09/13
 * @since 23/09/13
 */
@Getter
@Setter
public class TestModel3 implements Randomizable {

    private TestModel4 recur4;

    @Override
    public void fillRandom() {
        throw new UnsupportedOperationException("Use randomizer.");
    }

    @RequiredArgsConstructor
    static class Randz implements Randomizer<TestModel3> {

        final TestModel4 testModel;


        @Override
        public void fillRandom(TestModel3 model) {
            model.recur4 = testModel;
        }
    }

}
