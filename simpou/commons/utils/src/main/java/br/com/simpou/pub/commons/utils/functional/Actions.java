package br.com.simpou.pub.commons.utils.functional;

import java.util.ArrayList;
import java.util.List;

/**
 * Operações úteis sobre ações.
 *
 * @author Jonas Pereira
 * @since 2013-06-26
 */
public final class Actions {

    /**
     * Transforma uma ação em uma ação silenciosa, que ignora completamente exceções lançadas, as mesmas são
     * suprimidas e não propagadas. Útil para os casos em que existe a certeza de que erros não ocorrerão ou
     * são insignificantes. O retorno é null em caso de erro.
     */
    public static <P, R> SilentAction<P, R> asSilentIgnoreErrors(final Action<P, R> action) {
        return new SilentActionContainer<P, R>(action) {
            @Override
            public void doOnError(final P object, final Throwable t) {
            }
        };
    }

    /**
     * Transforma uma ação em uma ação silenciosa. Lança todas exceções como RuntimeException.
     */
    public static <P, R> SilentAction<P, R> asSilentThrowErrors(final Action<P, R> action) {
        return new BasicSilentAction<P, R>() {
            @Override
            protected R applyDelegate(final P object) throws Exception {
                return action.apply(object);
            }
        };
    }

    /**
     * @param iterable      Items sobre os quais serão aplicados uma ação um a um.
     * @param action        Ação que será aplicada sobre os todos os items um a um.
     * @param stopCondition Condição de parada. Se retornar true para o objeto atual o processamento é
     *                      interrompido e retornada a lista com os items presentes no momento. O objeto que
     *                      gerou a condição de parada não é incluído nesta lista.
     * @return Lista com os resultados dos processamentos na ordem em que são iterados.
     */
    public static <T, E> List<E> apply(final Iterable<T> iterable,
                                       final Action<T, E> action,
                                       final Condition<T> stopCondition) {
        final List<E> results = new ArrayList<>();
        for (final T item : iterable) {
            if (stopCondition.apply(item)) {
                break;
            }
            results.add(action.apply(item));
        }
        return results;
    }

    /**
     * @param iterable      Items sobre os quais serão aplicados uma ação um a um.
     * @param action        Ação que será aplicada sobre os todos os items um a um.
     * @param stopCondition Condição de parada. Se retornar true para o objeto atual o processamento é
     *                      interrompido.
     */
    public static <T, E> void apply(final Iterable<T> iterable,
                                    final VoidAction<T> action,
                                    final Condition<T> stopCondition) {
        for (final T item : iterable) {
            if (stopCondition.apply(item)) {
                break;
            }
            action.apply(item);
        }
    }

    /**
     * @param iterable Items sobre os quais serão aplicados uma ação um a um.
     * @param action   Ação que será aplicada sobre os todos os items um a um.
     * @return Lista com os resultados dos processamentos na ordem em que são iterados. Resultados null não
     * são incluídos.
     */
    public static <T, E> List<E> apply(final Iterable<T> iterable,
                                       final Action<T, E> action) {
        final List<E> results = new ArrayList<>();
        for (final T item : iterable) {
            final E result = action.apply(item);
            if (result != null) {
                results.add(result);
            }
        }
        return results;
    }

    /**
     * @param iterable Items sobre os quais serão aplicados uma ação um a um.
     * @param action   Ação que será aplicada sobre os todos os items um a um.
     * @return Lista com os resultados dos processamentos na ordem em que são iterados.
     */
    public static <T, E> List<E> apply(final Iterable<T> iterable,
                                       final SilentAction<T, E> action) {
        final List<E> results = new ArrayList<>();
        for (final T item : iterable) {
            results.add(Actions.apply(action, item));
        }
        return results;
    }

    /**
     * Aplica uma ação a um determinado valor. Caso uma exceção seja lançada, o tratamento de erro da ação é
     * invocado.
     *
     * @return Resultado da ação ou null caso alguma exceção seja lançada no meio do processo.
     * @see SilentAction#doOnError(Object, Throwable)
     */
    public static <P, R> R apply(final SilentAction<P, R> action, final P value) {
        try {
            return action.apply(value);
        } catch (Throwable t) {
            action.doOnError(value, t);
            return action.getDefault();
        }
    }

    /**
     * @param iterable Items sobre os quais serão aplicados uma ação um a um.
     * @param action   Ação que será aplicada sobre os todos os items um a um.
     */
    public static <T, E> void apply(final Iterable<T> iterable,
                                    final VoidAction<T> action) {
        for (final T item : iterable) {
            action.apply(item);
        }
    }

    public static void apply(final NoParamsAction action) {
        action.apply(null);
    }

    /**
     * @param input Entrada a ser processada se não nula.
     * @param condition Condição a ser verificada sobre a entrada para que a ação seja executada.
     * @param action Ação a ser executada.
     */
    public static <I> void conditional(final I input,
                                       final Action<I, Boolean> condition,
                                       final VoidAction<I> action) {
        if (input != null && condition.apply(input)) {
            action.apply(input);
        }

    }
}
