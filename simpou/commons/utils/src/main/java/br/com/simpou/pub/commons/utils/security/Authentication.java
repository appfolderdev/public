package br.com.simpou.pub.commons.utils.security;

/**
 * Created by jonas on 12/01/2017.
 */
public interface Authentication {

    /**
     * @return Credenciais para autenticação. O tipo de retorno vai depender do tipo suportado pelo
     * autorizador
     */
    Object getCredentials();

}
