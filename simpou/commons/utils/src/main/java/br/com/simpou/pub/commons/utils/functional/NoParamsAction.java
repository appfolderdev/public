package br.com.simpou.pub.commons.utils.functional;

/**
 * Realiza uma ação que não gera retornos e não necessita de parâmetros.
 *
 * @author Jonas Pereira
 * @version 2018-12-09
 */
public abstract class NoParamsAction extends VoidAction<Void> {
    /**
     * Ação a ser executada.
     */
    protected abstract void apply();

    @Override
    protected final void executeDelegate(Void object) {
        apply();
    }
}
