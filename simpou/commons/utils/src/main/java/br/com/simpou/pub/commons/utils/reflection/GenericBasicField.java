package br.com.simpou.pub.commons.utils.reflection;

import lombok.RequiredArgsConstructor;

import java.lang.reflect.Field;

/**
 * Condições que exige que um campo seja de um tipo básico. São considerados básicos os tipos primitivos e
 * seus boxings correspondentes, string, date, bigdecimal e enum. Campos genéricos de super classes são
 * considerados.
 *
 * @author Jonas Pereira
 * @since 2013-10-15
 */
@RequiredArgsConstructor
public class GenericBasicField extends BasicField {

    private final Class<?> subClass;

    @Override
    public Boolean apply(final Field field) {
        return isBasic(Reflections.getFieldType(subClass, field));
    }
}
