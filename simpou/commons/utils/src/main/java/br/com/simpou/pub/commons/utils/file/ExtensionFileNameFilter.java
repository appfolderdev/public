package br.com.simpou.pub.commons.utils.file;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Filtro sobre listagem de diretórios. Verifica se os arquivos listados
 * contêm uma extensão especificada.
 *
 * @author Jonas Pereira
 * @version 2011-07-12
 * @since 2011-07-12
 */
public final class ExtensionFileNameFilter implements FilenameFilter {

    /**
     * Extensão do arquivo sem ponto.
     */
    private final String extension;

    /**
     * <p>Constructor for ExtensionFileNameFilter.</p>
     *
     * @param extension Extensão do arquivo sem ponto.
     */
    public ExtensionFileNameFilter(final String extension) {
        this.extension = "." + extension;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean accept(final File dir, final String name) {
        return name.endsWith(extension);
    }
}
