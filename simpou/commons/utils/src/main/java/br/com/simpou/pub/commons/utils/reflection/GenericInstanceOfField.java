package br.com.simpou.pub.commons.utils.reflection;

import java.lang.reflect.Field;

/**
 * Condições que verifica se um campo é de um determinado tipo. Tipos genéricos em super classes são
 * considerados.
 *
 * @author Jonas Pereira
 * @since 2013-10-22
 */
public class GenericInstanceOfField
        extends InstanceOfField {

    private final Class<?> clasz;

    public GenericInstanceOfField(final Class<?> superClass, final Class<?> subClass) {
        super(superClass);
        clasz = subClass;
    }

    @Override
    protected Class<?> getType(Field field) {
        return Reflections.getFieldType(clasz, field);
    }
}
