package br.com.simpou.pub.commons.utils.functional;

import lombok.RequiredArgsConstructor;

/**
 * Container para outra ação. Não executa nada além da ação encapsulada. O tratamento de exceções é delegado
 * às subclasses.
 *
 * @author Jonas Pereira
 * @since 5/18/15
 */
@RequiredArgsConstructor
public abstract class SilentActionContainer<P, R> implements SilentAction<P, R> {

    private final Action<P, R> action;

    @Override
    public final R apply(final P object) {
        try {
            return applyDelegate(object);
        } catch (Exception e) {
            doOnError(object, e);
            return getDefault();
        }
    }

    protected R applyDelegate(final P object) throws Exception {
        return action.apply(object);
    }

    @Override
    public R getDefault() {
        return null;
    }
}

