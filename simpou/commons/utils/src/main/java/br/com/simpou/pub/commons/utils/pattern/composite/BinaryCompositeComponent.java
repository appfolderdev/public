package br.com.simpou.pub.commons.utils.pattern.composite;

import br.com.simpou.pub.commons.utils.functional.Action;
import br.com.simpou.pub.commons.utils.lang.Casts;
import lombok.AccessLevel;
import lombok.Getter;

import java.util.Map;

/**
 * @author Jonas Pereira
 * @since 03/03/2017
 */
class BinaryCompositeComponent<P, R> extends AbstractCompositeComponent<P, R> {

    private final AbstractCompositeComponent<P, R> operand;

    @Getter(AccessLevel.PROTECTED)
    private final BinaryCompositeOperation<P, R> operation;

    private BinaryCompositeComponent(final P basic,
                                     final CompositeComponent<P, R> composite,
                                     final CompositeComponent<P, R> operand,
                                     final CompositeOperation<P, R> operation) {
        super(
                basic,
                (AbstractCompositeComponent<P, R>) composite,
                getStringId(basic, composite, operand, operation)
        );
        this.operation = Casts.simpleCast(operation);
        this.operand = Casts.simpleCast(operand);
    }

    private static <P, R> String getStringId(final P basic,
                                             final CompositeComponent<P, R> composite,
                                             final CompositeComponent<P, R> operand,
                                             final CompositeOperation<P, R> operation) {
        if (basic == null) {
            return toStringWrap(composite) + operation + toStringWrap(operand);
        } else {
            return basic.toString() + operation + toStringWrap(operand);
        }
    }

    BinaryCompositeComponent(final P basic,
                             final CompositeComponent<P, R> operand,
                             final CompositeOperation<P, R> operation) {
        this(basic, null, operand, operation);
    }

    BinaryCompositeComponent(final CompositeComponent<P, R> composite,
                             final CompositeComponent<P, R> operand,
                             final CompositeOperation<P, R> operation) {
        this(null, composite, operand, operation);
    }

    @Override
    public boolean isComposite() {
        return operand != null;
    }

    @Override
    protected R getValue(final R value, final Action<P, R> conversor,
                         final Map<CompositeComponent, R> componentCache,
                         final Map<P, R> conversionCache) {
        // resolve valor com o qual será operado
        final R operandValue = getValue(operand, conversor, componentCache, conversionCache);
        ;
        // valor operação operando = resultado
        return operation.operate(value, operandValue);
    }
}
