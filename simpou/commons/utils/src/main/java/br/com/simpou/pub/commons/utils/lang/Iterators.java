package br.com.simpou.pub.commons.utils.lang;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 9000070 on 29/03/2017.
 */
public class Iterators {

    public static <T> List<T> iterate(final Iterable<T> data) {
        final List<T> list = new ArrayList<>();
        for (T item : data) {
            list.add(item);
        }
        return list;
    }

}
