package br.com.simpou.pub.commons.utils;

import java.util.Date;

/**
 * Created by jonas.pereira on 10/03/16.
 */
public class Constants {

    /**
     * Data bastante antiga: 01/01/70 00:00
     */
    public static final Date MOST_OLD_DATE = new Date(1);

    /**
     * Encoding padrão de toda aplicação.
     */
    public static final String DEFAULT_CHARSET = "UTF-8";

}
