package br.com.simpou.pub.commons.utils.rand;

import br.com.simpou.pub.commons.utils.functional.Action;
import lombok.RequiredArgsConstructor;

/**
 * Created by jonas on 13/01/2017.
 */
@RequiredArgsConstructor
public class RandomizerActionWrapper<T> implements Randomizer<T>, Action<T, T> {

    private final Randomizer<T> action;

    @Override
    public T apply(final T obj) {
        action.fillRandom(obj);
        return obj;
    }

    @Override
    public void fillRandom(final T obj) {
        action.fillRandom(obj);
    }
}
