package br.com.simpou.pub.commons.utils.file;

import lombok.AllArgsConstructor;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Filtro sobre listagem de diretórios. Verifica se os arquivos listados
 * obedecem a uma expressão regular.
 *
 * @author Jonas Pereira
 * @version 2011-09-16
 * @since 2011-09-16
 */
@AllArgsConstructor
public final class PatternFileNameFilter implements FilenameFilter {

    /**
     * Java Pattern/expressão regular..
     */
    private final String pattern;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean accept(final File dir, final String name) {
        return name.matches(pattern);
    }
}
