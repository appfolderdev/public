package br.com.simpou.pub.commons.utils.lang;

import java.util.*;

/**
 * Operações úteis sobre coleções.
 *
 * @author Jonas Pereira
 * @since 2013-02-09
 */
public class CollectionsHelper {

    public static <T> Set<T> asSet(final T... values) {
        return Collections.unmodifiableSet(new HashSet<>(Arrays.asList(values)));
    }

    /**
     * Compara dois Sets. São considerados iguais se os mesmos elementos do primeiro estiverem contidos no
     * segundo e vice-versa, ou seja, devem conter os mesmos elementos independente de suas posições.
     * <p/>
     * Também são considerados iguais se ambos forem null.
     *
     * @param set1 Primeiro conjunto.
     * @param set2 Segundo conjunto.
     * @return true se forem iguais.
     */
    public static boolean equals(final Set<?> set1, final Set<?> set2) {
        if (set1 == null) {
            return set2 == null;
        }

        if (set2 == null) {
            return false;
        }

        if (set1.size() != set2.size()) {
            return false;
        }

        for (final Object item : set1) {
            if (!set2.contains(item)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param list   Lista onde será buscado um elemento,
     * @param object Objeto a ser procurado.
     * @return Posição do objeto na lista ou -1 se não encontrado.
     */
    public static <T> int search(final List<T> list, final T object) {
        int count = 0;
        for (final T t : list) {
            if (t.equals(object)) {
                return count;
            }
            count++;
        }
        return -1;
    }

    /**
     * @param list   Lista.
     * @param offset Índice do primeiro item a ser incluído.
     * @param size   Quantidade de items a serem incluídos a partir do primeiro.
     * @return Sublista.
     */
    public static <T> List<T> subList(final List<T> list, final int offset, final int size) {
        final List<T> subList = new ArrayList<>(size);
        final int last = Math.min(offset + size, list.size());
        for (int i = offset; i < last; i++) {
            subList.add(list.get(i));
        }
        return Collections.unmodifiableList(subList);
    }
}
