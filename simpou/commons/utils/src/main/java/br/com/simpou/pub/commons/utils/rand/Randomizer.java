package br.com.simpou.pub.commons.utils.rand;

/**
 * Externaliza o preenchimento de um objeto com dados aleatórios válidos.
 *
 * @author Jonas Pereira
 * @version 2013-06-08
 * @see Randomizable
 * @since 2013-06-08
 */
public interface Randomizer<T> {

    /**
     * @param t Objeto que será preenchido com dados aleatórios válidos.
     */
    void fillRandom(T t);

}
