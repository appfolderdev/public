package br.com.simpou.pub.commons.utils.file;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Filtro sobre listagem de diretórios. Verifica se os arquivos listados
 * são do tipo diretório.
 *
 * @author Jonas Pereira
 * @version 2011-07-12
 * @since 2011-07-12
 */
public final class DirectoryFileNameFilter implements FilenameFilter {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean accept(final File dir, final String name) {
        return new File(dir.getAbsolutePath() + "/" + name).isDirectory();
    }
}
