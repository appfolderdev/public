package br.com.simpou.pub.commons.utils.pattern.composite;

/**
 * Regras para operar dois componentes compostos.
 *
 * @author Jonas Pereira
 * @since 03/03/2017
 */
public interface BinaryCompositeOperation<P, R> extends CompositeOperation<P, R> {

    R operate(final R value, final R operand);

}
