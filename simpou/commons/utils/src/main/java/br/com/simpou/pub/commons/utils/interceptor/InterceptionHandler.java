package br.com.simpou.pub.commons.utils.interceptor;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Executa os métodos de interceptação e interceptados.
 *
 * @author Jonas Pereira
 * @since 03/06/15
 */
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class InterceptionHandler<T, E extends T> implements InvocationHandler {

    private final InterceptionContext<T> context;

    /**
     * Objeto cujos métodos serão interceptados.
     */
    private final E intercepted;

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        if (!context.canIntercept(method)) {
            // método não deve ser interceptado
            return proceed(method, args);
        }

        Object result = null;

        final Interceptor interceptor = context.getInterceptor();
        try {
            interceptor.doBefore(method, args);
            result = proceed(method, args);
        } catch (Throwable t) {
            return interceptor.doOnError(t);
        } finally {
            interceptor.doAfter(result);
        }

        return result;
    }

    /**
     * Invoca o método de fato.
     */
    private Object proceed(final Method method, final Object[] args)
            throws IllegalAccessException, InvocationTargetException {
        return method.invoke(intercepted, args);
    }
}
