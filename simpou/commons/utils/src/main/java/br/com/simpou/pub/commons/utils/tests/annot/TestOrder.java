package br.com.simpou.pub.commons.utils.tests.annot;

import java.lang.annotation.*;

/**
 * Define a ordem em que um teste deve ser executado. O primeiro a executar
 * será o de ordem igual a um. Se a ordem não for definida, é considerado que
 * o método pode ser executado a qualquer momento.
 *
 * @author Jonas Pereira
 * @version 2012-05-15
 * @since 2012-05-15
 */
@Deprecated
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD
})
public @interface TestOrder {

    /**
     * Ordem em que um teste deve ser executado.
     */
    int value() default 1;
}
