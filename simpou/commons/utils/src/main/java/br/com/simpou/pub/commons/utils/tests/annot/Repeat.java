package br.com.simpou.pub.commons.utils.tests.annot;

import java.lang.annotation.*;

/**
 * Define um número de repetições de uma ação.
 *
 * @author Jonas Pereira
 * @version 2011-07-13
 * @since 2011-07-13
 */
@Deprecated
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,
        ElementType.TYPE
})
public @interface Repeat {

    /**
     * Número de repetições do método de teste.
     */
    int value() default 1;
}
