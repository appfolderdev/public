package br.com.simpou.pub.commons.utils.violation;

import br.com.simpou.pub.commons.utils.behavior.TypeNamed;
import br.com.simpou.pub.commons.utils.lang.Nulls;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Descrição da violação de uma regra.
 *
 * @author Jonas Pereira
 * @since 2012-12-28
 */
@EqualsAndHashCode(of = "code")
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Violation implements Comparable<Violation>, Serializable, TypeNamed {

    public static final String TYPE_NAME = "violation";

    @Getter
    @Setter
    private String type;

    /**
     * Código da regra violada. Este código é a chave de um bundle file de tradução para a mensagem
     * segundo o locale do usuário.
     */
    @Getter
    @Setter
    private String code;

    /**
     * Mensagem internacionalizada associada à regra violada. Deve existir um arquivo properties para cada locale do
     * sistema que descreve como o código do erro se relaciona com as mensagens traduzidas. Deve seguir o modelo de
     * tradução padrão do JAVA pelo ResourceBundle.
     */
    @Getter
    @Setter
    private String message;

    /**
     * Uma mensagem pode definir parâmetros de substituição nos arquivos de bundle. Esses parâmetros são da forma:
     * {num}, onde "num" é um número identificador da posição do parãmetro no array de objetos a ser inserido na
     * string.
     */
    @JsonIgnore
    private Object[] messageParams;

    /**
     * @param code          Código da regra violada. Este código é a chave de um bundle file de tradução para a
     *                      mensagem segundo o locale do usuário.
     * @param message       Mensagem internacionalizada associada à regra violada. Deve existir um arquivo properties
     *                      para cada locale do sistema que descreve como o código do erro se relaciona com as
     *                      mensagens traduzidas. Deve seguir o modelo de tradução padrão do JAVA pelo
     *                      ResourceBundle.
     * @param messageParams Uma mensagem pode definir parâmetros de substituição nos arquivos de bundle. Esses
     *                      parâmetros são da forma: {num}, onde "num" é um número identificador da posição do
     *                      parãmetro no array de objetos a ser inserido na string.
     */
    public Violation(final String code, final String message, final Object... messageParams) {
        this.code = code;
        this.message = message;
        this.messageParams = Nulls.clone(messageParams);
    }

    /**
     * @param code          Código da regra violada. Este código é a chave de um bundle file de tradução para a
     *                      mensagem segundo o locale do usuário.
     * @param messageParams Uma mensagem pode definir parâmetros de substituição nos arquivos de bundle. Esses
     *                      parâmetros são da forma: {num}, onde "num" é um número identificador da posição do
     *                      parãmetro no array de objetos a ser inserido na string.
     */
    public Violation(final String code, final Object... messageParams) {
        this.code = code;
        this.messageParams = Nulls.clone(messageParams);
    }

    @Override
    public int compareTo(final Violation violation) {
        return this.code.compareTo(violation.getCode());
    }

    public String getMessageOrCode() {
        return this.message == null ? this.code : this.message;
    }

    /**
     * @return Uma mensagem pode definir parâmetros de substituição nos arquivos de bundle. Esses parâmetros são da
     * forma: {num}, onde "num" é um número identificador da posição do parâmetro no array de objetos a ser
     * inserido na string.
     */
    public Object[] getMessageParams() {
        return Nulls.clone(this.messageParams);
    }

    /**
     * Setter para {@link Violation#messageParams}.
     *
     * @param messageParams mensagens.
     */
    public void setMessageParams(final Object... messageParams) {
        this.messageParams = Nulls.clone(messageParams);
    }

    @Override
    public String typeName() {
        return TYPE_NAME;
    }
}
