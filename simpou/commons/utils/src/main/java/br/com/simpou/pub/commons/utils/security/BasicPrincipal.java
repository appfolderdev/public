package br.com.simpou.pub.commons.utils.security;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.security.Principal;

/**
 * Created by jonas on 14/01/2017.
 */
@Getter
@RequiredArgsConstructor
public class BasicPrincipal implements Principal {

    private final String name;

}
