package br.com.simpou.pub.commons.utils.lang.annot;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Configura a geração aleatória de algo quantificável. Define a quantidade, valor determinado aleatoriamente
 * por meio de uma faixa viável, de elementos que serão produzidos aleatoriamente.
 *
 * @author Jonas Pereira
 * @version 2012-07-10
 * @since 2011-06-10
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RandomFillableConfig {

    /**
     * Quantidade mínima padrão.
     */
    int MIN_LENGTH = 10;

    /**
     * Quantidade máxima padrão.
     */
    int MAX_LENGTH = 100;

    /**
     * @return Quantidade mínima de elementos que podem ser gerados.
     */
    int minLength() default MIN_LENGTH;

    /**
     * @return Quantidade máxima de elementos que podem ser gerados.
     */
    int maxLength() default MAX_LENGTH;

    /**
     * @return true indica não deve ser gerado nenhum elemento.
     */
    boolean ignore() default false;
}
