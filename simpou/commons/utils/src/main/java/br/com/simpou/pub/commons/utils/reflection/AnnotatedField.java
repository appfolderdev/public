package br.com.simpou.pub.commons.utils.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Condições que exige que um campo esteja anotado com uma determinada anotação.
 *
 * @author Jonas Pereira
 * @version 2013-06-02
 * @since 2013-06-02
 */
public class AnnotatedField
        extends AnnotatedElementCondition<Field>
        implements FieldCondition {

    /**
     * @param annotation Anotação a ser verificada.
     */
    public AnnotatedField(final Class<? extends Annotation> annotation) {
        super(annotation);
    }
}
