package br.com.simpou.pub.commons.utils.lang;

public class Booleans {

    public static boolean is(final Boolean obj, final boolean condition) {
        if (obj == null) {
            return false;
        } else {
            return obj == condition;
        }
    }

    public static boolean isTrue(final Boolean obj) {
        return is(obj, true);
    }

    public static boolean isFalse(final Boolean obj) {
        return is(obj, false);
    }

}
