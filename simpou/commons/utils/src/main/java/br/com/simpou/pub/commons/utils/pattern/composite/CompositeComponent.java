package br.com.simpou.pub.commons.utils.pattern.composite;

import br.com.simpou.pub.commons.utils.functional.Action;

import java.io.Serializable;

/**
 * Tipo base para encadeamente de operações via componentes compostos. Use em situações nas quais se deseja
 * operar dois objetos de forma recursiva e infinitamente. O tipo associado ao componente poderá ser
 * diferente do tipo a ser operado de fato.
 *
 * @param <P> Tipo do objeto associado ao componente. O toString deste objeto assim como o dos operadores são
 *            usados para compor o toString geral que, por sua vez, são usados para diferenciar componentes
 *            via equals e hashcode.
 * @param <R> Resultado da conversão do objeto associado. Objeto a ser operado de fato com os outros
 *            componentes.
 * @author Jonas Pereira
 * @see Composites Para instanciar componentes;
 * @since 03/03/2017
 */
public interface CompositeComponent<P, R> extends Serializable {

    /**
     * @param conversor Conversor do tipo parametrizado ou associado para o tipo operacional. Use para os
     *                  casos em os componentes em si ainda não são capazes de converter os objetos a serem
     *                  operados
     * @return Resultado final acumulado de todas operações encadeadas recursivamente dos componentes
     * associados.
     */
    R getValue(Action<P, R> conversor);

    /**
     * @param component Componente com o qual o atual será operado. Na resolução da operação o objeto atual é
     *                  parametrizado primeir e o componente vem em seguida.
     * @param operation Regra de negócio para operar dois objetos.
     * @return Novo componente composto. Seu {@link #getValue(Action)} irá invocar explicitamente a operação
     * entre o objeto atual e o componente parametrizado.
     */
    CompositeComponent<P, R> compose(CompositeComponent<P, R> component, CompositeOperation<P, R> operation);

    /**
     * @param basic Transformado em componente unário.  O componente atual será operado com esta novo.
     * @see #compose(CompositeComponent, CompositeOperation)
     */
    CompositeComponent<P, R> compose(P basic, CompositeOperation<P, R> operation);

    /**
     * @return true se componente é composto por outros, ou seja, não é um componente básico.
     */
    boolean isComposite();
}
