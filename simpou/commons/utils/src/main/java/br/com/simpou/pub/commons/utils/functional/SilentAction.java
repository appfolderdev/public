package br.com.simpou.pub.commons.utils.functional;

/**
 * Adiciona um tratamento para possíveis erros que possam ocorrer durante a execução de uma ação.
 *
 * @author Jonas Pereira
 * @since 14/05/15.
 */
public interface SilentAction<P, R> extends Action<P, R> {

    /**
     * Executado se alguma exceção for lançada durante {@link Action#apply(Object)
     * execução de uma ação} .
     *
     * @param object Objeto parametrizado da execução no momento do erro.
     * @param e      Exceção lançada durante execução da ação.
     */
    void doOnError(P object, Throwable e);

    /**
     * @return Valor padrão a ser retornado em caso de erro.
     */
    R getDefault();
}
