package br.com.simpou.pub.commons.utils.cipher;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Auxilia na geração de MessageDigest estáticos.
 *
 * @author Jonas Pereira
 * @since 2011-09-26
 */
public class MessageDigests {

    /**
     * Gera o MessageDigest e converte exceção NoSuchAlgorithmException para unchecked RuntimeException. Útil
     * em contextos estáticos onde não se deseja realizar tratamento de erros em tempo de compilação.
     *
     * @param algorithm {@link Algorithms Algoritmo}.
     */
    public static MessageDigest getDigest(final String algorithm) {
        try {
            return MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
    }
}
