package br.com.simpou.pub.commons.utils.functional;

import br.com.simpou.pub.commons.utils.lang.PrintStackHelper;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Use para encadeamento de ações. Interessante para uso com aspectos pois garante uma maior coesão e pode
 * evitar side-effects. Classe voltada para obtenção das desejáveis características para execução de ações em
 * série: contenção, encadeamento e isolamento.
 *
 * @author Jonas Pereira
 * @since 14/05/15
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(of = "value")
public final class Monad<A> {

    /**
     * Valor atual armazenado no monad. Atua como a parte de contenção do Monad.
     */
    @Getter
    private final A value;

    /**
     * @return Monad do tipo parametrizado contendo a referência para o mesmo.
     */
    public static <A> Monad<A> unit(final A value) {
        return new Monad<>(value);
    }

    /**
     * Executa uma ação, permitindo o encadeamento de outras ações. O encadeamento é interrompido se alguma
     * exceção for lançada durante o processo, ou seja, não garante o encadeamento.
     */
    public <B> Monad<B> flatMap(final Action<A, Monad<B>> action) {
        return action.apply(value);
    }

    /**
     * @see #flatMap(Action)
     * @see Actions#apply(SilentAction, Object)
     * <p>
     * Garante o encadeamento e o isolamento de execução de ações em série. Em caso de error o metodo {@link
     * SilentAction#doOnError(Object, Throwable)} é invocado e, caso ainda
     * sim ocorra algum erro, o stacktrace e impresso e a cadeia de execução é prosseguida normalmente.
     */
    public Monad<A> flatMap(final SilentAction<A, Monad<A>> action) {
        try {
            Actions.apply(action, value);
        } catch (Throwable t) {
            PrintStackHelper.logStackTrace(t);
        }
        return this;
    }

}
