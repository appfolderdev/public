package br.com.simpou.pub.commons.utils.violation;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Exceção indicativa de violação de regras do aplicativo, sejam elas de negócio, de sistema ou outro tipo
 * qualquer.
 *
 * @author Jonas Pereira
 * @since 2012-12-28
 */
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class ViolationsException extends RuntimeException {

    /**
     * Conteúdo ou corpo da resposta HTTP. As violações serão e convertidas para este formato e o resultado de
     * sua conversão para o mime type solicitado resultará no conteúdo da mensagem de resposta.
     */
    @Getter
    private final ViolationsWrapper entity;

    /**
     * Ver {@link #ViolationsException(Collection)}
     *
     * @param violations Ver {@link #entity}.
     */
    public ViolationsException(final Violation... violations) {
        this(Arrays.asList(violations));
    }

    public ViolationsException(final Exception cause,
                               final Violation... violations) {
        this(cause, Arrays.asList(violations));
    }

    /**
     * @param violations Ver {@link #entity}. Violações irão compor o corpo da resposta no formato de um
     *                   objeto do tipo {@link ViolationsWrapper}.
     *                   Violações são ordenadas segundo seu mecanismo natural e conjuntos vazios são
     *                   removidos.
     */
    public ViolationsException(final Collection<? extends Violation> violations) {
        this(null, violations);
    }

    public ViolationsException(final Exception cause,
                               final Collection<? extends Violation> violations) {
        super(cause);
        this.entity = new ViolationsWrapper(violations);
    }

    @Override
    public String getMessage() {
        final StringBuilder msg = new StringBuilder();
        final List<Violation> violations = this.entity.joinAll();
        for (final Violation violation : violations) {
            if (violation.getMessage() == null) {
                msg.append(violation.getCode());
            } else {
                msg.append(violation.getMessage());
            }
            msg.append(' ');
        }
        return msg.toString().trim();
    }
}
