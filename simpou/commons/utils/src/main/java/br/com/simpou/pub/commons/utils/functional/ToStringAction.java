package br.com.simpou.pub.commons.utils.functional;

import java.util.Objects;

/**
 * Não realiza nenhuma ação, apenas ecoa a entrada para a saída via método toString.
 *
 * @author Jonas Pereira
 * @since 2016-03-08
 */
public final class ToStringAction<T> implements Action<T, String> {

    @Override
    public String apply(final T object) {
        return Objects.toString(object);
    }

}
