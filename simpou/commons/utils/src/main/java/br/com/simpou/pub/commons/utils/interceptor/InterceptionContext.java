package br.com.simpou.pub.commons.utils.interceptor;

import br.com.simpou.pub.commons.utils.functional.Condition;
import br.com.simpou.pub.commons.utils.reflection.Reflections;
import br.com.simpou.pub.commons.utils.lang.Casts;
import lombok.AccessLevel;
import lombok.Getter;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Contexto de interceptação formado por um tipo a ser interceptado e seu interceptador. Utilizando-se de um
 * contexto, várias intâncias do tipo especificado podem ser interceptadas.
 *
 * @author Jonas Pereira
 * @since 03/06/15
 */
public class InterceptionContext<T> {

    /**
     * Interceptador associado ao contexto. Um mesmo interceptador pode interceptar um número qualquer de
     * objetos do mesmo tipo do parametrizado.
     */
    @Getter(AccessLevel.PACKAGE)
    private final Interceptor interceptor;

    /**
     * @see Interception#interceptableType
     */
    private final Class<T> ifaceClass;

    private final InterceptCondition interceptCondition;

    InterceptionContext(final Interceptor interceptor,
                        final Class<T> ifaceClass,
                        final List<Method> methods) {
        this.interceptor = interceptor;
        this.ifaceClass = ifaceClass;
        this.interceptCondition = new InterceptCondition(ifaceClass, methods);

    }

    /**
     * @param intercepted Instância cujos métodos serão interceptados.
     * @return Nova intância interceptada.
     */
    public <E extends T> T intercept(final E intercepted) {
        final InvocationHandler handler = new InterceptionHandler<>(
                this,
                intercepted
        );
        final Class<?>[] instances = {ifaceClass};
        return Casts.objCast(ifaceClass,
                Proxy.newProxyInstance(ifaceClass.getClassLoader(), instances, handler),
                true
        );
    }

    boolean canIntercept(final Method method) {
        return interceptCondition.apply(method);
    }

    /**
     * Condição que define se um método será interceptado ou não.
     */
    private final class InterceptCondition implements Condition<Method> {

        /**
         * Armazena se método deve sofrer interceptação ou não para agilizar consultas semelhantes
         * posteriores.
         */
        private final Map<Method, Boolean> mapIntercept;

        /**
         * Métodos declarados explicitamente para serem interceptados, os outros não serão.
         */
        private final List<Method> interceptedMethods;

        /**
         * Interface de interceptação e suas superiores.
         */
        private final List<Class<?>> ifacesClass;

        private InterceptCondition(final Class<T> ifaceClass,
                                   final List<Method> methods) {
            this.interceptedMethods = methods;
            this.mapIntercept = new HashMap<>();
            this.ifacesClass = Reflections.getInterfaces(ifaceClass);
            this.ifacesClass.add(ifaceClass);
        }

        @Override
        public Boolean apply(final Method method) {
            // método em cache, regra já é conhecida
            if (mapIntercept.containsKey(method)) {
                return mapIntercept.get(method);
            }

            if (!interceptedMethods.isEmpty()) {
                if (!interceptedMethods.contains(method)) {
                    // regra: se métodos a serem interceptados forem explicitamente
                    // informados, somente eles serão interceptados
                    mapIntercept.put(method, false);
                    return false;
                }
            } else {
                // regra: métodos não declarados na interface e em suas superiores
                // não são interceptados
                boolean isDeclared = false;

                for (final Class<?> classs : ifacesClass) {
                    isDeclared |= Reflections.isDeclared(classs, method);
                }

                if (!isDeclared) {
                    mapIntercept.put(method, false);

                    return false;
                }
            }

            mapIntercept.put(method, true);

            return true;
        }
    }

}
