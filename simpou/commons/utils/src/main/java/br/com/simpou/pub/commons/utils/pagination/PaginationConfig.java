package br.com.simpou.pub.commons.utils.pagination;

import br.com.simpou.pub.commons.utils.lang.Assertions;
import lombok.Getter;

import java.io.Serializable;

/**
 * Configuração de uma paginação.
 *
 * @author Jonas Pereira
 * @since 2011-07-12
 */
@Getter
public final class PaginationConfig implements Serializable {

    private final int pageSize;

    private final long elements;

    private final int pagesToShow;

    /**
     * Inicializa campos.
     *
     * @param pageSize    Quantidade máxima de elementos por página.
     * @param elements    Quantidade total de elementos.
     * @param pagesToShow Quantidade de páginas a serem exibidas em listas. Se o número total de páginas for
     *                    maior que este número, a lista de páginas será uma janela deslocada com as páginas
     *                    mais próximas da página atual.
     */
    public PaginationConfig(final int pageSize, final long elements,
                            final int pagesToShow) {
        this.pageSize = Assertions.greaterThan(pageSize, 0);
        this.elements = Assertions.greaterThan(elements, 0L);
        this.pagesToShow = Assertions.greaterThan(pagesToShow, 0);
    }

}
