package br.com.simpou.pub.commons.utils.tests;

import lombok.RequiredArgsConstructor;

/**
 * Created by jonas.pereira on 27/05/15.
 */
@Deprecated
public final class Tests {

    private static final Object NEVER_EQUALS_OBJ = new Object() {
        @Override
        public boolean equals(final Object obj) {
            return this == obj;
        }

    };

    public static <T> void testEqualsAndHashCode(final T obj,
                                                 final T equalsObj,
                                                 final T notEquals,
                                                 final T nullObj,
                                                 final T notSameNullObj) {
        //        Assert.assertEquals(obj, obj);
        //        Assert.assertNotEquals(obj, NEVER_EQUALS_OBJ);
        //        Assert.assertNotEquals(obj, null);
        //        Assert.assertNotEquals(obj, notEquals);
        //        Assert.assertEquals(obj, equalsObj);
        //        Assert.assertNotEquals(obj, nullObj);
        //        Assert.assertNotEquals(nullObj, obj);
        //        Assert.assertEquals(nullObj, notSameNullObj);
        //        Assert.assertEquals(obj.hashCode(), equalsObj.hashCode());
        //        Assert.assertEquals(nullObj.hashCode(), nullObj.hashCode());
    }

    @RequiredArgsConstructor
    private static class EqualsObjContainer<T> {

        private final T value;

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final EqualsObjContainer that = (EqualsObjContainer) o;

            return this.value.equals(that.value);

        }

        @Override
        public int hashCode() {
            return this.value.hashCode();
        }
    }

}
