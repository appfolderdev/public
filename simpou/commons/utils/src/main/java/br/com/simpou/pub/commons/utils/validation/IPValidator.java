package br.com.simpou.pub.commons.utils.validation;

import br.com.simpou.pub.commons.utils.violation.Violation;
import br.com.simpou.pub.commons.utils.string.RegexHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validador de IP.
 *
 * @author Jonas Pereira
 * @version 2013-05-31
 * @since 2011-06-25
 */
public final class IPValidator implements Validator<String> {

    /**
     * Pattern.
     */
    static final Pattern PATTERN = Pattern.compile(RegexHelper.IP);

    static final String INVALID_LENGTH = "simpou.commons.utils.validation.ip.length";

    static final String NOT_MATCH = "simpou.commons.utils.validation.ip.notMatch";

    /**
     * <p>isValid.</p>
     *
     * @param value Valor.
     * @return Se valor é válido. Valor null é considerado válido.
     */
    public static Violation isValid(final String value) {
        final Violation violation;

        if (value == null || value.isEmpty()) {
            violation = null;
        } else if (value.length() < 7 || value.length() > 15) {
            violation = new Violation(INVALID_LENGTH);
        } else {
            final Matcher m = PATTERN.matcher(value);

            if (m.matches()) {
                violation = null;
            } else {
                violation = new Violation(NOT_MATCH);
            }
        }

        return violation;
    }

    /**
     * @param ip IP a ser validado.
     */
    @Override
    public Violation validate(final String ip) {
        return isValid(ip);
    }

//    @Getter
//    @AllArgsConstructor
//    public enum Violation implements RuleViolation {
//        NOT_MATCH("IP doesnt match " + RegexHelper.IP),
//        INVALID_LENGTH("IP must have between 7 and 15 characters.");
//
//        private String msg;
//
//        @Override
//        public String getType() {
//            return this.name();
//        }
//    }
}
