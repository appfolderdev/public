package br.com.simpou.pub.commons.utils.cipher;

import br.com.simpou.pub.commons.utils.Constants;
import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

/**
 * Trata das operações sobre encriptação de textos.
 *
 * @author Jonas Pereira
 * @since 2008-06-20
 */
public class Encryptor {

    /**
     * Número máximo de caracteres da chave: 64.
     */
    public static final int MAX_KEY_LENGTH = 64;

    /**
     * Algoritmo padrão usado para criptografia.
     *
     * @see Algorithms#PBE_MD5_DES
     */
    public static final String DEFAULT_ALGORITHM = Algorithms.PBE_MD5_DES;

    /**
     * Codificação padrão de caracteres utilizada: UTF8.
     */
    public static final String DEFAULT_CHARSET_NAME = "UTF8";

    private static final MessageDigest MD_MD5 = MessageDigests.getDigest(Algorithms.MD5);

    private final Cipher ecipher;

    private final Cipher dcipher;

    private static final MessageDigest SHA1_DIGEST;

    static {
        try {
            SHA1_DIGEST = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static synchronized byte[] cryptSha1(final String s) throws UnsupportedEncodingException {
        return SHA1_DIGEST.digest(s.getBytes(Constants.DEFAULT_CHARSET));
    }

    public static String cryptBase64(final byte[] b) {
        return Base64.encodeBase64String(b);
    }

    /**
     * Use para encriptar e decriptar utilizando um algoritmo de criptografia e uma chave a escolha.
     *
     * @param key       Chave de criptografia a ser utilizada.
     * @param algorithm Veja {@link Algorithms}.
     */
    public Encryptor(final SecretKey key, final String algorithm)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        this.ecipher = Cipher.getInstance(algorithm);
        this.ecipher.init(Cipher.ENCRYPT_MODE, key);
        this.dcipher = Cipher.getInstance(algorithm);
        this.dcipher.init(Cipher.DECRYPT_MODE, key);
    }

    /**
     * Usa {@link #DEFAULT_ALGORITHM algoritmo padrão} e {@link #DEFAULT_CHARSET_NAME codifiação padrão}.
     *
     * @param passPhrase Chave de criptografia a ser utilizada. Veja {@link #MAX_KEY_LENGTH} para limite de
     *                   caracteres.
     * @throws IllegalArgumentException Chave nula, vazia ou maior que o limite.
     */
    public Encryptor(final String passPhrase) throws Exception {
        if (passPhrase == null || passPhrase.length() < 1) {
            throw new IllegalArgumentException("Key cannot be null or empty.");
        }

        if (passPhrase.length() > MAX_KEY_LENGTH) {
            throw new IllegalArgumentException("Key length greater then max: " + MAX_KEY_LENGTH);
        }

        final byte[] salt = {
                (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32, (byte) 0x56,
                (byte) 0x34, (byte) 0xE3, (byte) 0x03
        };

        final int iterationCount = 19;
        final KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);
        final SecretKey secretKey = SecretKeyFactory.getInstance(DEFAULT_ALGORITHM).generateSecret(keySpec);

        ecipher = Cipher.getInstance(secretKey.getAlgorithm());
        dcipher = Cipher.getInstance(secretKey.getAlgorithm());

        final AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

        ecipher.init(Cipher.ENCRYPT_MODE, secretKey, paramSpec);
        dcipher.init(Cipher.DECRYPT_MODE, secretKey, paramSpec);
    }

    /**
     * @return String encriptada.
     */
    public String crypt(final String str) throws Exception {
        final byte[] decrypted = str.getBytes(DEFAULT_CHARSET_NAME);
        final byte[] enc = ecipher.doFinal(decrypted);
        return new Base64().encodeToString(enc);
    }

    /**
     * @return String decriptada.
     */
    public String decrypt(final String str) throws Exception {
        final byte[] dec = new Base64().decode(str);
        final byte[] decrypted = dcipher.doFinal(dec);
        return new String(decrypted, DEFAULT_CHARSET_NAME);
    }

    /**
     * Usa algoritimo {@link Algorithms#MD5} e codificação {@link #DEFAULT_CHARSET_NAME}.
     *
     * @return Texto encriptado usando algoritmo MD5.
     */
    public static synchronized String cryptMD5(final String text)
            throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final BigInteger hash = new BigInteger(1, MD_MD5.digest(text.getBytes(DEFAULT_CHARSET_NAME)));
        final int radix = 16;
        return hash.toString(radix);

        // não lembro por que coloquei isso aqui
        //        if (NumberHelper.isEven(crypted.length())) {
        //            crypted = "0" + crypted;
        //        }
    }

    /**
     * Usa algoritimo {@link Algorithms#SHA_256} e codificação {@link #DEFAULT_CHARSET_NAME}.
     *
     * @return Texto encriptado usando algoritmo MD5.
     * @see Hashs#getHash(String)
     */
    public static synchronized String cryptSHA256(final String text)
            throws NoSuchAlgorithmException, UnsupportedEncodingException {
        return Hashs.getHash(text);
    }
}
