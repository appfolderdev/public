package br.com.simpou.pub.commons.utils.validation;

import br.com.simpou.pub.commons.utils.lang.Assertions;
import br.com.simpou.pub.commons.utils.violation.Violation;

/**
 * Created by Jonas on 08/06/2015.
 */
public class Validations extends Assertions {

    public static String calculateCPFDigits(final String cpfNoDigits) {
        String cpf = cpfNoDigits;
        final char dig10;
        final char dig11;
        int sm, i, r, num, peso;

        // "try" - protege o codigo para eventuais erros de conversao de tipo (int)
        // Calculo do 1o. Digito Verificador
        sm = 0;
        peso = 10;
        for (i = 0; i < 9; i++) {
            // converte o i-esimo caractere do CPF em um numero:
            // por exemplo, transforma o caractere '0' no inteiro 0
            // (48 eh a posicao de '0' na tabela ASCII)
            num = (int) (cpf.charAt(i) - 48);
            sm = sm + (num * peso);
            peso = peso - 1;
        }

        r = 11 - (sm % 11);
        if ((r == 10) || (r == 11)) {
            dig10 = '0';
        } else {
            dig10 = (char) (r + 48); // converte no respectivo caractere numerico
        }
        cpf += dig10;

        // Calculo do 2o. Digito Verificador
        sm = 0;
        peso = 11;
        for (i = 0; i < 10; i++) {
            num = (int) (cpf.charAt(i) - 48);
            sm = sm + (num * peso);
            peso = peso - 1;
        }

        r = 11 - (sm % 11);
        if ((r == 10) || (r == 11)) {
            dig11 = '0';
        } else {
            dig11 = (char) (r + 48);
        }

        final String digs = dig10 + "" + dig11;
        return digs;
    }

    public static boolean isCPFValid(final String cpfFormat) {
        final String cpf = cpfFormat.replaceAll("\\.", "").replace("-", "");
        // considera-se erro CPF's formados por uma sequencia de numeros iguais
        if (cpf.equals("00000000000") ||
                cpf.equals("11111111111") ||
                cpf.equals("22222222222") || cpf.equals("33333333333") ||
                cpf.equals("44444444444") || cpf.equals("55555555555") ||
                cpf.equals("66666666666") || cpf.equals("77777777777") ||
                cpf.equals("88888888888") || cpf.equals("99999999999") ||
                (cpf.length() != 11)) {
            return (false);
        }

        final String digs = calculateCPFDigits(cpf);

        // Verifica se os digitos calculados conferem com os digitos informados.
        return (digs.charAt(0) == cpf.charAt(9)) && (digs.charAt(1) == cpf.charAt(10));
    }

    /**
     * Realiza a validação de um objeto
     *
     * @param validatable Objeto validável.
     * @return Objeto válido ou null caso objeto seja null.
     */
    public static <T extends Validatable> T valid(final T validatable) {
        if (validatable == null) {
            return null;
        } else {
            final Violation violation = validatable.validate();

            if (violation != null) {
                throwNewIllegalArgument(validatable, MSG_INVALID, violation.getCode());
            }

            return validatable;
        }
    }

    /**
     * Realiza a validação de um objeto
     *
     * @param obj       Objeto validável.
     * @param validator Validador do objeto.
     * @return Objeto válido ou null caso objeto seja null.
     * @throws NullPointerException Se validador for null.
     */
    public static <T> T valid(final T obj, final Validator<T> validator) {
        if (obj == null) {
            return null;
        } else {
            notNull(validator, getParamMessage(MSG_NOT_NULL, "validator"));

            final Violation violation = validator.validate(obj);

            if (violation != null) {
                throwNewIllegalArgument(obj, MSG_INVALID, violation.getCode());
            }

            return obj;
        }
    }

}
