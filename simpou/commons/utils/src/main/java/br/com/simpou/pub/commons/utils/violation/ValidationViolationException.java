package br.com.simpou.pub.commons.utils.violation;

/**
 * Created by Jonas on 02/07/2018.
 */
public class ValidationViolationException extends ViolationsException {

    public ValidationViolationException(final String code,
                                        final Object bean,
                                        final String fieldName,
                                        final Object invalidValue,
                                        final Object... params) {
        super(new ValidationViolation(bean, fieldName, invalidValue, code, params));
    }
}
