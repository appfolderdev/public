package br.com.simpou.pub.commons.utils.interceptor;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static br.com.simpou.pub.commons.utils.lang.Assertions.*;

/**
 * Ponto inicial do processo de interceptação. Após interceptação, os métodos passam a ser executado via
 * reflection API, logo há uma queda de desempenho.
 *
 * @author Jonas Pereira
 * @since 2011-07-27
 */
public final class Interception<T> {

    /**
     * Parâmetro de construtor.
     */
    private final Class<T> interceptableType;

    /**
     * Parâmetro de construtor.
     */
    private final List<Method> toInterceptMethods;

    private Interception(final Class<T> interceptableType,
                         final Method... toInterceptMethods) {
        interfaceRequired(
                notNull(interceptableType, getParamMessage(MSG_NOT_NULL, "interceptableType")),
                getParamMessage(MSG_NOT_AN_INTERFACE, "interceptableType")
        );
        this.interceptableType = interceptableType;
        this.toInterceptMethods = Collections.unmodifiableList(Arrays.asList(toInterceptMethods));
    }

    /**
     * @param interceptableType  Interface das classes que poderão ser interceptadas.
     * @param toInterceptMethods Métodos que deverão ser interceptados. Se vazio, então todos declarados na
     *                           classe são considerados.
     */
    public static <T> Interception<T> newInstance(final Class<T> interceptableType,
                                                  final Method... toInterceptMethods) {
        return new Interception<T>(interceptableType, toInterceptMethods);
    }

    /**
     * @param interceptor Aquele que executará operações antes e/ou depois dos métodos indicados para
     *                    interceptação no tipo parametrizado.
     * @return Contexto de interceptação. Use para obtenção de instâncias interceptadas do tipo parametrizado.
     */
    public InterceptionContext<T> getContext(final Interceptor interceptor) {
        return new InterceptionContext<T>(interceptor, interceptableType, toInterceptMethods);
    }

}
