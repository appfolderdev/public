package br.com.simpou.pub.commons.utils.reflection;

import java.lang.reflect.Field;

/**
 * Condições que verifica se um campo é de um determinado tipo.
 *
 * @author Jonas Pereira
 * @since 2013-09-03
 */
public class InstanceOfField
        implements FieldCondition {

    private final InstanceOfCondition instanceOfCondition;

    public InstanceOfField(final Class<?> clasz) {
        this.instanceOfCondition = new InstanceOfCondition(clasz);
    }

    @Override
    public Boolean apply(final Field field) {
        return instanceOfCondition.apply(getType(field));
    }

    protected Class<?> getType(final Field field) {
        return field.getType();
    }

}
