package br.com.simpou.pub.commons.utils.lang;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Arrays;
import java.util.List;

/**
 * Classe utilitária para operações com classloaders.
 *
 * @author Jonas Pereira
 * @version 2013-08-06
 * @since 2013-08-06
 */
public class ClassLoaders {

    public static ClassLoader newClassLoaderByUrls(final String... urls) throws MalformedURLException {
        return newClassLoaderByUrls(Arrays.asList(urls));
    }

    /**
     * @param urls Caminhos de arquivos jar e de diretórios de classpath a serem considerados pelo
     *             classloader.
     * @return ClassLoader.
     * @throws MalformedURLException Caso alguma URL não estiver de acordo.
     */
    public static ClassLoader newClassLoaderByUrls(final List<String> urls) throws MalformedURLException {
        final URL[] urlArray = new URL[urls.size()];
        for (int i = 0; i < urlArray.length; i++) {
            urlArray[i] = new File(urls.get(i)).toURI().toURL();
        }
        final PrivilegedAction<URLClassLoader> privilegedAction =
                new PrivilegedAction<URLClassLoader>() {
                    @Override
                    public URLClassLoader run() {
                        return new URLClassLoader(urlArray);
                    }
                };

        return AccessController.doPrivileged(privilegedAction);
    }
}
