package br.com.simpou.pub.commons.utils.file;

/**
 * Carregador de propriedades. Util em classes em que parâmetros devem ser
 * injetados de uma forma e na classe de teste correspondente deve ser mockado.
 *
 * @author Jonas Pereira
 * @since 2013-10-11
 */
public interface PropertiesLoader {

    /**
     * @param key    Chave ou nome da propriedade.
     * @param params Parâmetros, no caso da propriedade ser parametrizada.
     * @return Valor da propriedade ou null se não existir.
     */
    String get(String key, String... params);
}
