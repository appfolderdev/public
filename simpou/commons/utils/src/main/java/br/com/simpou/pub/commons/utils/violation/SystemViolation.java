package br.com.simpou.pub.commons.utils.violation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Descrição da violação da regra de validação.
 *
 * @author Jonas Pereira
 * @since 2013-02-10
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {}, callSuper = true)
public class SystemViolation extends Violation {
    public static final String TYPE = "system";

    /**
     * Exceção associada à regra violada se houver.
     */
    @JsonIgnore
    private transient Throwable exception;

    /**
     * @param code          código de erro.
     * @param messageParams mensagens.
     */
    public SystemViolation(final String code, final Object... messageParams) {
        this(null, code, messageParams);
    }

    /**
     * @param exception Exceção.
     */
    public SystemViolation(final Throwable exception) {
        this(exception, null);
    }

    /**
     * @param exception     Exceção.
     * @param code          código de erro.
     * @param messageParams mensagens.
     */
    public SystemViolation(final Throwable exception, final String code, final Object... messageParams) {
        super(code, messageParams);
        this.exception = exception;
        if (code == null) {
            super.setMessage(getExceptionMessage());
        }
        super.setType(TYPE);
    }

    /**
     * @return Mensagem da exceção associada.
     */
    @JsonIgnore
    public String getExceptionMessage() {
        return (this.exception == null) ? "" : this.exception.getMessage();
    }
}
