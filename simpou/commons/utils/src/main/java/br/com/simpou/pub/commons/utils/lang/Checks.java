package br.com.simpou.pub.commons.utils.lang;

import java.util.Map;
import java.util.Objects;

/**
 * Created by jonas on 5/18/15.
 */
public class Checks {

    /**
     * @param objects Objetos que devem ser não nulos.
     * @return true se nenhume objeto for null.
     */
    public static boolean isNotNull(final Object... objects) {
        for (final Object object : objects) {
            if (object == null) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array Array.
     * @return true se array for nulo ou vazio.
     */
    public static boolean isEmpty(final Object[] array) {
        return array == null || array.length == 0;
    }

    /**
     * @param array Array.
     * @return true se array for não nulo e não vazio.
     */
    public static boolean isNotEmpty(final Object[] array) {
        return !isEmpty(array);
    }

    /**
     * @param string String.
     * @return true se string for nula ou vazia.
     */
    public static boolean isEmpty(final String string) {
        return string == null || string.length() == 0;
    }

    /**
     * @param string String.
     * @return true se string for não nula e não vazia.
     */
    public static boolean isNotEmpty(final String string) {
        return !isEmpty(string);
    }

    /**
     * @param collection Coleção.
     * @return true se coleção é nula ou vazia.
     */
    public static boolean isEmpty(final Iterable<?> collection) {
        return collection == null || !collection.iterator().hasNext();
    }

    /**
     * @return true se mapa é nulo ou vazio.
     */
    public static boolean isEmpty(final Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    /**
     * @param collection Coleção.
     * @return true se coleção é não nula e não vazia.
     */
    public static boolean isNotEmpty(final Iterable<?> collection) {
        return !isEmpty(collection);
    }

    /**
     * @return true se mapa é não nulo e não vazio.
     */
    public static boolean isNotEmpty(final Map<?, ?> map) {
        return !isEmpty(map);
    }

    /**
     * @param value     Alvo da comparação.
     * @param toCompare Valores que serão comparados com o alvo.
     * @return true se pelo menos um dos valores forem igual ao alvo.
     */
    public static boolean isEqualsAlternative(final Object value, final Object... toCompare) {
        if (toCompare.length < 1) {
            return true;
        }

        for (final Object compare : toCompare) {
            if (Objects.equals(compare, value)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param value     Alvo da comparação.
     * @param toCompare Valores que serão comparados com o alvo.
     * @return true se todos valores forem igual ao alvo.
     */
    public static boolean isEqualsExclusive(final Object value, final Object... toCompare) {
        for (final Object compare : toCompare) {
            if (!Objects.equals(compare, value)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return Se valor esta dentro de um intervalo inclusive.
     */
    public static boolean isInRange(final double value, final double min, final double max) {
        return value >= min && value <= max;
    }

    /**
     * Comparação ascendente.
     *
     * @see #isSorted(int, Comparable[])
     */
    @SafeVarargs
    public static <T extends Comparable<T>> boolean isIncreasing(final T... values) {
        return isSorted(1, values);
    }

    /**
     * Comparação descendente.
     *
     * @see #isSorted(int, Comparable[])
     */
    @SafeVarargs
    public static <T extends Comparable<T>> boolean isDecreasing(final T... values) {
        return isSorted(-1, values);
    }

    /**
     * @param direction Positivo para ordenação ascendente e negativo para descendente.
     * @return true se os parâmetros, na ordem em que foram passados, estão ordenados. Somente valores não
     * nulos são usados para a realizar a comparaçao com o próximo.
     */
    @SafeVarargs
    public static <T extends Comparable<T>> boolean isSorted(final int direction, final T... values) {
        if (isEmpty(values)) {
            return true;
        }
        final int expDirection = (int) Math.signum(direction);
        T prev = values[0];
        for (int i = 1; i < values.length; i++) {
            final T next = values[i];
            if (next == null) {
                continue;
            }
            final int comparation = (int) Math.signum(next.compareTo(prev));
            if (comparation == 0 || comparation == expDirection) {
                prev = next;
            } else {
                return false;
            }
        }
        return true;
    }
}
