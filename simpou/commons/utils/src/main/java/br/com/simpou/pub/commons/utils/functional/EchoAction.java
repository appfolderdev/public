package br.com.simpou.pub.commons.utils.functional;

/**
 * Não realiza nenhuma ação, apenas ecoa a entrada para a saída.
 *
 * @author Jonas Pereira
 * @since 2013-06-27
 */
public class EchoAction<T> implements Action<T, T> {

    @Override
    public final T apply(final T object) {
        return object;
    }

}
