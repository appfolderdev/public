package br.com.simpou.pub.commons.utils.rand;

/**
 * @author jonas1
 * @version 23/09/13
 * @since 23/09/13
 */
public interface RandGenerator<T> {

    T generate();

}
