package br.com.simpou.pub.commons.utils.interceptor;

import br.com.simpou.pub.commons.utils.string.Strings;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Interceptador para registrar em logs as chamadas a métodos. São logadas as chamadas do método e seu retorno
 * e, em caso de erro, as exceções lançadas. Útil para debuggar threads, fazer auditorias de código, etc.
 *
 * @author Jonas Pereira
 * @since 2012-09-30
 */
public final class LogInterceptor implements Interceptor {

    static final String ON_ERROR_MSG = ", throws an exception {0} with message \"{1}\"";

    static final String ON_CALL_MSG = "Method \"{0}.{1}\" called with arguments: {2}";

    static final String NO_ARGS_MSG = "Method \"{0}.{1}\" called with no arguments";

    static final String ON_RETURN_MSG = " and returns \"{0}\"";

    private final Logger logger;

    private String msgSum;

    private LogInterceptor(final Logger logger) {
        this.logger = logger;
    }

    /**
     * @param ifaceClass Interface do objeto cujos método serão interceptados.
     * @param obj        Objeto a ser interceptado.
     * @param logger     Define onde e como serão gerados os logs.
     * @return Objeto interceptado.
     */
    public static <T, E extends T> T intercept(final Class<T> ifaceClass,
                                               final E obj,
                                               final Logger logger) {
        return Interception.newInstance(ifaceClass).getContext(new LogInterceptor(logger)).intercept(obj);
    }

    @Override
    public Object doOnError(final Throwable throwable) throws Throwable {
        final String exMsg;
        final Throwable redirect;

        final Throwable targetThrowable;
        if (throwable instanceof InvocationTargetException) {
            targetThrowable = ((InvocationTargetException) throwable).getTargetException();
        } else {
            targetThrowable = throwable;
        }
        if (targetThrowable.getMessage() == null && targetThrowable.getCause() != null) {
            exMsg = targetThrowable.getCause().getMessage();
            redirect = targetThrowable.getCause();
        } else {
            exMsg = targetThrowable.getMessage();
            redirect = targetThrowable;
        }

        msgSum += Strings.replaceParams(ON_ERROR_MSG, redirect.getClass().getName(), exMsg);
        throw redirect;
    }

    @Override
    public void doBefore(final Method method, final Object[] args) {
        final String msg;

        if (args == null) {
            msg = Strings.replaceParams(NO_ARGS_MSG,
                    method.getDeclaringClass().getSimpleName(), method.getName());
        } else {
            msg = Strings.replaceParams(ON_CALL_MSG,
                    method.getDeclaringClass().getSimpleName(),
                    method.getName(), argsToString(args));
        }

        msgSum = msg;
    }

    /**
     * @param args Array de argumentos.
     * @return String representativa do array.
     */
    private String argsToString(final Object[] args) {
        final StringBuilder builder = new StringBuilder("[");
        builder.append((args[0] == null) ? "null" : args[0].toString());

        for (int i = 1; i < args.length; i++) {
            builder.append(", ").append((args[i] == null) ? "null" : args[i].toString());
        }

        builder.append(']');

        return builder.toString();
    }

    @Override
    public void doAfter(final Object result) {
        msgSum += Strings.replaceParams(ON_RETURN_MSG, result);
        logger.write(msgSum);
    }

    /**
     * Define como será a gravação do log ou onde será exibido.
     */
    public interface Logger {

        /**
         * Grava log.
         *
         * @param msg Mensagem.
         */
        void write(String msg);
    }
}
