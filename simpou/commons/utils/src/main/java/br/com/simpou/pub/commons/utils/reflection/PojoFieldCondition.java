package br.com.simpou.pub.commons.utils.reflection;

import java.lang.reflect.Field;

/**
 * Condições pra um campo de uma classe do tipo POJO.
 *
 * @author Jonas Pereira
 * @since 2013-09-24
 */
public class PojoFieldCondition implements FieldCondition {

    @Override
    public Boolean apply(final Field field) {
        return Reflections.isPojoField(field);
    }
}
