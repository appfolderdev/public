package br.com.simpou.pub.commons.utils.reflection;

import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.net.URL;
import java.util.Collection;
import java.util.Set;

/**
 * @author jonas1
 * @version 22/09/13
 * @since 22/09/13
 */
public class ClassScannerImpl<T> implements ClassScanner<T> {

    @Override
    public Set<Class<? extends T>> scanTypes(
            final Class<T> typeClass, final ClassLoader... classLoaders) {
        final Collection<URL> forClassLoader = ClasspathHelper.forClassLoader(classLoaders);
        final ConfigurationBuilder config = new ConfigurationBuilder();
        config.addUrls(forClassLoader);

        final Reflections reflections = new Reflections(config);

        return reflections.getSubTypesOf(typeClass);
    }
}
