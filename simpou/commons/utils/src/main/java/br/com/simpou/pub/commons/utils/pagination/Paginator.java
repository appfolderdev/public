package br.com.simpou.pub.commons.utils.pagination;

import lombok.Getter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Auxilia na paginação de grandes quantidades de dados.
 *
 * @author Jonas Pereira
 * @since 2011-06-20
 */
public class Paginator implements Serializable {

    @Getter
    private final PaginationConfig config;

    /**
     * Página corrente.
     */
    private int actualPage;

    /**
     * Última página.
     */
    @Getter
    private final int lastPage;

    /**
     * Configura e inicializa a paginação. Calcula a última página e seta a primeira como a corrente.
     */
    public Paginator(final PaginationConfig paginationConfig) {
        this.config = paginationConfig;
        this.actualPage = 1;
        this.lastPage = Math.max(
                (int) Math.ceil(
                        (float) (paginationConfig.getElements()) / (float) paginationConfig.getPageSize()
                ),
                1
        );
    }

    /**
     * Vai para próxima página. Se já estiver na última, não realiza nada.
     *
     * @return Nova página corrente.
     */
    public PageLimits goNextPage() {
        if (actualPage < lastPage) {
            actualPage++;
        }

        return getLimits();
    }

    /**
     * Vai para página anterior. Se já estiver na primeira, não realiza nada.
     *
     * @return Nova página corrente.
     */
    public PageLimits goPreviousPage() {
        if (actualPage > 1) {
            actualPage--;
        }

        return getLimits();
    }

    /**
     * Vai para a primeira página.
     *
     * @return Nova página corrente.
     */
    public PageLimits goFirstPage() {
        actualPage = 1;

        return getLimits();
    }

    /**
     * Última página.
     *
     * @return Limites.
     */
    public PageLimits goLastPage() {
        actualPage = lastPage;

        return getLimits();
    }

    /**
     * Vai para uma página.
     *
     * @param page Página.
     * @return Limites.
     */
    public PageLimits goToPage(final int page) {
        if (page < 1 || page > lastPage) {
            throw new IllegalArgumentException("Page " + page);
        } else {
            this.actualPage = page;
        }

        return getLimits();
    }

    /**
     * @return Limites da página corrente.
     */
    public PageLimits getLimits() {
        final int offset = (actualPage - 1) * config.getPageSize();
        final int lastElement = offset + config.getPageSize() - 1;
        int size;

        if (lastElement >= config.getElements()) {
            size = (int) (config.getElements() - offset);
        } else {
            size = config.getPageSize();
        }

        return new PageLimits(offset, size);
    }

    /**
     * @return Lista de páginas. Se o número total de páginas for maior que o máximo configurado, a lista de
     * páginas será uma janela deslocada com as páginas mais próximas da página corrente. A página corrente
     * fica no meio seguida da metade do total à esquerda e outra metade restante à direita. Caso o número
     * total seja par, é incluído um a mais na direita.
     */
    public List<Integer> getPages() {
        final int halfMaxPagesToShow = config.getPagesToShow() / 2;
        int first;
        int last;

        if (countPages() <= config.getPagesToShow()) {
            first = 1;
            last = countPages();
        } else {
            if ((config.getPagesToShow() % 2) > 0) {
                first = Math.max(1, getActualPage() - halfMaxPagesToShow);
            } else {
                first = Math.max(1, getActualPage() - halfMaxPagesToShow + 1);
            }

            last = Math.min(first + config.getPagesToShow() - 1, countPages());

            if (last == countPages()) {
                first = last - config.getPagesToShow() + 1;
            }
        }

        final List<Integer> pages = new ArrayList<Integer>();
        for (int i = first; i <= last; i++) {
            pages.add(i);
        }

        return pages;
    }

    /**
     * @return Página corrente.
     */
    public int getActualPage() {
        return actualPage;
    }

    /**
     * @return Númeor total de páginas.
     */
    public int countPages() {
        return lastPage;
    }
}
