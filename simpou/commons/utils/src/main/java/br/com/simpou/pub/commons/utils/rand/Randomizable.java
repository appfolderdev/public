package br.com.simpou.pub.commons.utils.rand;

/**
 * Indica que um objeto pode ser preenchido randomicamente.
 *
 * @author Jonas Pereira
 * @version 2013-06-01
 * @since 2011-12-14
 */
public interface Randomizable {

    /**
     * Preenche o objeto atual com dados aleatórios válidos.
     */
    void fillRandom();
}
