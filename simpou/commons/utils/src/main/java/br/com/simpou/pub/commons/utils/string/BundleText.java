package br.com.simpou.pub.commons.utils.string;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.builder.CompareToBuilder;

import java.io.Serializable;

@ToString(of = "content")
@EqualsAndHashCode(of = "code")
public class BundleText implements Comparable<BundleText>, Serializable {

    @Getter
    @JsonIgnore
    private final String code;

    @JsonIgnore
    private final Serializable[] params;

    @Getter
    @Setter
    @JsonValue
    private String content;

    public BundleText(final String code, final Serializable... params) {
        this.code = code;
        content = code;
        this.params = params.clone();
    }

    public static BundleText of(final String... parts) {
        return new BundleText(Strings.concat(".", parts));
    }

    @Override
    public int compareTo(final BundleText other) {
        return new CompareToBuilder()
                .append(code, other.code)
                .build();
    }

    public Object[] getParams() {
        return params.clone();
    }
}
