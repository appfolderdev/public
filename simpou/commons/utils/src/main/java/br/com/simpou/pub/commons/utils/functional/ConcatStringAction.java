package br.com.simpou.pub.commons.utils.functional;

/**
 * Realiza uma ação de concatenar strings.
 *
 * @author Jonas Pereira
 * @since 2013-06-19
 */
public final class ConcatStringAction extends ConcatObjectAction<String> {

    /**
     * @see ConcatStringAction#ConcatStringAction(String, String, boolean,
     * int)
     */
    public ConcatStringAction(final String preffix,
                              final String suffix,
                              final boolean endSuffixAllowed,
                              final int maxStrLength) {
        super(preffix, suffix, endSuffixAllowed, new EchoAction<String>(), maxStrLength);
    }

    /**
     * @see ConcatStringAction#ConcatStringAction(String, String, boolean)
     */
    public ConcatStringAction(final String preffix,
                              final String suffix,
                              final boolean endSuffixAllowed) {
        super(preffix, suffix, endSuffixAllowed, new EchoAction<String>());
    }

    /**
     * Sem prefixo e sufixo no final não permitido.
     *
     * @see #ConcatStringAction(String, String, boolean)
     */
    public ConcatStringAction(final String suffix) {
        this("", suffix, false);
    }

}
