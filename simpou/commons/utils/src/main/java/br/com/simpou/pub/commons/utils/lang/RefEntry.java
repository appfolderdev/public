package br.com.simpou.pub.commons.utils.lang;

import lombok.Getter;

import java.util.Objects;

/**
 * Estrutura que define que um objeto é igual a outro se suas referências forem iguais.
 *
 * @author Jonas Pereira
 * @since 2013-10-30
 */
public class RefEntry<T> implements Comparable<RefEntry<T>> {

    @Getter
    private final T value;

    public RefEntry(final Object obj) {
        this.value = Casts.simpleCast(obj);
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public boolean equals(final Object obj) {
        return !(obj == null || !(obj instanceof RefEntry))
                && Objects.equals(this.value, ((RefEntry) obj).value);
    }

    @Override
    public int compareTo(final RefEntry<T> o) {
        return ReferenceComparator.compareRefs(this.value, o.value);
    }
}
