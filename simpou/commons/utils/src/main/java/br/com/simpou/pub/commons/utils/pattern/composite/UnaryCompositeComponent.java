package br.com.simpou.pub.commons.utils.pattern.composite;

import br.com.simpou.pub.commons.utils.functional.Action;

import java.util.Map;

/**
 * Created by jonas on 3/2/17.
 */
class UnaryCompositeComponent<P, R> extends AbstractCompositeComponent<P, R> {

    private UnaryCompositeComponent(final P basic,
                                    final CompositeComponent<P, R> composite) {
        super(
                basic,
                (AbstractCompositeComponent<P, R>) composite,
                getStringId(basic, composite)
        );
    }

    private static <P, R> String getStringId(final P basic,
                                             final CompositeComponent<P, R> composite) {
        if (basic == null) {
            return composite.toString();
        } else {
            return basic.toString();
        }
    }

    @Override
    public boolean isComposite() {
        return false;
    }

    UnaryCompositeComponent(final P basic) {
        this(basic, null);
    }

    UnaryCompositeComponent(final CompositeComponent<P, R> composite) {
        this(null, composite);
    }

    @Override
    protected CompositeOperation<P, R> getOperation() {
        throw new UnsupportedOperationException();
    }

    @Override
    protected R getValue(final R value, final Action<P, R> conversor,
                         final Map<CompositeComponent, R> componentCache,
                         Map<P, R> conversionCache) {
        return value;
    }
}
