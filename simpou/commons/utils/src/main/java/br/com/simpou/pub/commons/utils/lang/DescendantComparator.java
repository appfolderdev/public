package br.com.simpou.pub.commons.utils.lang;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Comparador descendente. Inverte a comparação padrão.
 *
 * @author Jonas Pereira
 * @since 2013-10-30
 */
public class DescendantComparator<T extends Comparable<T>> implements Comparator<T>, Serializable {

    @Override
    public int compare(final T o1, final T o2) {
        return o2.compareTo(o1);
    }

}
