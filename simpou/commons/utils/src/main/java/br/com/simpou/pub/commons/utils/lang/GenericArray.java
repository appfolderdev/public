package br.com.simpou.pub.commons.utils.lang;

import lombok.RequiredArgsConstructor;

/**
 * Created by jonas on 22/04/2016.
 */
@RequiredArgsConstructor
public class GenericArray<T> {

    private final T[] data;

    public T[] get() {
        return data;
    }

}
