package br.com.simpou.pub.commons.utils.compiler;

import br.com.simpou.pub.commons.utils.lang.Assertions;
import br.com.simpou.pub.commons.utils.lang.Casts;

import javax.tools.*;
import javax.tools.JavaCompiler.CompilationTask;
import java.net.URISyntaxException;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Arrays;

/**
 * Utilitário para compilação de código fonte Java em tempo de execução.
 *
 * @author Jonas Pereira
 * @since 2011-08-18
 */
public class RuntimeCompiler {

    private static final JavaCompiler COMPILER = ToolProvider.getSystemJavaCompiler();

    private static final CompilerClassLoader CLASS_LOADER =
            getClassLoader(RuntimeCompiler.class.getClassLoader());

    private static CompilerClassLoader getClassLoader(final ClassLoader loader) {
        final PrivilegedAction<CompilerClassLoader> privilegedAction =
                new PrivilegedAction<CompilerClassLoader>() {
                    @Override
                    public CompilerClassLoader run() {
                        return new CompilerClassLoader(loader);
                    }
                };

        //Classloaders should only be created inside doPrivileged block
        return AccessController.doPrivileged(privilegedAction);
    }

    /**
     * Executa {@link #compile(Class, String, String, String, Iterable, ClassLoader)}, sem opções de
     * compilação e com classloader padrão.
     */
    public static <T> Class<T> compile(final Class<T> superClass,
                                       final String packageName,
                                       final String className,
                                       final String javaSource) throws CompilerException {
        return compile(superClass, packageName, className, javaSource, null, null);
    }

    /**
     * @param superClass         Super classe. A classe a ser gerada deve ser subclasse desta.
     * @param packageName        Nome do pacote.
     * @param className          Nome simples da classe.
     * @param javaSource         Código fonte a ser compilado.
     * @param compilationOptions Opções de compilação. Útil, por exemplo, para informar o CLASSPATH de
     *                           compilação. Veja {@link javax.tools.JavaCompiler#getTask(java.io.Writer,
     *                           javax.tools.JavaFileManager, javax.tools.DiagnosticListener, Iterable,
     *                           Iterable, Iterable)}.
     * @return Classe compilada.
     * @throws CompilerException Erros de compilação.
     */
    public static <T> Class<T> compile(final Class<T> superClass,
                                       final String packageName,
                                       final String className,
                                       final String javaSource,
                                       final Iterable<String> compilationOptions,
                                       final ClassLoader classLoader) throws CompilerException {

        final CompilerClassLoader compilerClassLoader;
        if (classLoader == null) {
            compilerClassLoader = CLASS_LOADER;
        } else {
            compilerClassLoader = getClassLoader(classLoader);
        }

        Assertions.notNull(COMPILER, "Compiler not found.");
        Assertions.notNull(superClass);
        Assertions.notEmpty(className);
        Assertions.notEmpty(javaSource);

        final DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();

        final String qualifiedClassName = getQualifiedClassName(packageName, className);

        final CompilerJavaFileObject sourceObj;
        final CompilerJavaFileObject compiledObj;
        try {
            sourceObj = new CompilerJavaFileObject(className, javaSource);
            compiledObj = new CompilerJavaFileObject(qualifiedClassName);
        } catch (URISyntaxException e) {
            throw new CompilerException("Error processing class and package names.", e, diagnostics);
        }

        final StandardJavaFileManager standardFileManager = COMPILER.getStandardFileManager(
                diagnostics,
                null,
                null
        );
        final CompilerFileManager fileManager = new CompilerFileManager(
                standardFileManager,
                compilerClassLoader,
                compiledObj
        );

        final CompilationTask task = COMPILER.getTask(
                null,
                fileManager,
                diagnostics,
                compilationOptions,
                null,
                Arrays.asList(sourceObj)
        );
        final boolean resultOk = task.call();

        if (!resultOk) {
            throw new CompilerException("Compilation fail.", diagnostics);
        }

        Class<?> loadClass;

        try {
            loadClass = compilerClassLoader.loadClass(qualifiedClassName);
        } catch (Throwable ex) {
            throw new CompilerException("Compilation ok but fail on load class.", ex, diagnostics);
        }

        return Casts.rawClassCast(superClass, loadClass);
    }

    /**
     * @param className   Nome da classe.
     * @param packageName Nome Pacote.
     * @return Nome canônico da classe.
     */
    static String getQualifiedClassName(final String packageName,
                                        final String className) {
        if (packageName == null || packageName.trim().isEmpty()) {
            return className;
        } else {
            return packageName + "." + className;
        }
    }
}
