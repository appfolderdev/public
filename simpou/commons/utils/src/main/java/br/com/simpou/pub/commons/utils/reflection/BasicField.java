package br.com.simpou.pub.commons.utils.reflection;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Condições que exige que um campo seja de um tipo básico. São considerados básicos os tipos primitivos e
 * seus boxings correspondentes, string, date, bigdecimal e enum.
 *
 * @author Jonas Pereira
 * @version 2013-06-02
 * @since 2013-06-02
 */
public class BasicField implements FieldCondition {

    @Override
    public Boolean apply(final Field field) {
        return isBasic(field.getType());
    }

    /**
     * @param type Tipo a ser verificado.
     * @return true se tipo é básico. São considerados básicos os tipos primitivos e seus boxings
     * correspondentes, string, date, bigdecimal e enum.
     */
    public static boolean isBasic(final Class<?> type) {
        return type == String.class
                || Enum.class.isAssignableFrom(type)
                || type == Boolean.class
                || type == boolean.class
                || type == Character.class
                || type == char.class
                || type == Byte.class
                || type == byte.class
                || type == Short.class
                || type == short.class
                || type == Integer.class
                || type == int.class
                || type == Long.class
                || type == long.class
                || type == Float.class
                || type == float.class
                || type == Double.class
                || type == double.class
                || type == Date.class
                || type == BigDecimal.class;
    }
}
