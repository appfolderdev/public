package br.com.simpou.pub.commons.utils.lang;

import lombok.experimental.UtilityClass;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * Created by jonas on 22/01/17.
 */
@UtilityClass
public class Dates {

    private static final DateTimeFormatter DTF_CANONICAL = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSxxx");

    private static String formatCanonical(final ZonedDateTime dateTime) {
        return DTF_CANONICAL.format(dateTime);
    }

    public static String formatCanonical(final LocalDateTime dateTime) {
        return formatCanonical(dateTime, ZoneId.systemDefault());
    }

    public static String formatCanonical(final LocalDateTime dateTime, final ZoneId zoneId) {
        final ZonedDateTime zoned = dateTime.atZone(zoneId);
        return formatCanonical(zoned);
    }

    public LocalDateTime parseCanonical(final String dateTimeString) {
        return LocalDateTime.parse(dateTimeString, DTF_CANONICAL);
    }

    public static LocalDate toLocalDate(final Date date) {
        return toLocalDate(date, ZoneId.systemDefault());
    }

    public static LocalDate toLocalDate(final Date date, final ZoneId zoneId) {
        return date.toInstant()
                .atZone(zoneId)
                .toLocalDate();
    }

    public static Date toDate(final LocalDate localDate) {
        return toDate(localDate, ZoneId.systemDefault());
    }

    public static Date toDate(final LocalDate localDate, final ZoneId zoneId) {
        return Date.from(localDate.atStartOfDay(zoneId).toInstant());
    }

    public static long countDaysBetween(final LocalDate startDate, final LocalDate endDate) {
        return ChronoUnit.DAYS.between(startDate, endDate);
    }

    public static List<LocalDate> getDaysBetween(final LocalDate startDate, final LocalDate endDate) {
        return LongStream
                .rangeClosed(0, countDaysBetween(startDate, endDate))
                .mapToObj(startDate::plusDays)
                .collect(Collectors.toList());
    }

    public int getLastDayOfMonth(final LocalDate date) {
        return date.withDayOfMonth(date.getMonth().length(date.isLeapYear())).getDayOfMonth();
    }

    /**
     * Adiciona tempo no estilo dd/MM/yyyy, por exemplo, a uma data.
     *
     * @param days Dias do mês.
     */
    public static Date addDate(final Date date, final int years, final int months, final int days) {
        final Calendar c = getCalendar(date);
        c.add(Calendar.YEAR, years);
        c.add(Calendar.MONTH, months);
        c.add(Calendar.DAY_OF_MONTH, days);
        return c.getTime();
    }

    /**
     * @return Data mais recente.
     */
    public static Date chooseMostRecent(final Date... dates) {
        Assertions.notEmpty(Date.class, dates);
        Date last = dates[0];
        for (int i = 1; i < dates.length; i++) {
            if (last == null || (dates[i] != null && dates[i].after(last))) {
                last = dates[i];
            }
        }
        return last;
    }

    /**
     * @return Data mais antiga.
     */
    public static Date chooseMostOld(final Date... dates) {
        Assertions.notEmpty(Date.class, dates);
        Date older = dates[0];
        for (int i = 1; i < dates.length; i++) {
            if (older == null || (dates[i] != null && dates[i].before(older))) {
                older = dates[i];
            }
        }
        return older;
    }

    /**
     * Adiciona tempo no estilo HH:mm:ss, por exemplo, a uma data.
     *
     * @param hours Horas no formato 24h.
     */
    public static Date addTime(final Date date,
                               final int hours,
                               final int minutes,
                               final int seconds) {
        final Calendar c = getCalendar(date);
        c.add(Calendar.HOUR_OF_DAY, hours);
        c.add(Calendar.MINUTE, minutes);
        c.add(Calendar.SECOND, seconds);
        return c.getTime();
    }

    /**
     * Transforma date em calendar.
     */
    public static Calendar getCalendar(final Date date) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * @return Diferença em dias da data2 em relação data1. Se data1 maior, retorna valores negativos.
     */
    public static int diffDays(final Date date1, final Date date2) {
        Calendar after = getCalendar(date2),
                before = getCalendar(date1);

        if (after.get(Calendar.YEAR) == before.get(Calendar.YEAR)) {
            return after.get(Calendar.DAY_OF_YEAR) - before.get(Calendar.DAY_OF_YEAR);
        } else {
            final boolean swap = before.get(Calendar.YEAR) > after.get(Calendar.YEAR);
            if (swap) {
                //swap them
                Calendar temp = after;
                after = before;
                before = temp;
            }
            int extraDays = 0;

            final int dayOneOriginalYearDays = after.get(Calendar.DAY_OF_YEAR);

            while (after.get(Calendar.YEAR) > before.get(Calendar.YEAR)) {
                after.add(Calendar.YEAR, -1);
                // getActualMaximum() important for leap years
                extraDays += after.getActualMaximum(Calendar.DAY_OF_YEAR);
            }

            final int diff = extraDays - before.get(Calendar.DAY_OF_YEAR) + dayOneOriginalYearDays;
            return swap ? -diff : diff;
        }
    }
}
