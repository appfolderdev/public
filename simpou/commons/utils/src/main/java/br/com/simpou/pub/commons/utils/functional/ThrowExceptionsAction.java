package br.com.simpou.pub.commons.utils.functional;

/**
 * Realiza uma ação e lança runtime exceptions se alguma outra for lançada.
 *
 * @author Jonas Pereira
 * @version 2016-03-08
 */
public abstract class ThrowExceptionsAction<P, R> implements SilentAction<P, R> {

    /**
     * Ação a ser executada.
     */
    protected abstract R executeDelegate(P object) throws Exception;

    @Override
    public final R apply(final P object) {
        try {
            return executeDelegate(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void doOnError(final P object, final Throwable e) {
        throw new RuntimeException(e);
    }
}
