package br.com.simpou.pub.commons.utils.string;

import br.com.simpou.pub.commons.utils.functional.Action;
import br.com.simpou.pub.commons.utils.functional.Actions;
import br.com.simpou.pub.commons.utils.functional.ConcatStringAction;
import br.com.simpou.pub.commons.utils.functional.EchoAction;
import br.com.simpou.pub.commons.utils.lang.Casts;
import br.com.simpou.pub.commons.utils.lang.Checks;
import br.com.simpou.pub.commons.utils.lang.Numbers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

/**
 * Operações sobre texto.
 *
 * @author Jonas Pereira
 * @since 2011-06-25
 */
public final class Strings {

    public static final String ENCODE_UTF8 = "UTF-8";

    /**
     * Texto adicionado no final de uma string truncada.
     */
    public static final String SUFIX_TEXT_TRUNC = "...";

    private static final Map<Class<?>, Action<String, ?>> STRING_CONVERSORS;

    static {
        final Map<Class<?>, Action<String, ?>> mapConversors = new HashMap<>();

        mapConversors.put(Boolean.class, StringConversors.BOOLEAN);
        mapConversors.put(Character.class, StringConversors.CHARACTER);
        mapConversors.put(Byte.class, StringConversors.BYTE);
        mapConversors.put(Short.class, StringConversors.SHORT);
        mapConversors.put(Integer.class, StringConversors.INTEGER);
        mapConversors.put(Long.class, StringConversors.LONG);
        mapConversors.put(Float.class, StringConversors.FLOAT);
        mapConversors.put(Double.class, StringConversors.DOUBLE);

        mapConversors.put(boolean.class, StringConversors.BOOLEAN);
        mapConversors.put(char.class, StringConversors.CHARACTER);
        mapConversors.put(byte.class, StringConversors.BYTE);
        mapConversors.put(short.class, StringConversors.SHORT);
        mapConversors.put(int.class, StringConversors.INTEGER);
        mapConversors.put(long.class, StringConversors.LONG);
        mapConversors.put(float.class, StringConversors.FLOAT);
        mapConversors.put(double.class, StringConversors.DOUBLE);

        mapConversors.put(String.class, StringConversors.STRING);
        mapConversors.put(BigDecimal.class, StringConversors.BIG_DECIMAL);
        STRING_CONVERSORS = Collections.unmodifiableMap(mapConversors);
    }

    public static String addLeadingZeros(final String text, final int totalLength) {
        if (text == null) {
            return null;
        }
        if (text.length() >= totalLength) {
            return text;
        }
        final int zeros = totalLength - text.length();
        final String result = Numbers.addLeadingZeros(1, zeros + 1);
        return result.substring(0, zeros) + text;
    }

    /**
     * Concatena uma string ao início de outra caso a segunda já não a contenha em seu início.
     *
     * @param string String a ser modificada.
     * @param append String a ser adcionada no início caso já não esteja presente.
     * @return Nova string.
     */
    public static String appendAtBeginIfNotContains(final String string,
                                                    final String append) {
        String newString = string;

        if (!string.startsWith(append)) {
            newString = append + newString;
        }

        return newString;
    }

    /**
     * Concatena uma string ao final de outra caso a segunda já não a contenha em seu final.
     *
     * @param string String a ser modificada.
     * @param append String a ser adcionada ao fim caso já não esteja presente.
     * @return Nova string.
     */
    public static String appendAtEndIfNotContains(final String string,
                                                  final String append) {
        String newString = string;

        if (!string.endsWith(append)) {
            newString += append;
        }

        return newString;
    }

    public static String camelToSnake(String str) {
        if (isEmpty(str)) {
            return str;
        }

        // Regular Expression
        final String regex = "([a-z])([A-Z]+)";

        // Replacement string
        final String replacement = "$1_$2";

        // Replace the given regex
        // with replacement string
        // and convert it to lower case.
        str = str
                .replaceAll(
                        regex, replacement)
                .toLowerCase();

        // return string
        return str;
    }

    /**
     * @see #concat(String, Collection)
     */
    public static String concat(final String sep, final String... parts) {
        return concat(sep, Arrays.asList(parts));
    }

    /**
     * @param sep   Separador a ser inserido entre cada parte.
     * @param parts Strings a serem concatenadas na ordem em que aparecem.
     * @return String resultantes da concatenação das partes.
     */
    public static String concat(final String sep, final Collection<String> parts) {
        final ConcatStringAction concatter = new ConcatStringAction(sep);
        Actions.apply(parts, concatter);
        return concatter.getResult();
    }

    public static String concatDots(final String... parts) {
        return concat(".", parts);
    }

    /**
     * @param is Stream contendo uma string.
     * @return String extraída.
     * @throws java.io.IOException Problemas ao ler stream.
     */
    public static String extract(final InputStream is, final String charset) throws IOException {
        final StringBuilder stringBuilder = new StringBuilder();
        final BufferedReader br = new BufferedReader(
                new InputStreamReader(is, charset)
        );
        try {
            final int bufferSize = 128;
            final char[] charBuffer = new char[bufferSize];
            int bytesRead;
            while ((bytesRead = br.read(charBuffer)) > 0) {
                stringBuilder.append(charBuffer, 0, bytesRead);
            }
        } finally {
            br.close();
        }

        return stringBuilder.toString();
    }

    private static <T> Action<String, T> getConversorFor(final Class<T> type) {
        if (STRING_CONVERSORS.containsKey(type)) {
            return Casts.simpleCast(STRING_CONVERSORS.get(type));
        } else {
            throw new UnsupportedOperationException("String conversor for type " + type.getName() + " was not found.");
        }
    }

    /**
     * <p>isEmpty.</p>
     *
     * @param string String.
     * @return True se null ou vazio ou somente espaços.
     */
    public static boolean isEmpty(final String string) {
        return string == null || string.replaceAll(" ", "").equals("");
    }

    public static String nullIfEmpty(final String string) {
        if (string == null) {
            return null;
        }
        return string.trim().isEmpty() ? null : string;
    }

    /**
     * Substitui parâmetros num texto. Parâmetros são do tipo {0},{1}...{n}.
     *
     * @param text   Text contendo tags {0},{1},...
     * @param params Parâmetros.
     * @return Novo texto.
     */
    public static String replaceParams(final String text, final String... params) {
        return replaceParams(text, (Object[]) params);
    }

    /**
     * Substitui parâmetros num texto. Parâmetros são do tipo {0},{1}...{n}.
     *
     * @param text   Text contendo tags {0},{1},...
     * @param params Parâmetros.
     * @return Novo texto.
     */
    public static String replaceParams(final String text, final Object... params) {
        if (text == null || text.isEmpty()) {
            return text;
        }

        return MessageFormat.format(text, params);
    }

    /**
     * Converte um tipo string em outro. O tipo destino deve ser primitivo (boxing permitido), enum ou
     * String.
     *
     * @param type   Tipo do campo.
     * @param string String a ser convertida.
     * @return Objeto com tipo compatível.
     * @throws UnsupportedOperationException Caso tipo não seja suportado.
     */
    public static <T> T stringToType(final Class<T> type, final String string) {

        final Object object;
        if (Checks.isEmpty(string)) {
            object = null;
        } else if (Enum.class.isAssignableFrom(type)) {
            object = Casts.valueOfEnum(type, string);
        } else {
            try {
                return getConversorFor(type).apply(string);
            } catch (final Exception e) {
                throw new RuntimeException("Error converting string \"" + string + "\" to " + type.getName(), e);
            }
        }
        return Casts.simpleCast(object);
    }

    /**
     * <p>toLowerCaseFirstLetter.</p>
     *
     * @param text Texto.
     * @return Texto com a primeira letra maiúscula de cada palavra separada por espaço.
     */
    public static String toLowerCaseFirstLetter(final String text) {
        if (text == null || isEmpty(text)) {
            return text;
        }

        final String delim = " ";
        final StringTokenizer tokenizer = new StringTokenizer(text, delim, true);

        String first;
        final StringBuilder textLowerBuilder = new StringBuilder();

        String word;

        while (tokenizer.hasMoreElements()) {
            word = tokenizer.nextToken();

            if (word.equals(delim)) {
                textLowerBuilder.append(delim);
            } else {
                first = word.charAt(0) + "";
                textLowerBuilder.append(first.toLowerCase())
                        .append(word.substring(1));
            }
        }

        return textLowerBuilder.toString();
    }

    public static String toUpperCaseCamelCase(final String string) {
        final StringBuilder builder = new StringBuilder();
        for (final char c : toLowerCaseFirstLetter(string).toCharArray()) {
            if (c < 97) {
                // maiusculo
                builder.append("_");
            }
            builder.append(c);
        }
        return builder.toString().toUpperCase();
    }

    /**
     * <p>toUpperCaseFirstLetter.</p>
     *
     * @param text Texto.
     * @return Texto com a primeira letra maiúscula de cada palavra separada por espaço.
     */
    public static String toUpperCaseFirstLetter(final String text) {
        if (text == null || isEmpty(text)) {
            return text;
        }

        final String delim = " ";
        final StringTokenizer tokenizer = new StringTokenizer(text, delim, true);

        String first;
        final StringBuilder textUpperBuilder = new StringBuilder();

        String word;

        while (tokenizer.hasMoreElements()) {
            word = tokenizer.nextToken();

            if (word.equals(delim)) {
                textUpperBuilder.append(delim);
            } else {
                first = word.charAt(0) + "";
                textUpperBuilder.append(first.toUpperCase())
                        .append(word.substring(1));
            }
        }

        return textUpperBuilder.toString();
    }

    /**
     * <p>trunc.</p>
     *
     * @param string String.
     * @param chars  Número máximo de caracteres (primeiros).
     * @return String truncada.
     */
    public static String trunc(final String string, final int chars) {
        return trunc(string, chars, true);
    }

    public static String trunc(final String string, final int chars, boolean addDots) {
        String result;
        if (string.length() > chars) {
            result = string.substring(0, chars);
            if (addDots) {
                result += SUFIX_TEXT_TRUNC;
            }
        } else {
            result = string;
        }
        return result;
    }

    /**
     * Retira os caracteres de retorno de linha, mudança de linha, tabulações e espaços de uma string.
     */
    public static String unformat(final String string) {
        return unformat(string, true);
    }

    /**
     * Retira os caracteres de retorno de linha, mudança de linha e tabulações de uma string.
     */
    public static String unformat(final String string, final boolean removeSpaces) {
        if (string == null) {
            return null;
        }
        String s = string.replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "");
        if (removeSpaces) {
            s = s.replaceAll(" ", "");
        }
        return s;
    }

    private static class StringConversors {

        public static final Action<String, String> STRING = new EchoAction<>();

        public static final Action<String, Boolean> BOOLEAN = Actions.asSilentThrowErrors(
                new Action<>() {
                    @Override
                    public Boolean apply(final String string) {
                        return "true".equals(string);
                    }
                }
        );

        public static final Action<String, Character> CHARACTER = Actions.asSilentThrowErrors(
                new Action<>() {
                    @Override
                    public Character apply(final String string) {
                        if (string.length() != 1) {
                            throw new IllegalArgumentException("Expected one char but given " + string.length());
                        }
                        return Character.valueOf(string.charAt(0));
                    }
                }
        );

        public static final Action<String, Byte> BYTE = Actions.asSilentThrowErrors(
                new Action<>() {
                    @Override
                    public Byte apply(final String string) {
                        return Byte.valueOf(string);
                    }
                }
        );

        public static final Action<String, Short> SHORT = Actions.asSilentThrowErrors(
                new Action<>() {
                    @Override
                    public Short apply(final String string) {
                        return Short.valueOf(string);
                    }
                }
        );

        public static final Action<String, Integer> INTEGER = Actions.asSilentThrowErrors(
                new Action<>() {
                    @Override
                    public Integer apply(final String string) {
                        return Integer.valueOf(string);
                    }
                }
        );

        public static final Action<String, Long> LONG = Actions.asSilentThrowErrors(
                new Action<>() {
                    @Override
                    public Long apply(final String string) {
                        return Long.valueOf(string);
                    }
                }
        );

        public static final Action<String, Float> FLOAT = Actions.asSilentThrowErrors(
                new Action<>() {
                    @Override
                    public Float apply(final String string) {
                        return Float.valueOf(string);
                    }
                });

        public static final Action<String, Double> DOUBLE = Actions.asSilentThrowErrors(
                new Action<>() {
                    @Override
                    public Double apply(final String string) {
                        return Double.valueOf(string);
                    }
                });

        public static final Action<String, BigDecimal> BIG_DECIMAL = Actions.asSilentThrowErrors(
                new Action<>() {
                    @Override
                    public BigDecimal apply(final String string) {
                        return new BigDecimal(string);
                    }
                });
    }

}
