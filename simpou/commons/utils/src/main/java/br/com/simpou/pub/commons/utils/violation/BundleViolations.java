package br.com.simpou.pub.commons.utils.violation;

import br.com.simpou.pub.commons.utils.lang.Checks;
import br.com.simpou.pub.commons.utils.lang.PrintStackHelper;
import br.com.simpou.pub.commons.utils.string.Strings;
import lombok.extern.apachecommons.CommonsLog;

import java.util.*;

/**
 * Utilitário para internacionalização das mensagens da aplicação.
 *
 * @author Jonas Pereira
 * @since 2014-09-08
 */
@CommonsLog
public final class BundleViolations {

    /**
     * Classe utilitária.
     */
    private BundleViolations() {
    }

    /**
     * Ponto de teste para o construtor de classe utilitária.
     */
    static void callConstructor() {
        new BundleViolations();
    }

    /**
     * Caminho padrão, relativo ao diretório de recursos da aplicação cliente, dos arquivos i18n para
     * mensagens.
     */
    private static final String BUNDLE_NAME = "bundle";

    /**
     * Caminho padrão, relativo ao diretório de recursos da biblioteca em uso, dos arquivos i18n para
     * mensagens.
     */
    private static final String SYSTEM_BUNDLE_NAME = "META-INF/bundle";

    /**
     * Map de bundles da aplicação cliente. Prioridade sobre os bundles do sistema.
     */
    private static final Map<Locale, ResourceBundle> APP_BUNDLES = new HashMap<Locale, ResourceBundle>();

    /**
     * Map de bundles do sistema.
     */
    private static final Map<Locale, ResourceBundle> SYSTEM_APP_BUNDLES = new HashMap<Locale, ResourceBundle>();

    /**
     * Realiza checagem da configuração para tradução dos códigos.
     *
     * @param violations Exceção das violações.
     * @param locale     Locale da requisição.
     */
    public static void translate(final List<Violation> violations, final Locale locale) {
        if (!APP_BUNDLES.containsKey(locale)) {
            try {
                APP_BUNDLES.put(locale, ResourceBundle.getBundle(BUNDLE_NAME, locale));
            } catch (MissingResourceException e) {
                //TODO mau uso de exceptions
                log.warn("Bundle not found. Put a bundle.properties file in resources.");
            }
        }
        if (!SYSTEM_APP_BUNDLES.containsKey(locale)) {
            try {
                SYSTEM_APP_BUNDLES.put(locale, ResourceBundle.getBundle(SYSTEM_BUNDLE_NAME, locale));
            } catch (MissingResourceException e) {
                //TODO mau uso: não ignorar
            }
        }
        for (final Violation violation : violations) {
            // nao sobrescreve uma mensagem já preenchida
            if (violation.getMessage() != null || violation.getCode() == null) {
                continue;
            }

            final ResourceBundle appbundle = APP_BUNDLES.get(locale);
            final ResourceBundle sysAppbundle = SYSTEM_APP_BUNDLES.get(locale);

            //if (appbundle != null) {
            translateViolationCode(appbundle, sysAppbundle, violation);
            //}
        }
    }

    /**
     * Traduz uma violação segundo arquivo de bundle. Campo "message" é preenchido.
     */
    public static void translate(final Violation violation) {
        final Locale locale = Locale.getDefault();
        if (!APP_BUNDLES.containsKey(locale)) {
            APP_BUNDLES.put(locale, ResourceBundle.getBundle(BUNDLE_NAME, locale));
        }
        if (violation.getMessage() == null && violation.getCode() != null) {
            final ResourceBundle appbundle = APP_BUNDLES.get(locale);
            final ResourceBundle sysAppbundle = SYSTEM_APP_BUNDLES.get(locale);
            translateViolationCode(appbundle, sysAppbundle, violation);
        }
    }

    private static void translateViolationCode(final ResourceBundle appBundle,
                                               final ResourceBundle sysBundle,
                                               final Violation violation) {
        try {

            String msg = appBundle == null || !appBundle.containsKey(violation.getCode()) ?
                    null : appBundle.getString
                    (violation.getCode());
            if (msg == null) {
                msg = sysBundle == null || !sysBundle.containsKey(violation.getCode())
                        ? null : sysBundle.getString(violation.getCode());
                if (msg == null) {
                    return;
                }
            }

            if (Checks.isNotEmpty(violation.getMessageParams())) {
                msg = Strings.replaceParams(msg, violation.getMessageParams());
            }
            violation.setMessage(msg);
        } catch (final MissingResourceException e) {
            //irrelevante e não previsível
            // mas deve haver pelo menos uma declaração para passar no checkStyle
            PrintStackHelper.logStackTrace("Violation code translation throws an exception.", e);
        }
    }

}
