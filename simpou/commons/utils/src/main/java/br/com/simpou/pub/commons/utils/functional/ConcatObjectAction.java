package br.com.simpou.pub.commons.utils.functional;

import lombok.RequiredArgsConstructor;

/**
 * Realiza uma ação de concatenar objetos (string correspondente) de tipos quaisquer.
 *
 * @author Jonas Pereira
 * @since 2013-10-15
 */
@RequiredArgsConstructor
public class ConcatObjectAction<T> extends VoidAction<T> {

    /**
     * A cada concatenação, é incluído também um texto fixo antes do valor adicionado.
     */
    private final String preffix;

    /**
     * A cada concatenação, é incluído também um texto fixo após o valor adicionado.
     */
    private final String suffix;

    /**
     * Indica se um sufixo deve ser incluído ou não na última concatenação.
     */
    private final boolean endSuffixAllowed;

    /**
     * Ação responsável por converter um objeto em um string a ser concatenado .
     */
    private final Action<T, String> contentExtractor;

    /**
     * Número padrão máximo de caracteres que podem ser concatenados e armazenados.
     */
    private final int maxStrLength;

    /**
     * Número padrão máximo de caracteres que podem ser concatenados e armazenados.
     */
    public static final int MAX_STR_LENGTH = 1000;

    /**
     * Acumulador das concatenações.
     */
    private StringBuilder result = new StringBuilder(MAX_STR_LENGTH);

    /**
     * @see #ConcatObjectAction(String, String, boolean, Action, int) Usa {@link #MAX_STR_LENGTH} como
     * atributo "maxStrLength".
     */
    public ConcatObjectAction(final String preffix,
                              final String suffix,
                              final boolean endSuffixAllowed,
                              final Action<T, String> contentExtractor) {
        this(preffix, suffix, endSuffixAllowed, contentExtractor, MAX_STR_LENGTH);
    }

    /**
     * @return Resultado final de todas concatenações. O estado inicial é restaurado.
     */
    public final String getResult() {
        final String returnAux;
        if (endSuffixAllowed) {
            returnAux = result.toString();
        } else if (result.length() > 0) {
            returnAux = result.substring(0, result.length() - suffix.length());
        } else {
            returnAux = result.toString();
        }
        reset();
        return returnAux;
    }

    @Override
    protected final void executeDelegate(final T value) {
        final String toAppend = preffix + contentExtractor.apply(value) + suffix;
        if (result.length() + toAppend.length() > maxStrLength) {
            throw new StringIndexOutOfBoundsException("Max buffer length is " + maxStrLength + ". Actual "
                    + "length: " + result.length());
        } else {
            result.append(toAppend);
        }
    }

    /**
     * Retorna à condição inicial, nada concatenado.
     */
    public final void reset() {
        result.delete(0, result.length());
    }

    public static class ToStringContentExtractor<T> implements Action<T, String> {

        @Override
        public String apply(final T object) {
            return object.toString();
        }
    }
}
