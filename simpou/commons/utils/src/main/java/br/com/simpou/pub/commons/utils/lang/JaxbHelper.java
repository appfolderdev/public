package br.com.simpou.pub.commons.utils.lang;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.Writer;
import java.util.*;

/**
 * @author Jonas Pereira
 * @version 2013-08-20
 * @since 2013-08-20
 */
public class JaxbHelper {

    private static final Set<Class<?>> contextClasses = new HashSet<>();

    @Getter
    private static JAXBContext context;

    static {
        try {
            refreshContext();
        } catch (final JAXBException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static void addClassesToContext(final Class... classes) throws JAXBException {
        final List<Class<?>> list = Casts.simpleCast(Arrays.asList(classes));
        addClassesToContext(list);
    }

    public static void addClassesToContext(final Collection<Class<?>> classes) throws JAXBException {
        contextClasses.addAll(classes);
        refreshContext();
    }

    public static void addClassesToContext(final Class<?> clasz) throws JAXBException {
        contextClasses.add(clasz);
        refreshContext();
    }

    static Set<Class<?>> getContextClasses() {
        return Collections.unmodifiableSet(contextClasses);
    }

    public static JAXBContext getContextFrom(final Class<?>... classes) throws JAXBException {
        return JAXBContext.newInstance(classes);
    }

    public static Marshaller getMarshaller(final JAXBContext context, final Property... properties) throws Exception {
        final Marshaller marshaller = context.createMarshaller();
        for (final Property property : properties) {
            marshaller.setProperty(property.getName().toString(), property.getValue());
        }
        return marshaller;
    }

    //    public static void marshal(final JAXBContext context, final Writer writer, final MarshalType type, final Object object, final Property... properties) throws JAXBException, Exception {
    //        switch (type) {
    //            case MarshalType.JSON:
    //                marshalJson(context, writer, object, properties);
    //                break;
    //            case MarshalType.XML:
    //                marshalXml(context, writer, object, properties);
    //                break;
    //            default:
    //                throw new UnsupportedOperationException("Media type " + type + " not supported yet.");
    //        }
    //    }

    public static void marshalJson(final JAXBContext context, final Writer writer, final Object object, final Property... properties) throws Exception {
        //TODO substituir por jackson

        //        final Configuration config = new Configuration();
        ////        config.setSupressAtAttributes(true);
        ////        config.setImplicitCollections(true);
        //        final MappedNamespaceConvention con = new MappedNamespaceConvention(config);
        //        final XMLStreamWriter xmlStreamWriter = new MappedXMLStreamWriter(con, writer);
        //        final Marshaller marshaller = getMarshaller(context, properties);
        //        marshaller.marshal(object, xmlStreamWriter);
    }

    public static void marshalXml(final JAXBContext context, final Writer writer, final Object object, final Property... properties) throws Exception {
        final Marshaller marshaller = getMarshaller(context, properties);
        marshaller.marshal(object, writer);
    }

    private static void refreshContext() throws JAXBException {
        //TODO incluir locks
        context = getContextFrom(contextClasses.toArray(new Class[contextClasses.size()]));
    }

    /**
     * Converte um xml em objeto compatível JAXB.
     *
     * @param clasz Tipo do objeto a ser retornado.
     * @param xml   Conteúdo textual em formato xml.
     * @return Objeto convertido.
     * @throws JAXBException Caso ocorra.
     */
    public static <T> T unmarshalXml(final Class<T> clasz, final String xml) throws JAXBException {
        final JAXBContext localContext = JAXBContext.newInstance(clasz);
        final Unmarshaller unmarshaller = localContext.createUnmarshaller();
        final StringReader reader = new StringReader(xml);
        return Casts.simpleCast(unmarshaller.unmarshal(reader));
    }

    public enum MarshalType {

        XML, JSON

    }

    @RequiredArgsConstructor
    @Getter
    public static class Property {

        private final Object name;

        private final Object value;

    }
}
