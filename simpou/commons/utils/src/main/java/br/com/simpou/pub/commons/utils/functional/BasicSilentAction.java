package br.com.simpou.pub.commons.utils.functional;

import lombok.RequiredArgsConstructor;

/**
 * Ação silenciosa que por padrão transforma todas exceções em runtime. Retorna null em caso da ocorrência
 * de exceção.
 *
 * @author Jonas Pereira
 * @since 22/01/17
 */
@RequiredArgsConstructor
public abstract class BasicSilentAction<P, R> implements SilentAction<P, R> {

    @Override
    public final R apply(final P object) {
        try {
            return applyDelegate(object);
        } catch (Exception e) {
            doOnError(object, e);
            return getDefault();
        }
    }

    protected abstract R applyDelegate(final P object) throws Exception;

    @Override
    public R getDefault() {
        return null;
    }

    @Override
    public void doOnError(final P object, final Throwable t) {
        if (t instanceof RuntimeException) {
            throw (RuntimeException) t;
        } else if (t instanceof Error) {
            throw (Error) t;
        } else {
            throw new RuntimeException(t);
        }
    }
}

