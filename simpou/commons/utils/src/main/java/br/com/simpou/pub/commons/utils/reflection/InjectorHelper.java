package br.com.simpou.pub.commons.utils.reflection;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

/**
 * Injetor padrão de objetos. Baseado no mecanismo de injeção de dependências e localização de serviços nativo
 * do Java, em que o módulo (jar), configurado como dependência 'runtime', define qual implementação de uma
 * determinada interface deve ser instanciada; este módulo deve definir a pasta 'META-INF/services' dentro do
 * diretório de recursos do projeto, contendo um arquivo para cada combinação interface/implementação, cuja
 * interfface é indicada pelo nome do arquivo e a implementação pelo seu conteúdo, sempre considerando nomes
 * canônicos de classes.
 *
 * @author Jonas Pereira
 * @since 2013-05-13
 */
public final class InjectorHelper {

    /**
     * Instancia novo objeto baseado no mecanismo de 'service loader' do Java. Somente sua interface deve ser
     * conhecida em runtime, de forma que não seja possível a instanciação em tempo de compilação.
     *
     * @param <T>   Tipo do objeto esperado.
     * @param clasz Tipo a ser instanciado.
     * @return Nova instância de uma implementação da interface parametrizada.
     * @throws UnsupportedOperationException Se implementação a ser instanciada não for encontrada.
     * @throws Exception                     Se houver.
     */
    public static <T> T newInstance(final Class<T> clasz) throws Exception {
        return newInstances(clasz).get(0);
    }

    /**
     * Instancia novos objetos baseado no mecanismo de 'service loader' do Java. Somente sua interface deve
     * ser conhecida em runtime, de forma que não seja possível a instanciação em tempo de compilação.
     *
     * @param <T>   Tipo dos objetos esperados.
     * @param clasz Tipo a ser instanciado.
     * @return Novas instâncias da interface parametrizada.
     * @throws UnsupportedOperationException Se não for encontrada nenhuma implementação.
     * @throws Exception                     Se houver.
     */
    public static <T> List<T> newInstances(final Class<T> clasz) throws Exception {
        final ServiceLoader<T> loader = ServiceLoader.load(clasz);

        final List<T> instances = new ArrayList<T>();

        for (final T provider : loader) {
            instances.add(provider);
        }

        if (instances.isEmpty()) {
            throw new UnsupportedOperationException("Injection point "
                    + clasz.getName() + " not found.");
        }

        return instances;
    }
}
