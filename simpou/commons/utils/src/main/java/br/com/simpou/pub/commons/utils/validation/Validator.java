package br.com.simpou.pub.commons.utils.validation;

import br.com.simpou.pub.commons.utils.violation.Violation;

/**
 * Representa um validador de objetos.
 *
 * @author Jonas Pereira
 * @version 2013-05-31
 * @since 2012-07-25
 */
public interface Validator<T> {

    /**
     * @param obj Objeto a ser validado.
     * @return null se objeto válido. Se inválido retorna o motivo.
     */
    Violation validate(final T obj);
}
