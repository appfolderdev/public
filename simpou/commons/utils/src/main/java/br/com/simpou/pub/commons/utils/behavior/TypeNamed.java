package br.com.simpou.pub.commons.utils.behavior;

public interface TypeNamed {

    String typeName();

}
