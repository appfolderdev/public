package br.com.simpou.pub.commons.utils.validation;

import br.com.simpou.pub.commons.utils.violation.Violation;
import br.com.simpou.pub.commons.utils.string.RegexHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validador de e-mail.
 *
 * @author Jonas Pereira
 * @version 2012-07-29
 * @since 2011-06-25
 */
public final class EmailValidator implements Validator<String> {

    static final Pattern PATTERN = Pattern.compile(RegexHelper.EMAIL);

    static final String INVALID_LENGTH = "simpou.commons.utils.validation.email.length";

    static final String NOT_MATCH = "simpou.commons.utils.validation.email.notMatch";

    /**
     * @param value Valor a ser validado.
     * @return true se valor válido.
     */
    public static boolean isValid(final String value) {
        return _validate(value) == null;
    }

    private static Violation _validate(final String value) {
        final Violation violation;

        if (value == null || value.isEmpty()) {
            violation = null;
        } else if (value.length() < 5) {
            violation = new Violation(INVALID_LENGTH);
        } else {
            final Matcher m = PATTERN.matcher(value);

            if (m.matches()) {
                violation = null;
            } else {
                violation = new Violation(NOT_MATCH);
            }
        }

        return violation;
    }

    /**
     * @param email E-mail a ser validado.
     */
    @Override
    public Violation validate(final String email) {
        return _validate(email);
    }

}
