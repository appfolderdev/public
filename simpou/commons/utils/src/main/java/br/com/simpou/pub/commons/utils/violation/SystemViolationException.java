package br.com.simpou.pub.commons.utils.violation;

/**
 * Created by Jonas on 26/07/2015.
 */
public class SystemViolationException extends ViolationsException {

    public SystemViolationException(final Throwable t) {
        super(new SystemViolation(t));
    }

    public SystemViolationException(final String code, final Object... params) {
        super(new SystemViolation(code, params));
    }
}
