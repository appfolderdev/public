package br.com.simpou.pub.commons.utils.lang;

/**
 * Created by 9000070 on 23/03/2017.
 */
public class Threads {

    /**
     * Invoca {@link Thread#sleep(long)} se tempo de espera positivo. Encapsula checked exceptions.
     */
    public static void sleep(long timeMilis) {
        if (timeMilis > 0) {
            try {
                Thread.sleep(timeMilis);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
