package br.com.simpou.pub.commons.utils.validation;

import br.com.simpou.pub.commons.utils.violation.Violation;
import br.com.simpou.pub.commons.utils.string.RegexHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validador de mac address: 00:19:B9:FB:E2:58.
 *
 * @author Jonas Pereira
 * @since 2018-11-17
 */
public final class MacAddressValidator implements Validator<String> {

    static final Pattern PATTERN = Pattern.compile(RegexHelper.MAC_ADDRESS);

    static final String NOT_MATCH = "simpou.commons.utils.validation.macAddress.notMatch";

    /**
     * @param value Valor a ser validado.
     * @return Null se valor válido.
     */
    public static Violation isValid(final String value) {
        final Violation violation;
        if (value == null) {
            violation = null;
        } else {
            final Matcher m = PATTERN.matcher(value);
            if (m.matches()) {
                violation = null;
            } else {
                violation = new Violation(NOT_MATCH);
            }
        }
        return violation;
    }

    @Override
    public Violation validate(final String value) {
        return isValid(value);
    }

}
