package br.com.simpou.pub.commons.utils.violation;

import br.com.simpou.pub.commons.utils.lang.Checks;
import br.com.simpou.pub.commons.utils.string.Strings;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.*;

/**
 * @author Jonas Pereira
 * @since 17/11/13
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ViolationsWrapper implements Serializable {

    /**
     * Regras de negócio violadas.
     */
    private List<BusinessViolation> businessViolations = new ArrayList<>();

    /**
     * Erros de validação em campos.
     */
    private List<ValidationViolation> validationViolations = new ArrayList<>();

    /**
     * Regras de negócio violadas.
     */
    private List<SystemViolation> systemViolations = new ArrayList<>();

    /**
     * Erro interno.
     */
    @JsonIgnore
    private boolean internalError;

    /**
     * @param violation Violação.
     */
    public ViolationsWrapper(final Violation... violation) {
        this(Arrays.asList(violation));
    }

    /**
     * @param violations Lista de Violações.
     */
    public ViolationsWrapper(final Collection<? extends Violation> violations) {
        addViolations(violations);
    }

    /**
     * Adiciona uma Violação à lista.
     *
     * @param violation Violação.
     */
    public void addViolation(final Violation violation) {
        if (violation instanceof ValidationViolation) {
            this.validationViolations.add((ValidationViolation) violation);
        } else if (violation instanceof BusinessViolation) {
            this.businessViolations.add((BusinessViolation) violation);
        } else if (violation instanceof SystemViolation) {
            this.systemViolations.add((SystemViolation) violation);
            this.internalError = true;
        } else {
            throw new UnsupportedOperationException("Unknown violation type.");
        }
    }

    /**
     * @param violations Lista de Violações.
     */
    public void addViolations(final Collection<? extends Violation> violations) {
        for (final Violation violation : violations) {
            addViolation(violation);
        }
    }

    /**
     * O tipos de violações que não tiverem nenhum item serão anulados, ou seja, listas vazias se tornarão
     * null.
     */
    public void clearEmpties() {
        if (Checks.isEmpty(this.validationViolations)) {
            this.validationViolations = null;
        }
        if (Checks.isEmpty(this.businessViolations)) {
            this.businessViolations = null;
        }
        if (Checks.isEmpty(this.systemViolations)) {
            this.systemViolations = null;
        }
    }

    /**
     * Junta as ciolações.
     *
     * @return Lista com as violações.
     */
    public List<Violation> joinAll() {
        final List<Violation> violations = new ArrayList<>();
        if (this.validationViolations != null) {
            violations.addAll(this.validationViolations);
        }
        if (this.businessViolations != null) {
            violations.addAll(this.businessViolations);
        }
        if (this.systemViolations != null) {
            violations.addAll(this.systemViolations);
        }
        return violations;
    }

    public ViolationsWrapper prepareSerialization() {
        replaceParams();
        sortViolations();
        clearEmpties();
        return this;
    }

    /**
     * Substitui parâmetros.
     */
    public void replaceParams() {
        for (final Violation violation : this.businessViolations) {
            violation.setMessage(Strings.replaceParams(violation.getMessage(), violation.getMessageParams()));
        }
    }

    /**
     * Ordena a lista de Violações.
     */
    public void sortViolations() {
        Collections.sort(this.validationViolations);
        Collections.sort(this.businessViolations);
        Collections.sort(this.systemViolations);
    }

}
