package br.com.simpou.pub.commons.utils.validation;

import br.com.simpou.pub.commons.utils.violation.Violation;
import br.com.simpou.pub.commons.utils.string.RegexHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validador de telefone: +55(31)988888888.
 * +country code (area code) phone number
 *
 * @author Jonas Pereira
 * @since 2018-07-23
 */
public final class PhoneValidator implements Validator<String> {

    static final Pattern PATTERN = Pattern.compile(RegexHelper.PHONE);

    static final String NOT_MATCH = "simpou.commons.utils.validation.phone.notMatch";

    /**
     * @param value Valor a ser validado.
     * @return Null se valor válido.
     */
    public static Violation isValid(final String value) {
        final Violation violation;
        if (value == null) {
            violation = null;
        } else {
            final Matcher m = PATTERN.matcher(value);
            if (m.matches()) {
                violation = null;
            } else {
                violation = new Violation(NOT_MATCH);
            }
        }
        return violation;
    }

    /**
     * @param phone Telefone a ser validado.
     */
    @Override
    public Violation validate(final String phone) {
        return isValid(phone);
    }

}
