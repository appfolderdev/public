package br.com.simpou.pub.commons.utils.functional;

import java.io.Serializable;

/**
 * Use para passar métodos como parâmetros a outros métodos.
 *
 * @param <P> Tipo do objeto a ser processado pela execução de uma ação.
 * @param <R> Tipo do resultado do processamento da ação.
 * @author Jonas Pereira
 * @since 2013-06-19
 */
public interface Action<P, R> extends Serializable {

    /**
     * Executa uma ação qualquer de transformação do objeto parametrizado.
     */
    R apply(P object);

}
