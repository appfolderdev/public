package br.com.simpou.pub.commons.utils.tests.annot;

import java.lang.annotation.*;

/**
 * Indica que um método de teste não está associado a nenhum método da classe
 * que está sendo testada.
 *
 * @author Jonas Pereira
 * @version 2012-05-13
 * @since 2012-05-13
 */
@Deprecated
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ExtraTest {

}
