package br.com.simpou.pub.commons.utils.functional;

import lombok.RequiredArgsConstructor;

/**
 * Condição que sempre retorna um valor fixado independente da entrada.
 *
 * @author Jonas Pereira
 * @since 5/18/15
 */
@RequiredArgsConstructor
public final class FixedCondition<T> implements Condition<T> {

    private final boolean fixedValue;

    @Override
    public Boolean apply(final T t) {
        return fixedValue;
    }
}
