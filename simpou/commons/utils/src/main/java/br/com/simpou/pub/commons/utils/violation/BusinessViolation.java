package br.com.simpou.pub.commons.utils.violation;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Descrição da violação de regra de negócio.
 *
 * @author Jonas Pereira
 * @since 2012-12-28
 */
@Getter
@Setter
@NoArgsConstructor
public class BusinessViolation extends Violation {

    public static final String TYPE = "business";

    /**
     * @param code   Código do erro.
     * @param params parâmetros.
     */
    public BusinessViolation(final String code, final Object... params) {
        super(code, null, params);
        super.setType(TYPE);
    }
}
