package br.com.simpou.pub.commons.utils.lang;

import br.com.simpou.pub.commons.utils.violation.Violation;
import br.com.simpou.pub.commons.utils.violation.ViolationsException;
import lombok.Getter;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Use em retornos de métodos cujos resultados possa ser intermediários, ou seja, o resposta não
 * necessariamente necessita ser completa. Erros podem acontecer no meio do processo impedindo o
 * prosseguimento, o que não invalida o estado já obtido.
 * <p>
 * Use para armazenar tanto a resposta parcial ou completa, como as violações e erros que ocorreram e que
 * impossibilitaram o término. Se não forem declaradas violações e execeções, a resposta deve ser considera
 * como completa.
 *
 * @author Jonas Pereira
 * @since 22/03/2017
 */
public class PartialReturn<T> {

    @Getter
    private final T value;

    @Getter
    private final Collection<? extends Violation> violations;

    private final Exception exception;

    public PartialReturn(final T value,
                         final Collection<? extends Violation> violations,
                         final Exception exception) {
        this.value = value;
        this.violations = Collections.unmodifiableCollection(
                violations == null ? Collections.<Violation>emptyList() : violations
        );
        this.exception = exception;
    }

    public PartialReturn(final T value) {
        this(value, null, null);
    }

    public PartialReturn(final T value, final Violation... violations) {
        this(value, Arrays.asList(violations), null);
    }

    public PartialReturn(final T value, final Exception exception) {
        this(value, null, exception);
    }

    /**
     * @return False se não houverem violações e nem exceções.
     */
    public boolean isPartial() {
        return !violations.isEmpty() || exception != null;
    }

    /**
     * @return {@link br.com.simpou.pub.commons.utils.violation.ViolationsException} se houverem violações, caso contrário retorna campo de exceção. Se
     * ambos existirem, o campo exceção vai como causa da violação.
     */
    public Exception getException() {
        if (violations.isEmpty()) {
            return exception;
        } else {
            return new ViolationsException(this.exception, violations);
        }
    }
}
