package br.com.simpou.pub.commons.utils.functional;

/**
 * Realiza uma ação que não gera retornos. O parâmetro da ação é ecoado para a saída da execução.
 *
 * @author Jonas Pereira
 * @version 2013-06-19
 */
public abstract class VoidAction<T> implements Action<T, T> {

    /**
     * Ação a ser executada.
     */
    protected abstract void executeDelegate(T object);

    @Override
    public final T apply(final T object) {
        executeDelegate(object);
        return object;
    }
}
