package br.com.simpou.pub.commons.utils.reflection;

import java.lang.reflect.Field;

/**
 * Created by jonas on 21/01/17.
 */
public class NotJacocoFieldCondition implements FieldCondition {

    @Override
    public Boolean apply(final Field field) {
        return !field.getName().equals("$jacocoData");
    }
}
