package br.com.simpou.pub.commons.utils.lang;

import java.util.Iterator;

/**
 * Iterador sobre um array.
 *
 * @param <T> Tipo dos elementos do array.
 * @author Jonas Pereira
 * @version 2013-05-31
 * @since 2013-05-31
 */
public final class ArrayIterator<T> implements Iterator<T> {

    /**
     * Posição corrente do próximo elemento a ser retornado durante iteração.
     */
    private int position;

    /**
     * Referência ao array sobre o qual está sendo realizado a iteração.
     */
    private final T[] arrayRef;

    /**
     * @param arrayRef Array a ser iterado. Não deve ser null. É armazenado um clone, e não sua referência.
     */
    public ArrayIterator(final T[] arrayRef) {
        Assertions.notNull(arrayRef);
        this.arrayRef = arrayRef.clone();
        this.position = 0;
    }

    @Override
    public boolean hasNext() {
        return arrayRef != null && position < arrayRef.length;
    }

    @Override
    public T next() {
        if (!hasNext()) {
            throw new IndexOutOfBoundsException("Iteration already ended.");
        }

        return arrayRef[position++];
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
