package br.com.simpou.pub.commons.utils.string;

import java.util.Locale;

/**
 * Métodos úteis para manipulções de locales.
 *
 * @author Jonas Pereira
 * @version 2012-05-09
 * @since 2011-06-25
 */
public class StringLocales {

    /**
     * <p>getStringAsLocale.</p>
     *
     * @param locale String representativa de um locale. Exs.: pt, pt_BR,
     *               pt_BR_mg. Locale padrão caso string seja null.
     * @return a {@link java.util.Locale} object.
     */
    public static Locale getStringAsLocale(final String locale) {
        if (locale == null) {
            return Locale.getDefault();
        }

        final String[] parts = locale.split("_");

        switch (parts.length) {
            case 2:
                return new Locale(parts[0], parts[1]);

            case 1:
                return new Locale(parts[0]);

            case 3:
                return new Locale(parts[0], parts[1], parts[2]);

            default:
                throw new IllegalArgumentException(
                        "Cannot convert locale string \"" + locale +
                                "\" to a Locale object");
        }
    }
}
