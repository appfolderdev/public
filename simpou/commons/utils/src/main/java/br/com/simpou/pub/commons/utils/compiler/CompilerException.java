package br.com.simpou.pub.commons.utils.compiler;

import br.com.simpou.pub.commons.utils.lang.Assertions;
import lombok.Getter;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaFileObject;
import java.util.List;
import java.util.Locale;

/**
 * Erros de compilação.
 *
 * @author Jonas Pereira
 * @since 2011-08-18
 */
public class CompilerException extends Exception {

    /**
     * Mensagens de erro de compilação.
     */
    @Getter
    private final DiagnosticCollector<JavaFileObject> diagnostics;

    CompilerException(final String msg, final DiagnosticCollector<JavaFileObject> diagnostics) {
        this(msg, null, diagnostics);
    }

    CompilerException(final String msg,
                      final Throwable thrwbl,
                      final DiagnosticCollector<JavaFileObject> diagnostics) {
        super(msg, thrwbl);
        this.diagnostics = Assertions.notNull(diagnostics);
    }

    @Override
    public String getMessage() {
        final StringBuilder buffer = new StringBuilder(super.getMessage()).append('\n');
        final List<Diagnostic<? extends JavaFileObject>> list = diagnostics.getDiagnostics();
        for (final Diagnostic<? extends JavaFileObject> diagnostic : list) {
            buffer.append('(').append(diagnostic.getLineNumber());
            buffer.append(',').append(diagnostic.getColumnNumber()).append(") ");
            buffer.append(diagnostic.getMessage(Locale.getDefault())).append('\n');
        }
        return buffer.toString();
    }

    @Override
    public String toString() {
        return this.getMessage();
    }
}
