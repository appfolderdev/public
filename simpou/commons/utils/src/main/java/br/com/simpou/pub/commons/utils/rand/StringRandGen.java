package br.com.simpou.pub.commons.utils.rand;

/**
 * @author jonas1
 * @version 23/09/13
 * @since 23/09/13
 */
public class StringRandGen implements RandGenerator<String> {

    @Override
    public String generate() {
        return Randoms.getString();
    }
}
