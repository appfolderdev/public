package br.com.simpou.pub.commons.utils.compiler;

import br.com.simpou.pub.commons.utils.lang.Assertions;

import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import java.io.IOException;

/**
 * Representa um gerenciador de arquivos java.
 *
 * @author Jonas Pereira
 * @since 2011-08-18
 */
final class CompilerFileManager extends ForwardingJavaFileManager<JavaFileManager> {

    private CompilerClassLoader classLoader;

    /**
     * Arquivo Java compilado.
     */
    private CompilerJavaFileObject compiledObj;

    CompilerFileManager(final JavaFileManager fileManager,
                        final CompilerClassLoader classLoader) {
        super(fileManager);
        this.classLoader = Assertions.notNull(classLoader);
    }

    CompilerFileManager(final JavaFileManager fileManager,
                        final CompilerClassLoader classLoader,
                        final CompilerJavaFileObject compiledObj) {
        this(fileManager, classLoader);
        setObjects(compiledObj);
    }

    void setObjects(final CompilerJavaFileObject compilerJavaFileObject) {
        this.compiledObj = Assertions.notNull(compilerJavaFileObject);
        this.classLoader.setCompiledObj(compilerJavaFileObject);
    }

    @Override
    public JavaFileObject getJavaFileForOutput(final Location location,
                                               final String qualifiedName,
                                               final JavaFileObject.Kind kind,
                                               final FileObject outputFile) throws IOException {
        return compiledObj;
    }

    @Override
    public ClassLoader getClassLoader(final Location location) {
        return classLoader;
    }
}
