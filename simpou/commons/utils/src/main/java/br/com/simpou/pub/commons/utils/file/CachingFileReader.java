package br.com.simpou.pub.commons.utils.file;

import br.com.simpou.pub.commons.utils.functional.BasicSilentAction;
import lombok.EqualsAndHashCode;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Leitor de arquivos. Os arquivos lidos são mantidos em memória.
 * <p>
 * Created by jonas on 22/01/17.
 */
public class CachingFileReader extends BasicSilentAction<File, byte[]> {

    public static final long DEF_CACHE_LIMIT = 524288000;//500*1024*1024=500MB

    private final long cacheLimit;

    private static long length;

    private static final Map<CacheItem, byte[]> CACHE = new HashMap<>();

    /**
     * @see #CachingFileReader(long)
     * @see #DEF_CACHE_LIMIT
     */
    public CachingFileReader() {
        this(DEF_CACHE_LIMIT);
    }

    /**
     * @param cacheLimit Tamanho máximo em bytes a serem mantidos em memória.
     */
    public CachingFileReader(final long cacheLimit) {
        this.cacheLimit = cacheLimit;
    }

    @EqualsAndHashCode(of = "name")
    private class CacheItem implements Comparable<CacheItem> {

        private final String name;

        private final Date date;

        CacheItem(final File file) {
            this.name = file.getAbsolutePath();
            this.date = new Date();
        }

        @Override
        public int compareTo(final CacheItem cacheItem) {
            // mais antigo primeiro
            return date.compareTo(cacheItem.date);
        }
    }

    @Override
    protected byte[] applyDelegate(final File file) throws Exception {
//        final CacheItem fileKey = new CacheItem(file);
        final byte[] content;
//        synchronized (CACHE) {
//            if (CACHE.containsKey(fileKey)) {
//                content = CACHE.get(fileKey);
//            } else {
        content = FileHelper.read(file);
//                length += content.length;
//                if (length > cacheLimit) {
//                    // estourou o limite, escolhe o mais antigo pra sair
//                    final List<CacheItem> cacheItems = new ArrayList<>(CACHE.keySet());
//                    Collections.sort(cacheItems);
//                    final CacheItem toRemoveItem = cacheItems.get(0);
//                    length -= CACHE.get(toRemoveItem).length;
//                    CACHE.remove(toRemoveItem);
//                }
//                CACHE.put(fileKey, content);
//            }
//        }
        return content;
    }

    @Override
    public byte[] getDefault() {
        return new byte[0];
    }
}
