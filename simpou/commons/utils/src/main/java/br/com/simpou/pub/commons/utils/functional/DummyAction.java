package br.com.simpou.pub.commons.utils.functional;

/**
 * Created by jonas on 22/04/2016.
 */
public class DummyAction<T, E> implements Action<T, E> {

    @Override
    public E apply(final T object) {
        return null;
    }
}
