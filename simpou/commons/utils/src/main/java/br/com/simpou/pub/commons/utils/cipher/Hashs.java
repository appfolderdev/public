package br.com.simpou.pub.commons.utils.cipher;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

/**
 * Trata das operacoes sobre hash em arquivos e textos.
 *
 * @author Jonas Pereira
 * @since 2011-07-12
 */
public class Hashs {

    /**
     * MessageDigest.
     */
    private static final MessageDigest MD = MessageDigests.getDigest(Algorithms.SHA_256);

    /**
     * Codificação de caracteres utilizada.
     */
    private static final String DEFAULT_CHARSET_NAME = "UTF8";

    private static final int BUFFER_SIZE = 1024;

    private static final int CALC_HASH_PARAM_1 = 0xff;

    private static final int CALC_HASH_PARAM_2 = 0x100;

    private static final int CALC_HASH_PARAM_3 = 16;

    /**
     * @return Hash SHA-256 de um arquivo.
     */
    public static synchronized String getHash(final File file) throws IOException {
        MD.reset();

        final byte[] dataBytes = new byte[BUFFER_SIZE];
        int nRead;
        try (FileInputStream fis = new FileInputStream(file)) {
            while ((nRead = fis.read(dataBytes)) != -1) {
                MD.update(dataBytes, 0, nRead);
            }
        }

        return getHash();
    }

    /**
     * Executa {@link #getHash(String)}, transformando as exceções ocorridas em {@link RuntimeException}.
     */
    public static synchronized String getHashNoErrors(final String string) {
        try {
            return getHash(string);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * @return Hash SHA-256 de um texto.
     */
    public static synchronized String getHash(final String string) throws UnsupportedEncodingException {
        MD.reset();
        MD.update(string.getBytes(DEFAULT_CHARSET_NAME));
        return getHash();
    }

    private static String getHash() {
        final byte[] byteData = MD.digest();
        final StringBuilder sb = new StringBuilder();
        for (final byte aByteData : byteData) {
            final int tempVar = aByteData & CALC_HASH_PARAM_1;
            sb.append(Integer.toString(tempVar + CALC_HASH_PARAM_2, CALC_HASH_PARAM_3).substring(1));
        }
        return sb.toString();
    }
}
