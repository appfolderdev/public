package br.com.simpou.pub.commons.utils.reflection;

import br.com.simpou.pub.commons.utils.functional.Condition;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Condições que verifica se um tipo é de um determinado tipo.
 *
 * @author Jonas Pereira
 * @since 2013-10-21
 */
@AllArgsConstructor
public class InstanceOfCondition implements Condition<Class<?>> {

    @Getter
    private final Class<?> clasz;

    @Override
    public Boolean apply(final Class<?> type) {
        return clasz.isAssignableFrom(type);
    }

}
