package br.com.simpou.pub.commons.utils.pattern.composite;

import br.com.simpou.pub.commons.utils.functional.Action;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Jonas Pereira
 * @since 03/03/2017
 */
@EqualsAndHashCode(of = "stringId")
@RequiredArgsConstructor
abstract class AbstractCompositeComponent<P, R> implements CompositeComponent<P, R> {

    private final P basic;

    private final AbstractCompositeComponent<P, R> composite;

    private final String stringId;

    @Override
    public R getValue(final Action<P, R> operation) {
        return getValue(operation, new HashMap<CompositeComponent, R>(), new HashMap<P, R>());
    }

    protected R getValue(final Action<P, R> operation,
                         final Map<CompositeComponent, R> componentCache,
                         final Map<P, R> conversionCache) {
        if (componentCache.containsKey(this)) {
            return componentCache.get(this);
        }
        // resolve valor a ser operado
        final R value;
        if (composite != null) {
            value = getValue(composite, operation, componentCache, conversionCache);
        } else {
            if (conversionCache.containsKey(basic)) {
                value = conversionCache.get(basic);
            } else {
                value = operation.apply(basic);
                conversionCache.put(basic, value);
            }
        }
        final R result = getValue(value, operation, componentCache, conversionCache);
        componentCache.put(this, result);
        return result;
    }

    protected R getValue(final AbstractCompositeComponent<P, R> composite,
                         final Action<P, R> operation,
                         final Map<CompositeComponent, R> componentCache,
                         final Map<P, R> conversionCache) {
        final R value;
        if (componentCache.containsKey(composite)) {
            value = componentCache.get(composite);
        } else {
            value = composite.getValue(operation, componentCache, conversionCache);
            componentCache.put(composite, value);
        }
        return value;
    }

    protected abstract CompositeOperation<P, R> getOperation();

    protected abstract R getValue(R value, Action<P, R> conversor,
                                  Map<CompositeComponent, R> componentCache,
                                  Map<P, R> conversionCache);

    @Override
    public CompositeComponent<P, R> compose(final CompositeComponent<P, R> component,
                                            final CompositeOperation<P, R> operation) {
        return Composites.from(this, component, operation);
    }

    @Override
    public CompositeComponent<P, R> compose(final P basic,
                                            final CompositeOperation<P, R> operation) {
        return Composites.from(
                this,
                Composites.<P, R>from(basic),
                operation
        );
    }

    @Override
    public String toString() {
        return stringId;
    }

    protected static String toStringWrap(final CompositeComponent<?, ?> component) {
        String toString = component.toString();
        if (component.isComposite()) {
            toString = "(" + toString + ")";
        }
        return toString;
    }
}
