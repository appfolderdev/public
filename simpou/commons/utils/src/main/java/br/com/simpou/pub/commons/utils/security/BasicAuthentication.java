package br.com.simpou.pub.commons.utils.security;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by jonas on 12/01/2017.
 */
@Getter
@RequiredArgsConstructor
public class BasicAuthentication implements Authentication {

    private final String username;

    private final String password;

    @Override
    public Object getCredentials() {
        return this;
    }

}
