package br.com.simpou.pub.commons.utils.pattern.strategy;

import java.util.*;

import static org.apache.commons.collections4.IterableUtils.get;
import static org.apache.commons.collections4.IterableUtils.isEmpty;

/**
 * Implementação básica para gerenciamento de estratégias de um determinado tipo baseada em qualificadores em
 * tempo de execução.
 *
 * @param <K> Tipo do qualificador da estratégia.
 * @param <V> Tipo do conjunto de estratégias.
 * @see QualifiedStrategy
 */
public abstract class StrategiesRecordImpl<K, V extends QualifiedStrategy<K>>
        implements StrategiesRecord<K, V> {

    private final Map<K, List<V>> register;

    /**
     * @param strategies Todas as implementações do tipo da estratégia definida. Sugestão: injete todas elas
     *                   diretamente via @autowired no construtor das classes filhas.
     * @throws IllegalArgumentException Se um mesmo qualificador se associar a mais de uma estratégia.
     */
    protected StrategiesRecordImpl(final Collection<? extends V> strategies) {
        this.register = doMapping(strategies);
    }

    /**
     * @return Estratégia correspondente ao qualificador informado.
     * @throws IllegalArgumentException Se qualificador não estiver associado a nehuma estratégia.
     */
    @Override
    public final Iterable<V> getStrategies(final K qualifier) {
        return this.register.get(qualifier);
    }

    @Override
    public final Optional<V> getStrategy(final K qualifier) {
        final Iterable<V> strategies = getStrategies(qualifier);
        return isEmpty(strategies) ? Optional.empty() : Optional.of(get(strategies, 0));
    }

    private Map<K, List<V>> doMapping(final Collection<? extends V> strategies) {
        final Map<K, List<V>> mapping = new HashMap<>();
        for (final V strategy : strategies) {
            final Set<K> qualifiers = strategy.getQualifiers();
            for (final K qualifier : qualifiers) {
                final List<V> strategySet;
                if (mapping.containsKey(qualifier)) {
                    strategySet = mapping.get(qualifier);
                } else {
                    strategySet = new ArrayList<>();
                    mapping.put(qualifier, strategySet);
                }
                strategySet.add(strategy);
            }
        }
        final Map<K, List<V>> unmodifiableMapping = new HashMap<>();
        for (final Map.Entry<K, List<V>> entry : mapping.entrySet()) {
            unmodifiableMapping.put(entry.getKey(), Collections.unmodifiableList(entry.getValue()));
        }
        return Collections.unmodifiableMap(unmodifiableMapping);
    }
}
