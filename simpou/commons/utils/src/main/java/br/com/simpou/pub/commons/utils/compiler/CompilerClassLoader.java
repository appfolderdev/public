package br.com.simpou.pub.commons.utils.compiler;

import lombok.AccessLevel;
import lombok.Setter;

/**
 * @author Jonas Pereira
 * @since 2011-08-18
 */
final class CompilerClassLoader extends ClassLoader {

    /**
     * Classe Java compilada.
     */
    @Setter(AccessLevel.PACKAGE)
    private CompilerJavaFileObject compiledObj;

    CompilerClassLoader(final ClassLoader parentClassLoader) {
        super(parentClassLoader);
    }

    @Override
    public Class<?> findClass(final String qualifiedClassName)
            throws ClassNotFoundException {
        final byte[] bytes = compiledObj.getBytes();
        return defineClass(qualifiedClassName, bytes, 0, bytes.length);
    }
}
