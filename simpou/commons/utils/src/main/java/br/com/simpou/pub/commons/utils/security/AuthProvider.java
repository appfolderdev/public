package br.com.simpou.pub.commons.utils.security;

/**
 * Created by jonas on 12/01/2017.
 */
public interface AuthProvider {

    Authenticated authenticate(Authentication authMethod);
}
