package br.com.simpou.pub.commons.utils.reflection;

import java.util.Set;

/**
 * @author jonas1
 * @version 22/09/13
 * @since 22/09/13
 */
//TODO dar um papel melhor e mais amplo pra isso
public interface ClassScanner<T> {

    /**
     * Escaneia no classpath os tipo assoaciados ao escopo.
     *
     * @param typeClass    Classe do tipo assoaciado ao escopo.
     * @param classLoaders Opcional classloaders.
     * @return Tipos encontrados.
     */
    Set<Class<? extends T>> scanTypes(Class<T> typeClass, ClassLoader... classLoaders);
}
