package br.com.simpou.pub.commons.utils.security;

import lombok.Getter;

/**
 * Created by jonas on 12/01/2017.
 */
@Getter
public class Authenticated {

    private final String principal;

    private final boolean authenticated;

    private final String[] roles;

    /**
     * Autorizado.
     *
     * @see #Authenticated(String, String...)
     */
    public Authenticated(final String principal,
                         final String... roles) {
        this(principal, true, roles);
    }

    /**
     * Não autorizado.
     */
    public Authenticated() {
        this(null, false);
    }

    /**
     * @param authenticated True se autenticação foi bem sucedida.
     * @param principal     Identificação do usuário no sistema.
     * @param roles         Permissões de acesso. As repetições são omitidas.
     */
    public Authenticated(final String principal,
                         final boolean authenticated,
                         final String... roles) {
        this.authenticated = authenticated;
        this.principal = principal;
        this.roles = roles.clone();
    }
}
