package br.com.simpou.pub.commons.utils.violation;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Arrays;
import java.util.List;

/**
 * @author Jonas Pereira
 */
@Getter
@NoArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidationValuesWrapper {

    /**
     * Lista de valores.
     */
    @XmlElement(name = "item", type = String.class)
    private List<String> values;

    /**
     * @param array Array.
     */
    public ValidationValuesWrapper(final String... array) {
        this.values = Arrays.asList(array);
    }
}
