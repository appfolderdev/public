package br.com.simpou.pub.commons.utils.behavior;

import java.io.Serializable;

/**
 * Indica que um objeto pode ser identificado unicamente dentro de um conjunto. Usado em operações
 * envolvendo equals e hashcode.
 *
 * @author Jonas Pereira
 * @version 2017-03-03
 * @since 2012-06-11
 */
public interface Identifiable<T extends Serializable> {

    /**
     * @return Identificador único do objeto. Identificadores iguais entre dois objetos significa que o o
     * "equals" entre eles retorna "true" necessariamente.
     */
    T identity();

    /**
     * Atualiza identidade.
     */
    void checkedId(T id);

    void uncheckedId(Serializable id);
}
