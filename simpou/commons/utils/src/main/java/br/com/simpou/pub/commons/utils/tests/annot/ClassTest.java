package br.com.simpou.pub.commons.utils.tests.annot;

import java.lang.annotation.*;

/**
 * Armazena a classe que está sendo testada.
 *
 * @author Jonas Pereira
 * @version 2011-07-13
 * @since 2011-07-13
 */
@Deprecated
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ClassTest {

    /**
     * Ainda não implementado.
     *
     * @return FALSE.
     */
    boolean callSuper() default false;

    //TODO

    /**
     * Classe a ser testada.
     */
    Class<?> value();
}
