package br.com.simpou.pub.commons.utils.lang;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Representa um par chave/valor.
 *
 * @author Jonas Pereira
 * @since 2012-03-13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pair<A, B> implements Serializable {

    private A key;

    private B value;
}
