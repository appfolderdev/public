package br.com.simpou.pub.commons.utils.pattern.strategy;

import java.util.Optional;

public interface StrategiesRecord<K, V extends QualifiedStrategy<K>> {

    Iterable<V> getStrategies(K qualifier);

    Optional<V> getStrategy(K qualifier);
}
