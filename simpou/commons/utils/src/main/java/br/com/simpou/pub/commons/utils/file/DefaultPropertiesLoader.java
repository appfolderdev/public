package br.com.simpou.pub.commons.utils.file;

import java.io.IOException;

/**
 * Carregador de propriedades em aquivo no formato "*.properties".
 *
 * @author Jonas Pereira
 * @since 2013-10-11
 */
public class DefaultPropertiesLoader implements PropertiesLoader {

    private final PropertyHelper helper;

    public DefaultPropertiesLoader(final String filePath) throws IOException {
        this.helper = new PropertyHelper(filePath);
    }

    @Override
    public String get(final String key, final String... params) {
        try {
            return helper.getProperty(key, params);
        } catch (Exception ex) {
            return null;
        }
    }
}
