package br.com.simpou.pub.commons.utils.pattern.composite;

/**
 * Utilitários para trabalhar com componentes compostos. Use para instanciar e compor componentes.
 *
 * @author Jonas Pereira
 * @since 03/03/2017
 */
public class Composites {

    /**
     * @return Componente unário básico.
     */
    public static <P, R> CompositeComponent<P, R> from(final P value) {
        return new UnaryCompositeComponent<>(value);
    }

    /**
     * @return Componente únário composto.
     */
    public static <P, R> CompositeComponent<P, R> from(final CompositeComponent<P, R> component) {
        return new UnaryCompositeComponent<>(component);
    }

    // se public, usuario tende ao erro passando operações do tipo errado
    static <P, R> CompositeComponent<P, R> from(final CompositeComponent<P, R> component,
                                                final CompositeComponent<P, R> operand,
                                                final CompositeOperation<P, R> operation) {
        return new BinaryCompositeComponent<>(component, operand, operation);
    }

    /**
     * Compôe um novo componente composto binário a partir de outros dois.
     */
    public static <P, R> CompositeComponent<P, R> from(final CompositeComponent<P, R> component,
                                                       final CompositeComponent<P, R> operand,
                                                       final BinaryCompositeOperation<P, R> operation) {
        return new BinaryCompositeComponent<>(component, operand, operation);
    }
}
