package br.com.simpou.pub.commons.utils.lang;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Classe utilitária para operações com arrays.
 *
 * @author Jonas Pereira
 * @version 2012-12-06
 * @since 2012-12-06
 */
public class ArraysHelper {

    /**
     * @return HashSet contendo items parametrizados.
     */
    @SafeVarargs
    public static <T> Set<T> asSet(final T... items) {
        return new HashSet<T>(Arrays.asList(items));
    }

    /**
     * Adiciona um novo item no final de um array. É realizada uma cópia completa do array antigo para o
     * novo.
     *
     * @param array   Array.
     * @param newItem Novo item a ser adicionado ao array.
     * @return Novo array.
     */
    public static <T> T[] append(final T[] array, final T newItem) {
        final T[] newArray = Arrays.copyOf(array, array.length + 1);
        newArray[newArray.length - 1] = newItem;

        return newArray;
    }

    /**
     * Concatena dois arrays.
     *
     * @param array1 Primeiro array.
     * @param array2 Array a ser concatenado após o primeiro.
     * @return Primeiro argumento concatenado com o segundo.
     */
    public static <T> T[] concat(final T[] array1, final T[] array2) {
        final T[] newArray;

        if (array1 == null) {
            if (array2 == null) {
                return null;
            } else {
                newArray = Arrays.copyOf(array2, array2.length);
            }
        } else if (array2 == null) {
            newArray = Arrays.copyOf(array1, array1.length);
        } else {
            newArray = Arrays.copyOf(array1, array1.length + array2.length);
            for (int i = array1.length, j = 0; i < newArray.length; i++, j++) {
                newArray[i] = array2[j];
            }
        }
        return newArray;
    }
}
