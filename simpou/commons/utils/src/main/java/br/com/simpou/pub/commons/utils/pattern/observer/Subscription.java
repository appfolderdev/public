package br.com.simpou.pub.commons.utils.pattern.observer;

/**
 * Serviço que disponibiliza algum tipo de notificação para assinantes assim que algum evento ocorra.
 *
 * @author Jonas Pereira
 * @since 01/03/2017
 */
public interface Subscription<T> {

    /**
     * Assina o serviço. O objeto assinante será notificado assim que (e sempre que) novos eventos ocorram.
     *
     * @return Id único da assinatura. Use para remover a subscrição.
     */
    String subscribe(Subscriber<T> subscriber);

    /**
     * Desfaz a assinatura e deixa de receber as notificações.
     *
     * @param subscription Id único da assinatura.
     */
    void unsubscribe(String subscription);
}
