package br.com.simpou.pub.commons.utils.tests;

import br.com.simpou.pub.commons.utils.functional.SilentAction;

/**
 * @author Jonas Pereira
 * @since 26/03/15
 */
public final class CoberturaHelper {

    /**
     * @param <T> Tipo do objeto encapsulador dos recursos.
     * @param <E> Tipo do resultado ao final da execuçao da açao.
     */
    public interface TryWithResourcesAction<T extends AutoCloseable, E> extends SilentAction<T, E> {

        /**
         * @return Objeto encapsulador dos recursos. Necessarios para execuçao da acao e que sao fechados
         * automaticamente ao final da mesma.
         */
        T getResources() throws Exception;
    }

    /**
     * Executa uma expressao do tipo try-with-resources. Util para reduzir necessidade de cobertura de testes.
     * Este tipo de expressao exige muitos testes repetitivos e desnecessarios para se alcançar uma cobertura
     * total.
     *
     * @param <T> Recursos que devem ser fechados automaticamente.
     * @param <E> Tipo do retorno esperado ao final da execuçao.
     */
    public static <T extends AutoCloseable, E> E tryWithResources(final TryWithResourcesAction<T, E> action) {
        try (
                T closeable = action.getResources();
        ) {
            return action.apply(closeable);
        } catch (Exception e) {
            action.doOnError(null, e);
            return null;
        }
    }

}
