package br.com.simpou.pub.commons.utils.validation;

import br.com.simpou.pub.commons.utils.violation.Violation;

/**
 * Indica que um objeto pode ser validado.
 *
 * @author Jonas Pereira
 * @version 2013-05-31
 * @since 2012-07-13
 */
public interface Validatable {

    /**
     * @return null se objeto válido. Se inválido retorna o motivo ou lança unchecked exceptions.
     */
    Violation validate();
}
