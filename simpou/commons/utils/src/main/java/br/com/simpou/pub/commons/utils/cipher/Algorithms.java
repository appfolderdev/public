package br.com.simpou.pub.commons.utils.cipher;

/**
 * @author Jonas Pereira
 * @since 29/05/15
 */
public class Algorithms {

    /**
     * Blowfish.
     */
    public static final String BLOWFISH = "Blowfish";

    /**
     * MD5.
     */
    public static final String MD5 = "MD5";

    /**
     * DES.
     */
    public static final String DES = "DES";

    /**
     * DESede.
     */
    public static final String DESEDE = "DESede";

    /**
     * PBEWithMD5AndDES.
     */
    public static final String PBE_MD5_DES = "PBEWithMD5AndDES";

    /**
     * TripleDES.
     */
    public static final String TRIPLE_DES = "TripleDES";

    /**
     * SHA-256.
     */
    public static final String SHA_256 = "SHA-256";
}
