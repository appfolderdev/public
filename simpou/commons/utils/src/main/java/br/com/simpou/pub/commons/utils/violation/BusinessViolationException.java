package br.com.simpou.pub.commons.utils.violation;

/**
 * Created by Jonas on 26/07/2015.
 */
public class BusinessViolationException extends ViolationsException {

    public BusinessViolationException(final String code, final Object... params) {
        super(new BusinessViolation(code, params));
    }
}
