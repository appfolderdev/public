package br.com.simpou.pub.commons.utils.lang;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Auxilia na manipulação de números.
 *
 * @author Jonas Pereira
 * @version 2012-10-04
 * @since 2011-07-23
 */
public class Numbers {
    private static final DecimalFormat currencyFormatter = new DecimalFormat("0.00");

    /**
     * Compara números.
     *
     * @param n1 Número.
     * @param n2 Número.
     * @return {@code 0 se n1=n2, 1 se n1>n2 e -1 se n1<n2}
     */
    public static <T extends Number> int compare(final T n1, final T n2) {
        final double d1 = n1.doubleValue();
        final double d2 = n2.doubleValue();

        if (d2 < 0 && d1 > 0) {
            // condição que o long pode não suportar a soma e ciclar para
            //um número negativo
            return 1;
        }

        long result = Double.doubleToLongBits(d1) -
                Double.doubleToLongBits(d2);

        if (d1 < 0 && d2 < 0) {
            result *= -1;
        }

        if (result > 0) {
            return 1;
        } else if (result < 0) {
            return -1;
        } else {
            return 0;
        }
    }

    public static String formatCurrency(final BigDecimal bigDecimal) {
        //TODO testar isso bem
        final DecimalFormat df = new DecimalFormat();
        return currencyFormatter.format(bigDecimal.setScale(2, BigDecimal.ROUND_HALF_EVEN)).replace(",", ".");
    }

    /**
     * @param pattern   Padrão de formatação do Decimal.
     * @param separator Separador das casas decimais.
     * @return Conversor de Decimal/String.
     */
    public static DecimalFormat getDecimalFormat(final String pattern,
                                                 final char separator) {
        final DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(separator);

        final DecimalFormat format = new DecimalFormat(pattern);
        format.setParseBigDecimal(true);
        format.setDecimalFormatSymbols(symbols);

        return format;
    }

    /**
     * <p> isEven. </p>
     *
     * @param number a long.
     * @return a boolean.
     */
    public static boolean isEven(final long number) {
        return (Math.abs(number) % 2) > 0;
    }

    public static boolean isNumber(final Object obj) {
        return obj instanceof Number;
    }

    /**
     * <p> isOdd. </p>
     *
     * @param number a long.
     * @return a boolean.
     */
    public static boolean isOdd(final long number) {
        return !isEven(number);
    }

    public static String addLeadingZeros(final long number, int totalLength) {
        return String.format("%0" + totalLength + "d", number);
    }
}
