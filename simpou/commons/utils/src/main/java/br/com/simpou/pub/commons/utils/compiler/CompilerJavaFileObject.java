package br.com.simpou.pub.commons.utils.compiler;

import javax.tools.SimpleJavaFileObject;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Representa um arquivo java: fonte ou compilado.
 *
 * @author Jonas Pereira
 * @since 2011-08-18
 */
final class CompilerJavaFileObject extends SimpleJavaFileObject {

    /**
     * Código fonte.
     */
    private String source;

    /**
     * Código compilado, bytecodes.
     */
    private ByteArrayOutputStream byteCode = new ByteArrayOutputStream();

    /**
     * @param className Nome da classe.
     * @param source    Código fonte.
     */
    CompilerJavaFileObject(final String className, final String source) throws URISyntaxException {
        super(new URI(getSrcFileName(className)), Kind.SOURCE);
        this.source = source;
    }

    /**
     * @param className Nome da classe.
     */
    CompilerJavaFileObject(final String className) throws URISyntaxException {
        super(new URI(className), Kind.SOURCE);
    }

    /**
     * @param className Nome da classe.
     * @return Nome do arquivo fonte com extensão que originou a classe.
     */
    private static String getSrcFileName(final String className) {
        return className + Kind.SOURCE.extension;
    }

    /**
     * @return Bytecodes.
     */
    byte[] getBytes() {
        return byteCode.toByteArray();
    }

    @Override
    public String getCharContent(final boolean ignoreEncodingErrors) {
        return source;
    }

    @Override
    public OutputStream openOutputStream() {
        return byteCode;
    }
}
