package br.com.simpou.pub.commons.utils.pattern.composite;

import java.io.Serializable;

/**
 * @author Jonas Pereira
 * @since 03/03/2017
 */
interface CompositeOperation<P, R> extends Serializable {

}
