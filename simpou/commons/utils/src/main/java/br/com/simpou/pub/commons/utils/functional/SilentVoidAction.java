package br.com.simpou.pub.commons.utils.functional;

/**
 * Created by jonas on 04/02/17.
 */
public abstract class SilentVoidAction<T> implements SilentAction<T, Void> {

    @Override
    public Void apply(final T object) {
        applyDelegate(object);
        return getDefault();
    }

    protected abstract void applyDelegate(final T object);

    @Override
    public Void getDefault() {
        return null;
    }
}
