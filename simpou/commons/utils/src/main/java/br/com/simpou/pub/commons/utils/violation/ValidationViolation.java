package br.com.simpou.pub.commons.utils.violation;

import br.com.simpou.pub.commons.utils.behavior.TypeNamed;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Violação da regra de validação de um campo.
 *
 * @author Jonas Pereira
 * @since 2012-12-28
 */
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(of = {"beanClass"}, callSuper = true)
public class ValidationViolation extends Violation {
    public static final String TYPE = "validation";

    /**
     * Campo afetado pela validação.
     */
    private String field;

    /**
     * Valor inválido.
     */
    private Object invalidValue;

    /**
     * Classe do bean ao qual o campo validado pertence.
     */
    private String beanClass;

    /**
     * @param bean         Bean que sofreu a validação.
     * @param fieldName    Nome do campo do bean.
     * @param invalidValue Valor invalidado.
     * @param code         Código do erro.
     */
    public ValidationViolation(final Object bean, final String fieldName,
                               final Object invalidValue, final String code) {
        this(bean, fieldName, invalidValue, code, new Object[0]);
    }

    /**
     * @param bean          Bean que sofreu a validação.
     * @param fieldName     Nome do campo do bean.
     * @param invalidValue  Valor invalidado.
     * @param code          Código do erro.
     * @param messageParams Parâmetros da mensagem traduzida.
     */
    public ValidationViolation(final Object bean, final String fieldName,
                               final Object invalidValue, final String code,
                               final Object... messageParams) {
        this(fieldName, bean == null ? null : bean.getClass(), invalidValue, code, messageParams);
        if (bean != null && bean instanceof TypeNamed) {
            this.beanClass = ((TypeNamed) bean).typeName();
            super.setType(null);
        }
    }

    /**
     * @param beanClass     Classe do bean que sofreu a validação.
     * @param fieldName     Nome do campo do bean.
     * @param invalidValue  Valor invalidado.
     * @param code          Código do erro.
     * @param messageParams Parâmetros da mensagem traduzida.
     */
    public ValidationViolation(final String fieldName, final Class<?> beanClass,
                               final Object invalidValue, final String code,
                               final Object... messageParams) {
        super(code, null, messageParams);
        this.field = fieldName;
        this.invalidValue = invalidValue;
        super.setType(TYPE);
    }

    @Override
    public int compareTo(final Violation violation) {
        if (violation instanceof ValidationViolation) {
            final ValidationViolation validViolation = (ValidationViolation) violation;
            final String id1 = this.beanClass + getCode();
            final String id2 = validViolation.beanClass + validViolation.getCode();
            return id1.compareTo(id2);
        } else {
            return super.compareTo(violation);
        }
    }

    public void setBeanClasss(final Class<?> beanClass) {
        setBeanClass(beanClass.getName());
    }

    /**
     * Use caso haja mais de uma valor inválido. Um wrapper é usado neste caso.
     *
     * @param invalidValues Valores inválidos.
     */
    public void setInvalidValues(final Object[] invalidValues) {
        if (invalidValues.length > 1) {
            setInvalidValue(new ValidationValuesWrapper());
        } else if (invalidValues.length == 1) {
            setInvalidValue(invalidValues[0]);
        }
    }
}
