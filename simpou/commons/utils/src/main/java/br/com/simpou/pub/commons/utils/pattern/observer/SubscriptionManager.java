package br.com.simpou.pub.commons.utils.pattern.observer;

import lombok.extern.apachecommons.CommonsLog;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Gerenciador de assinaturas.
 *
 * @author Jonas Pereira
 * @since 01/03/2017
 */
@CommonsLog
public final class SubscriptionManager<T> implements Subscription<T> {

    private long idCounter = 0;

    private final Map<String, Subscriber<T>> subscribers = new HashMap<>();

    @Override
    public synchronized String subscribe(final Subscriber<T> subscriber) {
        // parcela única + parcela aleatória não identificável
        final String id = (++idCounter) + UUID.randomUUID().toString();
        subscribers.put(id, subscriber);
        return id;
    }

    @Override
    public synchronized void unsubscribe(final String subscription) {
        if (!subscribers.containsKey(subscription)) {
            throw new IllegalStateException("Subscription not found.");
        }
        subscribers.remove(subscription);
    }

    /**
     * Notifica todos assinantes de forma assíncrona.
     */
    public synchronized void notifyObservers(final T object) {
        for (final Subscriber<T> subscriber : subscribers.values()) {
            new Thread() {
                @Override
                public void run() {
                    //log.debug("Send notification to " + subscriber + "...");
                    try {
                        subscriber.update(object);
                    } catch (Exception e) {
                        log.error("Subscriber " + subscriber + " report an error.", e);
                    }
                    //finally {
                    //log.debug("Notification sent");
                    //}
                }
            }.start();
        }
    }
}
