package br.com.simpou.pub.commons.utils.file;

import lombok.AllArgsConstructor;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Filtro sobre listagem de diretórios. Verifica se os arquivos listados
 * contêm um padrão especificado.
 *
 * @author Jonas Pereira
 * @version 2011-07-12
 * @since 2011-07-12
 */
@AllArgsConstructor
public final class ContainsFileNameFilter implements FilenameFilter {

    /**
     * Padrão a ser verificado.
     */
    private final String txt;

    /**
     * Se a verificação deve ser case sensitive.
     */
    private final boolean matchCase;

    /**
     * Se o nome do arquivo deve bater exatamente com a descrição, ou seja,
     * todos caracteres.
     */
    private final boolean exactly;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean accept(final File dir, final String name) {
        final String txtCompare;
        final String nameCompare;

        if (!matchCase) {
            nameCompare = name.toLowerCase();
            txtCompare = txt.toLowerCase();
        } else {
            nameCompare = name;
            txtCompare = txt;
        }

        if (exactly) {
            return nameCompare.equals(txtCompare);
        } else {
            return nameCompare.contains(txtCompare);
        }
    }
}
