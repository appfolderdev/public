package br.com.simpou.pub.commons.utils.pagination;

import br.com.simpou.pub.commons.utils.lang.Assertions;
import lombok.Getter;

/**
 * Define uma página dentro de um mecanismo de paginação de dados.
 *
 * @author Jonas Pereira
 * @since 2011-07-12
 */
public final class PageLimits {

    @Getter
    private final int offset;

    @Getter
    private final int size;

    /**
     * @param offset Índice do primeiro elemento. Deve ser não negativo.
     * @param size   Quantidade de elementos a partir do primeiro inclusive. Deve ser não negativo.
     */
    public PageLimits(final int offset, final int size) {
        this.offset = Assertions.greaterThan(offset, -1, "Offset must be non negative.");
        this.size = Assertions.greaterThan(size, -1, "Size must be non negative.");
    }

    /**
     * @return Índice do primeiro elemento.
     */
    public int getFirst() {
        return offset;
    }

    /**
     * @return Índice do último elemento.
     */
    public int getLast() {
        return offset + size - 1;
    }
}
