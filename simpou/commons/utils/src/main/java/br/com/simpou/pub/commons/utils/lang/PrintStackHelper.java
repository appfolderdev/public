package br.com.simpou.pub.commons.utils.lang;

import lombok.Getter;
import lombok.extern.apachecommons.CommonsLog;

import java.util.Objects;
import java.util.UUID;

/**
 * Torna possível não logarstack traces em modo de testes.
 *
 * @author Jonas Pereira
 * @since 12/11/2014
 */
@CommonsLog
public final class PrintStackHelper {

    private static String lockHash;

    /**
     * Se true, indica que está em modo de testes, então nenhum log de erro é impresso.
     */
    private static boolean testMode;

    @Getter
    private static Throwable stack;

    public static String setTestMode() {
        if (lockHash == null) {
            PrintStackHelper.testMode = true;
            lockHash = UUID.randomUUID().toString();
        } else {
            throw new IllegalStateException("Locked by other class.");
        }
        return lockHash;
    }

    public static void setProductionMode(final String lockHash) {
        if (Objects.equals(lockHash, PrintStackHelper.lockHash)) {
            testMode = false;
            PrintStackHelper.lockHash = null;
            stack = null;
        } else {
            throw new IllegalStateException("Locked by other class.");
        }
    }

    /**
     * @see #logStackTrace(Object, Throwable)
     */
    public static void logStackTrace(final Throwable throwable) {
        logStackTrace(null, throwable);
    }

    /**
     * Imprime log de erro caso não esteja em modo de testes.
     */
    public static void logStackTrace(final Object msg, final Throwable throwable) {
        stack = throwable;
        if (!testMode) {
//            throwable.printStackTrace();
            log.error(msg, throwable);
        }
    }

}
