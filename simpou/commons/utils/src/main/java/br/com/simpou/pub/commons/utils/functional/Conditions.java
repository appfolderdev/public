package br.com.simpou.pub.commons.utils.functional;

import br.com.simpou.pub.commons.utils.lang.RefHashMap;
import br.com.simpou.pub.commons.utils.lang.RefHashSet;

import java.util.*;

/**
 * Grupo de condições. Verdadeiro se e somente se todas condiçoes componentes forem satisfeitas. Objeto
 * imutável.
 *
 * @author Jonas Pereira
 * @since 2013-06-07
 */
public final class Conditions<T> implements Iterable<Condition<T>>, Condition<T> {

    private final Set<Condition<T>> conditions;

    /**
     * Em caso de alguma condição ser falsa durante checagem, indica qual foi ela.
     */
    private Condition<T> lastFail;

    private Conditions(final Set<Condition<T>> conditions) {
        this.conditions = Collections.unmodifiableSet(conditions);
    }

    /**
     * @return ToString da condição violada, nos casos em que alguma não gere resultados afirmativo.
     */
    public String getLastFail() {
        return lastFail == null ? null : lastFail.toString();
    }

    /**
     * @see #from(java.util.Set)
     */
    public static <T> Conditions<T> from(final List<? extends Condition<T>> conditionList) {
        return from(new HashSet<>(conditionList));
    }

    /**
     * @return Objeto vazio, sem condições.
     */
    public static <T> Conditions<T> empty() {
        return from(Collections.<Condition<T>>emptyList());
    }

    /**
     * @return Instância imutável contendo todas condições não repetidas parametrizadas.
     */
    public static <T> Conditions<T> from(final Set<Condition<T>> conditionList) {
        return new Conditions<>(conditionList);
    }

    /**
     * Faz o merge de todas condições parametrizadas e retorna uma nova instância.
     *
     * @see #from(java.util.List)
     */
    public static <T> Conditions<T> from(final Condition<T> condition, final Conditions<T> conditions) {
        final List<Condition<T>> list = new ArrayList<>(conditions.conditions);
        list.add(condition);
        return from(list);
    }

    @Override
    public Iterator<Condition<T>> iterator() {
        return conditions.iterator();
    }

    /**
     * @param context Contexto a ser validado por todas as condições.
     * @return true se todas condições foram satisfeitas, false se alguma não foi. Use {@link #getLastFail()}
     * para obter informações sobre qual condição foi violada.
     * @throws java.lang.NullPointerException  Se alguma condição retornar null para algum contexto.
     * @throws java.lang.IllegalStateException Se alguma condição lançar alguma condição.
     */
    @Override
    public Boolean apply(final T context) {
        lastFail = null;
        for (final Condition<T> condition : conditions) {
            Boolean applyed;
            try {
                applyed = condition.apply(context);
                if (applyed == null) {
                    throw new NullPointerException(
                            "Condition \"" + condition + "\" returning null for arg \"" + context + "\""
                    );
                } else if (!applyed) {
                    lastFail = condition;
                    return false;
                }
            } catch (Throwable t) {
                throw new IllegalStateException(
                        "Condition \"" + condition + "\" throws an exception while applying context \""
                                + context + "\"",
                        t
                );
            }
        }
        return true;
    }

    /**
     * @return Negativa da condição.
     */
    public static <E> Condition<E> negate(final Condition<E> condition) {
        return new Condition<E>() {
            @Override
            public Boolean apply(final E context) {
                return !condition.apply(context);
            }
        };
    }

    /**
     * @param includeCondition Critério que definirá quais items serão incluídos na lista de retorno.
     * @return Lista com os items cuja execução da condição sobre eles retornam true.
     */
    public static <T> List<T> filter(final Iterable<T> values, final Condition<T> includeCondition) {
        final List<T> list = new ArrayList<T>();
        for (final T t : values) {
            if (includeCondition.apply(t)) {
                list.add(t);
            }
        }
        return list;
    }

    /**
     * Separa um conjunto de objetos em listas. Para cada condição, uma lista é retornada com os objetos que a
     * satisfazem. As condições são diferenciadas por suas referências em memória e não pelo seu
     * equals/hashcode.
     */
    public static <T> RefHashMap<Condition<T>, List<T>> separate(final Iterable<T> values,
                                                                 final Conditions<T> conditions) {
        final RefHashSet<T> notAddeds;
        {
            notAddeds = new RefHashSet<>();
            for (final T value : values) {
                notAddeds.addNew(value);
            }
        }

        final RefHashMap<Condition<T>, List<T>> result;
        {
            result = new RefHashMap<>();
            for (final Condition<T> condition : conditions) {
                final List<T> conditionList;
                if (result.containsRefKey(condition)) {
                    conditionList = result.getByRef(condition);
                } else {
                    conditionList = new ArrayList<>();
                    result.putNew(condition, conditionList);
                }
                for (final T value : values) {
                    if (condition.apply(value)) {
                        conditionList.add(value);
                        notAddeds.removeByRef(value);
                    }
                }
            }
            result.putNew(null, notAddeds.toList());
        }
        return result;
    }
}
