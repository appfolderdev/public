package br.com.simpou.pub.commons.utils.file;

import java.io.File;
import java.io.FilenameFilter;

public class FileNameFilters implements FilenameFilter {

    private final FilenameFilter[] filenameFilters;

    public FileNameFilters(final FilenameFilter... filenameFilter) {
        this.filenameFilters = filenameFilter;
    }

    @Override
    public boolean accept(final File dir, final String name) {
        for (final FilenameFilter filenameFilter : filenameFilters) {
            final boolean accepted = filenameFilter.accept(dir, name);
            if (!accepted) {
                return false;
            }
        }
        return true;
    }

}
