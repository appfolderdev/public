package br.com.simpou.pub.commons.utils.reflection;

import java.lang.reflect.Field;

/**
 * Condições que exige que um campo seja de fato declarado pela classe. O método getDeclaredFields por si só
 * não garante que o campos estejam realmente declarados principalmente em siturações de classes de internas e
 * herança.
 *
 * @author Jonas Pereira
 * @version 2013-06-02
 * @since 2013-06-02
 */
public class DeclaredField implements FieldCondition {

    @Override
    public Boolean apply(final Field field) {
        // essa condição exclui que campos associados a nested classes que não 
        // foram declarados sejam incluídos
        return !field.getName().startsWith("this$");
    }
}
