package br.com.simpou.pub.commons.utils.functional;

import br.com.simpou.pub.commons.utils.lang.Assertions;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by jonas on 04/02/17.
 */
public class Promise<T> {

    private Resolved<T> onResolved;

    @Getter
    private T resolvedObj;

    @Getter
    private boolean resolved;

    @Getter
    private boolean rejected;

    @Getter
    private Throwable lastError;

    /**
     * Use para obter o resultado do promise quando ele terminar de executar. Note que toda execução pode
     * ser assíncrona e não há qualquer previsão de término e/ou sucesso. Se o promise já estiver
     * resolvido, a ação de término é executada imediatamente.
     *
     * @param resolvedAction Ação a ser executada quando o promise for resolvido pelo dono, ou seja, quando as
     *                       operações relacionadas forem finalizadas.
     * @return This.
     * @throws NullPointerException  Se ação for nula.
     * @throws IllegalStateException Se já definida.
     */
    public Promise<T> then(final Resolved<T> resolvedAction) {
        Assertions.notNull(resolvedAction);
        if (this.onResolved != null) {
            throw new IllegalStateException("Then action already defined.");
        }
        this.onResolved = resolvedAction;
        if (this.isResolved()) {
            if (this.lastError == null) {
                onResolved.onSuccess(resolvedObj);
            } else {
                onResolved.onError(lastError);
            }
        }
        return this;
    }

    /**
     * Gera um novo promise. É importante que "dono" deste promise não exponha seu controlador para prevenir
     * que os clientes possam resolvê-lo, quebrando desta forma o encapsulamento. Desta forma o cliente poderá
     * apenas esperar a resolução do promise.
     */
    public static <T> PromiseController<T> value() {
        return new PromiseController<>(new Promise<T>());
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public static class PromiseController<T> {

        @Getter
        private final Promise<T> promise;

        /**
         * Sinaliza que a operação relacionada foi finalizada com sucesso.
         * <p>
         * As operações a serem realizadas
         * pelo cliente após a sinalização de término é executada de forma assíncrona e não interfere em
         * nada no fluxos do promise.
         *
         * @param obj Objeto que o cliente irá receber quando for notificado da resolução.
         * @throws IllegalArgumentException Hashid inválido.
         * @throws IllegalStateException    Promise já resolvido.
         */
        public Promise<T> resolve(final T obj) {
            if (promise.resolved) {
                throw new IllegalStateException("Already resolved.");
            }
            promise.resolvedObj = obj;
            promise.resolved = true;

            if (promise.onResolved != null) {
                new Thread() {
                    @Override
                    public void run() {
                        promise.onResolved.onSuccess(obj);
                    }
                }.start();
            }
            promise.notifyResolution();
            return promise;
        }

        /**
         * Sinaliza que a operação relacionada foi concluída com erro.
         * <p>
         * As operações a serem realizadas
         * pelo cliente após a sinalização de término é executada de forma assíncrona e não interfere em
         * nada no fluxos do promise.
         *
         * @param t Erro ocorrido. Não pode ser null pois é a única maneira do cliente saber o que ocorreu.
         * @throws IllegalArgumentException Hashid inválido.
         * @throws IllegalStateException    Promise já resolvido.
         * @throws NullPointerException     Se exceção nula.
         */
        public Promise<T> reject(final Throwable t) {
            if (promise.rejected) {
                throw new IllegalStateException("Already rejected.");
            }
            Assertions.notNull(t);

            promise.lastError = t;
            promise.rejected = true;

            if (promise.onResolved != null) {
                new Thread() {
                    @Override
                    public void run() {
                        promise.onResolved.onError(t);
                    }
                }.start();
            }
            promise.notifyResolution();

            return promise;
        }

    }

    private void notifyResolution() {
        synchronized (this) {
            notify();
        }
    }

    /**
     * Bloqueia a linha de execução até a resolução do promise. Aguarda também o término da operação then.
     *
     * @param timeout Tempo máximo de espera ou null para esperar indefinidamente.
     */
    public Promise<T> waitResolution(final Long timeout) throws InterruptedException {
        if (!resolved || !rejected) {
            synchronized (this) {
                if (timeout == null) {
                    wait();
                } else {
                    wait(timeout);
                }
            }
        }
        return this;
    }

    /**
     * @see #waitResolution(Long)
     */
    public Promise<T> waitResolution() throws InterruptedException {
        return waitResolution(null);
    }

    /**
     * Ação a ser executada quando o promise reportar término da operação, podendo ser uma rejeição (erro)
     * e/ou uma resolução (sucesso).
     */
    public interface Resolved<T> {

        /**
         * Método invocado no máximo uma vez quando a operação associada for concluída com sucesso.
         */
        void onSuccess(T obj);

        /**
         * Método invocado no máximo uma vez quando a operação associada for concluída com erro ou não for
         * concluída.
         */
        void onError(Throwable t);

    }

}
