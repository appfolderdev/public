package br.com.simpou.pub.commons.utils.string;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Operações úteis sobre Regex.
 *
 * @author Jonas Pereira
 * @since 2013-02-09
 */
public class RegexHelper {

    /**
     * Regex: Números positivos (não inclui zero). Qualquer número de dígitos
     * mas ao menos um deve ser maior que zero.
     */
    public static final String POSITIVE_INT = "(?=\\d*[1-9])\\d+";

    public static final String PHONE = "[+](\\d{1,3})\\((\\d{2,3})\\)(\\d{7,9})";

    public static final String MAC_ADDRESS = "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$";

    public static final String IP = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    public static final String EMAIL = "([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)";

    /**
     * Verifica se um valor segue determinado regex.
     *
     * @param pattern Regex.
     * @param value   Valor a ser validado.
     * @return true se valor ok.
     */
    public static boolean matches(final String pattern, final String value) {
        final Pattern patternObj = Pattern.compile(pattern);
        final Matcher matcher = patternObj.matcher(value);

        return matcher.matches();
    }
}
