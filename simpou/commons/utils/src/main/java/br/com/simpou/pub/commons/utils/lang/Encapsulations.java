package br.com.simpou.pub.commons.utils.lang;

import java.util.Date;

/**
 * @author Jonas Pereira
 * @since 27/03/15
 */
public final class Encapsulations {

    /**
     * @return Clone da data ou null se data null.
     */
    public static Date getDate(final Date date) {
        return Nulls.clone(date);
    }

    /**
     * @param array Array.
     * @return Clone do array ou null se array for null.
     */
//    public static <T> T[] clone(final T[] array) {
//        return (array == null) ? null : array.clone();
//    }

}
