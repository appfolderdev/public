package br.com.simpou.pub.commons.utils.lang;

import java.util.*;

/**
 * Operações úteis sobre enums.
 *
 * @author Jonas Pereira
 * @since 2013-08-27
 */
public class Enums {

    private static class EnumPosComparator<T extends Enum<T>> implements Comparator<T> {

        @Override
        public int compare(final T o1, final T o2) {
            return o1.ordinal() - o2.ordinal();
        }
    }

    /**
     * Ordena enums segundo seus ordinais.
     */
    public static <T extends Enum<T>> List<T> sort(final EnumSet<T> enumSet) {
        final Object[] toArray = enumSet.toArray();
        final List<T> sorted = Casts.simpleCast(Arrays.asList(toArray));
        Collections.sort(sorted, new EnumPosComparator<T>());
        return sorted;
    }

    public static <T> T getByOrdinal(final Class<T> clasz, final int ordinal) {
        final Enum[] enums = (Enum[]) clasz.getEnumConstants();
        return Casts.simpleCast(enums[ordinal]);
    }

    /**
     * @param ints       Posições de items de um enum.
     * @param enumClass  Classe do enum.
     * @param enumValues Todos os items do enum. Use método values() enum desejado.
     * @param <T>        Tipo do enum.
     * @return Conjunto com os enums das posições correspondentes parametrizadas. Posições inválidas são
     * desconsideradas.
     */
    public static <T extends Enum<T>> EnumSet<T> intsToEnumSet(final int[] ints,
                                                               final Class<T> enumClass,
                                                               final T[] enumValues) {
        final EnumSet<T> result;
        if (ints == null) {
            result = EnumSet.allOf(enumClass);
        } else {
            result = EnumSet.noneOf(enumClass);
            for (final int pos : ints) {
                if (pos < 0 || pos >= enumValues.length) {
                    continue;
                }
                result.add(enumValues[pos]);
            }
        }
        return result;
    }

    public static <T extends Enum<T>> Set<String> enumToNameSet(final Class<T> enumClass) {
        final EnumSet<T> enumSet = EnumSet.allOf(enumClass);
        final Set<String> nameSet = new HashSet<>(enumSet.size());
        for (final T value : enumSet) {
            nameSet.add(value.name());
        }
        return nameSet;
    }
}
