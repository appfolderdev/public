package br.com.simpou.pub.commons.utils.pattern.strategy;

import java.util.Set;

/**
 * Define uma forma para escolha de estratégias (implementações) de um determinado tipo baseada em
 * qualificadores em tempo de execução. Pode ser usada, por exemplo, se uma interface tem duas ou mais
 * implementações e a escolha da implementação a ser utilizada é definida por uma string, ou qualquer outro
 * tipo, cujo valor só está disponível no exato momento de uso, desta forma não se conhece qual delas injetar
 * via autowired no startup, logo esta interface pode ser uma estratégia qualificada.
 *
 * @param <T> Tipo do qualificador da estratégia.
 * @see StrategiesRecordImpl Gerenciador de estratégias.
 */
public interface QualifiedStrategy<T> {

    /**
     * @return Qualificadores a serem associados à estratégia. Sempre que for necesssário instanciar qualquer
     * implementação do tipo da estratégia, um destes qualificadores deve ser usado para determinar qual delas
     * será usada.
     */
    Set<T> getQualifiers();

}
