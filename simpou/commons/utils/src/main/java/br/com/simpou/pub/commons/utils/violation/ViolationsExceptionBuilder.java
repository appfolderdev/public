package br.com.simpou.pub.commons.utils.violation;

import br.com.simpou.pub.commons.utils.lang.Checks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jonas on 31/01/17.
 */
public class ViolationsExceptionBuilder {

    private List<Violation> violations = new ArrayList<>();

    public ViolationsExceptionBuilder add(final Violation violation) {
        violations.add(violation);
        return this;
    }

    public ViolationsExceptionBuilder addAll(final List<Violation> violation) {
        violations.addAll(violation);
        return this;
    }

    public ViolationsExceptionBuilder add(final Violation... violations) {
        return addAll(Arrays.asList(violations));
    }

    /**
     * @throws ViolationsException se houver alguma violação adicionada.
     */
    public void throwViolations() {
        if (Checks.isNotEmpty(violations)) {
            throw new ViolationsException(violations);
        }
    }

}
