package br.com.simpou.pub.commons.utils.security;

public class AccessDeniedException extends RuntimeException {

    public AccessDeniedException() {
        super();
    }

    public AccessDeniedException(final String message) {
        super(message);
    }
}

