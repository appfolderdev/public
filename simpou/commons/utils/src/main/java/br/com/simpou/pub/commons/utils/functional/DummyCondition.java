package br.com.simpou.pub.commons.utils.functional;

/**
 * Created by jonas on 31/01/2017.
 */
public class DummyCondition<T> implements Condition<T> {

    @Override
    public Boolean apply(final T object) {
        return true;
    }
}
