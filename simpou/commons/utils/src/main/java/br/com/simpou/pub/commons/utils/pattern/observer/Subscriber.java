package br.com.simpou.pub.commons.utils.pattern.observer;

/**
 * Objeto que assina algum serviço para receber notificações sobre a ocorrência de algum evento.
 *
 * @author Jonas Pereira
 * @since 01/03/2017
 */
public interface Subscriber<T> {

    /**
     * Ponto de entrada para o tratamente das notificações recebidas do serviço assinado.
     */
    void update(T object);
}
