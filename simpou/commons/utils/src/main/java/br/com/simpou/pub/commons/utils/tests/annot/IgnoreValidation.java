package br.com.simpou.pub.commons.utils.tests.annot;

import java.lang.annotation.*;

/**
 * Define que as validações devem ser ignoradas.
 *
 * @author Jonas Pereira
 * @version 2011-07-13
 * @since 2011-07-13
 */
@Deprecated
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface IgnoreValidation {

}
