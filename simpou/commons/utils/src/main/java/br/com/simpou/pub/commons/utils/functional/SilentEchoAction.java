package br.com.simpou.pub.commons.utils.functional;

/**
 * Não realiza nenhuma ação, apenas ecoa a entrada para a saída.
 *
 * @author Jonas Pereira
 * @since 2013-06-27
 */
public class SilentEchoAction<T> extends EchoAction<T> implements SilentAction<T, T> {

    @Override
    public void doOnError(final T object, final Throwable e) {
        // nunca entra aqui, apply sempre retorna o valor de entrada
    }

    @Override
    public T getDefault() {
        // nunca entra aqui
        return null;
    }
}
