<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text"/>

    <xsl:template match="/root/subroot/subsubroot">
        <xsl:apply-templates select="element"/>
    </xsl:template>

    <xsl:template match="element">
        <xsl:value-of select="."/>

    </xsl:template>

</xsl:stylesheet>
