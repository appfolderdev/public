package br.com.simpou.pub.commons.utils.reflection;

import br.com.simpou.pub.commons.utils.TestsConstants;
import br.com.simpou.pub.commons.utils.file.FileHelper;
import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.junit.Test;

import javax.xml.bind.annotation.XmlAttribute;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@ClassTest(Reflections.class)
public class ReflectionsTest {

    @Test
    public void testGetClassPath() {
        final Class<Reflections> clasz = Reflections.class;
        final String relPath = clasz.getName()
                .replaceAll("\\.",
                        FileHelper.getDelimPattern()) + ".class";
        final String baseDir = TestsConstants.CLASSES_DIR;

        final String path = Reflections.getClassPath(baseDir, clasz);
        //assertTrue(FileHelper.exists(path));
        assertEquals(baseDir + relPath, path);
    }

    @Test
    public void testGetField() {
        //TODO
    }

    @Test
    public void testGetFieldType() throws Exception {
        final Field genericField = Reflections.getField(SerializableClass.class, "field");
        assertEquals(Long.class, Reflections.getFieldType(SerializableClass.class, genericField));
        assertEquals(Long.class, Reflections.getFieldType(SubSerializableClass.class, genericField));
        final Field nonGenericField = Reflections.getField(SerializableClass.class, "notGeneric");
        assertEquals(Integer.class, Reflections.getFieldType(SerializableClass.class, nonGenericField));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGetFieldType_unsupported() throws Exception {
        assertEquals(Long.class, Reflections.getFieldType(GenericSerializableClass.class, Reflections.getField(GenericSerializableClass.class, "field")));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGetFieldType_unsupported2() throws Exception {
        assertEquals(Long.class, Reflections.getFieldType(MultSerializableClass.class, Reflections.getField(GenericSerializableClass.class, "field")));
    }

    @Test
    public void testGetFields_Class_Conditions() {
        //testGetFields_Class_FieldConditionArr
    }

    @Test
    public void testGetFields_Class_FieldConditionArr() {
        final FieldCondition basicFieldCond = new BasicField();
        final FieldCondition annotFieldCond = new AnnotatedField(XmlAttribute.class);
        final List<Field> fields = Reflections.getFields(MoreFields.class);
        for (final Field field : fields) {
            System.out.println("******" + field);
        }
        assertEquals(7, fields.size());
        assertEquals(5, Reflections.getFields(MoreFields.class, basicFieldCond).size());
        assertEquals(2, Reflections.getFields(MoreFields.class, basicFieldCond, annotFieldCond).size());
        assertEquals(3, Reflections.getFields(MoreFields.class, annotFieldCond).size());
    }

    @Test
    public void testGetGetterMethod() throws Exception {
        assertNotNull(Reflections.getGetterMethod(SomeFields.class.getDeclaredField("f2")));
    }

    @Test
    public void testGetInterfaces() {
        final List<Class<?>> interfaces = Reflections.getInterfaces(SubSubIface.class);
        assertEquals(3, interfaces.size());
        assertTrue(interfaces.contains(OtherIface.class));
        assertTrue(interfaces.contains(SuperIface.class));
        assertTrue(interfaces.contains(SubIface.class));
    }

    @Test
    public void testGetReturnTypes()
            throws ClassNotFoundException, NoSuchMethodException {
        Method method;
        List<String> returnTypesString;

        method = CustomTypedClass.class.getDeclaredMethod("testReturnType");
        returnTypesString = Reflections.getReturnTypes(method);
        assertEquals(Long.class.getCanonicalName(), returnTypesString.get(0));
        assertEquals(String.class.getCanonicalName(), returnTypesString.get(1));

        method = CustomTypedClass.class.getDeclaredMethod("testReturnTypeNull");
        returnTypesString = Reflections.getReturnTypes(method);
        assertNull(returnTypesString);
    }

    @Test
    public void testGetSignature() throws NoSuchMethodException {
        final Class[] params;
        Method method;
        String signature;
        String expected;

        params = new Class<?>[]{
                Object.class, String.class, Reflections.class
        };

        expected = "public final java.util.List<"
                + ReflectionsTest.class.getName()
                + "> methodSignature(java.lang.Object p1, java.lang.String p2, "
                + Reflections.class.getName() + " p3)";
        method = CustomTypedClass.class.getDeclaredMethod("methodSignature",
                params);
        signature = Reflections.getSignature(method, true);
        assertEquals(expected, signature);

        expected = "public abstract java.util.List<T> methodSignature(java.lang.Object p1, java.lang.String p2, "
                + Reflections.class.getName()
                + " p3) throws java.lang.Exception";
        method = TypedClass.class.getDeclaredMethod("methodSignature", params);
        signature = Reflections.getSignature(method, true);
        assertEquals(expected, signature);

        expected = "java.util.List<" + Reflections.class.getName()
                + "> methodSignature(java.lang.Object p1, java.lang.String p2, "
                + Reflections.class.getName()
                + " p3) throws java.lang.UnsupportedOperationException, java.lang.Exception";
        method = CustomTypedClass2.class.getDeclaredMethod("methodSignature",
                params);
        signature = Reflections.getSignature(method, false);
        assertEquals(expected, signature);

        expected = ReflectionsTest.class.getName()
                + " testSignatureClass()";
        method = CustomTypedClass.class.getDeclaredMethod("testSignatureClass");
        signature = Reflections.getSignature(method, false);
        assertEquals(expected, signature);
    }

    @Test
    public void testGetSrcPath() {
        final Class<Reflections> clasz = Reflections.class;
        final String relPath = clasz.getName()
                .replaceAll("\\.",
                        FileHelper.getDelimPattern()) + ".java";
        final String baseDir = TestsConstants.SRC_DIR;

        final String path = Reflections.getSrcPath(baseDir, clasz);
        //        assertTrue(FileHelper.exists(path));
        assertEquals(baseDir + relPath, path);
    }

    @Test
    public void testGetValue()
            throws NoSuchFieldException, IllegalAccessException {
        final SimpleClass obj = new SimpleClass();
        obj.values = new ArrayList<>();
        obj.values.add(Randoms.getString());
        obj.values.add(Randoms.getString());

        final List<String> values = Reflections.getValue(obj, "values");
        assertEquals(2, values.size());
    }

    @Test
    public void testIsClassGeneric() {
        boolean generic;

        generic = Reflections.isClassGeneric(TypedClass.class);
        assertTrue(generic);

        generic = Reflections.isClassGeneric(CustomTypedClass.class);
        assertFalse(generic);

        generic = Reflections.isClassGeneric(SimpleClass.class);
        assertFalse(generic);
    }

    @Test
    public void testIsDeclared() throws NoSuchMethodException {
        assertTrue(Reflections.isDeclared(
                ReflectionsTest.CustomTypedClass.class,
                ReflectionsTest.CustomTypedClass.class.getMethod(
                        "testReturnType"),
                ReflectionsTest.CustomTypedClass.class.getMethod(
                        "testSignatureClass")));
        assertFalse(Reflections.isDeclared(
                ReflectionsTest.CustomTypedClass.class,
                ReflectionsTest.CustomTypedClass.class.getMethod(
                        "equals", Object.class)));
    }

    @Test
    public void testIsPojoField() throws NoSuchFieldException {
        assertTrue(Reflections.isPojoField(SomeFields.class.getDeclaredField("f3")));
        assertFalse(Reflections.isPojoField(SomeFields.class.getDeclaredField("f2")));
        assertFalse(Reflections.isPojoField(SomeFields.class.getDeclaredField("f5")));
    }

    @Test
    public void testNewCollectionInstance() {
        //TODO
    }

    @Test
    public void testNewInstance_Class() {
        final ReflectionsTest newInstance = Reflections.newInstance(this.getClass());
        assertNotNull(newInstance);
    }

    @Test
    public void testNewInstance_Class_String() {
        //testNewInstance_Class
    }

    @Test(expected = RuntimeException.class)
    public void testNewInstance_error() {
        Reflections.newInstance(SomeFields.class);
    }

    @Test
    public void testSearchClass() {
        final Class<?>[] array = SubSubIface.class.getInterfaces();
        Class<?> found;

        found = Reflections.searchClass(array, SubIface.class);
        assertNotNull(found);

        found = Reflections.searchClass(array, SuperIface.class);
        assertNull(found);
    }

    interface SuperIface {

    }

    interface OtherIface {

    }

    interface SubIface extends SuperIface {

    }

    interface SubSubIface extends SubIface, OtherIface {

    }

    /*
     * Classes auxiliares de teste.
     */
    class CustomTypedClass extends TypedClass<ReflectionsTest> {

        @Override
        public final List<ReflectionsTest> methodSignature(
                final Object obj, final String arg, final Reflections helper) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Map<Long, String> testReturnType() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String testReturnTypeNull() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ReflectionsTest testSignatureClass() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    private class CustomTypedClass2 extends TypedClass<Reflections> {

        @Override
        public List<Reflections> methodSignature(final Object obj, final String arg,
                                                 final Reflections helper)
                throws UnsupportedOperationException, Exception {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    abstract class TypedClass<T> {

        public abstract List<T> methodSignature(Object obj, String arg,
                                                Reflections helper) throws Exception;
    }

    abstract class NotAcessible {

        protected void method2() {
        }

        abstract void method3();

        private void method1() {
        }
    }

    private class SubSerializableClass extends SerializableClass<Integer, String> {

    }

    private class SerializableClass<E, F> extends GenericSerializableClass<E, F, Long> {

    }

    private class MultSerializableClass<E> extends GenericSerializableClass<String, E, Long> {

    }

    private class GenericSerializableClass<E, F, T> {

        T field;

        Integer notGeneric;
    }

    private class SomeFields {

        @Getter
        @Setter
        public String f5;

        ReflectionsTest f4;

        @Setter
        private int f1;

        @Getter
        private Date f2;

        @Getter
        @Setter
        @XmlAttribute
        private String f3;

    }

    private class MoreFields extends SomeFields {

        @XmlAttribute
        Double f6;

        @XmlAttribute
        SomeFields f7;

    }

    @Data
    class SimpleClass {

        List<String> values;

    }
}
