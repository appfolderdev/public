package br.com.simpou.pub.commons.utils.reflection;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.IgnoreValidation;
import lombok.Getter;
import lombok.Setter;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@Ignore
@ClassTest(Annotations.class)
public class AnnotationsTest {

    @Test
    public void testContainsAnnotation() throws NoSuchMethodException {
        final Class<AnnotationsTest.AnnotatedClass> clasz = AnnotationsTest.AnnotatedClass.class;
        final List<Annotation> list1 = Arrays.asList(clasz.getMethod("method1")
                .getAnnotations());
        final List<Annotation> list2 = Arrays.asList(clasz.getMethod("method2")
                .getAnnotations());
        final AnnotationsTest.AnnotatedClass.Iface4 annot = clasz.getMethod(
                "method2")
                .getAnnotation(AnnotationsTest.AnnotatedClass.Iface4.class);
        assertFalse(Annotations.containsAnnotation(list1, annot));
        assertTrue(Annotations.containsAnnotation(list2, annot));
    }

    @Test
    public void testGetPojoFieldAnnotation() throws NoSuchFieldException {
        assertNotNull(Annotations.getPojoFieldAnnotation(PojoClass1.class.getDeclaredField("field"), AnnotatedClass.Iface1.class, false));
        assertNotNull(Annotations.getPojoFieldAnnotation(PojoClass2.class.getDeclaredField("field"), AnnotatedClass.Iface1.class, false));
        assertEquals("field", Annotations.getPojoFieldAnnotation(PojoClass3.class.getDeclaredField("field"), AnnotatedClass.Iface1.class, true).value());
        assertEquals("prop", Annotations.getPojoFieldAnnotation(PojoClass3.class.getDeclaredField("field"), AnnotatedClass.Iface1.class, false).value());
        assertNull(Annotations.getPojoFieldAnnotation(PojoClass4.class.getDeclaredField("field"), AnnotatedClass.Iface1.class, false));
    }

    @Test
    public void testIsAnnotatedWith_Class_Class() {
        //testIsAnnotatedWith_Class_List
    }

    @Test
    public void testIsAnnotatedWith_Class_List() {
        List<Class<? extends Annotation>> notDefineds;
        final List<Class<? extends Annotation>> annotations = new ArrayList<>();
        annotations.add(RunWith.class);
        annotations.add(ClassTest.class);
        notDefineds = Annotations.isAnnotatedWith(this.getClass(),
                annotations);
        assertTrue(notDefineds.isEmpty());
        annotations.add(IgnoreValidation.class);
        notDefineds = Annotations.isAnnotatedWith(this.getClass(),
                annotations);
        assertEquals(1, notDefineds.size());
        assertTrue(notDefineds.contains(IgnoreValidation.class));
    }

    @Test
    public void testIsAnnotatedWith_Method_Class() throws NoSuchMethodException {
        final Method method = AnnotationsTest.class.getDeclaredMethod(
                "testIsAnnotatedWith_Method_Class");
        assertTrue(Annotations.isAnnotatedWith(method, Test.class));
    }

    @Test
    public void testIsFieldAccessPriority() {
        //TODO
    }

    @Test
    public void testIsSameAnnotation() throws NoSuchMethodException {
        final Class<AnnotationsTest.AnnotatedClass> clasz = AnnotationsTest.AnnotatedClass.class;
        final AnnotationsTest.AnnotatedClass.Iface4 annot1 = clasz.getMethod(
                "method4")
                .getAnnotation(AnnotationsTest.AnnotatedClass.Iface4.class);
        final AnnotationsTest.AnnotatedClass.Iface4 annot2 = clasz.getMethod(
                "method2")
                .getAnnotation(AnnotationsTest.AnnotatedClass.Iface4.class);
        final AnnotationsTest.AnnotatedClass.Iface4 annot3 = clasz.getMethod(
                "method3")
                .getAnnotation(AnnotationsTest.AnnotatedClass.Iface4.class);
        assertTrue(Annotations.isSameAnnotation(annot1, annot2));
        assertTrue(Annotations.isSameAnnotation(annot2, annot3));
        assertTrue(annot1.equals(annot2));
        assertFalse(annot2.equals(annot3));
    }

    @Test
    public void testMatchOne() throws NoSuchMethodException {
        final List<Annotation> list1;
        List<Annotation> list2;

        final Class<AnnotationsTest.AnnotatedClass> clasz = AnnotationsTest.AnnotatedClass.class;

        list1 = Arrays.asList(clasz.getMethod("method1").getAnnotations());
        list2 = Arrays.asList(clasz.getMethod("method2").getAnnotations());
        assertTrue(Annotations.matchOne(list1, list2));

        list2 = Arrays.asList(clasz.getMethod("method3").getAnnotations());
        assertFalse(Annotations.matchOne(list1, list2));
    }

    interface AnnotatedClass {

        @Iface1
        @Iface2
        public void method1();

        @Iface1
        @Iface4(value = 1)
        public void method2();

        @Iface3
        @Iface4(value = 2)
        public void method3();

        @Iface4(value = 1)
        public void method4();

        @Retention(RetentionPolicy.RUNTIME)
        @interface Iface1 {

            String value() default "";
        }

        @Retention(RetentionPolicy.RUNTIME)
        @interface Iface2 {

        }

        @Retention(RetentionPolicy.RUNTIME)
        @interface Iface3 {

        }

        @Retention(RetentionPolicy.RUNTIME)
        @interface Iface4 {

            int value();
        }
    }

    class PojoClass1 {

        @Getter
        @Setter
        @AnnotatedClass.Iface1
        private int field;

    }

    class PojoClass2 {

        @Setter
        private int field;

        @AnnotatedClass.Iface1
        public int getField() {
            return this.field;
        }
    }

    class PojoClass3 {

        @Setter
        @AnnotatedClass.Iface1(value = "field")
        private int field;

        @AnnotatedClass.Iface1(value = "prop")
        public int getField() {
            return this.field;
        }
    }

    class PojoClass4 {

        @Getter
        @Setter
        private int field;
    }
}
