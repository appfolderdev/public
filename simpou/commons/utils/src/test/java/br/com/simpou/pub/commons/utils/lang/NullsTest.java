package br.com.simpou.pub.commons.utils.lang;

import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import lombok.AllArgsConstructor;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

@ClassTest(Nulls.class)
public class NullsTest {

    @Test
    public void testAfter() {
        final Date now = new Date();
        final Date future = Randoms.getDate(true);

        assertTrue(Nulls.after(future, now, true));
        assertFalse(Nulls.after(now, future, false));

        assertTrue(Nulls.after(future, null, true));
        assertFalse(Nulls.after(now, null, false));
    }

    @Test(expected = NullPointerException.class)
    public void testAfter_error() {
        assertTrue(Nulls.after(null, Randoms.getDate(), true));
    }

    @Test
    public void testBefore() {
        final Date now = new Date();
        final Date future = Randoms.getDate(true);

        assertFalse(Nulls.before(future, now, true));
        assertTrue(Nulls.before(now, future, false));

        assertTrue(Nulls.before(future, null, true));
        assertFalse(Nulls.before(now, null, false));
    }

    @Test(expected = NullPointerException.class)
    public void testBefore_error() {
        assertTrue(Nulls.before(null, Randoms.getDate(), true));
    }

    @Test
    public void testChoose() {
        final String value = "value";
        final String other = "other";
        assertEquals(value, Nulls.choose(value, other));
        assertEquals(other, Nulls.choose(null, other));
    }

    @Test
    public void testClone_Date() {
        Date date;
        date = null;
        assertNull(Nulls.clone(date));
        date = new Date();
        final Date clone = Nulls.clone(date);
        assertEquals(date, clone);
        assertNotSame(date, clone);
    }

    @Test
    public void testClone_GenericType() {
        String[] array = null;
        assertNull(Nulls.clone(array));

        array = new String[]{"p1", "p2"};
        final String[] clone = Nulls.clone(array);
        assertTrue(Arrays.equals(array, clone));
        assertNotSame(array, clone);
    }

    @Test
    public void testEntrySet() {
        final Map<String, Integer> map = new HashMap<>();
        map.put("v1", 2);
        final Map<String, Integer> nullMap = new HashMap<>();
        assertEquals(1, Nulls.entrySet(map).size());
        assertEquals(0, Nulls.entrySet(nullMap).size());
    }

    @Test
    public void testEquals() {
        final EqualsAndHashcode obj1 = new EqualsAndHashcode(2);
        final EqualsAndHashcode obj2 = new EqualsAndHashcode(3);
        final EqualsAndHashcode obj3 = new EqualsAndHashcode(2);
        assertFalse(Nulls.equals(obj1, obj2));
        assertTrue(Nulls.equals(obj1, obj3));
        assertTrue(Nulls.equals(null, null));
        assertFalse(Nulls.equals(null, obj3));
    }

    @Test
    public void testGetTime() {
        assertNull(Nulls.getTime(null));
        assertNotNull(Nulls.getTime(new Date()));
    }

    @Test
    public void testGet_GenericType_int() {
        final String[] array = {"s1", "s2"};
        assertNull(Nulls.get(array, 2));
        assertNull(Nulls.get(array, -1));
        assertEquals("s2", Nulls.get(array, 1));
    }

    @Test
    public void testGet_List_int() {
        final List<String> list = Arrays.asList("s1", "s2");
        assertNull(Nulls.get(list, 2));
        assertNull(Nulls.get(list, -1));
        assertEquals("s2", Nulls.get(list, 1));
    }

    //    @Test
    //    public void testExecute() {
    //        final Action<Long, String> action = Actions.asSilentIgnoreErrors(new Action<Long, String>() {
    //            @Override
    //            public String apply(Long object) {
    //                return "ok:"+object;
    //            }
    //        });
    //        assertEquals("ok:7", Nulls.execute(7L, "isnull", action));
    //        assertEquals("isnull", Nulls.execute(null, "isnull", action));
    //    }

    @Test
    public void testGet_list() {
        final List<String> list = Arrays.asList("p1", "p2");
        final List<String> nullList = null;
        assertSame(list, Nulls.get(list));
        assertEquals(0, Nulls.get(nullList).size());
    }

    @Test
    public void testGet_set() {
        final Set<String> set = ArraysHelper.asSet("p1", "p2");
        final Set<String> nullSet = null;
        assertSame(set, Nulls.get(set));
        assertEquals(0, Nulls.get(nullSet).size());
    }

    @Test
    public void testHashCode() {
        final EqualsAndHashcode obj = new EqualsAndHashcode(2);
        assertEquals(EqualsAndHashcode.hashcode, Nulls.hashCode(obj));
        assertEquals(0, Nulls.hashCode(null));
    }

    @Test
    public void testVerify() throws Exception {
        Assert.assertTrue(Nulls.anyNull(this, null, this));
        Assert.assertFalse(Nulls.anyNull(this, this));
    }

    @AllArgsConstructor
    class EqualsAndHashcode {

        static final int hashcode = 3;

        private final int field;

        @Override
        public boolean equals(final Object obj) {
            if (obj == null) {
                return false;
            }

            if (getClass() != obj.getClass()) {
                return false;
            }

            final EqualsAndHashcode other = (EqualsAndHashcode) obj;

            if (this.field != other.field) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            return hashcode;
        }
    }

}
