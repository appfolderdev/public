package br.com.simpou.pub.commons.utils.string;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Test;

import static br.com.simpou.pub.commons.utils.string.RegexHelper.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@ClassTest(RegexHelper.class)

public class RegexHelperTest {

    @Test
    public void testMatches_phone() {
        assertTrue(matches(PHONE, "+1(123)1234567"));
        assertTrue(matches(PHONE, "+55(31)912345678"));
    }

    @Test
    public void testMatches_positiveInt() {
        assertFalse(matches(POSITIVE_INT, "0"));
        assertFalse(matches(POSITIVE_INT, "00"));
        assertFalse(matches(POSITIVE_INT, "-0"));
        assertFalse(matches(POSITIVE_INT, "a"));
        assertFalse(matches(POSITIVE_INT, "ab"));
        assertFalse(matches(POSITIVE_INT, "1a"));
        assertFalse(matches(POSITIVE_INT, "a1"));
        assertFalse(matches(POSITIVE_INT, "-1a"));
        assertTrue(matches(POSITIVE_INT, "1"));
        assertTrue(matches(POSITIVE_INT, "0001"));
        assertTrue(matches(POSITIVE_INT, "0102030"));
    }

    @Test
    public void testMatches_url() {
        assertFalse(matches(IP, "aaa"));
        assertFalse(matches(IP, "1"));
        assertFalse(matches(IP, "1.1"));
        assertFalse(matches(IP, "1.1.1"));
        assertTrue(matches(IP, "1.1.1.1"));
    }
}
