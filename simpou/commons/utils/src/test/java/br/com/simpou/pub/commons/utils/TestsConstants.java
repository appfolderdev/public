package br.com.simpou.pub.commons.utils;

import br.com.simpou.pub.commons.utils.file.PropertyHelper;

import java.io.File;

/**
 * Parâmetros de configuração do projeto.
 *
 * @author <a href="mailto:jonasalvesap@gmail.com">Jonas Pereira</a>
 * @version 2012-05-12
 * @since 2011-06-23
 */
public interface TestsConstants {

    /**
     * Diretório base de trabalho.
     */
    String BASE_WORK_DIR = PropertyHelper.getMvnPrjDir();

    /**
     * Diretório dos recursos do sistema.
     */
    String RESOURCES_DIR = BASE_WORK_DIR + "src/main/resources/";

    /**
     * Diretório dos recursos do sistema.
     */
    String RESOURCES_TEST_DIR = BASE_WORK_DIR + "src" + File.separator +
            "test" + File.separator + "resources" + File.separator;

    /**
     * Diretório das classes do sistema.
     */
    String CLASSES_DIR = BASE_WORK_DIR + "target" + File.separator + "classes" +
            File.separator;

    /**
     * Diretório das classes de teste do sistema.
     */
    String TEST_CLASSES_DIR = BASE_WORK_DIR + "target/test-classes/";

    /**
     * Diretório das classes do sistema.
     */
    String SRC_DIR = BASE_WORK_DIR + "src" + File.separator + "main" +
            File.separator + "java" + File.separator;

    /**
     * Prefixo que deve ser adicionado às chaves dos arquivos de bundle
     * correspondentes a enums.
     */
    String ENUM_BUNDLE_KEY_PREFIX = "enum.";

    /**
     * Diretório base de configurações.
     */
    String BASE_CONFS_DIR = PropertyHelper.getHomeDir() + ".simpou/";
}
