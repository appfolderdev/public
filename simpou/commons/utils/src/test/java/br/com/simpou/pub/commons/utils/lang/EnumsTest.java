package br.com.simpou.pub.commons.utils.lang;

import org.junit.Before;
import org.junit.Test;

import java.util.EnumSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by jonas on 08/03/2016.
 */
public class EnumsTest {

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testSort() throws Exception {

    }

    @Test
    public void testGetByOrdinal() throws Exception {

    }

    @Test
    public void testIntsToEnumSet() throws Exception {
        // todos ok
        {
            final EnumSet<TestEnum> set = Enums.intsToEnumSet(
                    new int[]{0, 1},
                    TestEnum.class,
                    TestEnum.values()
            );
            assertEquals(2, set.size());
            assertTrue(set.contains(TestEnum.V1));
            assertTrue(set.contains(TestEnum.V2));
        }
        // algum não ok
        {
            final EnumSet<TestEnum> set = Enums.intsToEnumSet(
                    new int[]{2, 3, 4, -1},
                    TestEnum.class,
                    TestEnum.values()
            );
            assertEquals(1, set.size());
            assertTrue(set.contains(TestEnum.V3));
        }
        // todos
        {
            final EnumSet<TestEnum> set = new Enums().intsToEnumSet(
                    null,
                    TestEnum.class,
                    TestEnum.values()
            );
            assertEquals(3, set.size());
        }
    }

    enum TestEnum {
        V1, V2, V3;
    }
}
