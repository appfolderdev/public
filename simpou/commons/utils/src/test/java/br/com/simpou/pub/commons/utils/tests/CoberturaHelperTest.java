package br.com.simpou.pub.commons.utils.tests;

import br.com.simpou.pub.commons.utils.lang.Casts;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@ClassTest(CoberturaHelper.class)
public class CoberturaHelperTest {

    @Test
    public void testTryWithResources() throws Exception {
        final String expResult = "hello";
        final AutoCloseable closeable = mock(AutoCloseable.class);
        final Class<CoberturaHelper.TryWithResourcesAction<AutoCloseable, String>> genClass =
                Casts.simpleCast(CoberturaHelper.TryWithResourcesAction.class);
        final CoberturaHelper.TryWithResourcesAction<AutoCloseable, String> action = mock(genClass);
        when(action.apply(closeable)).thenReturn(expResult);
        when(action.getResources()).thenReturn(closeable);
        final String result = CoberturaHelper.tryWithResources(action);
        verify(closeable).close();//fecha recursos
        assertEquals(expResult, result);

        // exceçao no meio da execuçao
        final String errMsg = "test";
        when(action.apply(closeable)).thenThrow(new IllegalArgumentException(errMsg));
        try {
            CoberturaHelper.tryWithResources(action);
        } catch (final IllegalArgumentException iae) {
            assertEquals(errMsg, iae.getMessage());
        }
        verify(closeable, times(2)).close();//fecha recursos mesmo dando erro
    }
}
