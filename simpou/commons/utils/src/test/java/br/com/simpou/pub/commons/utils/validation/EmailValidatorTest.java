package br.com.simpou.pub.commons.utils.validation;

import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.violation.Violation;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

@ClassTest(EmailValidator.class)

public class EmailValidatorTest {

    @Test
    public void testIsValid() {
        final String v = Randoms.getString(5, 10, false);
        final String d = Randoms.getString(2, 4, false);
        final String d1 = Randoms.getString(1, false);
        final String s = ".";
        final String a = "@";

        assertTrue(EmailValidator.isValid(v + a + v + s + d));
        assertTrue(EmailValidator.isValid(v + a + v + s + d + s + d));
        assertTrue(EmailValidator.isValid(v + a + v + s + d + s + d + s + d));

        assertFalse(EmailValidator.isValid(v + a + v + s + d1));
        assertTrue(EmailValidator.isValid(v + a + v + s + d1 + s + d));
        assertFalse(EmailValidator.isValid(v + a + v + s + d + s + d1));
        assertTrue(EmailValidator.isValid(v + a + v + s + d1 + s + d + s + d));
        assertTrue(EmailValidator.isValid(v + a + v + s + d + s + d1 + s + d));
        assertFalse(EmailValidator.isValid(v + a + v + s + d + s + d + s + d1));

        assertFalse(EmailValidator.isValid(v + a + v + s));
        assertFalse(EmailValidator.isValid(v + a + v));
        assertFalse(EmailValidator.isValid(v + a));
        assertFalse(EmailValidator.isValid(v));

        assertFalse(EmailValidator.isValid(Randoms.getString(1, 4, true)));
    }

    @Test
    public void testIsValid_2() {
        assertTrue(EmailValidator.isValid(""));
        assertTrue(EmailValidator.isValid(null));
    }

    @Test
    public void testValidate() {
        final EmailValidator validator = new EmailValidator();

        // ok
        final Violation violationOk = validator.validate("a@a.com");
        assertNull(violationOk);

        // not ok
        final Violation violationNotOk = validator.validate("a.com.br");
        Assert.assertEquals(EmailValidator.NOT_MATCH, violationNotOk.getCode());
    }
}
