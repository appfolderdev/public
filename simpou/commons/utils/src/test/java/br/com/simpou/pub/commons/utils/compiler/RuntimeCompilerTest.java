package br.com.simpou.pub.commons.utils.compiler;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Assert;
import org.junit.Test;

import javax.tools.DiagnosticCollector;
import javax.tools.JavaFileObject;

import static org.junit.Assert.*;

@ClassTest(RuntimeCompiler.class)

public class RuntimeCompilerTest {

    private final String className = "JavaClassImpl";

    private final String ifacePackage = this.getClass().getPackage().getName();

    private final String ifaceClass = this.getClass().getSimpleName() + ".JavaInterface";

    private final String source = "package " + this.ifacePackage + ";\n" + "public class " + this.className
            + " implements " + this.ifaceClass + "{\n" + "@Override\n" + "public String echo(String msg) {\n"
            + "    return msg;\n" + "}\n" + "}\n";

    /**
     * @see RuntimeCompiler#compile(Class, String, String, String)
     */
    @Test
    public void testCompile() throws Exception {
        final Class<JavaInterface> instanceClass = RuntimeCompiler.compile(
                JavaInterface.class,
                this.ifacePackage,
                this.className,
                this.source
        );
        final JavaInterface instance = instanceClass.newInstance();
        final String msg = "Ok compilado!";
        final String echo;

        echo = instance.echo(msg);
        assertEquals(msg, echo);
    }

    /**
     * Erro de compilação no código da classe.
     *
     * @see RuntimeCompiler#compile(Class, String, String, String)
     */
    @Test
    public void testCompile_error() {
        try {
            RuntimeCompiler.compile(JavaInterface.class, this.ifacePackage, this.className, this.source + "}");
            fail();
        } catch (final CompilerException ex) {
            final DiagnosticCollector<JavaFileObject> diagnostics = ex.getDiagnostics();
            final String msg = ex.toString();
            final String message = ex.getMessage();
            assertTrue(msg.startsWith("Compilation fail."));
            assertNotNull(message);
            assertNotNull(diagnostics);
        }
    }

    /**
     * Nome do pacote incorreto: compila mas falha ao carregar classe.
     *
     * @see RuntimeCompiler#compile(Class, String, String, String)
     */
    @Test(expected = CompilerException.class)
    public void testCompile_error2() throws CompilerException {
        new RuntimeCompiler().compile(JavaInterface.class, this.ifacePackage + "a", this.className, this.source);
    }

    /**
     * Nome da classe incorreto.
     *
     * @see RuntimeCompiler#compile(Class, String, String, String)
     */
    @Test(expected = CompilerException.class)
    public void testCompile_error3() throws CompilerException {
        RuntimeCompiler.compile(JavaInterface.class, null, "OtherClass", this.source);
    }

    /**
     * Erro ao gerar URI da classe devido a caracteres ilegais no nome.
     *
     * @see RuntimeCompiler#compile(Class, String, String, String)
     */
    @Test(expected = CompilerException.class)
    public void testCompile_error4() throws CompilerException {
        RuntimeCompiler.compile(JavaInterface.class, this.ifacePackage, "\\uFFFD", this.source);
    }

    /**
     * Custom Classloader que gera erros: falha ao carregar classe.
     *
     * @see RuntimeCompiler#compile(Class, String, String, String, Iterable,
     * ClassLoader)
     */
    @Test(expected = CompilerException.class)
    public void testCompile_options() throws Exception {
        RuntimeCompiler.compile(
                JavaInterface.class,
                this.ifacePackage,
                this.className,
                this.source,
                null,
                new ClassloaderImpl()
        );
    }

    /**
     * @see RuntimeCompiler#getQualifiedClassName(String, String)
     */
    @Test
    public void testGetQualifiedClassName() {
        Assert.assertEquals(this.className, RuntimeCompiler.getQualifiedClassName(null, this.className));
        Assert.assertEquals(this.className, RuntimeCompiler.getQualifiedClassName("", this.className));
        Assert.assertEquals(this.className, RuntimeCompiler.getQualifiedClassName("  ", this.className));
    }

    public interface JavaInterface {

        String echo(String msg);
    }

    class ClassloaderImpl extends ClassLoader {

        @Override
        protected Class<?> loadClass(final String name, final boolean resolve) throws ClassNotFoundException {
            final Class<?> aClass = super.loadClass(name, resolve);
            if (aClass == JavaInterface.class) {
                throw new ClassNotFoundException(name);
            } else {
                return aClass;
            }
        }
    }

}
