package br.com.simpou.pub.commons.utils.pagination;

import br.com.simpou.pub.commons.utils.lang.Numbers;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.ExtraTest;
import br.com.simpou.pub.commons.utils.tests.annot.Repeat;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static br.com.simpou.pub.commons.utils.rand.Randoms.getInteger;
import static org.junit.Assert.*;

@Repeat(PaginatorTest.CONFIGS)

@ClassTest(Paginator.class)
public class PaginatorTest {

    static final int CONFIGS = 8;

    private static int configChooser;

    private PaginationConfig config;

    private Paginator paginationHelper;

    @Before
    public void setUp() throws CloneNotSupportedException {
        switch (configChooser) {
            case 0:
                // elementos cabem em uma página e menor que o máximo
                this.config = new PaginationConfig(200, 100, 10);
                break;
            case 1:
                // elementos cabem em uma página e menor que o máximo
                this.config = new PaginationConfig(100, 100, 10);
                break;
            case 2:
                // número de páginas igual ao máximo de páginas a serem exibidas
                // e número de elementos da ultima página exato
                this.config = new PaginationConfig(10, 100, 10);
                break;
            case 3:
                // número de páginas igual ao máximo de páginas a serem exibidas
                // e número de elementos da ultima página menor que o máximo
                this.config = new PaginationConfig(10, 98, 10);
                break;
            case 4:
                // número de páginas maior que o máximo de páginas a serem exibidas
                // e número de elementos da ultima página menor que o máximo,
                // todos comandos são disponiveis: proxima, anterior, ultima, primeira
                this.config = new PaginationConfig(10, 98, 5);
                break;
            case 5:
                // número de páginas maior que o máximo de páginas a serem exibidas
                // e número de elementos da ultima página exato,
                // somente alguns comandos são disponiveis: prox+ultima ou ant+primeira
                this.config = new PaginationConfig(9, 99, 10);
                break;
            case 6:
                // número de páginas menor que o máximo de páginas a serem exibidas
                // e número de elementos da ultima página exato
                // somente alguns comandos são disponiveis
                this.config = new PaginationConfig(10, 100, 11);
                break;
            case 7:
                // número de páginas menor que o máximo de páginas a serem exibidas
                // e número de elementos da ultima página menor que o máximo,
                this.config = new PaginationConfig(10, 98, 11);
                break;
        }

        configChooser = (configChooser + 1) % CONFIGS;
        this.paginationHelper = new Paginator(this.config);
    }

    @Test
    @Repeat(1)
    public void testCountPages() {
        //testGoLastPage
    }

    @Test
    @Repeat(1)
    public void testGetActualPage() {
        //testGoToPage
    }

    @Test
    @Repeat(1)
    public void testGetConfig() throws Exception {
        final PaginationConfig clone = this.paginationHelper.getConfig();
        assertEquals(this.config.getElements(), clone.getElements());
        assertEquals(this.config.getPagesToShow(), clone.getPagesToShow());
        assertEquals(this.config.getPageSize(), clone.getPageSize());
    }

    @Test
    @Repeat(1)
    public void testGetLastPage() {
        //testGoLastPage
    }

    @Test
    public void testGetLimits() throws CloneNotSupportedException {
        final PaginationConfig paginationConfig = this.paginationHelper.getConfig();
        final int pages = this.paginationHelper.countPages();
        final List<Integer> pageListRand = getPageRandomList();

        int page;

        for (int i = 0; i < pages; i++) {
            page = pageListRand.get(i);
            this.paginationHelper.goToPage(page);

            final PageLimits limits = this.paginationHelper.getLimits();

            final int first = (page - 1) * paginationConfig.getPageSize();
            final int last = (int) Math.min((first + paginationConfig.getPageSize()) -
                    1, this.config.getElements() - 1);
            final int offset = first;
            final int size = last - first + 1;

            assertEquals(first, limits.getFirst());
            assertEquals(last, limits.getLast());
            assertEquals(offset, limits.getOffset());
            assertEquals(size, limits.getSize());
        }
    }

    @Test
    public void testGetPages() {
        List<Integer> pagesList;
        int lastIndex;
        int firstIndex = 1;
        Object[] pagesArray;

        final int lastPage = this.paginationHelper.getLastPage();
        final boolean hasHiddenPages = lastPage > this.config.getPagesToShow();
        final int showedPages = hasHiddenPages ? this.config.getPagesToShow() : lastPage;
        final int halfFloor = this.config.getPagesToShow() / 2;
        final int halfCeil = Numbers.isEven(showedPages) ? (halfFloor + 1)
                : halfFloor;

        for (int i = 1; i < (lastPage + 1); i++) {
            this.paginationHelper.goToPage(i);

            lastIndex = Math.min((firstIndex + this.config.getPagesToShow()) - 1,
                    this.paginationHelper.getLastPage());

            pagesList = this.paginationHelper.getPages();
            pagesArray = this.paginationHelper.getPages().toArray();

            // testa quantidade de páginas
            assertEquals(showedPages, pagesList.size());
            assertEquals(showedPages, pagesArray.length);

            // testa índices extremos
            assertEquals(firstIndex, ((Integer) pagesArray[0]).intValue());
            assertEquals(lastIndex,
                    ((Integer) pagesArray[pagesArray.length - 1]).intValue());

            if ((i >= halfCeil) && (lastIndex < lastPage) && hasHiddenPages) {
                firstIndex++;
            }
        }
    }

    @Test
    public void testGoFirstPage() {
        final int page = getInteger(1, this.paginationHelper.countPages());
        this.paginationHelper.goToPage(page);

        final PageLimits limits = this.paginationHelper.goFirstPage();
        final List<Integer> pages = this.paginationHelper.getPages();
        final int firstPage = 1;
        final int lastPage = Math.min(this.paginationHelper.countPages(),
                (firstPage + this.config.getPagesToShow()) - 1);

        assertEquals(1, this.paginationHelper.getActualPage());
        assertEquals(0, limits.getFirst());
        assertTrue(pages.contains(firstPage));
        assertTrue(pages.contains(lastPage));
    }

    @Test
    public void testGoLastPage() {
        final int page = getInteger(1, this.paginationHelper.countPages());
        this.paginationHelper.goToPage(page);

        final PageLimits limits = this.paginationHelper.goLastPage();
        final List<Integer> pages = this.paginationHelper.getPages();
        final int lastPage = this.paginationHelper.countPages();
        final int firstPage = Math.max(1, lastPage - this.config.getPagesToShow() + 1);

        assertEquals(this.paginationHelper.countPages(),
                this.paginationHelper.getActualPage());
        assertEquals(this.paginationHelper.getLastPage(),
                this.paginationHelper.getActualPage());
        assertEquals(this.config.getElements() - 1, limits.getLast());
        assertTrue(pages.contains(firstPage));
        assertTrue(pages.contains(lastPage));
    }

    @Test
    public void testGoNextPage() {
        final int lastPage = this.paginationHelper.getLastPage();
        final List<Integer> pageListRand = getPageRandomList();

        int page;

        for (int i = 0; i < lastPage; i++) {
            page = pageListRand.get(i);
            this.paginationHelper.goToPage(page);
            this.paginationHelper.goNextPage();

            if (page == lastPage) {
                assertEquals(page, this.paginationHelper.getActualPage());
            } else {
                assertEquals(page + 1, this.paginationHelper.getActualPage());
            }
        }
    }

    @Test
    public void testGoPreviousPage() {
        final int lastPage = this.paginationHelper.getLastPage();
        final List<Integer> pageListRand = getPageRandomList();

        int page;

        for (int i = 0; i < lastPage; i++) {
            page = pageListRand.get(i);
            this.paginationHelper.goToPage(page);
            this.paginationHelper.goPreviousPage();

            if (page == 1) {
                assertEquals(page, this.paginationHelper.getActualPage());
            } else {
                assertEquals(page - 1, this.paginationHelper.getActualPage());
            }
        }
    }

    @Test
    public void testGoToPage() throws Exception {
        final int pages = this.paginationHelper.getLastPage();
        int page = -1;

        for (int i = 0; i < pages; i++) {
            //página válida
            page = getInteger(1, pages);
            this.paginationHelper.goToPage(page);
            assertEquals(page, this.paginationHelper.getActualPage());
        }

        // primeira página
        this.paginationHelper.goFirstPage();
        assertEquals(1, this.paginationHelper.getActualPage());

        // última página
        this.paginationHelper.goLastPage();
        assertEquals(this.paginationHelper.getLastPage(),
                this.paginationHelper.getActualPage());
    }

    @Test
    @Repeat(1)
    public void testGoToPage_error() throws Exception {
        final int pages = this.paginationHelper.getLastPage();
        final int page = this.paginationHelper.getActualPage();

        try {
            this.paginationHelper.goToPage(pages + 1);
            fail();
        } catch (final IllegalArgumentException e) {
        }

        assertEquals(page, this.paginationHelper.getActualPage());

        try {
            this.paginationHelper.goToPage(0);
            fail();
        } catch (final IllegalArgumentException e) {
        }

        assertEquals(page, this.paginationHelper.getActualPage());
    }

    @Repeat(1)
    @ExtraTest
    @Test(expected = IllegalArgumentException.class)
    public void testInvalidConfig() throws CloneNotSupportedException {
        new Paginator(new PaginationConfig(-1, -1, -1));
    }

    private List<Integer> getPageRandomList() {
        final int lastPage = this.paginationHelper.getLastPage();
        final List<Integer> pageList = new ArrayList<>();

        for (int i = 0; i < lastPage; i++) {
            pageList.add(i + 1);
        }

        int index;
        final List<Integer> pageListRand = new ArrayList<>();

        for (int i = 0; i < lastPage; i++) {
            index = getInteger(0, pageList.size() - 1);
            pageListRand.add(pageList.get(index));
            pageList.remove(index);
        }

        return pageListRand;
    }
}
