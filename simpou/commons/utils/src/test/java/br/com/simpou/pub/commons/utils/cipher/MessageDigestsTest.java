package br.com.simpou.pub.commons.utils.cipher;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Test;

import java.security.MessageDigest;

import static org.junit.Assert.assertNotNull;

@ClassTest(MessageDigests.class)

public class MessageDigestsTest {

    /**
     * @see MessageDigests#getDigest(String)
     */
    @Test(expected = RuntimeException.class)
    public void testGetDigest() {
        MessageDigest digest;

        digest = MessageDigests.getDigest(Algorithms.MD5);
        assertNotNull(digest);

        digest = new MessageDigests().getDigest(Algorithms.SHA_256);
        assertNotNull(digest);

        MessageDigests.getDigest("Jonas Pereira"); //RuntimeException
    }
}
