package br.com.simpou.pub.commons.utils.lang;

import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Assert;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@ClassTest(Dates.class)

//TODO adicionar testes mais abrangentes cobrindo mudanças de meses, anos bissextos, etc
public class DatesTest {

    private static final DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");

    /**
     * @see Dates#addDate(Date, int, int, int)
     */
    @Test
    public void testAddDate() throws Exception {
        final Date ref = df.parse("05/05/05 05:05:05");
        assertEquals(
                "06/06/06 05:05:05",
                df.format(Dates.addDate(ref, 1, 1, 1))
        );
    }

    /**
     * @see Dates#addTime(Date, int, int, int)
     */
    @Test
    public void testAddTime() throws Exception {
        final Date ref = df.parse("05/05/05 05:05:05");
        assertEquals(
                "05/05/05 06:06:06",
                df.format(Dates.addTime(ref, 1, 1, 1))
        );
    }

    /**
     * @see Dates#chooseMostOld(Date...)
     */
    @Test
    public void testChooseMostOld() {
        final Date future = Randoms.getDate(true);
        final Date past = Randoms.getDate(false);
        assertEquals(future, Dates.chooseMostRecent(future, past));
    }

    /**
     * @see Dates#chooseMostRecent(Date...)
     */
    @Test
    public void testChooseMostRecent() {
        final Date future = Randoms.getDate(true);
        final Date past = Randoms.getDate(false);
        assertEquals(past, Dates.chooseMostOld(future, past));
    }

    /**
     * @see Dates#diffDays(Date, Date)
     */
    @Test
    public void testDiffDays() throws Exception {
        // diferença de horas
        assertEquals(
                0,
                Dates.diffDays(
                        df.parse("05/05/05 00:00:01"),
                        df.parse("05/05/05 23:59:59")
                )
        );
        // mesmo ano positivo
        assertEquals(
                1,
                Dates.diffDays(
                        df.parse("05/05/05 00:00:01"),
                        df.parse("06/05/05 00:00:01")
                )
        );
        // mesmo ano negativo
        assertEquals(
                -1,
                Dates.diffDays(
                        df.parse("06/05/05 00:00:01"),
                        df.parse("05/05/05 00:00:01")
                )
        );
        // ano bissexto
        assertEquals(
                -366,
                Dates.diffDays(
                        df.parse("01/01/13 00:00:00"),
                        df.parse("01/01/12 00:00:00")
                )
        );

    }

    /**
     * @see Dates#getCalendar(Date)
     */
    @Test
    public void testGetCalendar() throws Exception {
        //testAddTime e testAddDate
    }

    /**
     * @see Dates#formatCanonical(LocalDateTime, ZoneId)
     */
    @Test
    public void formatCanonical() {
        final ZoneOffset zoneId = ZoneOffset.of("-3");
        final LocalDateTime dateTime = LocalDateTime.of(2022, 9, 5, 12, 34);
        final String output = Dates.formatCanonical(dateTime, zoneId);
        assertEquals("2022-09-05T12:34:00.000-03:00", output);
    }

    /**
     * @see Dates#parseCanonical(String)
     */
    @Test
    public void parseCanonical() {
        final LocalDateTime expected = LocalDateTime.of(2022, 9, 5, 12, 34);
        final LocalDateTime given = Dates.parseCanonical("2022-09-05T12:34:00.000-03:00");
        assertEquals(expected, given);
    }

    /**
     * @see Dates#toLocalDate(Date)
     * @see Dates#toDate(LocalDate)
     */
    @Test
    public void toLocalDate() {
        final LocalDate expected = LocalDate.of(2022, 9, 5);
        final Date date = Dates.toDate(expected);
        final LocalDate given = Dates.toLocalDate(date);
        assertEquals(expected, given);
    }

    /**
     * @see Dates#countDaysBetween(LocalDate, LocalDate)
     */
    @Test
    public void countDaysBetween() {
        final int expected = 12;
        final LocalDate begin = LocalDate.now();
        final LocalDate end = begin.plusDays(expected);
        final long given = Dates.countDaysBetween(begin, end);
        assertEquals(expected, given);

        assertEquals(0, Dates.countDaysBetween(begin, begin));
    }

    /**
     * @see Dates#getDaysBetween(LocalDate, LocalDate)
     */
    @Test
    public void getDaysBetween() {
        final int numDays = 12;
        final LocalDate begin = LocalDate.now();
        final LocalDate end = begin.plusDays(numDays);
        final List<LocalDate> days = Dates.getDaysBetween(begin, end);

        assertEquals(numDays + 1, days.size());//quant. correta de dias
        assertEquals(begin, days.get(0));
        assertEquals(end, days.get(days.size()-1));

        for (int i = 0; i < days.size()-1; i++) {
            final LocalDate cur = days.get(i);
            final LocalDate next = days.get(i+1);
            assertTrue(cur.isBefore(next));
            assertEquals(1, Dates.countDaysBetween(cur, next));
        }
    }

    /**
     * @see Dates#getLastDayOfMonth(LocalDate)
     */
    @Test
    public void getLastDayOfMonth() {
        assertEquals(28,Dates.getLastDayOfMonth(LocalDate.of(2022, 2, 1)));
        assertEquals(29,Dates.getLastDayOfMonth(LocalDate.of(2020, 2, 1)));
        assertEquals(31,Dates.getLastDayOfMonth(LocalDate.of(2020, 12, 1)));
    }

}
