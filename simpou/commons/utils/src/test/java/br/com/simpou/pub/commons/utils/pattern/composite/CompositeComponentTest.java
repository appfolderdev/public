package br.com.simpou.pub.commons.utils.pattern.composite;

import br.com.simpou.pub.commons.utils.functional.Action;
import lombok.extern.apachecommons.CommonsLog;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by jonas on 3/2/17.
 */
@CommonsLog
public class CompositeComponentTest {

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void getValue() throws Exception {

        // para checar se uma operaçao nao e executada mais de uma vez
        final Set<String> evals = new HashSet<>();

        final Action<String, Integer> stringToInt = new Action<String, Integer>() {
            @Override
            public Integer apply(final String s) {
                return Integer.valueOf(s);
            }
        };
        final BinaryCompositeOperation<String, Integer> sumOperation =
                new BinaryCompositeOperation<String, Integer>() {
                    @Override
                    public Integer operate(final Integer value, final Integer operand) {
                        final String eval = value.toString() + this + operand.toString();
                        Assert.assertFalse(evals.contains(eval));
                        evals.add(eval);
                        log.debug(eval);
                        return value + operand;
                    }

                    @Override
                    public String toString() {
                        return "+";
                    }
                };
        final BinaryCompositeOperation<String, Integer> multOperation =
                new BinaryCompositeOperation<String, Integer>() {
                    @Override
                    public Integer operate(final Integer value, final Integer operand) {
                        final String eval = value.toString() + this + operand.toString();
                        Assert.assertFalse(evals.contains(eval));
                        evals.add(eval);
                        log.debug(eval);
                        return value * operand;
                    }

                    @Override
                    public String toString() {
                        return "*";
                    }
                };

        //2
        final CompositeComponent<String, Integer> b1 = Composites.from("2");
        log.debug("Eval:" + b1);
        evals.clear();
        Assert.assertEquals(2, b1.getValue(stringToInt).intValue());

        //3
        final CompositeComponent<String, Integer> b2 = Composites.from("3");
        log.debug("Eval:" + b2);
        evals.clear();
        Assert.assertEquals(3, b2.getValue(stringToInt).intValue());

        //2+3=5
        final CompositeComponent<String, Integer> c1 = b1.compose(b2, sumOperation);
        log.debug("Eval:" + c1);
        evals.clear();
        Assert.assertEquals(5, c1.getValue(stringToInt).intValue());

        //2*(2+3)=10
        final CompositeComponent<String, Integer> c3 = Composites.from(b1, c1, multOperation);
        log.debug("Eval:" + c3);
        evals.clear();
        Assert.assertEquals(10, c3.getValue(stringToInt).intValue());

        //(2*(2+3)+2)=12
        final CompositeComponent<String, Integer> c4 = c3.compose(b1, sumOperation);
        log.debug("Eval:" + c4);
        evals.clear();
        Assert.assertEquals(12, c4.getValue(stringToInt).intValue());

        final CompositeComponent<String, Integer> c5 = c4.compose(c4, multOperation);
        log.debug("Eval:" + c5);
        evals.clear();
        Assert.assertEquals(144, c5.getValue(stringToInt).intValue());

        Assert.assertEquals("(2*(2+3))+2", c4.toString());
        Assert.assertEquals("((2*(2+3))+2)*((2*(2+3))+2)", c5.toString());

    }

}
