package br.com.simpou.pub.commons.utils.file;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

@ClassTest(PropertyHelper.class)
public class PropertyHelperTest {

    @Test
    public void testClose() throws Exception {
        //ITCase
    }

    @Test
    public void testFrom() {
        //ITCase
    }

    @Test
    public void testGetAllKeys() {
        //TODO
    }

    @Test
    public void testGetHomeDir() {
        final String dir = new File(PropertyHelper.getHomeDir()).getAbsolutePath();
        assertTrue(new File(dir).exists());
    }

    @Test
    public void testGetMvnPrjDir() throws Exception {
        final String prjDir = PropertyHelper.getMvnPrjDir();
        assertTrue(prjDir.endsWith("/commons/utils/"));
    }

    @Test
    public void testGetPrjDir() throws Exception {
        //testGetMvnPrjDir
    }

    @Test
    public void testGetProperty() throws Exception {
        //ITCase
    }

    @Test
    public void testGetSystemProperty() {
        final String prop = PropertyHelper.getSystemProperty("file.separator");
        assertEquals(File.separator, prop);
    }

    @Test
    public void testGetTempDir() {
        final String prop = PropertyHelper.getTempDir();
        assertNotNull(prop);
        assertFalse(prop.isEmpty());
    }

    @Test
    public void testGetUser() {
        final String prop = PropertyHelper.getUser();
        assertNotNull(prop);
        assertFalse(prop.isEmpty());
    }

    @Test
    public void testGetWorkDir() {
        final File file = new File(PropertyHelper.getWorkDir());
        final String dir = file.getAbsolutePath();
        final String dirName = new File(PropertyHelper.getWorkDir()).getName();
        final String curDir = new File("").getAbsolutePath();
        final String curDirName = new File(curDir).getName();
        assertEquals(curDir, dir);
        assertEquals(curDirName, dirName);
    }

    @Test
    public void testGetWorkDirName() {
        final File dirFile = new File(PropertyHelper.getWorkDir());
        final String dirName = PropertyHelper.getWorkDirName();
        assertEquals(dirFile.getName(), dirName);
    }

    @Test
    public void testIsWindows() {
        final boolean windows = PropertyHelper.isWindows();
        assertEquals(File.separator.equals("\\"), windows);
    }

    @Test
    public void testSetProperty_String_String() throws Exception {
        //ITCase
    }

    @Test
    public void testSetProperty_String_int() throws Exception {
        //ITCase
    }
}
