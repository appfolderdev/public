package br.com.simpou.pub.commons.utils.interceptor;

import br.com.simpou.pub.commons.utils.string.Strings;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.TestOrder;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@ClassTest(LogInterceptor.class)

public class LogInterceptorTest {

    private static LogInterceptor.Logger logger;

    private static Interface intercepted;

    @BeforeClass
    public static void setUpClass() {
        logger = mock(LogInterceptor.Logger.class);
        intercepted = LogInterceptor.intercept(Interface.class, new Implementation(), logger);
    }

    @Test
    public void testDoAfter() {
        //testDoBefore
    }

    @Test
    @TestOrder(1)
    public void testDoBefore() throws NoSuchMethodException {
        final Object nullReturn = null;
        final String argsString = "[p1, p2]";
        String msg;

        final Method method1 = Interface.class.getDeclaredMethod("method1",
                String.class, String.class);
        intercepted.method1("p1", "p2");
        msg = Strings.replaceParams(LogInterceptor.ON_CALL_MSG,
                method1.getDeclaringClass().getSimpleName(), method1.getName(),
                argsString);
        msg += Strings.replaceParams(LogInterceptor.ON_RETURN_MSG,
                nullReturn);
        verify(logger).write(msg);

        final Method method2 = Interface.class.getDeclaredMethod("method2");
        final String result = intercepted.method2();
        msg = Strings.replaceParams(LogInterceptor.NO_ARGS_MSG,
                method2.getDeclaringClass().getSimpleName(), method2.getName());
        msg += Strings.replaceParams(LogInterceptor.ON_RETURN_MSG, result);
        verify(logger).write(msg);
    }

    @Test(expected = RuntimeException.class)
    @TestOrder(2)
    public void testDoOnError() throws NoSuchMethodException {
        try {
            intercepted.method3("exceptMsg");
            fail();
        } catch (final Exception ex) {
            final Object nullReturn = null;
            final Method method = Interface.class.getDeclaredMethod("method3",
                    String.class);
            String msg = Strings.replaceParams(LogInterceptor.ON_CALL_MSG,
                    method.getDeclaringClass().getSimpleName(),
                    method.getName(), "[" + ex.getMessage() + "]");
            msg += Strings.replaceParams(LogInterceptor.ON_ERROR_MSG,
                    ex.getClass().getName(), ex.getMessage());
            msg += Strings.replaceParams(LogInterceptor.ON_RETURN_MSG,
                    nullReturn);
            verify(logger).write(msg);
        }

        intercepted.method4();//RuntimeException
    }

    @Test(expected = Exception.class)
    @TestOrder(2)
    public void testDoOnError_noMsg() throws Exception {
        intercepted.method3(null);
    }

    @Test
    public void testIntercept() {
        //setUpClass
    }

    private static interface Interface {

        void method1(String p1, String p2);

        String method2();

        void method3(String msg) throws Exception;

        void method4();
    }

    private static class Implementation implements Interface {

        @Override
        public void method1(final String p1, final String p2) {
        }

        @Override
        public String method2() {
            return "retValue";
        }

        @Override
        public void method3(final String msg) throws Exception {
            throw new Exception(msg, null);
        }

        @Override
        public void method4() {
            throw new RuntimeException(null, new Throwable("message"));
        }
    }
}
