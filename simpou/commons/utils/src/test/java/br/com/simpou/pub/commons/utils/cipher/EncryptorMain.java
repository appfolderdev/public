package br.com.simpou.pub.commons.utils.cipher;

public class EncryptorMain {
    public static void main(final String[] args) throws Exception {
        final Encryptor encryptor = new Encryptor("");
        final String crypted1 = encryptor.crypt("");
        final String crypted2 = encryptor.crypt("");
        System.out.println(crypted1);
        System.out.println(crypted2);
    }
}
