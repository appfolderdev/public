package br.com.simpou.pub.commons.utils.functional;

import br.com.simpou.pub.commons.utils.lang.PrintStackHelper;
import br.com.simpou.pub.commons.utils.tests.Tests;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.Objects;

@ClassTest(Monad.class)
public class MonadTest {

    private static final String ERROR_STRING_1 = "error1";

    private static final String ERROR_STRING_2 = "error2";

    private static final String RAND_STRING = "hello";

    private static final Action<String, Monad<String>> ACTION = new Action<String, Monad<String>>() {
        @Override
        public Monad<String> apply(final String object) {
            if (Objects.equals(object, MonadTest.ERROR_STRING_1)) {
                throw new RuntimeException();
            } else if (Objects.equals(object, MonadTest.ERROR_STRING_2)) {
                return Monad.unit(MonadTest.ERROR_STRING_2);
            }
            return Monad.unit(MonadTest.ERROR_STRING_1);
        }
    };

    private static Monad<String> monad;

    /**
     * @see Monad#equals(Object)
     */
    @Test
    public void testEquals() throws Exception {
        Tests.testEqualsAndHashCode(
                Monad.unit("ok"),
                Monad.unit("ok"),
                Monad.unit("nops"),
                Monad.unit(null),
                Monad.unit(null)
        );
    }

    /**
     * @see Monad#flatMap(Action)
     */
    @Test(expected = RuntimeException.class)
    public void testFlatMap() throws Exception {
        final String result = monad.flatMap(ACTION).getValue();
        Assert.assertEquals(ERROR_STRING_1, result);

        Monad.unit(RAND_STRING).flatMap(ACTION).flatMap(ACTION);//RuntimeException
    }

    /**
     * @see Monad#flatMap(SilentAction)
     */
    @Test
    public void testFlatMap_silent() throws Exception {
        final SilentActionContainer<String, Monad<String>> silentAction =
                new SilentActionContainer<String, Monad<String>>(MonadTest.ACTION) {
                    @Override
                    public void doOnError(final String object, final Throwable e) {
                        if (Objects.equals(object, MonadTest.ERROR_STRING_1)) {
                            throw new RuntimeException(e);
                        }
                    }
                };
        final Monad<String> partMonad = Monad.unit(RAND_STRING).flatMap(silentAction);
        Assert.assertEquals(RAND_STRING, partMonad.getValue());

        final String lockId = PrintStackHelper.setTestMode();
        try {
            Monad.unit(RAND_STRING).flatMap(ACTION).flatMap(silentAction);
            Assert.assertEquals(RuntimeException.class, PrintStackHelper.getStack().getClass());
        } finally {
            PrintStackHelper.setProductionMode(lockId);
        }
    }

    /**
     * Testado por {@link #testFlatMap_silent()}.
     *
     * @see Monad#getValue()
     */
    @Test
    public void testGetValue() throws Exception {

    }

    /**
     * Testado por {@link #testEquals()}.
     *
     * @see Monad#hashCode()
     */
    @Test
    public void testHashCode() throws Exception {

    }

    /**
     * Testado por {@link #testFlatMap()}.
     *
     * @see Monad#unit(Object).
     */
    @Test
    public void testUnit() throws Exception {
    }
}
