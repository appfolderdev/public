package br.com.simpou.pub.commons.utils.functional;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Assert;
import org.junit.Test;

@ClassTest(ConcatObjectAction.class)
public class ConcatObjectActionTest {

    /**
     * @see ConcatObjectAction#getResult()
     */
    @Test
    public void testGetResult() throws Exception {
        final ConcatObjectAction<Integer> concatObjectAction = new ConcatObjectAction<>(
                "x",
                "",
                true,
                new ConcatObjectAction.ToStringContentExtractor<Integer>(),
                4
        );
        concatObjectAction.executeDelegate(1);
        concatObjectAction.executeDelegate(2);
        try {
            concatObjectAction.executeDelegate(3);
            Assert.fail();// estoura o limite de caracteres
        } catch (final StringIndexOutOfBoundsException sioobe) {

        }
        Assert.assertEquals("x1x2", concatObjectAction.getResult());
        // limpa o resultado
        Assert.assertEquals(0, concatObjectAction.getResult().length());
    }

    /**
     * Testador por {@link #testGetResult()}.
     *
     * @see ConcatObjectAction#reset()
     */
    @Test
    public void testReset() throws Exception {

    }
}
