package br.com.simpou.pub.commons.utils.file;

import br.com.simpou.pub.commons.utils.TestsConstants;
import br.com.simpou.pub.commons.utils.string.StringLocales;
import br.com.simpou.pub.commons.utils.string.Strings;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.ExtraTest;
import org.junit.*;

import java.io.IOException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Set;

import static org.junit.Assert.*;

@ClassTest(BundleHelper.class)
public class BundleHelperITCase {

    private static final String bundleName = "bundleTest";

    private static final String bundleNameWithDir = "reldir/" + bundleName;

    private static final String localeStringEN = "_en_US";

    private static final String contentEnumDef = "elemento";

    private static final String contentEnumEN = "element";

    private static BundleHelper bundleHelper;

    private static BundleHelper bundleHelperEN;

    private static String bundle;

    private static String key;

    private static String key2;

    private static String param;

    private static String contentDef;

    private static String contentEN;

    private static String bundlePath;

    private static String bundlePathEN;

    private static String bundleWithDirPath;

    @BeforeClass
    public static void setUpClass() throws Exception {
        // cria arquivos de bundle direto no diretório de classes para 
        // funcionar em runtime
        final String baseDirClass = TestsConstants.TEST_CLASSES_DIR;
        bundlePath = baseDirClass + bundleName +
                PropertyHelper.FILE_PROPS_EXTENSION;
        bundlePathEN = baseDirClass + bundleName + localeStringEN +
                PropertyHelper.FILE_PROPS_EXTENSION;
        bundleWithDirPath = baseDirClass + bundleNameWithDir +
                PropertyHelper.FILE_PROPS_EXTENSION;

        final String content = " {0}.";
        key = "message";
        key2 = "message2";
        contentDef = "Olá" + content;
        contentEN = "Hello" + content;

        // gera arquivos no resources
        final String enumKeyName = TestsConstants.ENUM_BUNDLE_KEY_PREFIX +
                TestEnum.ELEMENT.name();

        FileHelper.write(bundlePath, key + "=" + contentDef, false, false);
        FileHelper.write(bundlePath, "\n" + enumKeyName + "=" + contentEnumDef,
                true, false);
        assertTrue(FileHelper.exists(bundlePath));

        FileHelper.write(bundlePathEN, key + "=" + contentEN, false, false);
        FileHelper.write(bundlePathEN,
                "\n" + enumKeyName + "=" + contentEnumEN, true, false);
        assertTrue(FileHelper.exists(bundlePathEN));

        FileHelper.write(bundleWithDirPath, " ", false, false);
        assertTrue(FileHelper.exists(bundleWithDirPath));

        // configura chave
        param = PropertyHelper.getUser();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        FileHelper.delete(bundlePath);
        FileHelper.delete(bundlePathEN);
        FileHelper.delete(bundleWithDirPath);

        if (!PropertyHelper.isWindows()) {
            assertFalse(FileHelper.exists(bundlePath));
            assertFalse(FileHelper.exists(bundlePathEN));
            assertFalse(FileHelper.exists(bundleWithDirPath));
        }
    }

    @Before
    public void setUp() throws IOException {
        Locale.setDefault(new Locale("pt", "BR"));
        bundleHelper = new BundleHelper(bundleName);
        bundleHelperEN = new BundleHelper(bundleName, Locale.US);
    }

    @After
    public void tearDown() throws IOException {
        bundleHelper.close();
        bundleHelperEN.close();
    }

    @Test
    public void testClose() {
        //tearDown
    }

    @Test
    public void testGetBundle_String() throws Exception {
        // obtém bundle em portugues
        bundle = bundleHelper.getBundle(key);
        assertEquals(Strings.replaceParams(contentDef), bundle);

        // obtém bundle em inglês
        bundle = bundleHelperEN.getBundle(key);
        assertEquals(Strings.replaceParams(contentEN), bundle);
    }

    @Test
    public void testGetBundle_String_ObjectArr() throws Exception {
        // obtém bundle em portugues
        bundle = bundleHelper.getBundle(key, param);
        assertEquals(Strings.replaceParams(contentDef, param), bundle);

        // obtém bundle em inglês
        bundle = bundleHelperEN.getBundle(key, param);
        assertEquals(Strings.replaceParams(contentEN, param), bundle);

        bundle = bundleHelperEN.getBundle(key, new String[0]);
        assertEquals(contentEN, bundle);
    }

    @Test
    public void testGetBundle_created() throws IOException {
        // cria bundle
        bundle = bundleHelper.getBundle(key2);

        // verifica se o bundle foi criado em todos arquivos
        final String text = FileHelper.read(bundlePath);
        assertTrue(text.contains(key2 + "="));

        final String textEN = FileHelper.read(bundlePathEN);
        assertTrue(textEN.contains(key2 + "="));
    }

    @Test
    public void testGetLocalesAvaiable() throws IOException {
        final Set<Locale> localesAvaiable = bundleHelper.getLocalesAvaiable(bundleName);
        assertEquals(1, localesAvaiable.size());
        assertTrue(localesAvaiable.contains(StringLocales.getStringAsLocale(
                localeStringEN.substring(1))));
    }

    @ExtraTest
    @Test(expected = MissingResourceException.class)
    public void testNewInstance_invalid() throws IOException {
        new BundleHelper(bundleName + "error");
    }

    @Test
    @ExtraTest
    public void testNewInstance_withDir() throws IOException {
        // testa bundle em subdiretórios
        new BundleHelper(bundleNameWithDir);
    }

    private enum TestEnum {
        ELEMENT
    }
}
