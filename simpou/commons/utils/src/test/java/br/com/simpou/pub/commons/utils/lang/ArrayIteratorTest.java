package br.com.simpou.pub.commons.utils.lang;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.TestOrder;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

@ClassTest(ArrayIterator.class)
@Ignore
public class ArrayIteratorTest {

    private static Integer[] array = {2, 5, 1};

    private static ArrayIterator<Integer> iterator;

    @BeforeClass
    public static void setUpClass() {
        array = new Integer[]{2, 5, 1};
        iterator = new ArrayIterator<>(array);
    }

    @Test
    @TestOrder(3)
    public void testHasNext_no() {
        assertFalse(iterator.hasNext());
    }

    @Test
    @TestOrder(1)
    public void testHasNext_yes() {
        assertTrue(iterator.hasNext());
    }

    @Test
    @TestOrder(2)
    public void testNext() {
        assertEquals(2, iterator.next().intValue());
        assertEquals(5, iterator.next().intValue());
        assertEquals(1, iterator.next().intValue());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    @TestOrder(4)
    public void testNext_outOfBounds() {
        iterator.next();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testRemove() {
        iterator.remove();
    }
}
