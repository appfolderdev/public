package br.com.simpou.pub.commons.utils.string;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.assertEquals;

@ClassTest(StringLocales.class)
public class StringLocalesTest {

    @Test
    public void testGetStringAsLocale() {
        final String DEFAULT_LOCALE = "pt_BR";
        final Locale locale = StringLocales.getStringAsLocale(DEFAULT_LOCALE);
        final String localeString = locale.toString();
        assertEquals(DEFAULT_LOCALE, localeString);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetStringAsLocale_error() {
        StringLocales.getStringAsLocale("pt_BR_v1_v2");
    }
}
