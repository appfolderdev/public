package br.com.simpou.pub.commons.utils.functional;

import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.tests.AssertExtension;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertNull;

@ClassTest(Actions.class)
public class ActionsTest {

    private static final Action<Integer, String> RUNTIME_EXCPT_ACTION = new Action<Integer, String>() {
        @Override
        public String apply(final Integer object) {
            throw new UnsupportedOperationException();
        }
    };

    private static final Action<Integer, String> ERROR_ACTION = new Action<Integer, String>() {
        @Override
        public String apply(final Integer object) {
            throw new Error();
        }
    };

    /**
     * @see Actions#apply(Iterable, Action)
     */
    @Test
    public void testApply_iterable() throws Exception {
        final List<Integer> iterable = Arrays.asList(10, 0, 30);
        final EchoAction<Integer> action = new EchoAction<>();
        final List<Integer> result = Actions.apply(iterable, action);
        AssertExtension.assertCollectionEquals(iterable, result);
    }

    /**
     * @see Actions#apply(Iterable, SilentAction)
     */
    @Test(expected = ArithmeticException.class)
    public void testApply_iterableSilent() throws Exception {
        final List<Integer> iterable = Arrays.asList(10, 0, 30);

        final Action<Integer, Integer> action = new Action<Integer, Integer>() {
            @Override
            public Integer apply(final Integer object) {
                return 30 / object;
            }
        };

        final SilentAction<Integer, Integer> silentAction = Actions.asSilentIgnoreErrors(action);
        final List<Integer> result = Actions.apply(iterable, silentAction);
        AssertExtension.assertCollectionEquals(Arrays.asList(1, null, 3), result);

        new Actions().apply(iterable, action);
    }

    /**
     * @see Actions#apply(Iterable, Action, Condition)
     */
    @Test
    public void testApply_iterableWithStopCondition() throws Exception {
        final Action<Integer, Integer> action = new Action<Integer, Integer>() {
            @Override
            public Integer apply(final Integer object) {
                return object + 1;
            }
        };
        final Condition<Integer> condition = new Condition<Integer>() {
            @Override
            public Boolean apply(final Integer object) {
                return object >= 5;
            }
        };
        {
            final List<Integer> iterable = Arrays.asList(1, 4, 2, 6);
            final List<Integer> result = Actions.apply(iterable, action, condition);
            AssertExtension.assertCollectionEquals(Arrays.asList(2, 5, 3), result);
        }
        {
            final List<Integer> emptyResult = Actions.apply(new ArrayList<Integer>(0), action, condition);
            Assert.assertTrue(emptyResult.isEmpty());
        }
    }

    /**
     * @see Actions#apply(NoParamsAction)
     */
    @Test
    public void testApply_noParams() throws Exception {
        //TODO
    }

    /**
     * Testado por {@link #testAsSilentIgnoreErrors()}, {@link #testAsSilentThrowErrors_error()} e {@link
     * #testAsSilentThrowErrors_runtimeException()}.
     *
     * @see Actions#apply(SilentAction, Object)
     */
    @Test
    public void testApply_silent() throws Exception {
    }

    /**
     * @see Actions#apply(Iterable, VoidAction)
     * @see ConcatObjectAction
     * @see ConcatStringAction
     */
    @Test
    public void testApply_voidAction() throws Exception {
        {
            final String prefix = ",";
            final ConcatStringAction action = new ConcatStringAction(prefix);
            final String v1 = Randoms.getString();
            final String v2 = Randoms.getString();
            final List<String> iterable = Arrays.asList(v1, v2);
            Actions.apply(iterable, action);
            final String result = action.getResult();
            Assert.assertEquals(v1 + prefix + v2, result);
        }
        {
            final ConcatObjectAction<String> action = new ConcatObjectAction<>("-", ",", true,
                    new Action<String, String>() {
                        @Override
                        public String apply(final String object) {
                            return object.substring(1);
                        }
                    }
            );
            final List<String> iterable = Arrays.asList("x1", "x2");
            Actions.apply(iterable, action);
            final String result = action.getResult();
            Assert.assertEquals("-1,-2,", result);
        }
    }

    /**
     * @see Actions#apply(Iterable, VoidAction, Condition)
     * @see ConcatStringAction
     */
    @Test
    public void testApply_voidActionWithStopCondition() throws Exception {
        final ConcatStringAction action = new ConcatStringAction("#", "-", false, 9);
        final Condition<String> condition = new Condition<String>() {
            private int count = 0;

            @Override
            public Boolean apply(final String object) {
                return this.count++ > 2;
            }
        };
        //concatena strings até certo ponto
        {
            final List<String> iterable = Arrays.asList("s", "i", "m", "p", "o", "u");
            Actions.apply(iterable, action, condition);
            final String result = action.getResult();
            Assert.assertEquals("#s-#i-#m", result);
        }
        //nenhum item na lista: não concatena nada
        {
            final List<String> iterable = Collections.emptyList();
            Actions.apply(iterable, action, condition);
            final String result = action.getResult();
            Assert.assertEquals("", result);
        }
    }

    /**
     * @see Actions#asSilentIgnoreErrors(Action)
     */
    @Test
    public void testAsSilentIgnoreErrors() throws Exception {
        final SilentAction<Integer, String> newAction = Actions.asSilentIgnoreErrors(ERROR_ACTION);
        final String result = Actions.apply(newAction, 1);
        assertNull(result);
    }

    /**
     * Caso em que uma {@link java.lang.RuntimeException} é lançada.
     *
     * @see Actions#asSilentThrowErrors(Action)
     */
    @Test(expected = Error.class)
    public void testAsSilentThrowErrors_error() throws Exception {
        final SilentAction<Integer, String> newAction = Actions.asSilentThrowErrors(ERROR_ACTION);
        Actions.apply(newAction, 1);
    }

    /**
     * Caso em que um {@link java.lang.Error} é lançado.
     *
     * @see Actions#asSilentThrowErrors(Action)
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testAsSilentThrowErrors_runtimeException() throws Exception {
        final SilentAction<Integer, String> newAction = Actions.asSilentThrowErrors(RUNTIME_EXCPT_ACTION);
        Actions.apply(newAction, 1);
    }

    /**
     * @see Actions#conditional(Object, Action, VoidAction)
     */
    @Test
    public void testConditional() throws Exception {
        //TODO
    }
}
