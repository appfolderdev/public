package br.com.simpou.pub.commons.utils.lang;

import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

@ClassTest(CollectionsHelper.class)

public class CollectionsHelperTest {

    @Test
    public void testAsSet() {
        //TODO criar testes
    }

    @Test
    public void testEquals() {
        final int n = 50;
        final Set<String> set1 = new HashSet<>();
        final Set<String> set2 = new TreeSet<>();

        //OK
        for (int i = 0; i < n; i++) {
            final String string = Randoms.getString();
            set1.add(string);
            set2.add(string);
        }

        assertTrue(CollectionsHelper.equals(set1, set2));

        // tamanhos diferentes
        String string;

        do {
            string = Randoms.getString();
            set1.add(string);
        } while (set1.size() == set2.size());

        assertFalse(CollectionsHelper.equals(set1, set2));

        // elementos diferentes
        set2.add(string + "error");
        assertFalse(CollectionsHelper.equals(set1, set2));

        // null set
        assertFalse(CollectionsHelper.equals(null, set2));
        assertFalse(CollectionsHelper.equals(set1, null));
        assertTrue(CollectionsHelper.equals(null, null));
    }

    @Test
    public void testSearch() {
        final List<String> list = Arrays.asList("v1", "v2", "v3");
        assertEquals(1, CollectionsHelper.search(list, "v2"));
        assertEquals(-1, CollectionsHelper.search(list, "v0"));
    }

    @Test
    public void testSubList() {
        final List<String> list = Arrays.asList("v1", "v2", "v3");
        assertEquals(Arrays.asList("v1"), CollectionsHelper.subList(list, 0, 1));
        assertEquals(Arrays.asList("v1", "v2", "v3"), CollectionsHelper.subList(list, 0, 10));
        assertEquals(Arrays.asList("v2", "v3"), CollectionsHelper.subList(list, 1, 2));
        assertTrue(CollectionsHelper.subList(list, 3, 2).isEmpty());
    }
}
