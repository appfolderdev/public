package br.com.simpou.pub.commons.utils.functional;

import br.com.simpou.pub.commons.utils.lang.RefHashMap;
import br.com.simpou.pub.commons.utils.tests.AssertExtension;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.TestOrder;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Ignore
@ClassTest(Conditions.class)
public class ConditionsTest {

    private static Conditions<Short> conditions;

    /**
     * @see Conditions#apply(Object)
     */
    @Test
    @TestOrder(2)
    public void testApply() throws Exception {
        Assert.assertTrue(conditions.apply((short) 3));
        Assert.assertTrue(conditions.apply((short) 5));
        Assert.assertNull(conditions.getLastFail());
        Assert.assertFalse(conditions.apply((short) -1));
        Assert.assertFalse(conditions.apply((short) 8));
        Assert.assertNotNull(conditions.getLastFail());
    }

    /**
     * Caso em que uma condição gera uma exceção.
     *
     * @see Conditions#apply(Object)
     */
    @TestOrder(4)
    @Test(expected = IllegalStateException.class)
    public void testApply_exception() throws Exception {
        conditions.apply((short) 50);
    }

    /**
     * Caso em que uma condição retorna null.
     *
     * @see Conditions#apply(Object)
     */
    @TestOrder(4)
    @Test(expected = IllegalStateException.class)
    public void testApply_null() throws Exception {
        conditions.apply((short) 500);
    }

    /**
     * @see Conditions#empty()
     */
    @Test
    public void testEmpty() throws Exception {
        final Conditions<Short> conditions = Conditions.empty();
        Assert.assertFalse(conditions.iterator().hasNext());
    }

    /**
     * @see Conditions#filter(Iterable, Condition)
     */
    @Test
    public void testFilter() {
        final Condition<Integer> condition = new Condition<Integer>() {
            @Override
            public Boolean apply(final Integer object) {
                return object > 3;
            }
        };
        final Iterable<Integer> values = Arrays.asList(1, 4, 8, 5, 3, 6);
        final List<Integer> filtered = Conditions.filter(values, condition);
        AssertExtension.assertCollectionEquals(Arrays.asList(4, 8, 5, 6), filtered);
    }

    /**
     * @see Conditions#from(Condition, Conditions)
     */
    @Test
    @TestOrder(1)
    public void testFrom_conditionConditions() throws Exception {
        conditions = Conditions.from(
                new FixedCondition<Short>(true),
                Conditions.from(Arrays.asList(
                        new Condition<Short>() {
                            @Override
                            public Boolean apply(final Short object) {
                                return object > 2;
                            }
                        },
                        new Condition<Short>() {
                            @Override
                            public Boolean apply(final Short object) {
                                if (object > 100) {
                                    return null;
                                } else if (object > 10) {
                                    throw new UnsupportedOperationException();
                                } else {
                                    return object < 6;
                                }
                            }

                            @Override
                            public String toString() {
                                return "Deve ser menor que 6";
                            }
                        }
                ))
        );
        Assert.assertNotNull(conditions);
    }

    /**
     * Executado por {@link Conditions#from(Condition, Conditions)}.
     *
     * @see Conditions#from(java.util.List)
     */
    @Test
    public void testFrom_list() throws Exception {

    }

    /**
     * Executado por {@link Conditions#from(java.util.List)}.
     *
     * @see Conditions#from(java.util.Set)
     */
    @Test
    public void testFrom_set() throws Exception {

    }

    /**
     * @see Conditions#getLastFail()
     */
    @Test
    @TestOrder(3)
    public void testGetLastFail() throws Exception {
        final String lastFail = conditions.getLastFail();
        Assert.assertEquals("Deve ser menor que 6", lastFail);
    }

    /**
     * @see Conditions#iterator()
     */
    @Test
    @TestOrder(2)
    public void testIterator() throws Exception {
        final Iterator<Condition<Short>> iterator = conditions.iterator();
        Assert.assertTrue(iterator.hasNext());
        iterator.next();
        iterator.next();
        iterator.next();
        Assert.assertFalse(iterator.hasNext());
    }

    /**
     * @see Conditions#negate(Condition)
     */
    @Test
    public void testNegate() {
        final Condition<Boolean> condition = Conditions.negate(new Condition<Boolean>() {
            @Override
            public Boolean apply(final Boolean t) {
                return t;
            }
        });
        assertTrue(condition.apply(false));
        assertFalse(condition.apply(true));
    }

    @Test
    public void testSeparate() throws Exception {
        final Condition<Integer> lowCond = new Condition<Integer>() {
            @Override
            public Boolean apply(final Integer value) {
                return value < 10;
            }
        };
        final Condition<Integer> highCond = new Condition<Integer>() {
            @Override
            public Boolean apply(final Integer value) {
                return value > 10;
            }
        };
        final List<Integer> values = Arrays.asList(1, 11, 12, 3, 10, 5);
        final RefHashMap<Condition<Integer>, List<Integer>> separated = Conditions.separate(
                values,
                Conditions.from(Arrays.asList(lowCond, highCond))
        );
        AssertExtension.assertCollectionEquals(
                Arrays.asList(1, 3, 5),
                separated.getByRef(lowCond)
        );
        AssertExtension.assertCollectionEquals(
                Arrays.asList(11, 12),
                separated.getByRef(highCond)
        );
        AssertExtension.assertCollectionEquals(
                Collections.singletonList(10),
                separated.getByRef(null)
        );
    }
}
