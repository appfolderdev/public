package br.com.simpou.pub.commons.utils.file;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.TestOrder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

import static br.com.simpou.pub.commons.utils.rand.Randoms.*;
import static org.junit.Assert.*;

@ClassTest(FileHelper.class)
public class FileHelperITCase {

    private static final int DIRS_LEVELS = 5;

    private static String rootDir;

    private static Map<String, String> filesStringMap;

    private static boolean isWindows;

    private final String fileExt1 = "txt";

    private final String fileExt2 = "html";

    private final String stringPattern = "strPttrn";

    @BeforeClass
    public static void setUpClass() throws Exception {
        rootDir = PropertyHelper.getTempDir() + getString(5, 10, false) +
                File.separator;
        FileHelper.mkdir(rootDir);
        isWindows = PropertyHelper.isWindows();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        assertTrue(FileHelper.delete(rootDir));
    }

    @Test
    public void testBackup() {
        //testWrite_4args
    }

    @Test
    public void testCopy() {
        //testMove
    }

    @Test
    public void testCount() {
        //testList
    }

    @Test
    @TestOrder(3)
    public void testDelete() throws Exception {
        final boolean deleted = FileHelper.delete(rootDir);
        assertTrue(deleted);
        assertFalse(FileHelper.exists(rootDir));
    }

    @Test
    @TestOrder(4)
    public void testDelete_error() throws Exception {
        final File fileRoot = new File(rootDir);
        final File file = new File(rootDir + "delete_error");
        FileHelper.write(file.getAbsolutePath(), "", false, false);
        assertTrue(file.exists());
        fileRoot.setWritable(false);

        try {
            FileHelper.delete(file.getAbsolutePath());
            assertTrue(isWindows);
        } catch (final IllegalArgumentException ex) {
        } finally {
            fileRoot.setWritable(true);
            FileHelper.delete(file.getAbsolutePath());
            assertFalse(file.exists());
        }
    }

    @Test
    public void testExists() {
        //testReadWrite
    }

    @Test
    public void testGetDelimPattern_1() {
        //UTCase
    }

    @Test
    public void testGetDelimPattern_2() {
        //UTCase
    }

    @Test
    public void testGetResourceFile() {
        //UTCase
    }

    @Test
    public void testLastModified()
            throws IOException, ParseException, InterruptedException {
        final Date now1;
        final Date now2;

        final String filePath = rootDir + getString(5, 10, false);
        final String content = getString(100, MAX_STRING_CHARS, true);
        final String content2 = getString(100, MAX_STRING_CHARS, true);
        FileHelper.write(filePath, content, false, false);
        now1 = FileHelper.lastModified(filePath);

        Thread.sleep(getInteger(1000, 2000));

        FileHelper.write(filePath, content2, false, false);
        now2 = FileHelper.lastModified(filePath);
        assertTrue(now1.before(now2));
        assertTrue(FileHelper.delete(filePath));
    }

    @Test
    public void testLength() {
        //testMove
    }

    @Test
    @TestOrder(1)
    public void testList() throws Exception {
        String name;
        String text;
        String content;

        filesStringMap = new HashMap<>();
        populateDirs(rootDir, 0);

        // todos arquivos recursivamente
        final List<String> list1 = FileHelper.list(rootDir, true, null);
        final Set<String> keySet = filesStringMap.keySet();
        assertEquals(keySet.size(), list1.size());

        // verifica conteúdo
        for (final String string : keySet) {
            list1.remove(string);
            // testa leitura/escrita
            content = filesStringMap.get(string);

            // arquivo, diretorio n tem conteudo
            if (content != null) {
                text = FileHelper.read(string);
                assertEquals(content, text);
            }
        }

        assertTrue(list1.isEmpty());

        // todos arquivos de segundo nivel
        final List<String> list2 = FileHelper.list(rootDir, false, null);
        final File[] listFiles = new File(rootDir).listFiles();
        assertEquals(listFiles.length, list2.size());
        assertEquals(listFiles.length, FileHelper.count(rootDir));

        for (final File file : listFiles) {
            list2.remove(file.getAbsolutePath());
        }

        assertTrue(list2.isEmpty());

        // todos arquivos com uma certa extensão
        final List<String> list3 = FileHelper.list(rootDir, true,
                new ExtensionFileNameFilter(this.fileExt1));

        // todos arquivos com nome exato, case sensitive
        final List<String> list4 = FileHelper.list(rootDir, true,
                new ContainsFileNameFilter(this.stringPattern, true, true));

        // todos arquivos com nome com certo padrão, case sensitive
        final List<String> list5 = FileHelper.list(rootDir, true,
                new ContainsFileNameFilter(this.stringPattern, true, false));

        // todos arquivos com nome exato, case não sensitive
        final List<String> list6 = FileHelper.list(rootDir, true,
                new ContainsFileNameFilter(this.stringPattern.toUpperCase(), false,
                        true));

        // todos arquivos que seguem um padrão
        final String pattern = "^" + this.stringPattern + "\\w+" + "." + this.fileExt1 + "$";
        final List<String> list7 = FileHelper.list(rootDir, true,
                new PatternFileNameFilter(pattern));

        for (final String string : keySet) {
            name = FileHelper.name(string);

            if (name.endsWith("." + this.fileExt1)) {
                assertTrue(list3.contains(string));
            }

            if (name.equals(this.stringPattern)) {
                assertTrue(list4.contains(string));
            }

            if (name.contains(this.stringPattern)) {
                assertTrue(list5.contains(string));
            }

            if (name.equals(this.stringPattern) ||
                    name.equals(this.stringPattern.toUpperCase()) ||
                    name.equals(this.stringPattern.toLowerCase())) {
                assertTrue(list6.contains(string));
            }

            if (name.matches(pattern)) {
                assertTrue(list7.contains(string));
            }
        }

        boolean deleted;

        if (!PropertyHelper.isWindows()) {
            // testa falha na remoção de um arquivo na recursão
            final File fileRoot = new File(rootDir);
            final File[] files = fileRoot.listFiles();
            File file = null;

            for (final File fileIter : files) {
                if (fileIter.isDirectory()) {
                    file = fileIter;

                    break;
                }
            }

            final boolean setWritableOK = file.setWritable(false);

            if (setWritableOK) {
                deleted = FileHelper.delete(rootDir);
                assertTrue(FileHelper.exists(rootDir));
                assertFalse(deleted);
                file.setWritable(true);
            }
        }

        deleted = FileHelper.delete(rootDir);
        assertTrue(deleted && !FileHelper.exists(rootDir));
    }

    @Test
    @TestOrder(2)
    public void testList_error() throws Exception {
        // diretório inexistente
        try {
            FileHelper.list(rootDir + "error", true, null);
            fail();
        } catch (final IllegalArgumentException e) {
        }

        // arquivo como diretório
        final String path = rootDir + "file";
        FileHelper.write(path, "", false, false);
        assertTrue(FileHelper.exists(path));

        try {
            FileHelper.list(path, true, null);
            fail();
        } catch (final IllegalArgumentException e) {
        }

        FileHelper.delete(path);
        assertFalse(FileHelper.exists(path));
    }

    @Test
    public void testMkdir() {
        //setUpClass
    }

    @Test
    public void testMove() throws Exception {
        final long length;
        final String text;
        final String name = getString(10, 20, false);
        final String srcDirPath = rootDir + getString(5, 10, false) + File.separator;
        final String tgtDirPath = rootDir + getString(5, 10, false) + File.separator;
        final String srcFilePath = srcDirPath + name;
        final String tgtFilePath = tgtDirPath + name;
        final String content = getString(100, MAX_STRING_CHARS, true);

        // testa escrita/leitura
        FileHelper.write(srcFilePath, content, false, false);

        // testa rollback
        final File file = new File(srcFilePath);
        file.setWritable(false);

        try {
            FileHelper.move(srcFilePath, tgtFilePath);
            fail();
        } catch (final RuntimeException ex) {
        }

        assertFalse(FileHelper.exists(tgtFilePath));

        // move
        file.setWritable(true);
        length = FileHelper.length(srcFilePath);
        FileHelper.move(srcFilePath, tgtFilePath);
        text = FileHelper.read(tgtFilePath);
        assertEquals(content, text);
        assertFalse(FileHelper.exists(srcFilePath));
        assertEquals(length, FileHelper.length(tgtFilePath));
        assertTrue(FileHelper.delete(tgtDirPath));
        assertTrue(FileHelper.delete(srcDirPath));
    }

    @Test
    public void testName() {
        //testList
    }

    @Test
    public void testNormalizeFilePathDelim() {
        //UTCase
    }

    @Test
    public void testPutDirPathEndDelim() throws IOException {
        //UTCase
    }

    @Test
    public void testReadResourceFile() throws IOException {
        final String exp = "Hash test file.";
        final String giv = new String(FileHelper.readResourceFile("hashTest.txt"));
        assertEquals(exp, giv);
    }

    @Test(expected = FileNotFoundException.class)
    public void testReadResourceFile_error() throws IOException {
        new String(FileHelper.readResourceFile("nopsFilePath"));
    }

    @Test
    public void testRead_File() throws Exception {
        //testWrite_4args
    }

    @Test
    public void testRead_String() throws Exception {
        //testWrite_4args
    }

    @Test
    public void testRead_String_String() throws Exception {
        //testWrite_5args
    }

    @Test
    public void testRead_error() throws Exception {
        String filePath;

        // arquivo inexistente
        filePath = rootDir + "a2#4$Rfgh.a";

        try {
            FileHelper.read(filePath);
            fail();
        } catch (final IllegalArgumentException ex) {
        }

        // sem permissão de leitura
        filePath = rootDir + "unreadable";
        FileHelper.write(filePath, "unreadable", false, false);

        final File file = new File(filePath);
        file.setReadable(false);
        assertFalse(!isWindows && file.canRead());

        try {
            FileHelper.read(filePath);
            assertTrue(isWindows); //prop não funciona no windows
        } catch (final IllegalArgumentException ex) {
        } finally {
            FileHelper.delete(filePath);
        }
    }

    @Test
    public void testRename() throws IOException, ParseException {
        final long length;
        boolean renamed;
        final String text;
        final String srcName = getString(10, 20, false);
        final String tgtName = getString(10, 20, false);
        final String filePath = rootDir + getString(5, 10, false) + File.separator;
        final String srcFilePath = filePath + srcName;
        final String tgtFilePath = filePath + tgtName;
        final String content = getString(100, MAX_STRING_CHARS, true);

        // cria arquivo a ser renomeado
        FileHelper.write(srcFilePath, content, false, false);

        // renomeia pro mesmo nome
        try {
            FileHelper.rename(srcFilePath, srcFilePath);
            fail();
        } catch (final IllegalArgumentException e) {
        }

        // renomeia pra um diretório diferente
        try {
            FileHelper.rename(srcFilePath, "otherDir");
            fail();
        } catch (final IllegalArgumentException e) {
        }

        if (!PropertyHelper.isWindows()) {
            // renomeia pra um arquivo ja existente
            final File fileRoot = new File(filePath);
            fileRoot.setWritable(false);
            renamed = FileHelper.rename(srcFilePath, tgtFilePath);
            assertFalse(renamed);
            fileRoot.setWritable(true);
        }

        // renomeia
        length = FileHelper.length(srcFilePath);
        renamed = FileHelper.rename(srcFilePath, tgtFilePath);
        text = FileHelper.read(tgtFilePath);
        assertTrue(renamed);
        assertEquals(content, text);
        assertFalse(FileHelper.exists(srcFilePath));
        assertEquals(length, FileHelper.length(tgtFilePath));
        assertTrue(FileHelper.delete(filePath));
    }

    @Test
    public void testWrite_3args() throws Exception {
        //testBackup
    }

    @Test
    public void testWrite_4args() throws Exception {
        String text;
        boolean deleted;
        final String filePath = rootDir + getString(10, 20, false);
        final String content1 = getString(1, MAX_STRING_CHARS, true);
        final String content2 = getString(1, MAX_STRING_CHARS, true);
        final String newContent = content2 + content1;

        // testa escrita/leitura
        FileHelper.write(filePath, content2, false, false);
        text = new String(FileHelper.read(new File(filePath)));
        assertEquals(content2, text);

        // testa concatenação de conteúdo
        FileHelper.write(filePath, content1, true, false);
        text = FileHelper.read(filePath);
        assertEquals(newContent, text);

        // testa realização de backup
        final String bkpFilePath = FileHelper.write(filePath, "", false, true);
        text = FileHelper.read(filePath);
        assertTrue(text.isEmpty());
        text = FileHelper.read(bkpFilePath);
        assertEquals(newContent, text);

        // limpa arquivos criados
        deleted = FileHelper.delete(filePath);
        assertTrue(deleted && !FileHelper.exists(filePath));
        deleted = FileHelper.delete(bkpFilePath);
        assertTrue(deleted && !FileHelper.exists(bkpFilePath));
    }

    @Test
    public void testWrite_5args() throws Exception {
        final String text = "áàõụ" + getString(100, MAX_STRING_CHARS, true);
        final String filePath = rootDir + getString(10, 20, false);
        FileHelper.write(filePath, text, false, false, FileHelper.UTF8_ENCODING);

        final String textRead = FileHelper.read(filePath, FileHelper.UTF8_ENCODING);
        assertEquals(text, textRead);

        final String textReadError = FileHelper.read(filePath, FileHelper.ISO_ENCODING);
        assertFalse(text.equals(textReadError));

        FileHelper.delete(filePath);
        assertFalse(FileHelper.exists(filePath));
    }

    private String getFileName() {
        String fileName;

        // cria file com nome com padrão de busca
        if (getBoolean() && getBoolean()) {
            // gera com padrão exato
            fileName = this.stringPattern;

            // gera com o padrão no início
            if (getBoolean()) {
                fileName += getString(3, 6, false);
            }
        } else {
            // gera um nome qualquer
            fileName = getString(1, 10, false);
        }

        return fileName;
    }

    private void populateDirs(final String dir, final int level)
            throws IOException, ParseException {
        String dirPathAux;
        final int dirs;
        final Set<String> dirPaths;

        populateFiles(dir);

        // cria novo nível
        if (level < (DIRS_LEVELS - 1)) {
            dirs = getInteger(3, 5);

            // lista diretórios
            dirPaths = new HashSet<>();

            while (dirPaths.size() < dirs) {
                dirPathAux = dir + getFileName();

                if (!FileHelper.exists(dirPathAux)) {
                    dirPaths.add(dirPathAux);
                }
            }

            // cria diretórios
            for (final String dirPath : dirPaths) {
                FileHelper.mkdir(dirPath);
                assertTrue(FileHelper.exists(dirPath));
                filesStringMap.put(dirPath, null);
            }

            // popula diretórios
            for (final String dirPath : dirPaths) {
                populateDirs(dirPath + File.separator, level + 1);
            }
        }
    }

    private void populateFiles(final String dir) throws IOException, ParseException {
        String fileContent;
        final int items = getInteger(3, 5);
        final Set<String> filePaths = new HashSet<>();

        // lista arquivos a serem criados
        String filePathAux;

        while (filePaths.size() < items) {
            filePathAux = dir + getFileName();

            if (!filePathAux.endsWith(this.stringPattern)) {
                filePathAux += ".";
                filePathAux += (getBoolean() ? this.fileExt1 : this.fileExt2);
            }

            if (!FileHelper.exists(filePathAux)) {
                filePaths.add(filePathAux);
            }
        }

        //cria arquivo
        for (final String filePath : filePaths) {
            fileContent = getString(1, MAX_STRING_CHARS, true);
            FileHelper.write(filePath, fileContent, false, false);
            assertTrue(FileHelper.exists(filePath));
            filesStringMap.put(filePath, fileContent);
        }
    }
}
