package br.com.simpou.pub.commons.utils.cipher;

import br.com.simpou.pub.commons.utils.TestsConstants;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

@ClassTest(Hashs.class)
public class HashsTest {

    /**
     * Hash do binário da classe a ser testada.
     */
    private static final String hash = "99ee7c675f44370c3bb0227cad2240c7db6052005cf87fb2f377b148d4d1110e";

    /**
     * @see Hashs#getHashNoErrors(String)
     */
    @Test(expected = RuntimeException.class)
    public void testGetHashNoErrors() {
        final String hashGen;
        final String hash = "bf74e4a280affafbdf6692bc6a9f3d66b03094fbfb4a91589bc7fd6b32664fdb";
        final String text = "jonas";

        hashGen = Hashs.getHashNoErrors(text);
        assertEquals(64, hashGen.length());
        assertEquals(hash, hashGen);

        new Hashs().getHashNoErrors(null);//RuntimeException
    }

    /**
     * @see Hashs#getHash(java.io.File)
     */
    @Test
    public void testGetHash_File() throws Exception {
        final String classPath = TestsConstants.RESOURCES_TEST_DIR + "hashTest.txt";
        final File fileClassPath = new File(classPath);
        final String hashGen = Hashs.getHash(fileClassPath);
        assertEquals(hash, hashGen);
    }

    /**
     * @see Hashs#getHash(String)
     */
    @Test
    public void testGetHash_String() throws Exception {
        final String hashGen;
        final String hash = "ed02457b5c41d964dbd2f2a609d63fe1bb7528dbe55e1abf5b52c249cd735797";
        final String text = "aaaaaa";

        hashGen = Hashs.getHash(text);
        assertEquals(64, hashGen.length());
        assertEquals(hash, hashGen);
    }
}
