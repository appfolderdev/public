package br.com.simpou.pub.commons.utils.lang;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

@ClassTest(Encapsulations.class)

public class EncapsulationsTest {

    @Test
    public void testGetDate() throws Exception {
        assertNull(Encapsulations.getDate(null));
        final Date date = new Date();
        final Date clone = new Encapsulations().getDate(date);
        assertNotSame(date, clone);
        assertEquals(date, clone);
    }
}
