package br.com.simpou.pub.commons.utils.file;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

@ClassTest(VelocityHelper.class)

public class VelocityHelperITCase {

    @Test
    public void testMerge() throws Exception {
        final VelocityHelper template = new VelocityHelper("TestVelocityTemplate.vsl");
        final String msg = "Hello velocity";
        final String msgMerged = "Message: " + msg + ". Type: type ";
        final String msgMergedType1 = msgMerged + " 1";
        final String msgMergedType2 = msgMerged + " 2";
        String source;

        template.setParameter("msg", msg);
        template.setParameter("type", true);
        source = template.merge();
        assertEquals(msgMergedType1, source);

        template.setParameter("type", false);
        source = template.merge();
        assertEquals(msgMergedType2, source);

        // somente para cobrir teste para o caso do ENGINE já estar inicializado.
        final VelocityHelper template2 = new VelocityHelper(
                "TestVelocityTemplate.vsl");
    }

    @Test
    public void testSetParameter() {
        //testMerge
    }
}
