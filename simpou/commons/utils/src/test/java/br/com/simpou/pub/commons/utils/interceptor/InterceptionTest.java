package br.com.simpou.pub.commons.utils.interceptor;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

interface SuperActionIface {

    int doMore(Integer p1);
}

interface ActionIface extends SuperActionIface {

    String doSomething(int p1, String p2, boolean throwException);
}

@ClassTest(Interception.class)

public class InterceptionTest {

    private static String states;

    private static ActionClass actionClass;

    private static ActionInterceptor actionInterceptor;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        actionClass = new ActionClass();
        actionInterceptor = new ActionInterceptor();
    }

    @Before
    public void setUp() {
        states = "";
    }

    /**
     * @see Interception#getContext(Interceptor)
     */
    @Test
    public final void testGetContext() {
        // testNewInstance
    }

    /**
     * @see Interception#newInstance(Class, java.lang.reflect.Method...)
     */
    @Test
    public final void testNewInstance() throws NoSuchMethodException, SecurityException {
        {
            final Interception<ActionIface> interception = Interception.newInstance(ActionIface.class);
            final InterceptionContext<ActionIface> context = interception.getContext(actionInterceptor);
            final ActionIface intercepted = context.intercept(actionClass);

            String stringResult;
            final int intResult;

            // intercepta antes e depois
            stringResult = intercepted.doSomething(1, "j", false);
            assertEquals("1j1", stringResult);
            assertEquals("01", states);

            setUp();

            // intercepta antes, depois e se houver error
            stringResult = intercepted.doSomething(1, "j", true);
            assertEquals("error", stringResult);
            assertEquals("021", states);

            setUp();

            // não deve interceptar métodos não declarados
            intercepted.equals(Boolean.FALSE);
            assertEquals("", states);

            setUp();

            intResult = intercepted.doMore(1);
            assertEquals(2, intResult);
            assertEquals("01", states);

            setUp();
        }
        {
            final Interception<ActionIface> interception = Interception.newInstance(
                    ActionIface.class,
                    ActionIface.class.getMethod("doMore", Integer.class)
            );
            final InterceptionContext<ActionIface> context = interception.getContext(new ActionInterceptor2());
            final ActionIface intercepted = context.intercept(actionClass);
            final String stringResult = intercepted.doSomething(2, "j", false);
            assertEquals("2j2", stringResult);
            assertEquals("", states);

            setUp();

            intercepted.doMore(1);
            assertEquals("ab", states);
        }
    }

    private static class ActionInterceptor extends AbstractInterceptorImpl {

    }

    private static class ActionInterceptor2 extends AbstractInterceptorImpl {

        @Override
        public void doAfter(final Object result) {
            InterceptionTest.states += "b";
        }

        @Override
        public void doBefore(final Method method, final Object[] args) {
            InterceptionTest.states += "a";
        }
    }

    private static class AbstractInterceptorImpl implements Interceptor {

        @Override
        public void doAfter(final Object result) {
            InterceptionTest.states += 1;
        }

        @Override
        public void doBefore(final Method method, final Object[] args) {
            InterceptionTest.states += 0;
        }

        @Override
        public Object doOnError(final Throwable t) throws Throwable {
            InterceptionTest.states += 2;

            return "error";
        }
    }
}

class ActionClass implements ActionIface {

    private int countInvokes = 0;

    @Override
    public int doMore(final Integer p1) {
        return p1 + 1;
    }

    @Override
    public String doSomething(final int p1, final String p2, final boolean throwException) {
        if (throwException) {
            throw new IllegalStateException();
        } else {
            return p1 + p2 + (++this.countInvokes);
        }
    }
}
