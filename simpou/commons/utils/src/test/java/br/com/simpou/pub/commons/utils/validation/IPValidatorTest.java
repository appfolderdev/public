package br.com.simpou.pub.commons.utils.validation;

import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.violation.Violation;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@ClassTest(IPValidator.class)

public class IPValidatorTest {

    private final Randoms rand = new Randoms();

    @Test
    public void testIsValid() {
        final int v = this.rand.getInteger(0, 255);
        final int iv = this.rand.getInteger(-1, 255, false);
        final String iv2 = this.rand.getString(1, 3, false);
        final String s = ".";

        assertNull(IPValidator.isValid(v + s + v + s + v + s + v));

        assertNotNull(IPValidator.isValid(iv + s + v + s + v + s + v));
        assertNotNull(IPValidator.isValid(v + s + iv + s + v + s + v));
        assertNotNull(IPValidator.isValid(v + s + v + s + iv + s + v));
        assertNotNull(IPValidator.isValid(v + s + v + s + v + s + iv));

        assertNotNull(IPValidator.isValid(iv2 + s + v + s + v + s + v));
        assertNotNull(IPValidator.isValid(v + s + iv2 + s + v + s + v));
        assertNotNull(IPValidator.isValid(v + s + v + s + iv2 + s + v));
        assertNotNull(IPValidator.isValid(v + s + v + s + v + s + iv2));

        assertNotNull(IPValidator.isValid(v + s + v + s + v));
        assertNotNull(IPValidator.isValid(v + s + v));
        assertNotNull(IPValidator.isValid(v + ""));

        assertNotNull(IPValidator.isValid(this.rand.getString(1, 6, true)));
        assertNotNull(IPValidator.isValid(this.rand.getString(-1, 15, true, false)));
    }

    @Test
    public void testIsValid_2() {
        assertNull(IPValidator.isValid(null));
        assertNull(IPValidator.isValid(""));
    }

    @Test
    public void testValidate() {
        final IPValidator validator = new IPValidator();

        // ok
        final Violation violationOk = validator.validate("127.0.0.1");
        assertNull(violationOk);

        // not ok
        final Violation violationNotOk = validator.validate("256.0.0.0");
        Assert.assertEquals(IPValidator.NOT_MATCH, violationNotOk.getCode());
    }
}
