package br.com.simpou.pub.commons.utils.file;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Test;

import java.io.File;
import java.io.FilenameFilter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@ClassTest(SubDirectoryFileNameFilter.class)
public class SubDirectoryFileNameFilterTest {

    @Test
    public void testAccept() {
        FilenameFilter filter;

        filter = new SubDirectoryFileNameFilter(false, new String[]{"p1", "p2"});
        assertTrue(filter.accept(new File("p1/p2"), null));
        assertFalse(filter.accept(new File("p2/p1"), null));
        assertFalse(filter.accept(new File("p2/p2"), null));
        assertTrue(filter.accept(new File("xx/p1/p2"), null));
        assertTrue(filter.accept(new File("xx/p1/p2/yy"), null));
        assertTrue(filter.accept(new File("xx/p1/zz/p2/yy"), null));

        filter = new SubDirectoryFileNameFilter(true, new String[]{"p1", "p2"});
        assertTrue(filter.accept(new File("p1/p2"), null));
        assertFalse(filter.accept(new File("p2/p1"), null));
        assertTrue(filter.accept(new File("xx/p1/p2"), null));
        assertTrue(filter.accept(new File("xx/p1/p2/yy"), null));
        assertFalse(filter.accept(new File("xx/p1/zz/p2/yy"), null));
    }
}
