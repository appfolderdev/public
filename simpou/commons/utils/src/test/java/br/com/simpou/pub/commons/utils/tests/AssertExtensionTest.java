package br.com.simpou.pub.commons.utils.tests;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.validation.Validatable;
import br.com.simpou.pub.commons.utils.violation.Violation;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import static br.com.simpou.pub.commons.utils.tests.AssertExtension.*;

@ClassTest(AssertExtension.class)
@Ignore
public class AssertExtensionTest {

    @Test
    public void testAssertCollectionEquals() {
        //TODO
    }

    @Test
    public void testAssertDeepEquals() {
        //TODO
    }

    @Test
    public void testAssertEmpty_Collection() {
        assertEmpty(new ArrayList<>());
    }

    @Test(expected = AssertionError.class)
    public void testAssertEmpty_Collection_notEmpty() {
        assertEmpty(Arrays.asList("item1"));
    }

    @Test(expected = AssertionError.class)
    public void testAssertEmpty_Collection_null() {
        final Collection<?> collection = null;
        assertEmpty(collection);
    }

    @Test
    public void testAssertEmpty_ObjectArr() {
        assertEmpty(new String[]{});
    }

    @Test(expected = AssertionError.class)
    public void testAssertEmpty_ObjectArr_notEmpty() {
        assertEmpty(new String[]{"item", "item"});
    }

    @Test(expected = AssertionError.class)
    public void testAssertEmpty_ObjectArr_null() {
        final Double[] array = null;
        assertEmpty(array);
    }

    @Test
    public void testAssertEndsWith() {
        assertEndsWith("end with", "with");
    }

    @Test(expected = AssertionError.class)
    public void testAssertEndsWith_error() {
        assertEndsWith("end with", "withx");
    }

    @Test
    public void testAssertGreaterThan() {
        assertGreaterThan(2, 1);
        assertGreaterThan(2.2565, 2.2564);
    }

    @Test(expected = AssertionError.class)
    public void testAssertGreaterThan_error() {
        assertGreaterThan(2.2565, 2.2566);
    }

    @Test
    public void testAssertInRange() {
        assertInRange(1, 0, 2);
        assertInRange(1, 0.99999, 1.00001);
    }

    @Test(expected = AssertionError.class)
    public void testAssertInRange_greater() {
        assertInRange(3, 0, 2);
    }

    @Test(expected = AssertionError.class)
    public void testAssertInRange_lower() {
        assertInRange(-1, 0, 2);
    }

    @Test
    public void testAssertLowerThan() {
        assertLowerThan(1, 2);
        assertLowerThan(2.2564, 2.2565);
    }

    @Test(expected = AssertionError.class)
    public void testAssertLowerThan_error() {
        assertLowerThan(2.2565, 2.2564);
    }

    @Test
    public void testAssertNotEmpty_Collection() {
        assertNotEmpty(Arrays.asList("item1"));
    }

    @Test(expected = AssertionError.class)
    public void testAssertNotEmpty_Collection_empty() {
        assertNotEmpty(new HashSet<>());
    }

    @Test(expected = AssertionError.class)
    public void testAssertNotEmpty_Collection_null() {
        final Collection<?> collection = null;
        assertNotEmpty(collection);
    }

    @Test
    public void testAssertNotEmpty_ObjectArr() {
        assertNotEmpty(new String[]{"item", "item"});
    }

    @Test(expected = AssertionError.class)
    public void testAssertNotEmpty_ObjectArr_empty() {
        assertNotEmpty(new String[]{});
    }

    @Test(expected = AssertionError.class)
    public void testAssertNotEmpty_ObjectArr_null() {
        final Double[] array = null;
        assertNotEmpty(array);
    }

    @Test
    public void testAssertNotEquals() {
        assertNotEquals("string1", "string2");
    }

    @Test(expected = AssertionError.class)
    public void testAssertNotEquals_error() {
        assertNotEquals("string", "string");
    }

    @Test
    public void testAssertStartsWith() {
        assertStartsWith("start with", "star");
    }

    @Test(expected = AssertionError.class)
    public void testAssertStartsWith_error() {
        assertStartsWith("start with", "starx");
    }

    @Test
    public void testAssertValid_GenericType_Validator() {
        //TODO
    }

    @Test
    public void testAssertValid_Validatable() {
        assertValid(new Validatable() {
            @Override
            public Violation validate() {
                return null;
            }
        });
    }

    @Test(expected = AssertionError.class)
    public void testAssertValid_error() {
        assertValid(new Validatable() {
            @Override
            public Violation validate() {
                throw new IllegalArgumentException();
            }
        });
    }
}
