package br.com.simpou.pub.commons.utils.lang;

import org.junit.Test;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertEquals;

/**
 * Created by jonas on 4/17/16.
 */
public class IOHelperTest {

    @Test
    public void testToString() throws Exception {
        final String original = "test read";
        final String read = IOHelper.toString(new ByteArrayInputStream(original.getBytes()));
        assertEquals(original, read);
    }

    @Test
    public void testToBytes() throws Exception {

    }

}
