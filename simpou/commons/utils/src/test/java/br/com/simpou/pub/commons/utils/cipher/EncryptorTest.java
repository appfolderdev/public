package br.com.simpou.pub.commons.utils.cipher;

import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.Repeat;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

@ClassTest(Encryptor.class)

public class EncryptorTest {

    private static final int N_ALGS = 4;

    private static final int N_TESTS_PER_ALG = 10;

    private static int algChooser;

    @BeforeClass
    public static void setUpClass() throws Exception {
        algChooser = -1;
    }

    @Before
    public void setUp() {
        algChooser = ((algChooser + 1) % N_ALGS);
    }

    @Test
    public void testCryptBase64() {
        //todo
    }

    /**
     * @see Encryptor#cryptMD5(String)
     */
    @Test
    public void testCryptMD5() throws Exception {
        final String textCrypt;
        final String crypted = "58a6ddae6132beab46409f221bd70505";
        final String decrypted = "Jonas Pereira";

        textCrypt = Encryptor.cryptMD5(decrypted);
        assertEquals(crypted, textCrypt);
    }

    /**
     * @see Encryptor#cryptSHA256(String)
     */
    @Test
    public void testCryptSHA256() throws Exception {
        final String textCrypt;
        final String crypted = "ed02457b5c41d964dbd2f2a609d63fe1bb7528dbe55e1abf5b52c249cd735797";
        final String decrypted = "aaaaaa";

        textCrypt = Encryptor.cryptSHA256(decrypted);
        assertEquals(crypted, textCrypt);
    }

    @Test
    public void testCryptSha1() {
        //todo
    }

    /**
     * Testa encriptação/decriptação para textos conhecidos.
     *
     * @see Encryptor#crypt(String)
     * @see Encryptor#decrypt(String)
     */
    @Test
    public void testCrypt_3() throws Exception {
        String textCrypt;
        Encryptor cryptHelper;
        final String text = "Jonas Pereira";
        final String crypt = "05yD8MVfdpVYQZjtk6/aQw==";
        final String key = "abc123@!#";
        final String crypt2 = "Omrts/tnY9wXtB84dhvYrw==";
        final String key2 = "alves";

        cryptHelper = new Encryptor(key);
        textCrypt = cryptHelper.crypt(text);
        assertEquals(crypt, textCrypt);

        cryptHelper = new Encryptor(key2);
        textCrypt = cryptHelper.crypt(text);
        assertEquals(crypt2, textCrypt);

    }

    /**
     * Palavra chave não pode ser maior que o limite de caracteres.
     *
     * @see Encryptor#Encryptor(String)
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCrypt_maxKeyLength() throws Exception {
        new Encryptor(Randoms.getString(Encryptor.MAX_KEY_LENGTH + 1, true));
    }

    /**
     * Palavra chave não pode ser vazia.
     *
     * @see Encryptor#Encryptor(String)
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCrypt_minKeyLength() throws Exception {
        new Encryptor("");
    }

    /**
     * Palavra chave não pode ser nula.
     *
     * @see Encryptor#Encryptor(String)
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCrypt_nullKey() throws Exception {
        new Encryptor(null);
    }

    /**
     * Testa encriptação/decriptação de vários textos para vários algoritmos.
     *
     * @see Encryptor#crypt(String)
     * @see Encryptor#Encryptor(String)
     */
    @Test
    @Repeat(N_TESTS_PER_ALG)
    public void testCrypt_passPhrase() throws Exception {
        final String passPhrase = Randoms.getString(1, Encryptor.MAX_KEY_LENGTH, true);
        final String secretString = Randoms.getString(10, 50, true);

        // Create encrypter/decrypter class
        final Encryptor encrypter = new Encryptor(passPhrase);

        // Encrypt the string
        final String encrypted = encrypter.crypt(secretString);

        // Decrypt the string
        final String decrypted = encrypter.decrypt(encrypted);

        // verification
        assertNotSame(secretString, encrypted);
        assertEquals(secretString, decrypted);
    }

    /**
     * Testa encriptação/decriptação de vários textos para vários algoritmos.
     *
     * @see Encryptor#crypt(String)
     * @see Encryptor#Encryptor(javax.crypto.SecretKey, String)
     */
    @Test
    @Repeat(N_ALGS * N_TESTS_PER_ALG)
    public void testCrypt_secretKey() throws Exception {
        final String secretString = Randoms.getString(10, 50, true);
        String algorithm = null;

        switch (algChooser) {
            case 0:
                algorithm = Algorithms.BLOWFISH;
                break;
            case 1:
                algorithm = Algorithms.DES;
                break;
            case 2:
                algorithm = Algorithms.DESEDE;
                break;
            case 3:
                algorithm = new Algorithms().TRIPLE_DES;
                break;
        }

        // Generate a temporary key for this example. In practice, you would
        // save this key somewhere. Keep in mind that you can also use a
        // Pass Phrase.
        final SecretKey secretKey = KeyGenerator.getInstance(algorithm).generateKey();

        // Create encrypter/decrypter class
        final Encryptor encrypter = new Encryptor(secretKey, secretKey.getAlgorithm());

        // Encrypt the string
        final String encrypted = encrypter.crypt(secretString);

        // Decrypt the string
        final String decrypted = encrypter.decrypt(encrypted);

        // verification
        assertNotSame(secretString, encrypted);
        assertEquals(secretString, decrypted);
    }

    /**
     * Testado por {@link #testCrypt_secretKey()} e {@link #testCrypt_passPhrase()}.
     *
     * @see Encryptor#crypt(String)
     */
    @Test
    public void testDecrypt() {
    }

}
