package br.com.simpou.pub.commons.utils.lang;

import br.com.simpou.pub.commons.utils.string.Strings;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.Test;

import java.io.Serializable;
import java.util.*;

import static org.junit.Assert.*;

@ClassTest(Assertions.class)

public class AssertionsTest {

    private static final String CUSTOM_MESSAGE = "custom message";

    private static final TestObject testObj = new TestObject(3);

    @Test
    public void testGetErrorMsg() {
        //testIsNull_String_ObjectArr
    }

    @Test
    public void testGetParamMessage_3args() {
        //testGetParamMessage_String_String
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetParamMessage_3args_error() {
        Assertions.getParamMessage("no args msg", "invalid msg", "param");
    }

    @Test
    public void testGetParamMessage_String_String() {
        final String paramName = "param1";
        final String paramMsg = Assertions.getParamMessage(Assertions.MSG_NOT_IN_RANGE, paramName);
        final String replacedMsg = Strings.replaceParams(Assertions.PARAM_MSG, paramName);
        assertEquals(replacedMsg + Assertions.MSG_NOT_IN_RANGE, paramMsg);
    }

    @Test
    public void testGreaterThan_3args() {
        //testGreaterThan_GenericType_GenericType_error
    }

    @Test
    public void testGreaterThan_GenericType_GenericType() {
        final Integer num = 2;
        assertEquals(num, Assertions.greaterThan(num, 1));
        assertEquals(num, Assertions.greaterThan(num, 0));
        assertNull(Assertions.greaterThan(null, 0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGreaterThan_GenericType_GenericType_error() {
        Assertions.greaterThan(2, 3, CUSTOM_MESSAGE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGreaterThan_GenericType_GenericType_error_2() {
        Assertions.greaterThan(0f, 0f, CUSTOM_MESSAGE);
    }

    @Test
    public void testInRange_3args() {
        final Integer num = 2;
        assertEquals(num, Assertions.inRange(num, 2, 4));
        assertEquals(num, Assertions.inRange(num, 0, 2));
        assertEquals(num, Assertions.inRange(num, 1, 3));
        assertNull(Assertions.inRange(null, 0, 3));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInRange_3args_error1() {
        Assertions.inRange(2, 3, 5, CUSTOM_MESSAGE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInRange_3args_error2() {
        Assertions.inRange(6, 3, 5);
    }

    @Test
    public void testInRange_4args() {
        //testInRange_3args_error1
    }

    @Test
    public void testInstanceOf_3args() {
        //testInstanceOf_Class_Object
    }

    @Test
    public void testInstanceOf_Class_Object() {
        final Integer num = 7;
        final Object objInteger = num;
        assertEquals(num.intValue(),
                Assertions.instanceOf(Integer.class, objInteger).intValue());

        assertNull(Assertions.instanceOf(Integer.class, null));
    }

    @Test(expected = ClassCastException.class)
    public void testInstanceOf_error() {
        final String num = "string";
        final Object objInteger = num;
        Assertions.instanceOf(Integer.class, objInteger);
    }

    @Test
    public void testInterfaceRequired_Class() {
        final Class<?> clasz = Serializable.class;
        assertSame(clasz, Assertions.interfaceRequired(clasz, null));
        assertNull(Assertions.interfaceRequired(null));
    }

    @Test
    public void testInterfaceRequired_Class_String() {
        //testInterfaceRequired_Class
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInterfaceRequired_Class_error() {
        Assertions.interfaceRequired(this.getClass(), null);
    }

    @Test
    public void testIsFalse() {
        final String msg = "trueMessage";
        final TestObject testObjRet = Assertions.isFalse(testObj, false, msg);
        assertSame(testObj, testObjRet);
        assertEquals(testObj, testObjRet);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsFalse_error() {
        Assertions.isFalse(testObj, true, null);
    }

    @Test
    public void testIsNull_GenericType() {
        final String value = null;
        assertNull(Assertions.isNull(value));
    }

    @Test
    public void testIsNull_GenericType_String() {
        final String value = "not null";
        final String message = "message";

        try {
            Assertions.isNull(value, message);
            fail();
        } catch (final IllegalArgumentException e) {
            assertEquals(Assertions.getErrorMsg(value, message), e.getMessage());
        }
    }

    @Test
    public void testIsNull_GenericType_error() {
        final String value = "not null";

        try {
            Assertions.isNull(value);
            fail();
        } catch (final IllegalArgumentException e) {
            assertEquals(Assertions.getErrorMsg(value, Assertions.MSG_NULL), e.getMessage());
        }
    }

    @Test
    public void testIsNull_ObjectArr() {
        Assertions.isNull(null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsNull_ObjectArr_error() {
        Assertions.isNull(null, null, Boolean.TRUE);
    }

    @Test
    public void testIsNull_String_ObjectArr() {
        final String message = "message";

        try {
            Assertions.isNull(message, null, null, Boolean.TRUE);
            fail();
        } catch (final IllegalArgumentException e) {
            assertEquals(Assertions.getErrorMsg(true, message), e.getMessage());
        }
    }

    @Test
    public void testIsTrue() {
        // testIsFalse
    }

    @Test
    public void testLengthRequired_3args() {
        //testLengthRequired_GenericType_int
    }

    @Test
    public void testLengthRequired_GenericType_int() {
        final String[] array = {"aa", "bb"};
        assertSame(array, Assertions.lengthRequired(array, 2));
        assertNull(Assertions.lengthRequired(null, 2, null));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLengthRequired_GenericType_int_error() {
        final String[] array = {"aa", "bb", "c"};
        Assertions.lengthRequired(array, 2);
    }

    @Test
    public void testLowerThan_3args() {
        //testLessThan_GenericType_GenericType_error
    }

    @Test
    public void testLowerThan_GenericType_GenericType() {
        final Integer num = 2;
        assertEquals(num, Assertions.lowerThan(num, 3));
        assertEquals(num, Assertions.lowerThan(num, 4));
        assertNull(Assertions.lowerThan(null, 0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLowerThan_GenericType_GenericType_error() {
        Assertions.lowerThan(2, 1, CUSTOM_MESSAGE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLowerThan_GenericType_GenericType_error_2() {
        Assertions.lowerThan(1D, 1D, CUSTOM_MESSAGE);
    }

    @Test
    public void testNotEmpty_2args_1() {
        //testNotEmpty_GenericType
    }

    @Test
    public void testNotEmpty_2args_2() {
        //testNotEmpty_Class_GenericType
    }

    @Test
    public void testNotEmpty_Class_GenericType() {
        final Integer[] array = {1, 2, 4};
        assertSame(array, Assertions.notEmpty(Integer.class, array));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotEmpty_Class_GenericType_error() {
        Assertions.notEmpty(String.class, new String[]{});
    }

    @Test
    public void testNotEmpty_GenericType() {
        final Queue<String> collection = new PriorityQueue<>();
        collection.add("opa");

        final Queue<String> collectionRet = Assertions.notEmpty(collection);
        assertSame(collection, collectionRet);
    }

    @Test
    public void testNotEmpty_GenericType_String() {
        //testNotEmpty_GenericType_error1
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotEmpty_GenericType_error1() {
        Assertions.notEmpty(new TreeSet<String>(), CUSTOM_MESSAGE);
    }

    @Test(expected = NullPointerException.class)
    public void testNotEmpty_GenericType_error2() {
        final Set<Character> collection = null;
        Assertions.notEmpty(collection);
    }

    @Test
    public void testNotEmpty_String() {
        final String string = "jonas";
        final String stringRet = Assertions.notEmpty(string);
        assertSame(string, stringRet);
    }

    @Test
    public void testNotEmpty_String_String() {
        //testNotEmpty_String_error1
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotEmpty_String_error1() {
        Assertions.notEmpty("", CUSTOM_MESSAGE);
    }

    @Test(expected = NullPointerException.class)
    public void testNotEmpty_String_error2() {
        final String string = null;
        Assertions.notEmpty(string);
    }

    @Test
    public void testNotNull_2args_1() {
        //testNotNull_Class_GenericType
    }

    @Test
    public void testNotNull_2args_2() {
        final Long[] array = {};
        assertSame(array, Assertions.notNull(array, ""));
    }

    @Test
    public void testNotNull_Class_GenericType() {
        final Long[] array = {};
        assertSame(array, Assertions.notNull(Long.class, array));
    }

    @Test
    public void testNotNull_GenericType() {
        final AssertionsTest ret = Assertions.notNull(this);
        assertSame(this, ret);
    }

    @Test
    public void testNotNull_GenericType_String() {
        //testNotNull_ObjectArr_error
    }

    @Test
    public void testNotNull_ObjectArr() {
        Assertions.notNull(1, "", this.getClass(), 'q');
    }

    @Test(expected = NullPointerException.class)
    public void testNotNull_ObjectArr_error() {
        Assertions.notNull(CUSTOM_MESSAGE, "", null, 1);
    }

    @Test
    public void testNotNull_String_ObjectArr() {
        //testNotNull_ObjectArr_error
    }

    @Test
    public void testNotNull_array() {
        final String[] array = {"m1", "m2"};
        assertSame(array, Assertions.notNull(String.class, array));
    }

    @Test(expected = NullPointerException.class)
    public void testNotNull_array_error() {
        final String[] array = null;
        assertSame(array, Assertions.notNull(array, "errorMsg"));
    }

    @Test
    public void testSizeRequired_3args() {
        //testSizeRequired_GenericType_int_error
    }

    @Test
    public void testSizeRequired_GenericType_int() {
        final List<Character> list = Arrays.asList('a', 'b');
        assertSame(list, Assertions.sizeRequired(list, 2));
        assertNull(Assertions.sizeRequired(null, 2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSizeRequired_GenericType_int_error() {
        Assertions.sizeRequired(Arrays.asList('a', 'b'), 3);
    }

    //    @Test
    //    public void testMatches_String_String() {
    //        Assertions.matches(RegexHelper.POSITIVE_INT, "1");
    //    }

    //    @Test
    //    public void testMatches_4args() {
    //        //testMatches_String_String
    //    }

    //    @Test
    //    public void testCheck_GenericType_Condition() {
    //        final Condition<Integer> condition = new Condition<Integer>() {
    //            @Override
    //            public Boolean apply(Integer t) {
    //                return t > 2;
    //            }
    //
    //            @Override
    //            public String toString() {
    //                return "hello";
    //            }
    //
    //        };
    //        Assertions.check(3, condition);
    //        try {
    //            Assertions.check(1, condition);
    //            fail();
    //        } catch (IllegalArgumentException e) {
    //            assertTrue(e.getMessage().contains(Strings.replaceParams(Assertions.MSG_CHECK_FAIL, "hello")));
    //        }
    //    }
    //
    //    @Test
    //    public void testCheck_3args() {
    //        //testCheck_GenericType_Condition
    //    }

    //    @Test(expected = IllegalArgumentException.class)
    //    public void testMatches_invalid() {
    //        Assertions.matches(RegexHelper.POSITIVE_INT, "-1");
    //    }

    @Data
    @AllArgsConstructor
    private static class TestObject {

        private int value;

    }
}
