package br.com.simpou.pub.commons.utils.file;

import br.com.simpou.pub.commons.utils.TestsConstants;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

@ClassTest(FileHelper.class)
public class FileHelperTest {

    @Test
    public void testBackup() {
        //ITCase
    }

    @Test
    public void testCopy() throws Exception {
        //ITCase
    }

    @Test
    public void testCount() {
        //ITCase
    }

    @Test
    public void testDelete() throws Exception {
        //ITCase
    }

    @Test
    public void testExists() {
        //ITCase
    }

    @Test
    public void testGetDelimPattern_0args() {
        final String delim = File.separator;
        final String delimp = FileHelper.getDelimPattern();

        if (delim.equals("/")) {
            assertEquals(delim, delimp);
        } else {
            assertEquals("\\\\", delimp);
        }
    }

    @Test
    public void testGetDelimPattern_String() {
        String delim;
        String delimp;

        delim = "/";
        delimp = FileHelper.getDelimPattern(delim);
        assertEquals(delim, delimp);

        delim = "\\";
        delimp = FileHelper.getDelimPattern(delim);
        assertEquals("\\\\", delimp);
    }

    @Test
    public void testGetResourceFile() throws IOException {
        final String fileName = "log4j.xml";
        final File file = FileHelper.getResourceFile(fileName);
        String path = file.getCanonicalPath();

        // corrige caracter de espaço
        if (path.contains("%20")) {
            path = path.replaceAll("%20", " ");
        }

        final File realPath;
        realPath = new File(TestsConstants.TEST_CLASSES_DIR + fileName);
        assertEquals(realPath.getCanonicalPath(), path);
    }

    @Test
    public void testLastModified() {
        //ITCase
    }

    @Test
    public void testLength() {
        //ITCase
    }

    @Test
    public void testList() {
        //ITCase
    }

    @Test
    public void testMkdir() {
        //ITCase
    }

    @Test
    public void testMove() throws Exception {
        //ITCase
    }

    @Test
    public void testName() {
        //ITCase
    }

    @Test
    public void testNormalizeFilePathDelim() {
        assertEquals("/a/b/c", FileHelper.normalizeFilePathDelim("/a/b/c"));
        assertEquals("/a/b/c", FileHelper.normalizeFilePathDelim("\\a\\b\\c"));
        assertEquals("/a/b/c", FileHelper.normalizeFilePathDelim("/a/b\\c"));
        //assertEquals("/a/b/d", FileHelper.normalizeFilePathDelim("/a/b\\c/../d"));
    }

    @Test
    public void testPutDirPathEndDelim() throws IOException {
        assertEquals("/a/b/c" + File.separator,
                FileHelper.putDirPathEndDelim("/a/b/c"));
        assertEquals("/a/b/c/", FileHelper.putDirPathEndDelim("/a/b/c/"));
        assertEquals("/a/b/c\\", FileHelper.putDirPathEndDelim("/a/b/c\\"));
    }

    @Test
    public void testReadResourceFile() {
        //ITCase
    }

    @Test
    public void testRead_File() throws Exception {
        //ITCase
    }

    @Test
    public void testRead_String() throws Exception {
        //ITCase
    }

    @Test
    public void testRead_String_String() throws Exception {
        //ITCase
    }

    @Test
    public void testRename() {
        //ITCase
    }

    @Test
    public void testWrite_3args() {
        //ITCase
    }

    @Test
    public void testWrite_4args() throws Exception {
        //ITCase
    }

    @Test
    public void testWrite_5args() throws Exception {
        //ITCase
    }
}
