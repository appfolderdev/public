package br.com.simpou.pub.commons.utils.violation;

import br.com.simpou.pub.commons.utils.lang.Casts;
import br.com.simpou.pub.commons.utils.lang.PrintStackHelper;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.TestOrder;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Ludmily Silva
 * @since 15/09/2014
 */

@ClassTest(BundleViolations.class)
public class BundleViolationsTest {

    private static Map<Locale, ResourceBundle> APP_BUNDLES;

    @BeforeClass
    public static void setup() {
        final Class<Map<Locale, ResourceBundle>> mapClass = Casts.simpleCast(Map.class);
        APP_BUNDLES = mock(mapClass);
    }

    @Test
    public void testCallConstructor() throws Exception {
        BundleViolations.callConstructor();
    }

    @Test
    @TestOrder(1)
    public void testTranslate_list() {
        final String tstHash = PrintStackHelper.setTestMode();

        final Locale locale = new Locale("pt", "BR");
        final ResourceBundle resourceBundle = new ResourceBundle() {
            @Override
            public Enumeration<String> getKeys() {
                return null;
            }

            @Override
            protected Object handleGetObject(final String s) {
                return "string";
            }
        };

        final Object[] messageparams = {"1", "2"};
        final Object[] nullparams = {null};
        final List<Violation> violations = Arrays.asList(
                new Violation("resource_not_found", null, nullparams),
                new Violation("resource_not_found", "message", nullparams),
                new Violation(null, "message", messageparams),
                new Violation("invalid_date_format", new Object[]{"x"}),
                new Violation("invalid", null, nullparams),
                new Violation(null, null, nullparams)
        );
        when(APP_BUNDLES.get(locale)).thenReturn(resourceBundle);
        BundleViolations.translate(violations, locale);
        assertEquals("Recurso não encontrado.", violations.get(0).getMessage());
        assertEquals("message", violations.get(1).getMessage());
        assertEquals("Formato de data inválida: x", violations.get(3).getMessage());

        final Locale otherLocale = new Locale("en", "US");
        when(APP_BUNDLES.get(otherLocale)).thenReturn(null);
        BundleViolations.translate(violations, otherLocale);
        //ok, não gerou nullPointer

        PrintStackHelper.setProductionMode(tstHash);
    }

    @Test
    public void testTranslate_single() {
        //TODO
    }
}
