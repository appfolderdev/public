package br.com.simpou.pub.commons.utils.validation;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.violation.Violation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Jonas on 08/06/2015.
 */
@ClassTest(Validations.class)

public class ValidationsTest {

    @Test
    public void isCPFValid() {
        Assert.assertTrue(Validations.isCPFValid("647.582.002-46"));
    }

    @Test
    public void testValid_GenericType() {
        final TestValidatable testValidatable = new TestValidatable(true);
        final TestValidatable valid = Validations.valid(testValidatable);
        Assert.assertSame(testValidatable, valid);
        Assert.assertNull(Validations.valid(null));
    }

    @Test
    public void testValid_GenericType_Validator() {
        final TestValidatable testValidatable = new TestValidatable(true);
        final TestValidatable valid = Validations.valid(testValidatable, testValidatable.getValidator());
        Assert.assertSame(testValidatable, valid);
        Assert.assertNull(Validations.valid(null, null));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValid_GenericType_Validator_error() {
        final TestValidatable testValidatable = new TestValidatable(false);
        Validations.valid(testValidatable, testValidatable.getValidator());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValid_GenericType_error() {
        Validations.valid(new TestValidatable(false));
    }

    @AllArgsConstructor
    private class TestValidator implements Validator<TestValidatable> {

        private final boolean valid;

        @Override
        public Violation validate(final TestValidatable obj) {
            return this.valid ? null : new Violation("");
        }
    }

    private class TestValidatable implements Validatable {

        @Getter
        private final TestValidator validator;

        public TestValidatable(final boolean valid) {
            this.validator = new TestValidator(valid);
        }

        @Override
        public Violation validate() {
            return this.validator.validate(this);
        }
    }

}
