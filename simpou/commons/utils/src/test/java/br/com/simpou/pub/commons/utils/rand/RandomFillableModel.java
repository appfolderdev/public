package br.com.simpou.pub.commons.utils.rand;

import lombok.Data;

@Data
public class RandomFillableModel implements Randomizable {

    private String value;

    @Override
    public void fillRandom() {
        value = Randoms.getString();
    }
}
