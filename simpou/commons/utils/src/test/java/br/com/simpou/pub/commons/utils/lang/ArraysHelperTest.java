package br.com.simpou.pub.commons.utils.lang;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.*;

@ClassTest(ArraysHelper.class)

public class ArraysHelperTest {

    @Test
    public void testAppend() {
        final String[] array = {"1", "2"};
        final String newItem = "3";
        final String[] newArray = ArraysHelper.append(array, newItem);
        assertEquals(array.length + 1, newArray.length);

        for (int i = 0; i < array.length; i++) {
            assertEquals(array[i], newArray[i]);
        }

        assertEquals(newItem, newArray[newArray.length - 1]);
    }

    @Test
    public void testAsSet() {
        final String v1 = "v1";
        final String v2 = "v2";
        final Set<String> set = ArraysHelper.asSet(new String[]{v1, v2});
        assertEquals(2, set.size());
        assertTrue(set.contains(v1));
        assertTrue(set.contains(v2));
    }

    @Test
    public void testConcat() {
        final String[] array1 = {"a", "b"};
        final String[] array2 = {"c", "d", "e"};
        final String[] array3 = ArraysHelper.concat(array1, array2);
        assertEquals(array1.length + array2.length, array3.length);

        for (int i = 0; i < array1.length; i++) {
            assertEquals(array1[i], array3[i]);
        }

        for (int i = array1.length, j = 0; i < array3.length; i++, j++) {
            assertEquals(array2[j], array3[i]);
        }

        assertNull(ArraysHelper.concat(null, null));
        assertArrayEquals(array1, ArraysHelper.concat(array1, null));
        assertArrayEquals(array2, ArraysHelper.concat(null, array2));
    }
}
