package br.com.simpou.pub.commons.utils.reflection;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
@ClassTest(InjectorHelper.class)
public class InjectorHelperTest {

    @Test(expected = UnsupportedOperationException.class)
    public static void testNewInstances() throws Exception {
        InjectorHelper.newInstances(TestInterface.class);
    }

    @Test
    public void testNewInstance() throws Exception {
    }

    interface TestInterface {

    }
}
