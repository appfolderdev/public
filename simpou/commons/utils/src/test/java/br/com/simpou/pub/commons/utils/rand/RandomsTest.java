package br.com.simpou.pub.commons.utils.rand;

import br.com.simpou.pub.commons.utils.lang.Casts;
import br.com.simpou.pub.commons.utils.lang.Dates;
import br.com.simpou.pub.commons.utils.pagination.PageLimits;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.Repeat;
import br.com.simpou.pub.commons.utils.validation.EmailValidator;
import br.com.simpou.pub.commons.utils.validation.IPValidator;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

import static br.com.simpou.pub.commons.utils.rand.Randoms.*;
import static br.com.simpou.pub.commons.utils.tests.AssertExtension.*;
import static org.junit.Assert.*;

@ClassTest(Randoms.class)
public class RandomsTest {

    private static void testGetInteger_3args(final boolean allowNull) {
        Integer decimal;
        final int lowerLimit = getInteger(5, 100);
        final int upperLimit = getInteger(101, 1000);
        boolean lowerOK = false;
        boolean upperOK = false;

        // superior e inferior
        do {
            decimal = getInteger(lowerLimit, upperLimit, allowNull);

            if (decimal == null) {
                if (!allowNull) {
                    fail();
                }
            } else {
                assertFalse((decimal >= lowerLimit) && (decimal <= upperLimit));

                if (decimal < lowerLimit) {
                    lowerOK = true;
                } else if (decimal > upperLimit) {
                    upperOK = true;
                }
            }
        } while (!lowerOK || !upperOK);

        // superior
        decimal = getInteger(-1, upperLimit, allowNull);

        if (decimal == null) {
            if (!allowNull) {
                fail();
            }
        } else {
            assertTrue(decimal > upperLimit);
        }

        // inferior
        decimal = getInteger(lowerLimit, -1, allowNull);

        if (decimal == null) {
            if (!allowNull) {
                fail();
            }
        } else {
            assertTrue(decimal < lowerLimit);
        }
    }

    private static void testGetInteger_int_int(final int minValue, final int maxValue) {
        final int decimal = getInteger(minValue, maxValue);
        assertTrue((decimal >= minValue) && (decimal <= maxValue));
    }

    private static void testGetStringNumber_int_int(final int minLength, final int maxLength) {
        final String stringNumber;
        stringNumber = getStringNumber(minLength, maxLength);
        assertTrue((stringNumber.length() >= minLength)
                && (stringNumber.length() <= maxLength));

        try {
            Integer.valueOf(stringNumber);
        } catch (final NumberFormatException e) {
            fail();
        }
    }

    private static void testGetString_4args(final boolean allowNull) {
        int length;
        String string;

        final int maxHalf = Randoms.MAX_STRING_CHARS / 2;
        final int lowerLimit = getInteger(2, maxHalf - 1);
        final int upperLimit = getInteger(maxHalf + 1,
                Randoms.MAX_STRING_CHARS - 1);

        // limitado superior e inferior
        string = getString(lowerLimit, upperLimit, getBoolean(), allowNull);

        if (string == null) {
            if (!allowNull) {
                fail();
            }
        } else {
            length = string.length();
            assertFalse((length >= lowerLimit) && (length <= upperLimit));
        }

        // limitado superior
        string = getString(-1, upperLimit, getBoolean(), allowNull);

        if (string == null) {
            if (!allowNull) {
                fail();
            }
        } else {
            length = string.length();
            assertTrue(length > upperLimit);
        }

        // limitado inferior
        string = getString(lowerLimit, -1, getBoolean(), allowNull);

        if (string == null) {
            if (!allowNull) {
                fail();
            }
        } else {
            length = string.length();
            assertTrue(length < lowerLimit);
        }

        // sem limitação
        string = getString(-1, -1, getBoolean(), true);
        assertNull(string);
    }

    private static void testGetSubList(final boolean allowEmpty) {
        List<RandomsTest> sublist;
        List<RandomsTest> list = new ArrayList<>();
        final int n = getInteger(2, 5);

        for (int i = 0; i < n; i++) {
            list.add(new RandomsTest());
        }

        // lista cheia
        sublist = getSubList(list, allowEmpty);
        assertTrue(!(!allowEmpty && (sublist.size() < 1)));

        for (final RandomsTest randomHelperTest : sublist) {
            assertTrue(randomHelperTest instanceof RandomsTest);
        }

        // lista vazia
        list = new ArrayList<>();
        sublist = getSubList(list, true);
        assertEquals(0, sublist.size());

        // lista com um elemento
        list.add(new RandomsTest());
        sublist = getSubList(list, allowEmpty);

        if (allowEmpty) {
            assertTrue(sublist.size() <= 1);
        } else {
            assertTrue(sublist.size() == 1);
        }

        // cobertura bloco que verifica se lista vazia depois do preenchimento
        // e se permite vazia, caso não inclui um aleatoriamente.
        list.add(new RandomsTest());

        for (int i = 0; i < 5; i++) {
            sublist = getSubList(list, false);
            assertTrue(sublist.size() < 3);
        }
    }

    /**
     * @see Randoms#getLocalDate()
     */
    @Test
    public void getLocalDate() {
        final LocalDate localDate = Randoms.getLocalDate();
        assertNotNull(localDate);
    }

    /**
     * @see Randoms#getLocalDateTime()
     */
    @Test
    public void getLocalDateTime() {
        final LocalDateTime localDate = Randoms.getLocalDateTime();
        assertNotNull(localDate);
    }

    /**
     * @see Randoms#getLocalDateTime(boolean)
     */
    @Test
    public void getLocalDateTime_future() {
        final LocalDateTime now = LocalDateTime.now();

        final LocalDateTime future = Randoms.getLocalDateTime(true);
        assertNotNull(now.isBefore(future));

        final LocalDateTime past = Randoms.getLocalDateTime(false);
        assertTrue(now.isAfter(past));
    }

    /**
     * @see Randoms#getLocalDateTime(LocalDate, LocalDate)
     */
    @Test
    public void getLocalDateTime_range() {
        final LocalDate begin = LocalDate.now();
        final LocalDate end = begin.plusDays(1);

        final LocalDateTime date = Randoms.getLocalDateTime(begin, end);
        assertFalse(date.isBefore(begin.atStartOfDay()));
        assertFalse(date.isAfter(end.atTime(23, 59,59)));
    }

    /**
     * @see Randoms#getLocalDate(boolean)
     */
    @Test
    public void getLocalDate_future() {
        final LocalDate today = LocalDate.now();

        final LocalDate future = Randoms.getLocalDate(true);
        assertNotNull(today.isBefore(future));

        final LocalDate past = Randoms.getLocalDate(false);
        assertTrue(today.isAfter(past));
    }

    /**
     * @see Randoms#getLocalDate(LocalDate, LocalDate)
     */
    @Test
    public void getLocalDate_range() {
        final int maxDays = 10;

        final LocalDate begin = LocalDate.now();
        final LocalDate end = begin.plusDays(maxDays);

        final LocalDate date = Randoms.getLocalDate(begin, end);
        final long daysBetween = Dates.countDaysBetween(begin, date);

        assertTrue(daysBetween <= maxDays);
    }

    /**
     * @see Randoms#getLocalTime()
     */
    @Test
    public void getLocalTime() {
        final LocalTime time = Randoms.getLocalTime();
        assertNotNull(time);
    }

    /**
     * @see Randoms#getLocalTime(boolean) ()
     */
    @Test
    public void getLocalTime_morning() {
        final LocalTime morning = Randoms.getLocalTime(true);
        assertTrue(morning.isBefore(LocalTime.of(12, 00)));

        final LocalTime night = Randoms.getLocalTime(false);
        assertTrue(night.isAfter(LocalTime.of(11, 59)));
    }

    /**
     * @see Randoms#fill(Iterable, Randomizer)
     */
    @Test
    public void testFill_list() {
        final TestRandomnizer randz = new TestRandomnizer();
        final RandomFillableModel obj1 = new RandomFillableModel();
        final RandomFillableModel obj2 = new RandomFillableModel();
        assertNull(obj1.getValue());
        assertNull(obj2.getValue());
        final List<RandomFillableModel> objs = Arrays.asList(obj1, obj2);
        final List<RandomFillableModel> ret = Randoms.fill(objs, randz);
        assertSame(objs, ret);
        assertNotNull(obj1.getValue());
        assertNotNull(obj2.getValue());
    }

    /**
     * @see Randoms#fill(Object, Randomizer)
     */
    @Test
    public void testFill_obj() {
        final TestRandomnizer randz = new TestRandomnizer();
        final RandomFillableModel obj = new RandomFillableModel();
        assertNull(obj.getValue());
        final RandomFillableModel ret = Randoms.fill(obj, randz);
        assertSame(obj, ret);
        assertNotNull(obj.getValue());
    }

    @Test
    public void testGetArray_3args() {
        final int min = 2;
        final int max = 5;
        final RandomFillableModel[] array = getArray(RandomFillableModel.class, min,
                max);
        assertTrue((array.length >= min) && (array.length <= max));

        for (final RandomFillableModel model : array) {
            assertTrue(model.getValue().length() > 0);
        }
    }

    @Test
    public void testGetArray_4args_1() {
        //TODO
    }

    @Test
    public void testGetArray_4args_2() {
        //TODO
    }

    @Test
    public void testGetBigDecimal_0args() {
        final BigDecimal value = getBigDecimal();
        assertNotNull(value);
    }

    @Test
    public void testGetBigDecimal_long_long() {
        //testGetInteger_int_int
        final long minValue = 10;
        final long maxValue = 100;
        final BigDecimal bd = getBigDecimal(minValue, maxValue);
        assertTrue((bd.compareTo(new BigDecimal(minValue)) > -1)
                && (bd.compareTo(new BigDecimal(maxValue)) < 1));
    }

    @Test
    public void testGetBoolean() {
        final boolean b = getBoolean();
    }

    @Test
    public void testGetByte() {
        final Byte value = getByte();
        assertNotNull(value);
    }

    /**
     * @see Randoms#getCNPJ()
     */
    @Test
    public void testGetCNPJ() {
        final String cnpj = getCNPJ();
        assertEquals(18, cnpj.length());

        final StringTokenizer tokenizer = new StringTokenizer(getCNPJ(45), "/-.");
        assertEquals(5, tokenizer.countTokens());
        assertEquals(2, tokenizer.nextToken().length());
        assertEquals(3, tokenizer.nextToken().length());
        assertEquals(3, tokenizer.nextToken().length());
        assertEquals("0045", tokenizer.nextToken());
        assertEquals(2, tokenizer.nextToken().length());
    }

    /**
     * @see Randoms#getCPF()
     */
    @Test
    public void testGetCPF() {
        final String cpf = getCPF();
        assertEquals(14, cpf.length());

        final StringTokenizer tokenizer = new StringTokenizer(getCPF(45), "-.");
        assertEquals(4, tokenizer.countTokens());
        assertEquals(3, tokenizer.nextToken().length());
        assertEquals(3, tokenizer.nextToken().length());
        assertEquals("045", tokenizer.nextToken());
        assertEquals(2, tokenizer.nextToken().length());
    }

    @Test
    public void testGetChar() {
        final char c = getChar(getBoolean());
    }

    @Test
    public void testGetDate_0args() throws ParseException {
        final Date date = getDate();
    }

    @Test
    public void testGetDate_Date_Date() {
        final Date start = getDate(false);
        final Date end = getDate(true);
        final Date rand = getDate(start, end);
        assertTrue(rand.after(start) || rand.equals(start));
        assertTrue(rand.before(end) || rand.equals(end));
    }

    @Test
    public void testGetDate_boolean() {
        final boolean future = getBoolean();
        final Date date = getDate(future);
        final Date now = new Date();
        final long diff = date.getTime() - now.getTime();
        assertEquals(future, diff > 0);
    }

    @Test
    @Repeat(10)
    public void testGetDecreasePercent() {
        assertInRange(Randoms.getDecreasePercent(), 0, 1);
    }

    @Test
    public void testGetDigit() {
        final int digit = getDigit();
        assertTrue((digit > -1) && (digit < 10));
    }

    @Test
    public void testGetDouble() {
        final Double value = getDouble();
        assertNotNull(value);
    }

    @Test
    public void testGetEmail_0args() {
        final String email = getEmail();
        assertTrue(EmailValidator.isValid(email));
    }

    @Test
    public void testGetEmail_boolean() {
        String email;

        email = getEmail(false);
        assertTrue(EmailValidator.isValid(email));
        assertEquals(2, email.split("\\.").length);

        email = getEmail(true);
        assertTrue(EmailValidator.isValid(email));
        assertEquals(3, email.split("\\.").length);
    }

    @Test
    public void testGetEnum() {
        final TesteEnum testeEnum = getEnum(TesteEnum.class);
        assertNotNull(testeEnum);
        assertEquals(TesteEnum.class, testeEnum.getClass());
    }

    @Test
    public void testGetEnums() {
        final int enumsLength = TesteEnum.values().length;
        final List<TesteEnum> testeEnums = getEnums(TesteEnum.class);

        for (final TesteEnum testeEnum : testeEnums) {
            assertNotNull(testeEnum);
            assertTrue(testeEnum instanceof TesteEnum);
        }

        // garantir cobertura
        if ((testeEnums.size() == 0) || (testeEnums.size() == enumsLength)) {
            testGetEnums();
        }
    }

    @Test
    public void testGetFloat() {
        final float f = getFloat();
    }

    /**
     * @see Randoms#getFloat(int, int)
     */
    @Test
    public void testGetFloat_int_int() {
        assertInRange(getFloat(1, 2), 1, 2);
        assertEquals(getFloat(10, 10), 10, 1);
    }

    @Test
    public void testGetIP() {
        final String ip = getIP();
        assertNull(IPValidator.isValid(ip));
    }

    @Test
    public void testGetId() {
        final long v1 = Randoms.getId();
        assertEquals(v1 + 1, Randoms.getId());
    }

    @Test
    @Repeat(10)
    public void testGetIncreasePercent() {
        assertInRange(Randoms.getIncreasePercent(), 1, 2);
    }

    @Test
    public void testGetInteger_0args() {
        final Integer value = getInteger();
        assertNotNull(value);
    }

    @Test
    public void testGetInteger_3args() {
        testGetInteger_3args(true);
        testGetInteger_3args(false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetInteger_3args_error_1() {
        // limite inferior nulo
        getInteger(0, 1, getBoolean());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetInteger_3args_error_2() {
        // limite superior nulo
        getInteger(1, 0, getBoolean());
    }

    @Test
    public void testGetInteger_int_int() {
        testGetInteger_int_int(-10, -10);
        testGetInteger_int_int(-20, -10);
        testGetInteger_int_int(0, 0);
        testGetInteger_int_int(10, 20);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetInteger_int_int_error() {
        testGetInteger_int_int(10, -10);
    }

    @Test
    public void testGetLimits() {
        final int maxElements;
        final PageLimits limits;

        maxElements = getInteger(10, 50);
        limits = getLimits(maxElements);
        assertTrue(limits.getLast() < maxElements);
        assertTrue(limits.getFirst() > -1);
    }

    @Test
    public void testGetLimits_invalid() {
        final int maxElements;
        final PageLimits limits;

        // elementos não positivos
        maxElements = getInteger(0, 1);

        try {
            limits = getLimits(-1 * maxElements);
            fail();
        } catch (final IllegalArgumentException e) {
        }
    }

    @Test
    public void testGetListIgnoreType() {
        final Class<?> clasz = RandomFillableModel.class;
        final List<?> list = getListIgnoreType(clasz, 2, 3);
        assertInRange(list.size(), 2, 3);
    }

    @Test
    public void testGetList_3args() throws Exception {
        final int min = 2;
        final int max = 5;
        final List<RandomFillableModel> list = getList(RandomFillableModel.class,
                min, max);
        assertTrue((list.size() >= min) && (list.size() <= max));

        for (final RandomFillableModel model : list) {
            assertTrue(model.getValue().length() > 0);
        }
    }

    @Test
    public void testGetList_4args() {
        final TestRandomnizer randomizer = new TestRandomnizer();
        final List<RandomFillableModel> list = getList(RandomFillableModel.class, randomizer, 2, 2);
        assertEquals(randomizer.value, list.get(0).getValue());
        assertEquals(randomizer.value, list.get(1).getValue());
    }

    @Test
    public void testGetList_4args_1() {
        //TODO
    }

    @Test
    public void testGetList_4args_2() {
        //TODO
    }

    @Test
    public void testGetList_Class() {
        //TODO
    }

    @Test
    public void testGetList_Class_Randomizer() {
        //TODO
    }

    @Test
    public void testGetLocale() {
//        final String locale = getLocale();
//        assertNull(LocaleValidator.isValid(locale));
    }

    @Test
    public void testGetLong_0args() {
        final Long value = getLong();
        assertNotNull(value);
    }

    @Test
    public void testGetLong_int_int() {
        final long min = 0;
        final long max = 10;
        final long vLong = getLong((int) min, (int) max);
        assertTrue((vLong >= min) && (vLong <= max));
    }

    @Test
    public void testGetNumber_int() {
        // testGetStringNumber_int
        final int length = 2;
        final int number = getNumber(length);
        final int n = (number + "").length();
        assertEquals(length, n);
    }

    @Test
    public void testGetNumber_int_int() {
        // testGetStringNumber_int_int
        final int min = 2;
        final int max = 9;
        int number;
        final int n;

        //válido
        number = getNumber(min, max);
        n = (number + "").length();
        assertTrue((n >= min) && (n <= max));

        //inválido
        try {
            number = getNumber(max, min);
            fail();
        } catch (final IllegalArgumentException e) {
        }
    }

    @Test
    @Repeat(10)
    public void testGetPercent() {
        assertInRange(Randoms.getIncreasePercent(), 1, 2);
    }

    @Test
    public void testGetPositiveInt() {
        final int n = getPositiveInt();
        assertTrue(n > -1);
    }

    @Test
    public void testGetSHA256() {
        final String value = getSHA256();
        assertEquals(64, value.length());
    }

    @Test
    public void testGetShort() {
        final Short value = getShort();
        assertNotNull(value);
    }

    @Test
    public void testGetSingleIgnoreType() {
        final Class<?> clasz = RandomFillableModel.class;
        final RandomFillableModel obj = Casts.simpleCast(getSingleIgnoreType(clasz));
        assertNotNull(obj);
    }

    @Test
    public void testGetSingle_Class() throws Exception {
        final RandomFillableModel single = getSingle(RandomFillableModel.class);
        assertNotNull(single);
        assertNotNull(single.getValue());
    }

    @Test
    public void testGetSingle_Class_Randomizer() {
        final TestRandomnizer randomizer = new TestRandomnizer();
        final RandomFillableModel single = getSingle(RandomFillableModel.class, randomizer);
        assertEquals(randomizer.value, single.getValue());
    }

    @Test
    public void testGetSingle_Collection() {
        //testGetSingle_GenericType
    }

    @Test
    public void testGetSingle_GenericType() {
        List<RandomsTest> list;

        list = new ArrayList<>();

        final int n = getInteger(10, 100);

        for (int i = 0; i < n; i++) {
            list.add(new RandomsTest());
        }

        RandomsTest ret;

        // lista cheia
        ret = getSingle(list);
        assertNotNull(ret);
        assertTrue(list.contains(ret));

        // lista vazia
        list = new ArrayList<>();
        ret = getSingle(list);
        assertNull(ret);

        // lista com um elemento
        final RandomsTest rHelper = new RandomsTest();
        list.add(rHelper);
        ret = getSingle(list);
        assertEquals(rHelper, ret);
    }

    @Test
    public void testGetStringNumber_int() {
        final int length;
        final String string;

        length = getInteger(1, Randoms.MAX_STRING_NUMBER_CHARS);
        string = getStringNumber(length);
        assertEquals(length, string.length());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetStringNumber_int_error_1() {
        getStringNumber(Randoms.MAX_STRING_NUMBER_CHARS + 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetStringNumber_int_error_2() {
        getStringNumber(0);
    }

    @Test
    public void testGetStringNumber_int_int() {
        int minLength;
        int maxLength;

        minLength = getInteger(1, Randoms.MAX_STRING_NUMBER_CHARS);
        maxLength = minLength;
        testGetStringNumber_int_int(minLength, maxLength);

        final int halfLength = Randoms.MAX_STRING_NUMBER_CHARS / 2;
        minLength = getInteger(1, halfLength - 1);
        maxLength = getInteger(halfLength + 1,
                Randoms.MAX_STRING_NUMBER_CHARS);
        testGetStringNumber_int_int(minLength, maxLength);
    }

    @Test
    public void testGetString_0args() {
        final String string = getString();
        assertGreaterThan(string.length(), 0);
        assertLowerThan(string.length(), 11);
    }

    @Test
    public void testGetString_3args() {
        int minLength;
        int maxLength;
        String text;

        minLength = 5;
        maxLength = 10;
        text = getString(minLength, maxLength, getBoolean());
        assertTrue((text.length() >= minLength)
                && (text.length() <= maxLength));

        minLength = getInteger(1, Randoms.MAX_STRING_CHARS);
        maxLength = minLength;
        text = getString(minLength, maxLength, getBoolean());
        assertTrue((text.length() == minLength)
                && (text.length() == maxLength));
    }

    @Test
    public void testGetString_4args() {
        testGetString_4args(true);
        testGetString_4args(false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetString_4args_error_1() {
        // acima do limite superior
        getString(-1, Randoms.MAX_STRING_CHARS + 1, false, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetString_4args_error_2() {
        // sem limitação e não permite NULL
        getString(-1, -1, false, false);
    }

    @Test
    public void testGetString_5args() {
        String string;
        final boolean includeEspecial = false;
        boolean allowNull;
        boolean allowEmpty;
        final int stopCondition = 1000;
        int stopConditionCounter;

        allowNull = true;
        allowEmpty = false;
        string = "";
        stopConditionCounter = 0;

        while (true) {
            if (stopConditionCounter > stopCondition) {
                fail("Null string was not generated.");
            }

            string = Randoms.getString(2, 5, includeEspecial, allowNull,
                    allowEmpty);

            if (string == null) {
                break;
            } else if (string.isEmpty()) {
                fail("Empty string not allowed.");
            }

            stopConditionCounter++;
        }

        allowNull = false;
        allowEmpty = true;
        string = "no empty string";
        stopConditionCounter = 0;

        while (true) {
            if (stopConditionCounter > stopCondition) {
                fail("Empty string was not generated.");
            }

            string = Randoms.getString(2, 5, includeEspecial, allowNull,
                    allowEmpty);

            if (string == null) {
                fail("Null string not allowed.");
            } else if (string.isEmpty()) {
                break;
            }

            stopConditionCounter++;
        }
    }

    @Test
    public void testGetString_int_boolean() {
        //testGetList_3args
    }

    @Test
    public void testGetString_int_boolean_1() {
        // condições de fronteira
        int length;

        length = 1;
        assertEquals(length, getString(length, getBoolean()).length());

        length = Randoms.MAX_STRING_CHARS;
        assertEquals(length, getString(length, getBoolean()).length());

        length = Randoms.MAX_STRING_CHARS - 1;
        assertEquals(length, getString(length, getBoolean()).length());
    }

    @Test
    public void testGetString_int_boolean_2() {
        // condições internas à fronteira
        final int length;

        length = getInteger(2, Randoms.MAX_STRING_CHARS - 2);
        assertEquals(length, getString(length, getBoolean()).length());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetString_int_boolean_error_1() {
        getString(Randoms.MAX_STRING_CHARS + 1, getBoolean());
    }

    @Test
    public void testGetString_int_boolean_error_2() {
        final String string = getString(0, getBoolean());
        assertEquals("", string);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetString_int_boolean_error_3() {
        getString(-1, getBoolean());
    }

    @Test
    public void testGetSubList() {
        testGetSubList(true);
        testGetSubList(false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetSubList_error_1() {
        // lista nula
        getSubList(null, getBoolean());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetSubList_error_2() {
        // lista vazia sem permitir retorno nulo
        getSubList(new ArrayList<RandomsTest>(), false);
    }

    @Test
    public void testGetUrl() {
        assertEquals(4, Randoms.getUrl(3, 3).split("/").length);
        assertInRange(Randoms.getUrl(1, 4).split("/").length, 2, 5);
    }

    private enum TesteEnum {

        ENUM1,
        ENUM2,
        ENUM4,
        ENUM5,
        ENUM6,
        ENUM7,
        ENUM3

    }

    class TestRandomnizer implements Randomizer<RandomFillableModel> {

        final String value = getString();

        @Override
        public void fillRandom(final RandomFillableModel t) {
            t.setValue(this.value);
        }
    }
}
