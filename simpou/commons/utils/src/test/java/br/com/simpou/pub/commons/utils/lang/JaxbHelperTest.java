package br.com.simpou.pub.commons.utils.lang;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.TestOrder;
import lombok.Data;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.StringWriter;

import static org.junit.Assert.assertEquals;

/**
 * @author jonas.pereira
 * @version 08/07/13
 * @since 08/07/13
 */
@ClassTest(JaxbHelper.class)
@Ignore
public class JaxbHelperTest {

    private static String xml;

    private static MarshalObject obj;

    @BeforeClass
    public static void setUpClass() throws Exception {
        obj = new MarshalObject();
        obj.setId(1);
        obj.setName("string");
    }

    @Test
    public void testAddClassesToContext_Class() throws Exception {
        //testMarshalXml
    }

    @Test
    public void testAddClassesToContext_ClassArr() throws Exception {
    }

    @Test
    public void testAddClassesToContext_Collection() throws Exception {
    }

    @Test
    public void testGetContext() {
    }

    @Test
    public void testGetContextClasses() {
    }

    @Test
    public void testGetContextFrom() throws Exception {
    }

    @Test
    public void testGetMarshaller() throws Exception {
    }

    @Test
    public void testMarshal() throws Exception {
    }

    @Test
    @TestOrder(3)
    public void testMarshalJson() throws Exception {
        final StringWriter writer;
        writer = new StringWriter();
        JaxbHelper.marshalJson(JaxbHelper.getContext(), writer, obj);
        System.out.println(writer);
        assertEquals("{\"marshaled\":{\"@id_name\":\"1\",\"name\":\"string\"}}", writer.toString());
    }

    @Test
    @TestOrder(1)
    public void testMarshalXml() throws Exception {
        final StringWriter writer;
        JaxbHelper.addClassesToContext(MarshalObject.class);

        writer = new StringWriter();
        JaxbHelper.marshalXml(JaxbHelper.getContext(), writer, obj);
        System.out.println(writer);
        xml = writer.toString();
    }

    @Test
    @TestOrder(2)
    public void testUnmarshalXml() throws Exception {
        final MarshalObject object = JaxbHelper.unmarshalXml(MarshalObject.class, xml);
        assertEquals(obj.getId(), object.getId());
        assertEquals(obj.getName(), object.getName());
    }

}

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "marshaled")
class MarshalObject {

    @XmlAttribute(name = "id_name")
    private int id;

    private String name;

}
