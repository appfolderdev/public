package br.com.simpou.pub.commons.utils.interceptor;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

@ClassTest(InterceptorDecorator.class)

public class InterceptorDecoratorTest {

    private static String status;

    private static Interceptor interceptor;

    @BeforeClass
    public static void setUpClass() throws Exception {
        interceptor = new InterceptorImpl();
        interceptor = new DecoratorImpl(interceptor);
    }

    @Before
    public void setUp() {
        status = "";
    }

    @Test
    public void testDoAfter() {
        interceptor.doAfter(null);
        assertEquals("25", status);
    }

    @Test
    public void testDoBefore() {
        interceptor.doBefore(null, null);
        assertEquals("14", status);
    }

    @Test
    public void testDoOnError() throws Exception, Throwable {
        interceptor.doOnError(null);
        assertEquals("36", status);
    }

    private static class InterceptorImpl implements Interceptor {

        @Override
        public void doAfter(final Object result) {
            InterceptorDecoratorTest.status += "2";
        }

        @Override
        public void doBefore(final Method method, final Object[] args) {
            InterceptorDecoratorTest.status += "1";
        }

        @Override
        public Object doOnError(final Throwable throwable) throws Throwable {
            InterceptorDecoratorTest.status += "3";

            return null;
        }
    }

    private static class DecoratorImpl extends InterceptorDecorator {

        public DecoratorImpl(final Interceptor interceptor) {
            super(interceptor);
        }

        @Override
        public void decorateAfter() {
            InterceptorDecoratorTest.status += "5";
        }

        @Override
        public void decorateBefore() {
            InterceptorDecoratorTest.status += "4";
        }

        @Override
        public Object decorateOnError(final Object lastReturn, final Throwable throwable)
                throws Throwable {
            InterceptorDecoratorTest.status += "6";

            return null;
        }
    }
}
