package br.com.simpou.pub.commons.utils.functional;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.ExtraTest;
import br.com.simpou.pub.commons.utils.tests.annot.TestOrder;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by 9000070 on 27/02/2017.
 */
@ClassTest(Promise.class)

@CommonsLog
public class PromiseTest {

    private static Promise.PromiseController<String> promiseController;

    private static Promise.PromiseController<String> promiseControllerError;

    private static Promise<String> promise;

    private static Promise<String> promiseError;

    /**
     * @see Promise#getLastError()
     */
    @Test
    public void testGetLastError() throws Exception {
        //testReject
    }

    /**
     * @see Promise#getResolvedObj()
     */
    @Test
    public void testGetResolvedObj() throws Exception {
        //testResolve
    }

    /**
     * @see Promise#isRejected()
     */
    @Test
    public void testIsRejected() throws Exception {
        //testReject
    }

    /**
     * @see Promise#isResolved()
     */
    @Test
    public void testIsResolved() throws Exception {
        //testResolve
    }

    /**
     * @see br.com.simpou.pub.commons.utils.functional.Promise.PromiseController#reject(Throwable)
     */
    @Test
    @TestOrder(2)
    @ExtraTest
    public void testReject() throws Exception {
        final Promise<String> resolved = promiseControllerError.reject(new RuntimeException("error"));
        Assert.assertSame(promiseError, resolved);
        Assert.assertFalse(promiseError.isResolved());
        Assert.assertTrue(promiseError.isRejected());
        Assert.assertNotNull(promiseError.getLastError());
        Assert.assertNull(promiseError.getResolvedObj());
    }

    /**
     * @see br.com.simpou.pub.commons.utils.functional.Promise.PromiseController#resolve(Object)
     */
    @Test
    @TestOrder(2)
    @ExtraTest
    public void testResolve() throws Exception {
        final Promise<String> resolved = promiseController.resolve("success");
        Assert.assertSame(promise, resolved);
        Assert.assertTrue(promise.isResolved());
        Assert.assertNull(promise.getLastError());
        Assert.assertNotNull(promise.getResolvedObj());
    }

    /**
     * @see Promise#then(Promise.Resolved)
     */
    @Test
    @TestOrder(2)
    public void testThen() throws Exception {
        final Promise.Resolved<String> resolvedAction = new Promise.Resolved<String>() {
            @Override
            public void onError(final Throwable t) {
                Assert.assertEquals("error", t.getMessage());
            }

            @Override
            public void onSuccess(final String obj) {
                Assert.assertEquals("success", obj);
            }
        };
        promise.then(resolvedAction);
    }

    /**
     * @see Promise#value()
     */
    @Test
    @TestOrder(1)
    public void testValue() throws Exception {
        promiseController = Promise.value();
        promise = promiseController.getPromise();
        Assert.assertNotNull(promise);
        Assert.assertFalse(promise.isResolved());
        Assert.assertNull(promise.getLastError());
        Assert.assertNull(promise.getResolvedObj());

        promiseControllerError = Promise.value();
        promiseError = promiseControllerError.getPromise();
    }

    /**
     * @see Promise#waitResolution()
     */
    @Test
    public void testWaitResolution() throws Exception {
        final Promise.PromiseController<Integer> promisePair = Promise.value();
        final Promise<Integer> slowPromise = promisePair.getPromise();
        slowPromise.then(new Promise.Resolved<Integer>() {
            @Override
            public void onError(final Throwable t) {
                Assert.fail(t.getMessage());
            }

            @Override
            public void onSuccess(final Integer obj) {
                Assert.assertEquals(7, obj.intValue());
            }
        });

        // ainda não resolveu
        Assert.assertFalse(slowPromise.isResolved());

        // executa uma operação assíncrona longa dentro do promise
        new ResolutionThread(promisePair).start();

        // resolve em 1000ms mas só espera 150ms
        //        log.debug("Waiting promise for 150ms...");
        slowPromise.waitResolution(150L);
        //        log.debug("Waiting 150ms ended.");
        Assert.assertFalse(slowPromise.isResolved());

        // espera até terminar
        //        log.debug("Waiting promise infinitely...");
        slowPromise.waitResolution();
        //        log.debug("Promise report resolved.");
        Assert.assertTrue(slowPromise.isResolved());
    }

    /**
     * @see Promise#waitResolution(Long)
     */
    @Test
    public void testWaitResolution_timeout() {
        //testWaitResolution
    }

    @RequiredArgsConstructor
    class ResolutionThread extends Thread {

        private final Promise.PromiseController<Integer> promise;

        @Override
        public void run() {
            //            log.debug("Promise operando...");
            try {
                Thread.sleep(300);
            } catch (final InterruptedException e) {
                Assert.fail(e.getMessage());
            }
            //            log.debug("Promise resolvido");
            this.promise.resolve(7);
        }
    }

}
