package br.com.simpou.pub.commons.utils.file;

import br.com.simpou.pub.commons.utils.string.Strings;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.IgnoreValidation;
import br.com.simpou.pub.commons.utils.tests.annot.TestOrder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.MissingResourceException;

import static org.junit.Assert.*;

@IgnoreValidation
@Ignore
@ClassTest(PropertyHelper.class)
public class PropertyHelperITCase {

    private static PropertyHelper propertyHelper;

    private static String key;

    private static String key2;

    private static String param;

    private static String value;

    private static String value2;

    private static String filePath;

    private static String content;

    @BeforeClass
    public static void setUpClass() throws Exception {
        filePath = PropertyHelper.getTempDir() + "teste.txt"; //UUID.randomUUID();
        key = "prop1";
        key2 = "prop2";
        param = "param";
        value = "value {0}";
        value2 = "value2";
        content = key + "=" + value;
        FileHelper.write(filePath, content, false, false);
        propertyHelper = new PropertyHelper(filePath);
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        propertyHelper.close();
        FileHelper.delete(filePath);
        assertFalse(FileHelper.exists(filePath));
    }

    @Test(expected = RuntimeException.class)
    public void testFrom() {
        PropertyHelper.from("nopsFile");
    }

    @Test
    @TestOrder(3)
    public void testGetProperty() throws Exception {
        String prop = propertyHelper.getProperty(key, param);
        assertEquals(Strings.replaceParams(value, param), prop);

        prop = propertyHelper.getProperty(key2);
        assertEquals(value2, prop);

        try {
            prop = propertyHelper.getProperty("key_error");
            fail();
        } catch (final MissingResourceException ex) {
        }
    }

    @Test
    @TestOrder(2)
    public void testSetProperty_String_String()
            throws FileNotFoundException, IOException {
        propertyHelper.setProperty(key2, value2);

        final String text = FileHelper.read(filePath);
        final String props = key2 + "=" + value2 + "\n" + content;
        assertEquals(props,
                text.substring(text.indexOf("\n") + 1, text.length()));
    }

    @Test
    @TestOrder(1)
    public void testSetProperty_String_int() throws Exception {
        String prop;

        prop = propertyHelper.setProperty(key2, 0);
        assertTrue(prop.startsWith(PropertyHelper.DEF_VALUE_NOT_SETTED_PREFIX));

        prop = propertyHelper.setProperty(key2, 2);
        assertTrue(prop.startsWith(PropertyHelper.DEF_VALUE_NOT_SETTED_PREFIX));
        assertTrue(prop.contains("{0}"));
        assertTrue(prop.contains("{1}"));
    }
}
