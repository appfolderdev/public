package br.com.simpou.pub.commons.utils.string;

import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

@ClassTest(Strings.class)
public class StringsTest {

    @Test
    public void testAppendAtBeginIfNotContains() throws Exception {
        final String string = "string";
        final String append = "new";
        String newString;

        newString = Strings.appendAtBeginIfNotContains(string, append);
        assertEquals(append + string, newString);

        newString = Strings.appendAtBeginIfNotContains(newString, append);
        assertEquals(append + string, newString);
    }

    @Test
    public void testAppendAtEndIfNotContains() throws Exception {
        final String string = "string";
        final String append = "new";
        String newString;

        newString = Strings.appendAtEndIfNotContains(string, append);
        assertEquals(string + append, newString);

        newString = Strings.appendAtEndIfNotContains(newString, append);
        assertEquals(string + append, newString);
    }

    @Test
    public void testConcatDots() throws Exception {
        assertEquals("a.b", Strings.concatDots("a", "b"));
    }

    @Test
    public void testConcat_array() throws Exception {
        assertEquals("a, b", Strings.concat(", ", "a", "b"));
    }

    @Test
    public void testConcat_collection() throws Exception {
        //testConcat_array
    }

    @Test
    public void testExtract() throws Exception {
        final String payload = Randoms.getString();
        final InputStream is = new ByteArrayInputStream(payload.getBytes(StandardCharsets.UTF_8));
        final String read = Strings.extract(is, "UTF-8");
        assertEquals(payload, read);
    }

    @Test
    public void testIsEmpty() {
        assertTrue(Strings.isEmpty(null));
        assertTrue(Strings.isEmpty(""));
        assertTrue(Strings.isEmpty("     "));
        assertFalse(Strings.isEmpty(Randoms.getString(10, 100, true)));
    }

    @Test
    public void testReplaceParams_String_ObjectArr() {
        String replaced;

        replaced = Strings.replaceParams(null);
        assertNull(replaced);

        replaced = Strings.replaceParams("", new String[3]);
        assertEquals("", replaced);
    }

    @Test
    public void testReplaceParams_String_StringArr() {
        final int nParams = Randoms.getInteger(10, 100);
        String text = "";
        String textOK = "";
        final String[] params = new String[nParams];
        String textParam;

        for (int i = 0; i < nParams; i++) {
            textParam = Randoms.getString(1, 50, true);
            text += ("param " + i + ":{" + i + "}, ");
            textOK += ("param " + i + ":" + textParam + ", ");
            params[i] = textParam;
        }

        final String replaced = Strings.replaceParams(text, params);
        assertEquals(textOK, replaced);
    }

    @Test
    public void testStringToType() throws Exception {
        assertEquals(2L, Strings.stringToType(Long.class, "2").longValue());
        assertTrue(Strings.stringToType(Boolean.class, "true"));
    }

    @Test
    public void testToLowerCaseFirstLetter() {
        //testToUpperCaseFirstLetter
    }

    @Test
    public void testToUpperCaseCamelCase() {
        assertEquals("TESTE_OK", Strings.toUpperCaseCamelCase("testeOk"));
        assertEquals("TESTE_OK_OUTRO", Strings.toUpperCaseCamelCase("TesteOkOutro"));
    }

    @Test
    public void testToUpperCaseFirstLetter() {
        String textUpper;
        String text = "";
        String textOK = "";
        String first;
        String word;
        final int words = Randoms.getInteger(1, 3);

        for (int i = 0; i < words; i++) {
            first = Randoms.getString(1, false);
            word = Randoms.getString(1, 10, true) + " ";
            text += (first.toLowerCase() + word);
            textOK += (first.toUpperCase() + word);
        }

        textUpper = Strings.toUpperCaseFirstLetter(text);
        assertEquals(textOK, textUpper);

        final String textLower = Strings.toLowerCaseFirstLetter(textUpper);
        assertEquals(text, textLower);

        textUpper = Strings.toUpperCaseFirstLetter(null);
        assertNull(textUpper);

        textUpper = Strings.toUpperCaseFirstLetter("");
        assertEquals("", textUpper);
    }

    @Test
    public void testTrunc() {
        String text;
        String textTrunc;
        final int chars = 10;

        text = Randoms.getString(chars + 1, true);
        textTrunc = Strings.trunc(text, chars);
        assertEquals(text.substring(0, chars) + Strings.SUFIX_TEXT_TRUNC,
                textTrunc);

        text = Randoms.getString(chars - 1, true);
        textTrunc = Strings.trunc(text, chars);
        assertEquals(text, textTrunc);

        text = Randoms.getString(chars, true);
        textTrunc = Strings.trunc(text, chars);
        assertEquals(text, textTrunc);

        assertEquals("12345", Strings.trunc("123456", 5, false));
    }

    /**
     * @see Strings#unformat(String)
     */
    @Test
    public void testUnformat() {
        assertEquals("unformatedstring", Strings.unformat(" u n   formatedstring "));
        assertEquals("unformatedstring", Strings.unformat("unform\n\t\t\n\n\ratedstring"));
        assertEquals("unformatedstring", Strings.unformat("un for\nmat\r\ned\t\ns  tr\t\ting"));
        assertNull(Strings.unformat(null));
    }

    /**
     * @see Strings#unformat(String, boolean)
     */
    @Test
    public void testUnformat_spaces() {
        assertEquals(
                "un formateds  tring",
                Strings.unformat("un for\nmat\r\ned\t\ns  tr\t\ting", false)
        );
    }
}
