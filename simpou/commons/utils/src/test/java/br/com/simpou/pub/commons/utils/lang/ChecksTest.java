package br.com.simpou.pub.commons.utils.lang;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@ClassTest(Checks.class)
public class ChecksTest {

    /**
     * @see Checks#isDecreasing(Comparable[])
     */
    @Test
    public void testIsDecreasing() {
        assertTrue(Checks.isDecreasing(3, 2, 1));
        assertTrue(Checks.isDecreasing(3, 3, 2, 1));
        assertFalse(Checks.isDecreasing(3, 1, 2));
        assertFalse(Checks.isDecreasing(2, 3, 1));
        assertFalse(Checks.isDecreasing(1, 2, 3));
        assertTrue(Checks.isDecreasing(1));
        assertTrue(Checks.isDecreasing());

        final Long nullLong = null;
        assertTrue(Checks.isDecreasing(nullLong));
        assertTrue(Checks.isDecreasing(3L, nullLong, 1L));
    }

    @Test
    public void testIsEmpty_Iterable() {
        final List<String> list = null;
        assertTrue(Checks.isEmpty(list));
        assertTrue(Checks.isEmpty(new ArrayList<Boolean>()));
        assertFalse(Checks.isEmpty(Arrays.asList(1)));
    }

    @Test
    public void testIsEmpty_Map() {
        Map<String, String> map = null;
        assertTrue(Checks.isEmpty(map));
        assertTrue(Checks.isEmpty(new HashMap<>()));

        map = new HashMap<>();
        map.put("key", "value");
        assertFalse(Checks.isEmpty(map));
    }

    @Test
    public void testIsEmpty_ObjectArr() {
        final String[] array = null;
        assertTrue(Checks.isEmpty(array));
        assertTrue(Checks.isEmpty(new Character[0]));
        assertFalse(Checks.isEmpty(new Integer[]{1}));
    }

    @Test
    public void testIsEmpty_String() {
        final String string = null;
        assertTrue(Checks.isEmpty(string));
        assertTrue(Checks.isEmpty(""));
        assertFalse(Checks.isEmpty(" "));
    }

    @Test
    public void testIsEqualsAlternative() {
        assertFalse(Checks.isEqualsAlternative("x", "y", "z"));
        assertFalse(Checks.isEqualsAlternative("x", "y", "z", null, 1));
        assertTrue(Checks.isEqualsAlternative("x", "y", "x", "z"));
        assertTrue(Checks.isEqualsAlternative("x"));
    }

    @Test
    public void testIsEqualsExclusive() {
        assertFalse(Checks.isEqualsExclusive("x", "y", "z"));
        assertFalse(Checks.isEqualsExclusive("x", "y", "z", null, 1));
        assertFalse(Checks.isEqualsExclusive("x", "y", "x", "z"));
        assertTrue(Checks.isEqualsExclusive("x", "x"));
        assertTrue(Checks.isEqualsExclusive("x", "x", "x"));
        assertTrue(Checks.isEqualsExclusive("x"));
    }

    @Test
    public void testIsInRange() {
        assertTrue(Checks.isInRange(3, 2, 4));
        assertTrue(Checks.isInRange(3, 3, 4));
        assertTrue(Checks.isInRange(3, 2, 3));
        assertFalse(Checks.isInRange(3, 1, 2));
        assertFalse(Checks.isInRange(3, 4, 5));
    }

    /**
     * @see Checks#isIncreasing(Comparable[])
     */
    @Test
    public void testIsIncreasing() {
        assertTrue(Checks.isIncreasing(1, 2, 3));
        assertTrue(Checks.isIncreasing(1, 1, 2, 3));
        assertFalse(Checks.isIncreasing(1, 3, 2));
        assertFalse(Checks.isIncreasing(2, 1, 3));
        assertFalse(Checks.isIncreasing(3, 2, 1));
        assertTrue(Checks.isIncreasing(1));
        assertTrue(Checks.isIncreasing());

        final Long nullLong = null;
        assertTrue(Checks.isIncreasing(nullLong));
        assertTrue(Checks.isIncreasing(1L, nullLong, 3L));
    }

    @Test
    public void testIsNotEmpty_Iterable() {
        final List<String> list = null;
        assertFalse(Checks.isNotEmpty(list));
        assertFalse(Checks.isNotEmpty(new ArrayList<Boolean>()));
        assertTrue(Checks.isNotEmpty(Arrays.asList(1)));
    }

    @Test
    public void testIsNotEmpty_Map() {
        final Map<String, String> list = null;
        assertFalse(Checks.isNotEmpty(list));
    }

    @Test
    public void testIsNotEmpty_ObjectArr() {
        final String[] array = null;
        assertFalse(Checks.isNotEmpty(array));
        assertFalse(Checks.isNotEmpty(new Character[0]));
        assertTrue(Checks.isNotEmpty(new Integer[]{1}));
    }

    @Test
    public void testIsNotEmpty_String() {
        final String string = null;
        assertFalse(Checks.isNotEmpty(string));
        assertFalse(Checks.isNotEmpty(""));
        assertTrue(Checks.isNotEmpty(" "));
    }

    @Test
    public void testIsNotNull() {
        assertFalse(Checks.isNotNull(1, null, 'a'));
        assertTrue(Checks.isNotNull(1, 'a', true));
    }

    /**
     * @see Checks#isSorted(int, Comparable[])
     */
    @Test
    public void testIsSorted() {
        //testIsIncreasing
        //testIsDecreasing
    }
}
