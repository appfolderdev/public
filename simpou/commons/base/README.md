# simpou-base-commons

Add some configurations to your pom:

* site reports
* integration e unit test profiles
* utilities libs

## Usage

    <parent>
        <artifactId>simpou-base-commons</artifactId>
        <groupId>br.com.simpou</groupId>
        <version>${simpouPublicVersion}</version>
    </parent>


## Tests

Runs unit tests and creates the code coverage report for unit tests:

    mvn clean test

Runs unit and integration tests and creates code coverage reports for unit and integration tests:

    mvn -Prun-all-tests clean verify

Reports are generated in:

    ${project.build.directory}/coverage-reports/jacoco/

Unit tests are:

    **/*Test.java

Integration tests are:

    **/IT*.java,**/*IT.java,**/*ITCase.java

## Site reports

* maven-changes-plugin


        Config: ${project.basedir}/src/changes/changes.xml
* maven-pmd-plugin


        Config: ${user.home}/.simpou/global/simpou-pmd-rules_${simpouPublicVersion}.xml
* maven-javadoc-plugin
* maven-jxr-plugin
* maven-checkstyle-plugin


        Config: ${user.home}/.simpou/global/simpou-checkstyle_${simpouPublicVersion}.xml
* findbugs-maven-plugin
* jdepend-maven-plugin
* javancss-maven-plugin
* maven-surefire-report-plugin
* taglist-maven-plugin
* maven-project-info-reports-plugin
* jacoco-maven-plugin (use verify command before site)

Runs unit tests, coverage reports and all others site reports:

    mvn clean verify site

Runs unit and integration tests, coverage reports and all others site reports:

    mvn -Prun-all-tests clean verify site

Maven multi-module projects use stage command:

    mvn -Prun-all-tests clean verify site:site site:stage

site will be saved in:

    ${project.basedir}/staging/
