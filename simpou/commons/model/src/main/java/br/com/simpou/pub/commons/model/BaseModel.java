package br.com.simpou.pub.commons.model;

import java.io.Serializable;

/**
 * Sinaliza que um objeto pode ser obtido de alguma maneira.
 *
 * @author Jonas Pereira
 * @version 2013-06-01
 * @since 2012-09-29
 */
public interface BaseModel extends Serializable, Cloneable {

    BaseModel doClone();
}
