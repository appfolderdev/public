package br.com.simpou.pub.commons.model.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateSerializer extends JsonSerializer<Date> {

    private final DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    public void serialize(final Date value,
                          final JsonGenerator gen,
                          final SerializerProvider serializers) throws IOException {
        if (value != null) {
            gen.writeString(df.format(value));
        }
    }
}
