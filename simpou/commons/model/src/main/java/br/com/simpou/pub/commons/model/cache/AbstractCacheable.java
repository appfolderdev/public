package br.com.simpou.pub.commons.model.cache;

import br.com.simpou.pub.commons.utils.lang.Nulls;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.Date;

/**
 * Implementação básica de um objeto cujas operações de cache sobre ele estão disponíveis. A sua data de
 * modificação é o exato instante em que entrou em memória e ela somente é modificada em caso de nova
 * instanciação, ou seja, a alteração em seus campos não irão indicar um novo objeto, já que a marca temporal
 * para uma mesma instância é imutável.
 * <p/>
 * A marca temporal não é serializada juntamente com o objeto, caso este processo ocorra, a informação
 * anterior será perdida e uma nova será inicializada no processo de desserialização, seja ele qual for.
 * <p/>
 * É possível forçar a data de criação pela subclasse durante instanciação, isso é útil caso haja listas de
 * elementos cacheáveis, sendo a data mais recente considerada como a de modificação.
 * <p/>
 * <p/>
 * <b> Não use esta classe a menos que queira este exato comportamento, caso contrário o mecanismo de cache
 * será prejudicado. </b>
 *
 * @author Jonas Pereira
 * @since 2013-03-07
 */
@ToString(of = {"lastModified"})
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class AbstractCacheable implements Cacheable {

    /**
     * Momento de entrada em memória do objeto.
     */
    //A idéia é que o mecanismo de cache não interfira no modelo de dados do cliente.
    private final transient Date lastModified;

    /**
     * Força uma data de modificação qualquer.
     *
     * @param lastModified Data de modificação forçada.
     */
    protected AbstractCacheable(final Date lastModified) {
        this.lastModified = Nulls.clone(lastModified);
    }

    /**
     * A data de modificação será o momento de invocação deste construtor.
     */
    public AbstractCacheable() {
        this.lastModified = new Date();
    }

    @Override
    public final Date lastModified() {
        return Nulls.clone(this.lastModified);
    }

}
