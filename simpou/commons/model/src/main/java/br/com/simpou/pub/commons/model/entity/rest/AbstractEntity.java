package br.com.simpou.pub.commons.model.entity.rest;

import br.com.simpou.pub.commons.model.entity.EntityConstants;
import br.com.simpou.pub.commons.model.serialize.ResponseObject;
import br.com.simpou.pub.commons.utils.lang.Encapsulations;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

//TODO trocar nome
@MappedSuperclass
//@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractEntity<T extends Serializable> implements RestEntity<T> {

    //    @JsonView(GlobalView.class)
    //    @JacksonXmlProperty(localName =LINK_NAME_XML)
    //    @JsonProperty(Link.LINK_NAME_JSON)
    //    public Link<? extends RestEntity<T>> link;
    //
    //    public void link(final Link<? extends RestEntity<T>> link){
    //        this.link = link;
    //    }

    @JsonIgnore
    @Column(name = "updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    @JsonIgnore
    @Column(name = "created", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public static boolean exists(final AbstractEntity entity) {
        return entity != null && entity.exists();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AbstractEntity<?> that = (AbstractEntity<?>) o;
        return Objects.equals(identity(), that.identity());
    }

    @Override
    public final boolean exists() {
        return identity() != null;
    }

    public final Date getCreated() {
        return Encapsulations.getDate(this.created);
    }

    public void setCreated(final Date created) {
        if (this.created == null) {
            this.created = created;
            //        } else {
            //            throw new UnsupportedOperationException();
        }
    }

    //    @JsonView(GlobalView.class)
    @JsonProperty(EntityConstants.ID_NAME)
    public Serializable getId() {
        return restId().pathId();
    }

    //    @JsonView(GlobalView.class)
    @JsonUnwrapped
    @JsonProperty(ResponseObject.LINK_NAME_JSON)
    @JacksonXmlProperty(localName = ResponseObject.LINK_NAME_XML, isAttribute = true)
    public String getLink() {
        if (identity() == null) {
            return null;
        }
        final String appName = appName();
        return (appName == null ? "" : "/" + appName) + EntityConstants.REST_ENTITY_PATH + "/" + typeName() + "/" + restId().pathId() + "/";
    }

    public Date getUpdated() {
        return Encapsulations.getDate(this.updated);
    }

    public void setUpdated(final Date updated) {
        if (this.updated == null) {
            this.updated = updated;
            //        } else {
            //            throw new UnsupportedOperationException();
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(identity());
    }

    @Override
    public Date lastModified() {
        return getUpdated();
    }

    @Override
    public String objectTag() {
        return this.getClass() + "#" + identity();
    }

    @PrePersist
    protected void preCreate() {
        preUpdate();
        this.created = new Date();
    }

    @PreUpdate
    protected void preUpdate() {
        validate();
        updated();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "[" + identity() + "]";
    }

    @Override
    public final void uncheckedId(final Serializable id) {
        checkedId((T) id);
    }

    public void updated() {
        this.updated = new Date();
    }

    protected void validate() {
        // método gancho: default ser válido
    }
}
