package br.com.simpou.pub.commons.model;

/**
 * Modelo concreto básico.
 *
 * @author Jonas Pereira
 * @version 2013-06-01
 * @since 2013-06-01
 */
public abstract class AbstractModel implements BaseModel {

    @Override
    public BaseModel doClone() {
        try {
            return (BaseModel) super.clone();
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException(ex);
        }
    }

}
