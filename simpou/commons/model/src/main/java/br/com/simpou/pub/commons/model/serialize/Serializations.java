package br.com.simpou.pub.commons.model.serialize;

import br.com.simpou.pub.commons.utils.lang.Casts;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.*;

import static com.fasterxml.jackson.databind.MapperFeature.DEFAULT_VIEW_INCLUSION;

/**
 * Created by jonas on 21/04/2016.
 */
public class Serializations {

    private static final ObjectMapper JACKSON_OBJECT_MAPPER = new ObjectMapper();

    public static <T> T applyView(final T obj, final Class<?> view) throws IOException {
        final ObjectMapper objectMapper = new ObjectMapper().configure(DEFAULT_VIEW_INCLUSION, false);
        return applyView(obj, view, objectMapper);
    }

    public static <T> T applyView(final T obj, final Class<?> view, final ObjectMapper objectMapper) throws IOException {
        return objectMapper
                .readerWithView(view)
                .forType(obj.getClass())
                .readValue(objToJsonJackson(obj, objectMapper));
    }

    /**
     * Recupera um objeto a partir de uma arquivo binário.
     *
     * @see #objToBinFile(Serializable, File)
     */
    public static <T extends Serializable> T binFileToObj(final Class<T> type,
                                                          final File fromFile) throws IOException, ClassNotFoundException {
        try (
                final FileInputStream f_in = new FileInputStream(fromFile);
                final ObjectInputStream obj_in = new ObjectInputStream(f_in)
        ) {
            final Object obj = obj_in.readObject();

            final Class<?> objClass = obj.getClass();
            if (type.isAssignableFrom(objClass)) {
                return Casts.simpleCast(obj);
            } else {
                final Class<? extends Class> typeClass = type.getClass();
                throw new IllegalArgumentException(
                        "Invalid type. Expected " + objClass + " but given " + typeClass
                );

            }
        }
    }

    public static <T> T jsonToObjByJackson(final Class<T> type, final String json) {
        return jsonToObjByJackson(type, json, JACKSON_OBJECT_MAPPER);
    }

    public static <T> T jsonToObjByJackson(final Class<T> type, final String json, final ObjectMapper objectMapper) {
        try {
            return objectMapper.readValue(json, type);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Salva um objeto em arquivo binário.
     */
    public static void objToBinFile(final Serializable obj,
                                    final File toFile) throws IOException {
        try (
                final FileOutputStream f_out = new FileOutputStream(toFile);
                final ObjectOutputStream obj_out = new ObjectOutputStream(f_out)
        ) {
            obj_out.writeObject(obj);
        }
    }

    public static <T> byte[] objToJsonJackson(final T obj) {
        return objToJsonJackson(obj, JACKSON_OBJECT_MAPPER);
    }

    public static <T> byte[] objToJsonJackson(final T obj, final ObjectMapper objectMapper) {
        final byte[] asBytes;
        try {
            asBytes = objectMapper.writeValueAsBytes(obj);
        } catch (final JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return asBytes;
    }

    public static byte[] objToXmlJackson(final Object obj, final XmlMapper mapper) throws IOException {
        return mapper.writeValueAsBytes(obj);
    }

    public static <T> T xmlToObject(final Class<T> type, final XmlMapper mapper, final byte[] content) throws IOException {
        return mapper.readValue(content, type);
    }

    public static <T> T xmlToObject(final Class<T> type, final byte[] content) throws IOException {
        return xmlToObject(type, new XmlMapper(), content);
    }
}
