package br.com.simpou.pub.commons.model.mixin;

import lombok.Getter;

import java.util.Collection;
import java.util.Collections;

public class MixinsDefinition {

    @Getter
    private final Collection<MixinDefinitionItem> itemList;

    public MixinsDefinition(final Collection<MixinDefinitionItem> itemList) {
        this.itemList = Collections.unmodifiableCollection(itemList);
    }
}
