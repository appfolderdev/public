package br.com.simpou.pub.commons.model.view;

import br.com.simpou.pub.commons.model.entity.EntityUtil;
import br.com.simpou.pub.commons.model.entity.rest.RestEntity;
import br.com.simpou.pub.commons.model.serialize.ResponseObject;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;

import java.io.Serializable;

import static br.com.simpou.pub.commons.model.entity.EntityConstants.REST_ENTITY_PATH;

@Getter
public class Link implements Serializable {

    @JsonView(GlobalView.class)
    @JsonUnwrapped
    @JsonProperty(ResponseObject.LINK_NAME_JSON)
    @JacksonXmlProperty(localName = ResponseObject.LINK_NAME_XML)
    private final String ref;

    @JsonView(GlobalView.class)
    @JsonProperty("@type")
    @JacksonXmlProperty(localName = "_type", isAttribute = true)
    private final String type = "link";

    public Link(final String ref) {
        this.ref = ref;
    }

    public Link(final String baseUrl, final String name) {
        this(baseUrl + "/" + name);
    }

    public Link(final RestEntity<?> entity) {
        final String id;
        if (entity.identity() != null) {
            id = entity.restId().pathId() + "";
        } else {
            id = "";
        }
        //        final String appName = entity.appName();
        //        this.ref = (appName == null ? "" : "/" + appName) + REST_ENTITY_PATH + "/" + entity.typeName() + "/" + id;
        this.ref = REST_ENTITY_PATH + "/" + entity.typeName() + "/" + id;
    }

    public static Link from(final RestEntity<?> entity) {
        return EntityUtil.exists(entity) ? new Link(entity) : null;
    }

    //    @Override
    //    public void setRef(String ref) {
    //        throw new UnsupportedOperationException();
    //    }
    //
    //    @Override
    //    public void setType(String type) {
    //        throw new UnsupportedOperationException();
    //    }
    //
    //    @Override
    //    public Date lastModified() {
    //        return null;
    //    }
    //
    //    @Override
    //    public String objectTag() {
    //        return null;
    //    }
}
