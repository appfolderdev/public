package br.com.simpou.pub.commons.model.serialize;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

/**
 * Created by jonas on 11/01/2017.
 */
@Getter
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseLink implements Serializable {

    private final RelLinkType rel;

    private final String href;

    public ResponseLink(final String href) {
        this(null, href);
    }
}
