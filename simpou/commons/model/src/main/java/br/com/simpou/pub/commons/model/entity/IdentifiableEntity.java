package br.com.simpou.pub.commons.model.entity;

import br.com.simpou.pub.commons.model.IdentifiableModel;

import java.io.Serializable;

/**
 * Entidade identificável. Há um identificador único por entidade.
 *
 * @author Jonas Pereira
 * @version 2013-06-30
 * @since 2012-06-11
 */
public interface IdentifiableEntity<T extends Serializable>
        extends BaseEntity, IdentifiableModel<T> {

}
