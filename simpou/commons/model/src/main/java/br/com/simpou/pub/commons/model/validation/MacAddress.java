package br.com.simpou.pub.commons.model.validation;

import br.com.simpou.pub.commons.utils.validation.MacAddressValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

@Constraint(validatedBy = MacAddress.ValidPhoneImpl.class)
@Documented
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface MacAddress {

    String MESSAGE = "{br.com.simpou.pub.commons.model.validator.MacAddress.invalid}";

    String message() default MESSAGE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    public class ValidPhoneImpl implements ConstraintValidator<MacAddress, String> {

        @Override
        public void initialize(final MacAddress constraintAnnotation) {
        }

        @Override
        public boolean isValid(final String value,
                               final ConstraintValidatorContext context) {
            return MacAddressValidator.isValid(value) == null;
        }
    }
}
