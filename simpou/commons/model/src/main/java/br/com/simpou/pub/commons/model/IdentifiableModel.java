package br.com.simpou.pub.commons.model;

import br.com.simpou.pub.commons.utils.behavior.Identifiable;

import java.io.Serializable;

/**
 * Modelo identificável.
 *
 * @author Jonas Pereira
 * @version 2013-06-30
 * @since 2013-06-30
 */
public interface IdentifiableModel<T extends Serializable>
        extends BaseModel, Identifiable<T> {

}
