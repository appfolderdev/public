package br.com.simpou.pub.commons.model.mixin;

import lombok.Value;

@Value
public class MixinDefinitionItem {

    private Class<?> target;

    private Class<?> mixin;
}
