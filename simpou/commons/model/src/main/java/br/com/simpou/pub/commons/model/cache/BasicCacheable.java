package br.com.simpou.pub.commons.model.cache;

import lombok.ToString;

import java.util.Date;

/**
 * @author Jonas Pereira
 * @since 22/04/2016
 */
@ToString(of = {"objectTag"}, callSuper = true)
public class BasicCacheable extends AbstractCacheable {

    private final String objectTag;

    public BasicCacheable(final String objectTag) {
        this(new Date(), objectTag);
    }

    public BasicCacheable(final Cacheable cacheable) {
        this(CacheableHelper.lastModified(cacheable), CacheableHelper.objectTag(cacheable));
    }

    public BasicCacheable(final Date lastModified, final String objectTag) {
        super(lastModified);
        this.objectTag = objectTag;
    }

    @Override
    public String objectTag() {
        return this.objectTag;
    }
}
