package br.com.simpou.pub.commons.model.entity;

/**
 * Entidade concreta identificável via identificador único do tipo Long.
 *
 * @author Jonas Pereira
 * @version 2013-06-06
 * @since 2013-06-01
 */
public abstract class AbstractIndexedEntity extends AbstractIdentifiableEntity<Long>
        implements IndexedEntity {

    /**
     * Tipo do id é setado para {@link java.lang.Long}.
     */
    public AbstractIndexedEntity() {
        super(Long.class);
    }
}
