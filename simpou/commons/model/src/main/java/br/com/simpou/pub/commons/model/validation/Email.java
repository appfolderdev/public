package br.com.simpou.pub.commons.model.validation;

import br.com.simpou.pub.commons.utils.validation.EmailValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

/**
 * Java Beans validator: valida se string representa um e-mail.
 *
 * @author Jonas Pereira
 * @version 2012-10-01
 * @since 2012-10-01
 */
@Constraint(validatedBy = Email.ValidEmailImpl.class)
@Documented
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface Email {

    String MESSAGE = "{br.com.simpou.pub.commons.model.validator.Email.invalid}";

    String message() default MESSAGE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    public class ValidEmailImpl implements ConstraintValidator<Email, String> {

        @Override
        public void initialize(final Email constraintAnnotation) {
        }

        @Override
        public boolean isValid(final String value,
                               final ConstraintValidatorContext context) {
            return EmailValidator.isValid(value);
        }
    }
}
