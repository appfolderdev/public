package br.com.simpou.pub.commons.model.jpa.entity;

import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(AbstractJpaIndexedEntity.class)
public abstract class AbstractJpaIndexedEntity_ extends AbstractJpaIdentifiableEntity_ {

}
