package br.com.simpou.pub.commons.model.entity.rest;

import java.io.Serializable;

public interface RestId extends Serializable {

    Serializable pathId();

}
