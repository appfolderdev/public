package br.com.simpou.pub.commons.model.validation;

import br.com.simpou.pub.commons.utils.validation.IPValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

/**
 * Java Beans validator: valida se string representa um endereço IP.
 *
 * @author Jonas Pereira
 * @version 2012-10-01
 * @since 2012-10-01
 */
@Constraint(validatedBy = IP.ValidIPImpl.class)
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IP {

    String MESSAGE = "{br.com.simpou.pub.commons.model.validator.IP.invalid}";

    String message() default MESSAGE;

    Class<?>[] groups() default {
    }
            ;

    Class<? extends Payload>[] payload() default {
    }
            ;

    public class ValidIPImpl implements ConstraintValidator<IP, String> {

        @Override
        public void initialize(final IP constraintAnnotation) {
        }

        @Override
        public boolean isValid(final String value,
                               final ConstraintValidatorContext context) {
            return IPValidator.isValid(value) == null;
        }
    }
}
