package br.com.simpou.pub.commons.model.entity;

/**
 * Entidade identificável via identificador único do tipo Long.
 *
 * @author Jonas Pereira
 * @version 2012-10-01
 * @since 2012-09-29
 */
public interface IndexedEntity extends IdentifiableEntity<Long> {

}
