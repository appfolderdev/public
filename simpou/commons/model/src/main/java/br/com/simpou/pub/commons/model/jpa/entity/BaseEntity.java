package br.com.simpou.pub.commons.model.jpa.entity;

import br.com.simpou.pub.commons.model.dto.IdentifiableModel;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Objects;

//@Getter
//@Setter
@SuperBuilder
@NoArgsConstructor
//@AllArgsConstructor(access = AccessLevel.PRIVATE)
@MappedSuperclass
public abstract class BaseEntity<I extends Serializable> implements IdentifiableModel<I> {

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BaseEntity)) {
            return false;
        }
        final BaseEntity<?> that = (BaseEntity<?>) o;
        return Objects.equals(getId(), that.getId());
    }

    public abstract I getId();

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public final I identity() {
        return getId();
    }

    public boolean isNew() {
        return getId() == null;
    }
}
