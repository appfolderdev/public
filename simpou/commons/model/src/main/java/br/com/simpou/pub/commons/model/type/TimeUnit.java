package br.com.simpou.pub.commons.model.type;

import br.com.simpou.pub.commons.utils.lang.Dates;

import java.util.Date;

public enum TimeUnit {
    NANOSECONDS,
    MICROSECONDS,
    MILLISECONDS,
    SECONDS,
    MINUTES,
    HOURS,
    DAYS,
    WORK_DAYS,
    WEEKS,
    MONTHS,
    YEARS,
    FOREVER;

    public Date add(final Date date, final int amount) {
        switch (this) {
            case DAYS:
                return Dates.addDate(date, 0, 0, amount);
            case MONTHS:
                return Dates.addDate(date, 0, amount, 0);
            case YEARS:
                return Dates.addDate(date, amount, 0, 0);
            default:
                throw new UnsupportedOperationException(this.name());
        }
    }

}
