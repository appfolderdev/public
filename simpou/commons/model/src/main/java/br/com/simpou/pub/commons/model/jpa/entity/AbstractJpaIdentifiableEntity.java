package br.com.simpou.pub.commons.model.jpa.entity;

import br.com.simpou.pub.commons.model.entity.AbstractIdentifiableEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entidade JPA identificável concreta.
 *
 * @author Jonas Pereira
 * @version 2013-06-06
 * @since 2013-06-06
 */
@MappedSuperclass
@Access(AccessType.PROPERTY)
public abstract class AbstractJpaIdentifiableEntity<T extends Serializable>
        extends AbstractIdentifiableEntity<T> {

    /**
     * @see br.com.simpou.pub.commons.model.entity.AbstractIdentifiableEntity#AbstractIdentifiableEntity(Class)
     */
    public AbstractJpaIdentifiableEntity(final Class<T> clasz) {
        super(clasz);
    }

    /**
     * Parâmetro "clasz" é null.
     *
     * @see #AbstractJpaIdentifiableEntity(Class)
     */
    public AbstractJpaIdentifiableEntity() {
        this(null);
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Override
    public T getId() {
        return super.getId();
    }
}
