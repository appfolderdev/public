package br.com.simpou.pub.commons.model.validation;

import br.com.simpou.pub.commons.utils.validation.PhoneValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

/**
 * Java Beans validator: valida se string representa um telefone em formato internacional:
 * Ex.:+55(31)988888888 (+country code (area code) phone number).
 *
 * @author Jonas Pereira
 * @since 2018-07-23
 */
@Constraint(validatedBy = Phone.ValidPhoneImpl.class)
@Documented
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface Phone {

    String MESSAGE = "{br.com.simpou.pub.commons.model.validator.Phone.invalid}";

    String message() default MESSAGE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    public class ValidPhoneImpl implements ConstraintValidator<Phone, String> {

        @Override
        public void initialize(final Phone constraintAnnotation) {
        }

        @Override
        public boolean isValid(final String value,
                               final ConstraintValidatorContext context) {
            return PhoneValidator.isValid(value) == null;
        }
    }
}
