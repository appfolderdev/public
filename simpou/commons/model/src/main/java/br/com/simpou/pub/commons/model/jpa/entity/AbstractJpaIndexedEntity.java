package br.com.simpou.pub.commons.model.jpa.entity;

import br.com.simpou.pub.commons.model.entity.IndexedEntity;

import javax.persistence.MappedSuperclass;

/**
 * Entidade JPA com ID numérico. Utiliza tipo automático de gerador de IDs.
 *
 * @author Jonas Pereira
 * @version 2013-06-06
 * @since 2012-05-17
 */
@MappedSuperclass
public abstract class AbstractJpaIndexedEntity
        extends AbstractJpaIdentifiableEntity<Long>
        implements IndexedEntity {

    public AbstractJpaIndexedEntity() {
        super(Long.class);
    }

}
