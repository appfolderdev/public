package br.com.simpou.pub.commons.model.entity.rest;

import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@RequiredArgsConstructor
public class LongRestId implements RestId {

    public static final String ID_PATH = "{id}";

    private final Long id;

    @Override
    public Serializable pathId() {
        return id;
    }
}
