package br.com.simpou.pub.commons.model.validation;

import br.com.simpou.pub.commons.utils.lang.Casts;
import br.com.simpou.pub.commons.utils.lang.Checks;
import br.com.simpou.pub.commons.utils.violation.ValidationViolation;
import br.com.simpou.pub.commons.utils.violation.Violation;
import br.com.simpou.pub.commons.utils.violation.ViolationsException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.*;

/**
 * Auxilia nas operações de validação de beans.
 *
 * @author jonas.pereira
 * @since 17/01/13
 */
public class ValidatorHelper {

    /**
     * Objeto representante da não ocorrência de violações.
     */
    private static final Set<ConstraintViolation<?>> EMPTY_VIOLATIONS = new HashSet<>(0);

    /**
     * Implementação do validador do Beans Validation.
     */
    private static final Validator VALIDATOR;

    static {
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        VALIDATOR = factory.getValidator();
    }

    private static Object[] extractParams(final ConstraintViolation v) {
        final Collection paramsList = v.getConstraintDescriptor().getAttributes().values();
        return paramsList.toArray(new Object[0]);
    }

    /**
     * Valida um bean segundo regras definidas via beans validation (JSR 303).
     *
     * @param bean Bean a ser validado. Seus atributos devem definir anotações do beans validation.
     * @return Erros de validação se houverem ou lista vazia.
     */
    public static <T> Set<ConstraintViolation<T>> invokeBeansValidation(
            final T bean) {
        // normalmente isso não retorna null, mas é possível que alguma
        // implementação o faça
        final Set<ConstraintViolation<T>> errors = VALIDATOR.validate(bean);

        if (Checks.isEmpty(errors)) {
            return Casts.simpleCast(EMPTY_VIOLATIONS);
        } else {
            return errors;
        }
    }

    private static <T> void throwException(final Set<ConstraintViolation<T>> constraintViolations) {
        if (!constraintViolations.isEmpty()) {
            final List<Violation> violations = new ArrayList<>();
            for (final ConstraintViolation<T> v : constraintViolations) {
                violations.add(toViolation(v));
            }
            throw new ViolationsException(violations);
        }
    }

    public static ValidationViolation toViolation(final ConstraintViolation v){
        return new ValidationViolation(
                v.getRootBean(),
                v.getPropertyPath() + "",
                v.getInvalidValue(),
                v.getMessageTemplate(),
                extractParams(v)
        );
    }

    public static <T> void validate(final Validator validator, final T o) {
        throwException(validator.validate(o));
    }

    public static <T> void validateProperties(final Validator validator, final T o, final String... props) {
        final Set<ConstraintViolation<T>> constraintViolations = new HashSet<>();
        for (final String prop : props) {
            constraintViolations.addAll(validator.validateProperty(o, prop));
        }
        throwException(constraintViolations);
    }

    public static <T> void validateValue(final Class<T> beanType, final String propertyName, final Object value) {
        final Set<ConstraintViolation<T>> constraintViolations = VALIDATOR.validateValue(beanType, propertyName, value);
        throwException(constraintViolations);
    }
}
