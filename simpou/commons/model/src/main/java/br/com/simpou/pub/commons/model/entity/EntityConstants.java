package br.com.simpou.pub.commons.model.entity;

import lombok.experimental.UtilityClass;

/**
 * Created by jonas on 02/02/17.
 */
@UtilityClass
public class EntityConstants {

    public static final int PK_ALLOCATION_SIZE = 1;

    public static final int PK_INITIAL_VALUE = 1;

    public static final String LIST_SUFFIX = "List";

    public static final String ID_NAME = "id";

    public static final char JOIN_CHAR = '_';

    public static final String ID_SUFFIX = JOIN_CHAR + ID_NAME;

    public static final String ROLE_PREFFIX = "ROLE_";

    public static final String REST_ENTITY_NAME = "entity";

    public static final String REST_ENTITY_PATH = "/" + REST_ENTITY_NAME;

}
