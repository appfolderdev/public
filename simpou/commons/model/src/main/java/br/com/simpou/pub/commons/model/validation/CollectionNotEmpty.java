package br.com.simpou.pub.commons.model.validation;

import br.com.simpou.pub.commons.utils.lang.Checks;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Collection;
import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

/**
 * Java Beans validator: valida se uma coleção tem pelo menos um item.
 *
 * @author Jonas Pereira
 * @since 2017-01-31
 */
@Constraint(validatedBy = CollectionNotEmpty.ValidLocaleImpl.class)
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CollectionNotEmpty {

    String MESSAGE = "{br.com.simpou.pub.commons.model.validator.CollectionNotEmpty.invalid}";

    String message() default MESSAGE;

    Class<?>[] groups() default {
    };

    Class<? extends Payload>[] payload() default {
    };

    public class ValidLocaleImpl implements ConstraintValidator<CollectionNotEmpty, Collection<?>> {

        @Override
        public void initialize(final CollectionNotEmpty constraintAnnotation) {
        }

        @Override
        public boolean isValid(final Collection<?> value,
                               final ConstraintValidatorContext context) {
            return Checks.isNotEmpty(value);
        }
    }
}
