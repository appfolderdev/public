package br.com.simpou.pub.commons.model.validation;

import br.com.simpou.pub.commons.utils.lang.Casts;
import br.com.simpou.pub.commons.utils.lang.Checks;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.HashSet;
import java.util.Set;

/**
 * Auxilia nas operações de validação de beans.
 *
 * @author Jonas Pereira
 * @version 2013-06-02
 * @since 2013-06-02
 */
public class BeansValidator {

    /**
     * Objeto representante da não ocorrência de violações.
     */
    private static final Set<ConstraintViolation<?>> EMPTY_VIOLATIONS = new HashSet<ConstraintViolation<?>>(0);

    /**
     * Implementação do validador do Beans Validation.
     */
    private static final Validator VALIDATOR;

    static {
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        VALIDATOR = factory.getValidator();
    }

    /**
     * Valida um bean segundo regras definidas via beans validation (JSR 303).
     *
     * @param bean Bean a ser validado. Seus atributos devem definir anotações do beans validation.
     * @return Erros de validação se houverem ou lista vazia.
     */
    public static <T> Set<ConstraintViolation<T>> invoke(final T bean) {
        // normalmente isso não retorna null, mas é possível que alguma 
        // implementação o faça
        final Set<ConstraintViolation<T>> errors = VALIDATOR.validate(bean);

        if (Checks.isEmpty(errors)) {
            return Casts.simpleCast(EMPTY_VIOLATIONS);
        } else {
            return errors;
        }
    }
}
