package br.com.simpou.pub.commons.model.entity.rest;

import br.com.simpou.pub.commons.model.cache.Cacheable;
import br.com.simpou.pub.commons.utils.behavior.TypeNamed;

public interface RestModel extends Cacheable, TypeNamed {

}
