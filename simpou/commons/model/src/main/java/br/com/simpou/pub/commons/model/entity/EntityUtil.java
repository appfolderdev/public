package br.com.simpou.pub.commons.model.entity;

import br.com.simpou.pub.commons.model.entity.rest.RestEntity;
import lombok.experimental.UtilityClass;

import java.io.Serializable;

@UtilityClass
public class EntityUtil {

    public boolean exists(final RestEntity<?> entity) {
        return entity != null && entity.exists();
    }

    public <T extends Serializable> T getId(final RestEntity<T> entity) {
        return entity == null ? null : entity.identity();
    }
}
