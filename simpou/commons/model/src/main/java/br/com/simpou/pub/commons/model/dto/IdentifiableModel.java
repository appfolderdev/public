package br.com.simpou.pub.commons.model.dto;

import java.io.Serializable;

public interface IdentifiableModel<I extends Serializable> extends Identifiable<I>, Serializable {
}
