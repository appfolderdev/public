package br.com.simpou.pub.commons.model.entity;

import br.com.simpou.pub.commons.model.IdentifiableModelHelper;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Entidade identificável concreta.
 *
 * @author Jonas Pereira
 * @version 2013-06-30
 * @since 2013-06-06
 */
@RequiredArgsConstructor
public abstract class AbstractIdentifiableEntity<T extends Serializable>
        extends AbstractEntity
        implements IdentifiableEntity<T> {

    @Setter
    @Getter
    private T id;

    private final Class<T> clasz;

    @Override
    public final boolean equals(final Object obj) {
        return IdentifiableModelHelper.equalsOf(this, obj);
    }

    @Override
    public final int hashCode() {
        return IdentifiableModelHelper.hashCodeFrom(this);
    }

    @Override
    public T identity() {
        return id;
    }

    @Override
    public void checkedId(final T cid) {
        setId(cid);
    }

    @Override
    public void uncheckedId(final Serializable uid) {
        checkedId(IdentifiableModelHelper.toCheckedId(clasz, uid));
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "#" + identity();
    }
}
