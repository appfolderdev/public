package br.com.simpou.pub.commons.model.exception;

/**
 * Identidade inválida de entidade.
 *
 * @author Jonas Pereira
 * @version 2013-06-01
 * @since 2013-06-01
 */
public class IllegalIdentityException extends RuntimeException {

    /**
     * @param expected Tipo esperado.
     * @param given    Tipo fornecido.
     */
    public IllegalIdentityException(final Class<?> expected,
                                    final Class<?> given) {
        super("Illegal identity type. Expected: " + expected.getName() +
                ", given: " + given.getName());
    }

    public IllegalIdentityException(final String msg) {
        super(msg);
    }

    public IllegalIdentityException(final int expected,
                                    final int given) {
        super("Expected: " + expected + " parts, given: " + given);
    }

    public IllegalIdentityException(final String msg,
                                    final Throwable throwable) {
        super(msg, throwable);
    }
}
