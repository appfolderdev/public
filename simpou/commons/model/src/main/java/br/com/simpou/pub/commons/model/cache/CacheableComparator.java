package br.com.simpou.pub.commons.model.cache;

import org.apache.commons.lang3.builder.CompareToBuilder;

import java.util.Comparator;

import static br.com.simpou.pub.commons.model.cache.CacheableHelper.lastModified;

/**
 * Ordena por data de modificação decrescente. Nulls ficam por último.
 *
 * @author Jonas Pereira
 * @since 05/04/2020
 */
public class CacheableComparator implements Comparator<Cacheable> {
    @Override
    public int compare(final Cacheable o1, final Cacheable o2) {
        return new CompareToBuilder()
                .append(objectTag(o2), objectTag(o1))
                .append(lastModified(o2), lastModified(o1))
                .build();
    }

    private int objectTag(final Cacheable cacheable) {
        final String objectTag = CacheableHelper.objectTag(cacheable);
        return objectTag == null ? 0 : 1;
    }
}
