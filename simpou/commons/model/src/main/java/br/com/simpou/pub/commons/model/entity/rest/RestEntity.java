package br.com.simpou.pub.commons.model.entity.rest;

import br.com.simpou.pub.commons.utils.behavior.Identifiable;

import java.io.Serializable;

public interface RestEntity<T extends Serializable> extends RestModel, Identifiable<T> {

    String appName();

    boolean exists();

    Class<T> idType();

    RestId restId();

}
