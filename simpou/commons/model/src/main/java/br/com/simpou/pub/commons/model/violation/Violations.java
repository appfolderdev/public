package br.com.simpou.pub.commons.model.violation;

import br.com.simpou.pub.commons.utils.string.Strings;
import br.com.simpou.pub.commons.utils.violation.BusinessViolationException;
import br.com.simpou.pub.commons.utils.violation.ValidationViolation;
import lombok.experimental.UtilityClass;

import javax.validation.ConstraintViolation;
import javax.validation.Path;
import java.util.*;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;

/**
 * @author Jonas Pereira
 * @since 2012-12-28
 */
@UtilityClass
public class Violations {

    /**
     * Importa os erros de validação sobre as entidades vindos do Beans Validation.
     *
     * @param violations Violações do Beans Validations.
     * @return Violações de validação extraídas.
     */
    public List<ValidationViolation> extractViolations(final Set<? extends ConstraintViolation<?>> violations) {
        final List<ValidationViolation> validViolations = new ArrayList<>();

        for (final ConstraintViolation<?> violation : violations) {
            final ValidationViolation validation = new ValidationViolation();
            validation.setInvalidValue(violation.getInvalidValue());
            validation.setMessage(violation.getMessage());
            validation.setBeanClass(violation.getRootBeanClass().getName());

            if (violation.getPropertyPath() != null) {
                final Iterator<Path.Node> iterator = violation.getPropertyPath().iterator();
                if (iterator.hasNext()) {
                    validation.setField(iterator.next().getName());
                }
            }
            validation.setCode(violation.getMessageTemplate());
            validViolations.add(validation);
        }
        return validViolations;
    }

    public void required(final Object fieldValue, final String... fieldNames) {
        if (fieldValue == null) {
            throw new BusinessViolationException("business.fieldRequired", Strings.concatDots(fieldNames));
        }
    }

    public void required(final Collection<?> fieldValue, final String... fieldNames) {
        if (isEmpty(fieldValue)) {
            throw new BusinessViolationException("business.atLeastOneRequired", Strings.concatDots(fieldNames));
        }
    }

}
