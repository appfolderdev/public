package br.com.simpou.pub.commons.model.cache;

/**
 * Classe cuja herdeira terá seu cache desabilitado.
 *
 * @author Jonas Pereira
 * @since 2014-02-27
 */
public class DummyCacheable extends BasicCacheable {

    public DummyCacheable() {
        super(null, null);
    }

}
