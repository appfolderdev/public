package br.com.simpou.pub.commons.model.cache;

import lombok.experimental.UtilityClass;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.apache.commons.collections4.IterableUtils.isEmpty;

/**
 * Operações úteis para manipulação de objetos cacheáveis.
 *
 * @author Jonas Pereira
 * @since 05/02/2015.
 */
@UtilityClass
public class CacheableHelper {

    /**
     * @param cacheable Cacheable.
     * @return boolean
     */
    public static boolean isCacheDisabled(final Cacheable cacheable) {
        return cacheable == null || isCacheDisabled(cacheable.lastModified(), cacheable.objectTag());
    }

    /**
     * @param lastModified Ultima data de modificação.
     * @param objectTag    Objeto.
     * @return boolean.
     */
    public static boolean isCacheDisabled(final Date lastModified, final String objectTag) {
        return lastModified == null || objectTag == null;
    }

    /**
     * Forma null-safe para obter a data de modificação.
     */
    public static Date lastModified(final Cacheable cacheable) {
        return cacheable == null ? null : cacheable.lastModified();
    }

    /**
     * @param date      Data a ser comparada.
     * @param cacheable Data de modificação a ser considerada.
     * @return Data mais recente entre a data de modificação do cacheable e a data parametrizada.
     */
    public static Date lastModifiedDate(final Date date, final Cacheable cacheable) {
        final Date lastModified = lastModified(cacheable);
        if (date == null) {
            return lastModified;
        }

        if (lastModified == null) {
            return date;
        }

        return date.after(lastModified) ? date : lastModified;
    }

    /**
     * @return Objeto com a data de modificação mais recente.
     */
    public static Cacheable lastModifiedItem(final Cacheable... cacheables) {
        return lastModifiedItem(Arrays.asList(cacheables));
    }

    /**
     * @return Objeto com a data de modificação mais recente.
     */
    public static Cacheable lastModifiedItem(final List<? extends Cacheable> cacheables) {
        final Cacheable cacheable;
        if (isEmpty(cacheables)) {
            cacheable = new DummyCacheable();
        } else {
            Collections.sort(cacheables, new CacheableComparator());
            cacheable = cacheables.get(0);
        }
        return cacheable;
    }

    /**
     * @param date       Data a ser comparada.
     * @param cacheables Datas de modificação a serem consideradas.
     * @return Data mais recente entre as datas de modificação dos cacheables e a data parametrizada.
     */
    public static Date lastModifiedItemDate(final Date date, final Cacheable... cacheables) {
        final Cacheable fieldsLastModified = lastModifiedItem(cacheables);
        return lastModifiedDate(date, fieldsLastModified);
    }

    /**
     * Forma null-safe para obter a data de objectTag.
     */
    public static String objectTag(final Cacheable cacheable) {
        return cacheable == null ? null : cacheable.objectTag();
    }
}
