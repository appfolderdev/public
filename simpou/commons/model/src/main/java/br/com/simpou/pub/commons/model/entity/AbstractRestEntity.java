package br.com.simpou.pub.commons.model.entity;

/**
 * Entidade base com suporte completo ao REST.
 *
 * @author Jonas Pereira
 * @since 2016-04-02
 */
public abstract class AbstractRestEntity extends AbstractIndexedEntity implements RestEntity {

    @Override
    public String objectTag() {
        return this.getClass().getName() + "#" + identity();
    }
}
