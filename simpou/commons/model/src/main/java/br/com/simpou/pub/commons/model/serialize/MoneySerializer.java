package br.com.simpou.pub.commons.model.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;

public class MoneySerializer extends JsonSerializer<BigDecimal> {

    @Override
    public void serialize(final BigDecimal value,
                          final JsonGenerator gen,
                          final SerializerProvider serializers) throws IOException, JsonProcessingException {
        if (value != null) {
//            gen.writeString(value.toString().replace(".", ","));
            gen.writeString(value.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
        }
    }
}
