package br.com.simpou.pub.commons.model.dto;

import java.io.Serializable;

public interface Identifiable<T extends Serializable> {

    T identity();
}
