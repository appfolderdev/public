package br.com.simpou.pub.commons.model;

import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Modelo identificável concreto.
 *
 * @author Jonas Pereira
 * @version 2013-06-30
 * @since 2013-06-30
 */
@RequiredArgsConstructor
public abstract class AbstractIdentifiableModel<T extends Serializable>
        extends AbstractModel
        implements IdentifiableModel<T> {

    @Setter
    private T id;

    private final Class<T> clasz;

    @Override
    public final boolean equals(final Object obj) {
        return IdentifiableModelHelper.equalsOf(this, obj);
    }

    @Override
    public final int hashCode() {
        return IdentifiableModelHelper.hashCodeFrom(this);
    }

    @Override
    public T identity() {
        return id;
    }

    @Override
    public void checkedId(final T cid) {
        setId(cid);
    }

    @Override
    public void uncheckedId(final Serializable uid) {
        checkedId(IdentifiableModelHelper.toCheckedId(clasz, uid));
    }

    @XmlTransient
    public T getid() {
        return id;
    }
}
