package br.com.simpou.pub.commons.model.entity.rest;

import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(AbstractRestEntity.class)
public abstract class AbstractRestEntity_ extends AbstractEntity_ {

}
