package br.com.simpou.pub.commons.model.entity.rest;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractRestEntity extends AbstractEntity<Long> {

    @Override
    public Class<Long> idType() {
        return Long.class;
    }

    @Override
    public RestId restId() {
        return new LongRestId(identity());
    }
}
