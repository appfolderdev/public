package br.com.simpou.pub.commons.model.jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import br.com.simpou.pub.commons.model.entity.RestEntity;
import br.com.simpou.pub.commons.utils.lang.Encapsulations;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by jonas on 4/2/16.
 */
@Getter
@Setter
@MappedSuperclass
public abstract class AbstractJpaRestEntity extends AbstractJpaIndexedEntity implements RestEntity {

    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    @PrePersist
    @PreUpdate
    protected void preSave() {
        updated = new Date();
    }

    @Override
    public Date lastModified() {
        return Encapsulations.getDate(updated);
    }

    @Override
    public String objectTag() {
        return this.getClass() + "#" + identity();
    }

}
