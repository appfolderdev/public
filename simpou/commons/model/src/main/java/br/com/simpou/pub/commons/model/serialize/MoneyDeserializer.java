package br.com.simpou.pub.commons.model.serialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.math.BigDecimal;

public class MoneyDeserializer extends JsonDeserializer<BigDecimal> {

    @Override
    public BigDecimal deserialize(final JsonParser p, final DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return BigDecimal.valueOf(p.getNumberValue().doubleValue());
    }
}
