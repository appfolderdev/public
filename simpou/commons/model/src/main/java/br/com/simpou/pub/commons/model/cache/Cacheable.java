package br.com.simpou.pub.commons.model.cache;

import java.io.Serializable;
import java.util.Date;

/**
 * Torna um objeto apto a ser usado em operações envolvendo caches.
 *
 * @author Jonas Pereira
 * @since 2013-02-27
 */
public interface Cacheable extends Serializable {

    /**
     * @return Data da última modificação do objeto. Se houverem outros objetos associadas a este que também
     * foram modificadas, todas suas datas de modificação serão atualizadas. Null caso entidade não tenha sido
     * modificada ainda.
     */
    Date lastModified();

    /**
     * @return Identificador único do objeto dentro de um conjunto.
     */
    String objectTag();

}
