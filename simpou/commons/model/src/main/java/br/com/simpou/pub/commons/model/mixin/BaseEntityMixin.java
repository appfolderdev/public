package br.com.simpou.pub.commons.model.mixin;

import br.com.simpou.pub.commons.model.view.GlobalView;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;

public abstract class BaseEntityMixin/*<I extends Serializable>*/ {

    public static final String TYPE_NAME = "@type";

    //    @JsonView({GlobalView.class})
    //    I id;

    //    @JsonView({GlobalView.class})
    //    String entityType;

    @JsonView(GlobalView.class)
    abstract String getLink();

    @JsonView({GlobalView.class})
    abstract Serializable getId();

    @JsonProperty(BaseEntityMixin.TYPE_NAME)
    @JacksonXmlProperty(isAttribute = true)
    @JsonView({GlobalView.class})
    abstract String typeName();

}
