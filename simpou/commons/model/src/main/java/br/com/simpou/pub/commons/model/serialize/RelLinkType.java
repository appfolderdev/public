package br.com.simpou.pub.commons.model.serialize;

/**
 * Created by jonas on 11/01/2017.
 */
public enum RelLinkType {

    //Gives alternate representations of the current document.
    ALTERNATE,

    //Gives a link to the author of the current document or article.
    AUTHOR,

    //Provides a link to context-sensitive help.
    HELP,

    //Indicates that the main content of the current document is covered by the copyright license described
    //by the referenced document.
    LICENSE,

    //Indicates that the current document is a part of a series, and that the next document in the series is
    //the referenced document.
    NEXT,

    //Indicates that the current document is a part of a series, and that the previous document in the
    //series is the referenced document.
    PREV,

    //Gives a link to a resource that can be used to search through the current document and its related
    // pages.
    SEARCH

}
