package br.com.simpou.pub.commons.model.serialize;

import br.com.simpou.pub.commons.utils.functional.Action;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Date;

/**
 * Created by jonas on 04/02/17.
 */
@RequiredArgsConstructor
public class ConditionalResponseObject implements ResponseObject {

    private final transient Date lastModified;

    private final transient String objectTag;

    @Getter
    private final transient Action<ConditionalResponseObject, ?> actionIfNotCached;

    @Override
    public Date lastModified() {
        return lastModified;
    }

    @Override
    public String objectTag() {
        return objectTag;
    }

}
