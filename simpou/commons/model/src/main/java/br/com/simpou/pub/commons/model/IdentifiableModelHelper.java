package br.com.simpou.pub.commons.model;

import br.com.simpou.pub.commons.model.exception.IllegalIdentityException;
import br.com.simpou.pub.commons.utils.behavior.Identifiable;
import br.com.simpou.pub.commons.utils.lang.Casts;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Jonas Pereira
 * @version 2015-07-14
 * @since 2013-06-30
 */
public class IdentifiableModelHelper<T extends Serializable> {

    public static <T extends Serializable> T toCheckedId(final Class<T> idClass,
                                                         final Serializable id) {
        if (id == null) {
            return null;
        } else if (idClass.isAssignableFrom(id.getClass())) {
            return Casts.objCast(idClass, id);
        } else {
            throw new IllegalIdentityException(idClass, id.getClass());
        }
    }

    public static int hashCodeFrom(final Identifiable model) {
        int hash = 7;
        hash = 79 * hash + (model.identity() != null ? model.identity().hashCode() : 0);
        return hash;
    }

    public static boolean equalsOf(final Identifiable obj1, final Object obj2) {
        if (obj2 == null) {
            return false;
        }

        if (obj1.getClass() != obj2.getClass()) {
            return false;
        }

        final Identifiable<?> identObj = (Identifiable<?>) obj2;
        final Serializable otherId = identObj.identity();
        final Serializable thisId = obj1.identity();
        if (otherId == null) {
            return thisId == null;
        }

        if (otherId.getClass() != thisId.getClass()) {
            return false;
        }

        return Objects.equals(otherId, thisId);
    }
}
