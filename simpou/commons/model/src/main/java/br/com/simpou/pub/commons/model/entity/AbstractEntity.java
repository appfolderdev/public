package br.com.simpou.pub.commons.model.entity;

import br.com.simpou.pub.commons.utils.violation.Violation;

/**
 * Entidade concreta básica.
 *
 * @author Jonas Pereira
 * @version 2013-06-30
 * @since 2012-06-11
 */
public class AbstractEntity implements BaseEntity {

    /**
     * {@inheritDoc}
     */
    @Override
    public Violation validate() {
        //validação OK
        return null;
    }

    @Override
    public BaseEntity doClone() {
        try {
            return (BaseEntity) super.clone();
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException(ex);
        }
    }
}
