package br.com.simpou.pub.commons.model.entity;

import br.com.simpou.pub.commons.model.BaseModel;
import br.com.simpou.pub.commons.utils.validation.Validatable;

/**
 * Entidade.
 *
 * @author Jonas Pereira
 * @version 2013-06-01
 * @since 2012-06-06
 */
public interface BaseEntity extends Validatable, BaseModel {

    @Override
    BaseEntity doClone();
}
