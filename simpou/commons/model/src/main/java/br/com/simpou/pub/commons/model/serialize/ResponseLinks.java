package br.com.simpou.pub.commons.model.serialize;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by jonas on 11/01/2017.
 */
@Getter
public class ResponseLinks implements Serializable {

    private final List<? extends ResponseLink> links;

    private final Metadata meta;

    public ResponseLinks(final ResponseLink... links) {
        this(Arrays.asList(links));
    }

    public ResponseLinks(final List<? extends ResponseLink> links) {
        this.links = Collections.unmodifiableList(links);
        this.meta = new Metadata(links.size());
    }

    @Getter
    @RequiredArgsConstructor
    public class Metadata {

        private final long count;
    }
}
