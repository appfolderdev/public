package br.com.simpou.pub.commons.model.jpa.entity;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(AbstractJpaIndexedEntity.class)
public abstract class AbstractJpaIdentifiableEntity_ {

    public static volatile SingularAttribute<AbstractJpaIndexedEntity, Long> id;
}
