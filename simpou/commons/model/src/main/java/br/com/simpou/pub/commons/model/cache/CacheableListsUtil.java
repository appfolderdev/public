package br.com.simpou.pub.commons.model.cache;

import br.com.simpou.pub.commons.utils.Constants;
import br.com.simpou.pub.commons.utils.lang.Checks;
import lombok.*;

import java.util.*;

/**
 * @author Jonas Pereira
 * @since 27/02/14
 */
@NoArgsConstructor
@ToString(of = {"lastModified", "objectTag"})
@EqualsAndHashCode(of = {"objectTag", "lastModified"})
public class CacheableListsUtil implements Cacheable {

    /**
     * Compara as datas de modificação.
     */
    private static final Comparator<Cacheable> MODIFICATION_DATE_COMPARATOR = new Comparator<Cacheable>() {

        @Override
        public int compare(final Cacheable o1, final Cacheable o2) {
            final int result;
            int nullsResult = testNulls(o1, o2);
            if (nullsResult < 2) {
                //algum é null
                result = nullsResult;
            } else {
                final Date lastModified1 = CacheableHelper.lastModified(o1);
                final Date lastModified2 = CacheableHelper.lastModified(o2);
                nullsResult = testNulls(lastModified1, lastModified2);
                if (nullsResult < 2) {
                    result = nullsResult;
                } else {
                    result = lastModified1.compareTo(lastModified2);
                }
            }

            return result;
        }

        /**
         * Testa os nulos.
         *
         * @param o1 Objeto.
         * @param o2 Objeto
         * @return Objeto.
         */
        private int testNulls(final Object o1, final Object o2) {
            final int result;
            if (o1 == null) {
                if (o2 == null) {
                    result = 0;
                } else {
                    result = -1;
                }
            } else if (o2 == null) {
                result = 1;
            } else {
                result = 2;
            }
            return result;
        }
    };

    /**
     * Ultima data de modificação.
     */
    @Setter(AccessLevel.PACKAGE)
    private Date lastModified;

    /**
     * Tag do Objeto.
     */
    @Setter
    private String objectTag;

    /**
     * Lista atual.
     */
    @Getter(AccessLevel.PACKAGE)
    private List<? extends Cacheable> currentList;

    /**
     * Retorna uma lista atualizada.
     *
     * @param list de Cacheable.
     */
    public CacheableListsUtil(final Iterable<? extends Cacheable> list) {
        updateListCacheInfo(list);
    }

    /**
     * @return Data de modificação do item mais recente da liste.
     */
    public static Date lastModified(final Iterable<? extends Cacheable> list) {
        final Date lastModified;
        if (Checks.isEmpty(list)) {
            lastModified = null;
        } else {
            Cacheable max = new BasicCacheable(Constants.MOST_OLD_DATE, null);
            for (final Cacheable item : list) {
                if (MODIFICATION_DATE_COMPARATOR.compare(item, max) > 0) {
                    max = item;
                }
            }
            lastModified = max.lastModified();
        }
        return lastModified;
    }

    /**
     * @return Tipo dos elementos presentes na lista. Se lista vazia ou nula, retorna tipo Class.
     */
    public Class<?> itemType() {
        return size() > 0 ? this.currentList.get(0).getClass() : Class.class;
    }

    @Override
    public Date lastModified() {
        return this.lastModified == null ? null : (Date) this.lastModified.clone();
    }

    @Override
    public String objectTag() {
        return this.objectTag;
    }

    /**
     * @return Número de elementos armazenados.
     */
    public int size() {
        return this.currentList == null ? 0 : this.currentList.size();
    }

    /**
     * Atualiza a lista.
     *
     * @param list list de Cacheable.
     */
    public final void updateListCacheInfo(final Iterable<? extends Cacheable> list) {
        final List<Cacheable> currentListAux;
        if (Checks.isEmpty(list)) {
            this.lastModified = null;
            this.objectTag = null;
            currentListAux = new ArrayList<>(0);
        } else {
            Cacheable max = new BasicCacheable(Constants.MOST_OLD_DATE, null);
            currentListAux = new ArrayList<>();
            for (final Cacheable item : list) {
                if (item == null) {
                    continue;
                }
                currentListAux.add(item);
                if (MODIFICATION_DATE_COMPARATOR.compare(item, max) > 0) {
                    max = item;
                }
            }
            if (max.objectTag() == null) {
                this.lastModified = null;
                this.objectTag = null;
            } else {
                this.lastModified = max.lastModified();
                this.objectTag = "list" + currentListAux.size() + max.objectTag();
            }
        }
        this.currentList = Collections.unmodifiableList(currentListAux);
    }

}
