package br.com.simpou.pub.commons.model.serialize;

import br.com.simpou.pub.commons.model.cache.Cacheable;
import br.com.simpou.pub.commons.model.entity.EntityConstants;

import java.io.Serializable;

/**
 * Objeto a ser contido no corpo de uma resposta REST.
 *
 * @author Jonas Pereira
 * @since 2013-07-29
 */
public interface ResponseObject extends Cacheable, Serializable {

    String ROOT_NAME = "response";

    String JSON_ITEM_TYPE_NAME = "@type";
    String XML_ITEM_TYPE_NAME = "_type";
    String XML_LIST_NAME = "data";
    String XML_ITEM_NAME = "item";
    String LINK_NAME_JSON = "href";
    String LINKS_NAME_JSON = LINK_NAME_JSON + EntityConstants.LIST_SUFFIX;
    String LINK_NAME_XML = "_href";
    String LINKS_NAME_XML = LINK_NAME_XML + EntityConstants.LIST_SUFFIX;
}
