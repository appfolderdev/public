package br.com.simpou.pub.commons.model.entity;

import br.com.simpou.pub.commons.model.serialize.ResponseObject;

/**
 * Created by jonas on 4/2/16.
 */
public interface RestEntity extends IndexedEntity, ResponseObject {

}
