package br.com.simpou.pub.commons.model.validation;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import lombok.Data;
import org.junit.Ignore;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author jonas.pereira
 * @since 17/01/13
 */
@ClassTest(ValidatorHelper.class)
@Ignore
public class ValidatorHelperTest {

    @Test
    public void testInvokeBeansValidation() throws Exception {
        final Bean bean = new Bean();
        List<ConstraintViolation<Bean>> constraintViolations;

        constraintViolations = new ArrayList<>(ValidatorHelper.invokeBeansValidation(
                bean));
        assertEquals(1, constraintViolations.size());
        assertEquals("{javax.validation.constraints.NotNull.message}",
                constraintViolations.get(0).getMessageTemplate());

        bean.setString("aa");
        constraintViolations = new ArrayList<>(new ValidatorHelper().invokeBeansValidation(
                bean));
        assertEquals(1, constraintViolations.size());
        assertEquals("{javax.validation.constraints.Size.message}",
                constraintViolations.get(0).getMessageTemplate());

        bean.setString("aaaa");
        constraintViolations = new ArrayList<>(ValidatorHelper.invokeBeansValidation(
                bean));
        assertEquals(0, constraintViolations.size());
    }

    @Test
    public void testToViolation() {
        //TODO criar teste
    }

    @Test
    public void testValidateValue() {
        //TODO criar teste
    }

    @Data
    private class Bean {

        @NotNull
        @Size(min = 3, max = 10)
        private String string;
    }
}
