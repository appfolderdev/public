package br.com.simpou.pub.commons.model.cache;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

class BasicCacheableTest {

    @Test
    void newInstance_fixedDate() {
        final Date lastModified = new Date(1586101477212L);
        final String tag = "tag";
        final BasicCacheable cacheable = new BasicCacheable(lastModified, tag);
        assertNotSame(lastModified, cacheable.lastModified()); //clone
        assertEquals(lastModified, cacheable.lastModified());
        assertEquals(tag, cacheable.objectTag());
    }

    @Test
    void newInstance_nulls() {
        final BasicCacheable cacheable = new BasicCacheable(null, null);
        assertNull(cacheable.lastModified());
        assertNull(cacheable.objectTag());
    }

    @Test
    void newInstance_today() {
        final String tag = "tag2";
        final BasicCacheable cacheable = new BasicCacheable(tag);
        assertNotNull(cacheable.lastModified());
        assertEquals(tag, cacheable.objectTag());
    }
}
