package br.com.simpou.pub.commons.model.serialize;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by jonas on 11/01/2017.
 */
public class ResponseLinksTest {

    @Test
    public void testSerialize() throws Exception {
        final ResponseLinks obj = new ResponseLinks(
                new ResponseLink(RelLinkType.NEXT, "/1"),
                new ResponseLink(RelLinkType.PREV, "/2")
        );
        final String json = new String(Serializations.objToJsonJackson(obj));
        final String exp = "{\"links\":[{\"rel\":\"NEXT\",\"href\":\"/1\"},{\"rel\":\"PREV\"," +
                "\"href\":\"/2\"}],\"meta\":{\"count\":2}}";
        Assert.assertEquals(json, exp);
    }
}
