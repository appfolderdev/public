package br.com.simpou.pub.commons.model.cache;

import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

class DummyCacheableTest {

    @Test
    void doTests() {
        final Cacheable cacheable = new DummyCacheable();
        assertNull(cacheable.lastModified());
        assertNull(cacheable.objectTag());
        assertTrue(CacheableHelper.isCacheDisabled(cacheable));
    }

}
