package br.com.simpou.pub.commons.model.validation;

import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import lombok.Data;
import org.junit.Ignore;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static br.com.simpou.pub.commons.utils.tests.AssertExtension.assertEmpty;
import static org.junit.Assert.assertEquals;

@ClassTest(BeansValidator.class)
@Ignore
public class BeansValidatorTest {

    @Test
    public void testInvoke_email() throws Exception {
        Set<ConstraintViolation<Bean>> violations;
        final Bean bean = new Bean();
        bean.setEmail("a@a.com");
        violations = BeansValidator.invoke(bean);
        assertEmpty(violations);

        bean.setEmail("a.com");
        violations = BeansValidator.invoke(bean);

        final ConstraintViolation<Bean> violation = new ArrayList<>(violations).get(0);
        assertEquals(Email.MESSAGE, violation.getMessage());
    }

    @Test
    public void testInvoke_ip() throws Exception {
        Set<ConstraintViolation<Bean>> violations;
        final Bean bean = new Bean();
        bean.setIp("10.0.0.1");
        violations = BeansValidator.invoke(bean);
        assertEmpty(violations);

        bean.setIp("192.168.256.0");
        violations = BeansValidator.invoke(bean);

        final ConstraintViolation<Bean> violation = new ArrayList<>(violations).get(0);
        assertEquals(IP.MESSAGE, violation.getMessage());
    }

    @Test
    public void testInvoke_notNull() throws Exception {
        final Bean bean = new Bean();
        List<ConstraintViolation<Bean>> constraintViolations;
        bean.setString(null);
        constraintViolations = new ArrayList<>(BeansValidator.invoke(
                bean));
        assertEquals(1, constraintViolations.size());
        assertEquals("{javax.validation.constraints.NotNull.message}",
                constraintViolations.get(0).getMessageTemplate());

        bean.setString("aa");
        constraintViolations = new ArrayList<>(new BeansValidator().invoke(
                bean));
        assertEquals(1, constraintViolations.size());
        assertEquals("{javax.validation.constraints.Size.message}",
                constraintViolations.get(0).getMessageTemplate());

        bean.setString("aaaa");
        constraintViolations = new ArrayList<>(BeansValidator.invoke(
                bean));
        assertEquals(0, constraintViolations.size());
    }

    @Data
    private class Bean {

        @NotNull
        @Size(min = 3, max = 10)
        private String string = "abc";

        @Email
        private String email;

        @IP
        private String ip;
    }
}
