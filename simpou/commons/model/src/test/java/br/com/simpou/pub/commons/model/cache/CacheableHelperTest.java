package br.com.simpou.pub.commons.model.cache;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

class CacheableHelperTest {

    /**
     * @see CacheableHelper#isCacheDisabled(Cacheable)
     */
    @Test
    void isCacheDisabled() {
        assertTrue(CacheableHelper.isCacheDisabled(null));
        assertTrue(CacheableHelper.isCacheDisabled(new DummyCacheable()));
        assertTrue(CacheableHelper.isCacheDisabled(new BasicCacheable(null, "tag")));
        assertTrue(CacheableHelper.isCacheDisabled(new BasicCacheable((String) null)));
        assertFalse(CacheableHelper.isCacheDisabled(new BasicCacheable("tag")));
    }

    /**
     * @see CacheableHelper#lastModified(Cacheable)
     */
    @Test
    void lastModified() {
        assertNull(CacheableHelper.lastModified(null));
        assertNull(CacheableHelper.lastModified(new DummyCacheable()));
        assertNotNull(CacheableHelper.lastModified(new BasicCacheable("tag")));
    }

    /**
     * @see CacheableHelper#lastModifiedDate(Date, Cacheable)
     */
    @Test
    void lastModifiedDate() {
        assertNull(CacheableHelper.lastModifiedDate(null, null));

        final Date before = new Date(1586101455555L);
        final Date after = new Date(1586101455556L);
        assertEquals(before, CacheableHelper.lastModifiedDate(before, null));
        assertEquals(before, CacheableHelper.lastModifiedDate(before, new DummyCacheable()));
        assertEquals(after, CacheableHelper.lastModifiedDate(before, new BasicCacheable(after, "tag")));
        assertEquals(after, CacheableHelper.lastModifiedDate(after, new BasicCacheable(before, "tag")));
    }

    /**
     * @see CacheableHelper#lastModifiedItem(Cacheable...)
     */
    @Test
    void lastModifiedItem() {
        assertNull(CacheableHelper.lastModifiedItem().lastModified());
        final String expected = "t1";
        final Cacheable given = CacheableHelper.lastModifiedItem(
                new BasicCacheable(new Date(1586101455555L), "t2"),
                null,
                new DummyCacheable(),
                new BasicCacheable(new Date(1586101477212L), expected),
                new BasicCacheable(new Date(1586101499999L), null) // é o mais recente mas cache desabilitada pois sem tag
        );
        assertEquals(expected, given.objectTag());
    }

    /**
     * @see CacheableHelper#lastModifiedItemDate(Date, Cacheable...)
     */
    @Test
    void lastModifiedItemDate() {
        final Date lastInList = new Date(1586101455555L);
        final Date before = new Date(1586101455554L);
        final Cacheable[] cacheables = {
                new BasicCacheable(lastInList, "t1"),
                new BasicCacheable(before, "t2")
        };
        final Date after = new Date(1586101455556L);
        assertEquals(lastInList, CacheableHelper.lastModifiedItemDate(before, cacheables));
        assertEquals(after, CacheableHelper.lastModifiedItemDate(after, cacheables));
    }

    /**
     * @see CacheableHelper#objectTag(Cacheable)
     */
    @Test
    void objectTag() {
        assertNull(CacheableHelper.objectTag(null));
        assertNull(CacheableHelper.objectTag(new DummyCacheable()));
        assertEquals("tag", CacheableHelper.objectTag(new BasicCacheable(null, "tag")));
    }

}
