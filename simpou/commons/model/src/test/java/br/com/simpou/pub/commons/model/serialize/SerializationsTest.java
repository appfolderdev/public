package br.com.simpou.pub.commons.model.serialize;

import br.com.simpou.pub.commons.utils.file.PropertyHelper;
import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.TestOrder;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.Serializable;
import java.util.UUID;

/**
 * Created by jonas.pereira on 08/06/16.
 */

@ClassTest(Serializations.class)
public class SerializationsTest {

    private static final File BIN_FILE = new File(PropertyHelper.getTempDir() + File.separator +
            SerializationsTest.class.getName() + UUID.randomUUID() + ".bin");

    @Test
    public void testApplyView() throws Exception {
        final TestView testView = Serializations.applyView(new TestView("show", "hide"), TestView.View.class);
        Assert.assertEquals("show", testView.getShow());
        Assert.assertNull(testView.getHide());
    }

    @Test
    @TestOrder(2)
    public void testBinFileToObj() throws Exception {
        final TestObj testObj = Serializations.binFileToObj(TestObj.class, BIN_FILE);
        Assert.assertEquals("binTest", testObj.getName());
    }

    @Test
    public void testJsonToObjByJackson() throws Exception {
        //testObjToJsonJackson
    }

    @Test
    @TestOrder(1)
    public void testObjToBinFile() throws Exception {
        Serializations.objToBinFile(new TestObj("binTest"), BIN_FILE);
    }

    @Test
    public void testObjToJsonJackson() throws Exception {
        final String name = Randoms.getString();
        final TestObj obj = new TestObj(name);
        final byte[] bytes = Serializations.objToJsonJackson(obj);
        final String json = new String(bytes);
        Assert.assertEquals("{\"name\":\"" + name + "\"}", json);
        final TestObj givObj = Serializations.jsonToObjByJackson(TestObj.class, json);
        Assert.assertEquals(obj, givObj);
    }
}

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "name")
class TestObj implements Serializable {

    private String name;

}

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
class TestView implements Serializable {

    @JsonView(View.class)
    private String show;

    private String hide;

    static class View {

    }

}


