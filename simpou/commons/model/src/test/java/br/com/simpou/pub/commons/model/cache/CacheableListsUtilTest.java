package br.com.simpou.pub.commons.model.cache;

import br.com.simpou.pub.commons.utils.rand.Randoms;
import br.com.simpou.pub.commons.utils.tests.annot.ClassTest;
import br.com.simpou.pub.commons.utils.tests.annot.TestOrder;
import lombok.RequiredArgsConstructor;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static br.com.simpou.pub.commons.utils.tests.AssertExtension.assertEmpty;
import static br.com.simpou.pub.commons.utils.tests.AssertExtension.assertNotEquals;
import static org.junit.Assert.*;

@ClassTest(CacheableListsUtil.class)
public class CacheableListsUtilTest {

    private static CacheableListsUtil util;

    @BeforeClass
    public static void setUpClass() throws Exception {
        util = new CacheableListsUtil();
    }

    @Test
    public void testEquals() {
        //testUpdateInfo
    }

    @Test
    public void testGetCurrentList() {
        //testEquals
    }

    @Test
    @TestOrder(2)
    public void testHashCode() {
        //testUpdateInfo
    }

    @Test
    public void testItemType() {
        //testUpdateListCacheInfo
    }

    @Test
    public void testLastModified() {
        //testUpdateInfo
    }

    @Test
    public void testLastModified_static() {
        final Date futureDate = Randoms.getDate(true);
        final Date pastDate = Randoms.getDate(false);
        assertEquals(
                futureDate.getTime(),
                CacheableListsUtil.lastModified(Arrays.asList(
                        new CacheableImpl(futureDate, ""),
                        new CacheableImpl(pastDate, "")
                )).getTime()
        );
    }

    @Test
    public void testObjectTag() {
        //testUpdateInfo
    }

    @Test
    public void testSetLastModified() {
        //testUpdateInfo
    }

    //    @Test
    //    public void testCanEqual() {
    //        //testEquals
    //    }

    @Test
    public void testSetObjectTag() {
        //testUpdateInfo
    }

    @Test
    public void testSize() {
        //testUpdateListCacheInfo
    }

    @Test
    public void testToString() {
        final String toString = util.toString();
        assertFalse(toString.contains(CacheableListsUtil.class.getName()));
    }

    @Test
    @TestOrder(1)
    public void testUpdateListCacheInfo() {
        final CacheableListsUtil utilNull = new CacheableListsUtil(null);
        assertNull(utilNull.lastModified());
        assertNull(utilNull.objectTag());
        assertEmpty(utilNull.getCurrentList());

        util.setObjectTag("tag");
        final List<Cacheable> list = new ArrayList<>();
        list.add(null);
        list.add(null);
        final Date pastDate = Randoms.getDate(false);
        list.add(new CacheableImpl(pastDate, "v1"));
        list.add(null);
        list.add(new CacheableImpl(new Date(), "v2"));
        list.add(null);
        util.updateListCacheInfo(list);
        assertEquals(list.get(4).lastModified(), util.lastModified());
        assertEquals("list2v2", util.objectTag());
        list.add(null);
        final Date futureDate = Randoms.getDate(true);
        list.add(new CacheableImpl(futureDate, "v3"));
        list.add(null);
        list.add(new CacheableImpl2());
        list.add(new CacheableImpl(null, "v4"));
        util.updateListCacheInfo(list);
        assertEquals(futureDate, util.lastModified());
        assertEquals(list.get(7).lastModified(), util.lastModified());

        assertEquals("CacheableListsUtil(lastModified=" + futureDate.toString() + ", objectTag=list5v3)",
                util.toString());
        assertEquals(util, util);
        assertEquals(util.hashCode(), util.hashCode());
        assertNotEquals(util, utilNull);
        {
            utilNull.setObjectTag(util.objectTag());
            assertNotEquals(util, utilNull);
            assertNotEquals(utilNull, util);
            utilNull.setLastModified(util.lastModified());
            assertEquals(util, utilNull);
            assertEquals(utilNull, util);
            assertEquals(util.hashCode(), utilNull.hashCode());
            utilNull.setLastModified(pastDate);
            assertNotEquals(util, utilNull);
            assertNotEquals(utilNull, util);
        }
        assertFalse(util.equals(null));
        assertFalse(util.equals(this));

        assertEquals(util.size(), 5);
        assertEquals(util.itemType().getName(), CacheableImpl.class.getName());
    }

    private class CacheableImpl2 extends AbstractCacheable {

        @Override
        public String objectTag() {
            return "objectTag";
        }
    }

    @RequiredArgsConstructor
    private class CacheableImpl implements Cacheable {

        private final Date date;

        private final String name;

        @Override
        public Date lastModified() {
            return this.date;
        }

        @Override
        public String objectTag() {
            return this.name;
        }

    }
}
