# simpou-commons

## Módulos

* [simpou-commons-utils](simpou-commons-utils/README.md)
* [simpou-commons-model](simpou-commons-model/README.md)
* [simpou-commons-persistence](simpou-commons-persistence/README.md)
* [simpou-commons-web](simpou-commons-web/README.md)
