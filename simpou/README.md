# Repositório

https://appfolder.dev/dev/repo/mvn/

# Javadocs

https://appfolder.dev/dev/docs/java/simpou-public

# Uso

Configure o repositório no seu *pom.xml*:

    	<repositories>
            ...
            <repository>
                <id>simpou-repo</id>
                <url>http://dev.simpou.com.br/repo/mvn</url>
            </repository>
            ..
    	</repositories>

Importe a biblioteca desejada:

    	<dependencies>
            ...
            <dependency>
                <groupId>${simpouGroup}</groupId>
                <artifactId>${simpouArtifact}</artifactId>
                <version>0.10.5</version>
            </dependency>
            ...
        </dependencies>

Grupos e artefatos disponíveis:

    br.com.simpou.pub.commons
        public-commons-base
        public-commons-model
        public-commons-persistence
        public-commons-utils
        public-commons-web
    br.com.simpou.pub.scaffold
        public-scaffold-config
        public-scaffold-core
        public-scaffold-context
        public-scaffold-mojo
        public-scaffold-processor
    br.com.simpou.pub.spring
        public-spring-base
        public-spring-context
        public-spring-persistence

# Powered by

![jetbrains](resources/logos/jetbrains-logo.png)
![intellij](resources/logos/intellij-logo.png)
